<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserReport extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserReport_model');
		$this->load->library('upload');
		$this->load->library('image_lib');
	}

	public function index($isWinLoss='')
	{
		$getGameType = $this->Crud_model->GetData('ludo_mst_rooms','roomTitle');
		$data=array(
			'heading'=>"Manage Report",
			'bread'=>"Manage Report",
			'getGameType'=>$getGameType,
			'isWinLoss'=>$isWinLoss,
		);
		$this->load->view('userReport/list',$data);
	}

	public function ajax_manage_page($isWinLoss='')
	{
		//print_r($_POST);exit();
		$SearchData = $this->input->post('SearchData');
		$SearchData1 = $this->input->post('SearchData1');
		$SearchData2 = $this->input->post('SearchData2');
		$SearchData3 = $this->input->post('SearchData3');
		$SearchData4 = $this->input->post('SearchData4');

		if($_SESSION[SESSION_NAME]['role']=='Admin'){
			$cond = "cdh.coinsDeductHistoryId!='0' and u.id!=''";
		}else{
			$cond = "cdh.agendId='".$_SESSION[SESSION_NAME]['id']."'";
		}
		//select * from *table_name* where *datetime_column* >= '01/01/2009' and *datetime_column* <= curdate()
		if($isWinLoss!='' && empty($SearchData) && empty($SearchData1)){
			$cond .= " and  date(cdh.created) = '".date("Y-m-d")."'";
		}
		if(!empty($SearchData)){
			$cond .= " and  date(cdh.created) >= '".date("Y-m-d",strtotime($SearchData))."'";
		}
		if(!empty($SearchData1)){
			$cond .= " and  date(cdh.created) <= '".date("Y-m-d",strtotime($SearchData1))."'";
		}
		if(!empty($SearchData2)){
			$cond .= " and cdh.isWin='".$SearchData2."'";
		}
		if(!empty($SearchData3)){
			$cond .= " and cdh.gameType LIKE '%".$SearchData3."%'";
		}
		if(!empty($SearchData4)){
			$cond .= " and u.playerType='".$SearchData4."'";
		}

		$getUsers = $this->UserReport_model->get_datatables('coins_deduct_history cdh',$cond);
		//print_r($this->db->last_query());exit();
		if(empty($_POST['start']))
		{
			$no =0;   
		}else{
			 $no =$_POST['start'];
		}
		$data = array();


		foreach ($getUsers as $userData) 
		{
			if($userData->isWin=='Win'){
				$sign = '+ ';
			}else{
				$sign = '- ';
			}

			if($userData->playerType=='Real' && $userData->isWin=='Win'){
				$coins= $userData->coins + $userData->adminAmount;
			}else{
				$coins= $userData->coins;
			}
			
			//$coins= $userData->coins;
			$no++;
			$nestedData = array();
			$nestedData[] = $no;
			$nestedData[] = ucfirst($userData->user_name);
			$nestedData[] = $userData->mobile;
			$nestedData[] = $userData->playerType;
			$nestedData[] = $userData->tableId;
			$nestedData[] = ucfirst($userData->game);
			$nestedData[] = $userData->gameType;
			$nestedData[] = $userData->betValue;
			$nestedData[] = $userData->isWin;
			$nestedData[] = $sign."".$coins;
			$nestedData[] = $userData->mainWallet;
			$nestedData[] = $userData->winWallet;
			$nestedData[] = $userData->adminCommition.'%';
			$nestedData[] = $userData->adminAmount;
			$nestedData[] = date('d-m-Y H:i:s',strtotime($userData->created));
			
			$data[] = $nestedData;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->UserReport_model->count_all('coins_deduct_history cdh',$cond),
					"recordsFiltered" => $this->UserReport_model->count_filtered('coins_deduct_history cdh',$cond),
					"data" => $data,
					"csrfHash" => $this->security->get_csrf_hash(),
					"csrfName" => $this->security->get_csrf_token_name(),
				);
		echo json_encode($output);
	}

	public function exportAction(){
		$cond = "cdh.coinsDeductHistoryId!='0' and u.id!=''";
		$getUserData = $this->UserReport_model->getReportData('coins_deduct_history cdh',$cond);

		if(!empty($getUserData)) {
			$filename = "Reports".date('d-m-Y H:i').".csv";
			$fp = fopen('php://output', 'w');
			$header = array('User Name','User Mobile','User Type','Table Id','Game','Game Typ','Bet Value','Is Wi','Win/Loss Coin','Admin Commissio','Admin Amoun','Date');
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename='.$filename);
			fputcsv($fp, $header);
			$sr=1;
			foreach ($getUserData as $report) {
				if(!empty($report->user_name)){ $user_name = $report->user_name; }else{ $user_name = 'NA'; }

				if(!empty($report->mobile)){ $mobile = $report->mobile; }else{ $mobile = 'NA'; }

				if(!empty($report->created)){ $created = date("d/m/Y",strtotime($report->created)); }else{ $created = 'NA'; }

				if($report->isWin=='Win'){
					$sign = '+ ';
				}else{
					$sign = '- ';
				}

				if($report->playerType=='Real' && $report->isWin=='Win'){
					$coins= $report->coins + $report->adminAmount;
				}else{
					$coins= $report->coins;
				}

				fputcsv($fp, array($user_name,$mobile,$report->playerType,$report->tableId,$report->game,$report->gameType,$report->betValue,$report->isWin,$sign."".$coins,$report->adminCommition.'%',$report->adminAmount,$created));
				$sr++;

			}
		} else {
			$this->session->set_flashdata('message', 'Record not avaliable.');
			redirect(USERREPORT);
		}
	}

}