<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RoleAccess extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	$this->load->model('RoleAccess_model');
	} 

	public function index()
	{
		$data=array(
			'heading'=>"Manage Agent",
			'bread'=>"Manage Agent",
			);
		$this->load->view('roleAccess/list',$data);
	}

	public function ajax_manage_page()
	{	
		$cond = "al.role!='Admin'";
		$getUsers = $this->RoleAccess_model->get_datatables('admin_login al',$cond);

		if(empty($_POST['start']))
		{
			$no =0;   
		}else{
			 $no =$_POST['start'];
		}
		$data = array();
				  
		foreach ($getUsers as $getUserData) 
		{
			$btn = '';

			$btn .= anchor(site_url(ROLEACCESSUPDATE.'/'.base64_encode($getUserData->id)),'<span class="btn btn-info btn-circle btn-xs"  data-placement="right" title="Update"><i class="fa fa-edit"></i></span>');

         	$btn .= '&nbsp;'.anchor(site_url(ROLEACCESSVIEW.'/'.base64_encode($getUserData->id)),'<span class="btn btn-warning btn-circle btn-xs"  data-placement="right" title="Role Access"><i class="fa fa-universal-access"></i> Role Access</span>');


			if($getUserData->status=="Active"){
				$status = '<label class="btn btn-success btn-xs">'.$getUserData->status.'</label>';
			}else{
				$status = '<label class="btn btn-danger btn-xs">'.$getUserData->status.'</label>';
			}
			$no++;
			$nestedData = array();
			$nestedData[] = $no;
			$nestedData[] = ucfirst($getUserData->name);
			$nestedData[] = $getUserData->email;
			$nestedData[] = $getUserData->commission;
			$nestedData[] = $status;
			$nestedData[] = $btn;
			
			$data[] = $nestedData;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->RoleAccess_model->count_all('admin_login al',$cond),
					"recordsFiltered" => $this->RoleAccess_model->count_filtered('admin_login al',$cond),
					"data" => $data,
					"csrfHash" => $this->security->get_csrf_hash(),
					"csrfName" => $this->security->get_csrf_token_name(),
				);
		echo json_encode($output);
	}

	public function create()
	{
		$data = array(
//			'heading' => 'Create Agent',
//			'breadhead' => 'Manage Agent',
//			'bread' => 'Create Agent',
			'button' => 'Create',
			'action' => site_url(ROLEACCESSACTION),
			'name' => set_value('name'),
//			'email' => set_value('email'),
//			'commission' => set_value('commission'),
			'password' => set_value('password'),
			'id' => '0',
//                    'user_id'=>'0',
                    
                    //new data added for user_details table 6/14/2021
            'user_id' => set_value('user_id'),
            'playerId' => set_value('playerId'),
            'playerType' => set_value('playerType'),
            'registrationType' => set_value('registrationType'),
            'socialId' => set_value('socialId'),
            'user_name' => set_value('user_name'),
            'email_id' => set_value('email_id'),
            'country_name' => set_value('country_name'),
            'mobile' => set_value('mobile'),
            'profile_img' => set_value('profile_img'),
            'adharUserName' => set_value('adharUserName'),
            'adharFron_img' => set_value('adharFron_img'),
            'adharBack_img' => set_value('adharBack_img'),
            'panUserName' => set_value('panUserName'),
            'pan_img' => set_value('pan_img'),
            'adharCard_no' => set_value('adharCard_no'),
            'panCard_no' => set_value('panCard_no'),
            'kyc_status' => set_value('kyc_status'),
            'kycDate' => set_value('kycDate'),
            'status' => set_value('status'),
            'otp' => set_value('otp'),
            'otp_verify' => set_value('otp_verify'),
            'blockuser' => set_value('blockuser'),
            'signup_date' => set_value('signup_date'),
             'last_login' => set_value('last_login'),


        
        

		);

		$this->load->view('roleAccess/form',$data);
	}

	public function update($id)
	{
		$cond = "id='".base64_decode($id)."' ";
        $row = $this->RoleAccess_model->GetData("user_details",'',$cond,'','','','1');
		$data = array(
			'heading' => 'Update Agent',
			'breadhead' => 'Manage Agent',
			'bread' => 'Update Agent',
			'button' => 'Update',
			'action' => site_url(ROLEACCESSACTION),
			'id' => set_value('id',$row->id),
			'name' => set_value('name',$row->name),
			'email' => set_value('email',$row->email),
			'commission' => set_value('commission',$row->commission),
			'password' => set_value('password',$row->password),
		);
		$this->load->view('roleAccess/form',$data);
	}
	public function action()
	{

		$cond = "id='".$_POST['id']."' ";
//                $uid="user_id".$_POST['user_id']."' ";
        $row = $this->RoleAccess_model->GetData("user_details",'',$cond,'','','','1');
        //below code commented on 6/14/2021
//		$data = array(
//			'name' => $this->input->post('name',TRUE),
//			'email' => $this->input->post('email',TRUE),
//			'commission' => $this->input->post('commission',TRUE),
//			'role' => "User",
//		);
                //end commented on 6/14/2021

        $data = array( //code added 6/14/2021
			'user_id' => $this->input->post('user_id',TRUE),
			'playerId' => $this->input->post('playerId',TRUE),
			'playerType' => $this->input->post('playerType'),
            	        'registrationType' => $this->input->post('registrationType'),
			'name' => $this->input->post('name'),
			'socialId' => $this->input->post('socialId'),
			'user_name' => $this->input->post('user_name'),
			'email_id' => $this->input->post('email_id'),
			'country_name' => $this->input->post('country_name'),
			'mobile' => $this->input->post('mobile'),
			'profile_img' => $this->input->post('profile_img'),
			'adharUserName' => $this->input->post('adharUserName'),
			'adharFron_img' => $this->input->post('adharFron_img'),
			'adharBack_img' => $this->input->post('adharBack_img'),
            	        'panUserName' => $this->input->post('panUserName'),
			'panCard_no' => $this->input->post('panCard_no'),
			'kyc_status' => $this->input->post('kyc_status'),
			'kycDate' => $this->input->post('kycDate'),
			'status' => $this->input->post('status'),
			'otp' => $this->input->post('otp'),
			'otp_verify' => $this->input->post('otp_verify'),
			'blockuser' => $this->input->post('blockuser'),
			'signup_date' => $this->input->post('signup_date'),
                        'last_login' => $this->input->post('last_login'),

		);

    	if($_POST['button'] == 'Create')
		{ 
			$data['created'] = date("Y-m-d H:i:s");
			$data['password'] = md5($this->input->post('password',TRUE));
			// $data['showPassword'] = $this->input->post('password',TRUE);
			$this->RoleAccess_model->SaveData("user_details",$data);

			$this->session->set_flashdata('message', 'Record has been created successfully');
		}
		else
		{
			$data['modified'] = date("Y-m-d H:i:s");
			$this->RoleAccess_model->SaveData("user_details",$data,$cond);
			
			$this->session->set_flashdata('message', 'Record has been updated successfully');
		}
		redirect("users");
	}

	public function delete()
	{
		$cond = "id = '".$_POST['id']."'";
		$getData = $this->RoleAccess_model->GetData("admin_login",'',$cond,'','','','1');
		if(!empty($getData))
		{
			$this->RoleAccess_model->DeleteData("admin_login",$cond,'1');
			$this->session->set_flashdata('message', 'Record has been deleted successfully');
		}
		else
		{
			$this->session->set_flashdata('message', 'No Record found');
		}
		redirect(ROLEACCESS);
	}

	public function roleAccess($id){
		$getmenus = $this->RoleAccess_model->GetData('admin_menus','',"parentId='0' and type='MENU'");

		$getmenus_ids = $this->RoleAccess_model->GetData('admin_menu_mapping','menuId','adminId="'.base64_decode($id).'"');
	
		$getsubmenus_ids = $this->RoleAccess_model->GetData('admin_menu_mapping','subMenuId','adminId="'.base64_decode($id).'"');

		$selected_ids=[];
		foreach ($getmenus_ids as $key => $value) {
			 $selected_ids[]=$value->menuId; 
		}
		
		$selected_submenu_ids=[];
		foreach ($getsubmenus_ids as $key => $value) {
			 $selected_submenu_ids[]=$value->subMenuId; 
		}

		$data = array(
			'action'=>site_url(ROLEACCESSMENUACTION),
			'getmenus'=>$getmenus,
			'adminId'=>$id,
			'selected_menu_id'=>$selected_ids,
			'selected_submenu_ids'=>$selected_submenu_ids,
		);
		$this->load->view('roleAccess/roleAccess',$data);
	}

	public function roleAccessAction(){
		$getmenus_data = $this->RoleAccess_model->GetData('admin_menu_mapping','menuId','adminId="'.base64_decode($_POST['adminId']).'"');

    	$getsubmenus_ids = $this->RoleAccess_model->GetData('admin_menu_mapping','menuId,subMenuId','adminId="'.base64_decode($_POST['adminId']).'"');

    	foreach ($getmenus_data as $key => $value) {
    		$menus[] = $value->menuId;  
    	}

    	foreach ($getsubmenus_ids as $key => $value) {
    		$submenu[] = $value->subMenuId;  
    	}

    	if(empty($menus)) $menus=[0];
			$array_diff = array_diff($_POST['menu'],$menus);
			$array_diff2 = array_diff($menus,$_POST['menu']);


		if(empty($submenu)) $submenu=[0];		
			$array_diff3 = array_diff($_POST['submenu'],$submenu);

			$array_diff4 = array_diff($submenu,$_POST['submenu']);
 
	 
		// to UNCHECK DELETE THE MENU CHECKBOX -----
		foreach ($array_diff2 as $key1 => $value1) {
			$this->RoleAccess_model->DeleteData('admin_menu_mapping',"adminId='".base64_decode($_POST['adminId'])."' and menuId='".$value1."'");
		}
		// to UNCHECK DELETE THE SUBMENU CHECKBOX -----
		foreach ($array_diff4 as $key2 => $value2) {


			$this->RoleAccess_model->DeleteData('admin_menu_mapping',"adminId='".base64_decode($_POST['adminId'])."' and subMenuId='".$value2."'  and subMenuId!='0'");
		}
 
		// to UNCHECK INSERT THE MENU CHECKBOX -----
		foreach ($array_diff as $key1 => $value1) {

			$data = array(
				"adminId"=> base64_decode($_POST['adminId']),
				"menuId"	  => $value1,
				"subMenuId"  => 0,
		    	);
			$this->RoleAccess_model->SaveData("admin_menu_mapping",$data);
		}

		// to UNCHECK INSERT THE SUBMENU CHECKBOX -----
		foreach ($array_diff3 as $key2 => $value2) {

			$submenu_parent = $this->RoleAccess_model->get_submenu_data($value2);

			$data = array(
				"adminId"=> base64_decode($_POST['adminId']),
				"menuId"	  => $submenu_parent->parentId,
				"subMenuId"  => $value2,
		    	);

			$this->RoleAccess_model->SaveData("admin_menu_mapping",$data);
		}
		$this->session->set_flashdata('message', 'Role Access has been successfully.'); 
		redirect(ROLEACCESS);
	}
}
