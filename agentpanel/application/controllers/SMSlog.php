<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SMSlog extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('SMSlog_model');
	} 

	function index() {
		$data=array(
				'heading'    =>"Manage SMS Log",
				'bread'      =>"Manage SMS Log",
				'create_btn' => "Create",
				);
		$this->load->view('smslog/list',$data);
	}

	function ajax_manage_page() {
		$no = 0;
		if($_POST['start']) {
			$no = $_POST['start'];
		}

		$getBonusData = $this->SMSlog_model->get_datatables('mst_sms_body bs');
		$data = array();

		foreach ($getBonusData as $listData) {
			$btn = '';
			
			$no++;
			$nestedData   = array();
			$nestedData[] = $no;
			$nestedData[] = $listData->smsType;
			$nestedData[] = $listData->smsBody;

			$data[]       = $nestedData;
		}

		$output = array(
			'draw'            => $_POST['draw'],
			'recordsTotal'    => $this->SMSlog_model->count_all('mst_sms_body bs'),
			'recordsFiltered' => $this->SMSlog_model->count_filtered('mst_sms_body bs'),
			'data'            => $data,
			"csrfHash"        => $this->security->get_csrf_hash(),
			"csrfName"        => $this->security->get_csrf_token_name(),
		);

		echo json_encode($output);
	}

}
?>