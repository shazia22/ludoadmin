<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MatchHistory extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('MatchHistory_model');
		
	} 

	public function index()
	{
		$data=array(
				'heading'=>"Manage Game Record",
				'bread'=>"Manage Game Record"
			);
		$this->load->view('matchHistory/list',$data);
	}

	public function ajax_manage_page()
	{
		$condition = "cdh.coinsDeductHistoryId!='0' and u.id!=''";
		if(!empty($this->input->post('SearchData8')) && !empty($this->input->post('SearchData9'))) {
			$condition .= " and date(cdh.created) between '".date("Y-m-d",strtotime($this->input->post('SearchData8')))."' and '".date("Y-m-d",strtotime($this->input->post('SearchData9')))."' ";
		}else if(!empty($this->input->post('SearchData8'))) {
			$condition .= " and date(cdh.created) = '".date("Y-m-d",strtotime($this->input->post('SearchData8')))."'";
		}else if(!empty($this->input->post('SearchData9'))) {
			$condition .= " and date(cdh.created) = '".date("Y-m-d",strtotime($this->input->post('SearchData9')))."'";
		}
		
		$getUsers = $this->MatchHistory_model->get_datatables('coins_deduct_history cdh',$condition);
		// print_r($this->db->last_query());exit;

		if(empty($_POST['start']))
		{
			$no =0;   
		}else{
			 $no =$_POST['start'];
		}
		$data = array();

		
		foreach ($getUsers as $userData) 
		{
			$table='';
			$cond= "tableId='".$userData->tableId."'";
			$getData = $this->MatchHistory_model->matchesData($cond);
			$srNo=1;
			$table .= '<table class="table table-striped table-bordered" width="100%"><thead>
				  <th>No.</th>
				  <th>User Name</th>
				  <th>Win-Loss-Type</th>
				  <th>Is Win</th>
				  <th>Win/Loss Coins</th>
				</thead><tbody>';
					foreach ($getData as $record) {
						if(!empty($record->user_name)){
							$userName = $record->user_name;
						}else{
							$userName = "NA";
						}
						$table.='<tr>
						<td>'.$srNo++.'</td>
						<td>'.$userName.'</td>
						<td>'.$record->winLossType.'</td>
						<td>'.$record->isWin.'</td>
						<td>'.abs($record->coins).'</td>
						</tr>';
					}

			$table .='</tbody></table>';

			$no++;
			$nestedData = array();
			$nestedData[] = $no;
			$nestedData[] = $userData->tableId;
			$nestedData[] = $userData->adminCommition;
			$nestedData[] = $userData->gameType;
			$nestedData[] = $userData->entryFee;
			$nestedData[] = date('d  M Y h:i A',strtotime($userData->created));
			$nestedData[] = $table;
			//$nestedData[] =$btn;
			
			$data[] = $nestedData;
		}
		
		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->MatchHistory_model->count_all('coins_deduct_history cdh',$condition),
					"recordsFiltered" => $this->MatchHistory_model->count_filtered('coins_deduct_history cdh',$condition),
					"data" => $data,
					"csrfHash" => $this->security->get_csrf_hash(),
					"csrfName" => $this->security->get_csrf_token_name(),
				);
		echo json_encode($output);
	}

	public function view($id){
		$cond= "tableId='".base64_decode($id)."'";
		$getData = $this->MatchHistory_model->matchesData($cond);
		$data= array(
			'heading'=>'View Game Record',
			'breadhead'=>'View Game Record',
			'bread'=>'View Game Record',
			'matchesHistory'=>$getData,
			);
		$this->load->view('matchHistory/view',$data);
	}

	public function exportAction(){
		$condition = "cdh.coinsDeductHistoryId!='0' and u.id!=''";
		$getUserData = $this->MatchHistory_model->getExportData('coins_deduct_history cdh',$condition);

		if(!empty($getUserData)) {
			$filename = "GameRecord".date('d-m-Y H:i').".csv";
			$fp = fopen('php://output', 'w');
			$header = array('Table Id','Game Type','Bet Value','Date');
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename='.$filename);
			fputcsv($fp, $header);
			$sr=1;
			foreach ($getUserData as $report) {
				if(!empty($report->created)){ $created = date('d  M Y h:i A',strtotime($report->created)); }else{ $created = 'NA'; }

				fputcsv($fp, array($report->tableId,$report->gameType,ucfirst($report->betValue),$created));
				$sr++;

			}
		} else {
			$this->session->set_flashdata('message', 'Record not avaliable.');
			redirect(MATCHHISTORY);
		}
	}

}
?>