<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentTransaction extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	    $this->load->model('PaymentTransaction_model');
	} 

	public function index()
	{
		$data=array(
			'heading'=>"Transaction Details",
			'bread'=>"Transaction Details",
			);
		$this->load->view('payTransaction/list',$data);
	}

	public function ajaxList()
	{
		$SearchData2 = $this->input->post('SearchData2');
		$SearchData3 = $this->input->post('SearchData3');
		$condition='ual.id!=""';
		if(!empty($SearchData2)){
			$condition .= " and ual.type='".$SearchData2."'";
		}
		if(!empty($SearchData3)){
			$condition .= " and ual.status='".$SearchData3."'";
		}
		if(!empty($this->input->post('SearchData')) && !empty($this->input->post('SearchData1'))) {
            $condition .= " and date(ual.created) between '".date("Y-m-d",strtotime($this->input->post('SearchData')))."' and '".date("Y-m-d",strtotime($this->input->post('SearchData1')))."' ";
        }else if(!empty($this->input->post('SearchData'))) {
            $condition .= " and date(ual.created) = '".date("Y-m-d",strtotime($this->input->post('SearchData')))."'";
        }else if(!empty($this->input->post('SearchData1'))) {
            $condition .= " and date(ual.created) = '".date("Y-m-d",strtotime($this->input->post('SearchData1')))."'";
        }

		$getTransaction = $this->PaymentTransaction_model->get_datatables('user_account ual',$condition);
		if(empty($_POST['start']))
        {
            $no =0;   
        }else{
             $no =$_POST['start'];
        }
		$data = array();
         		  
		foreach ($getTransaction as $transaction) 
		{
			
            if($transaction->type=='Deposit')
            {      
            	$type = '<a class="label label-info">'.$transaction->type.'</a>';
            }
            elseif($transaction->type=='Withdraw')
            {
            	$type = '<a class="label label-warning">'.$transaction->type.'</a>';
			}else{
				$type = 'NA';
			}


			if($transaction->paymentType=='mainWallet' || $transaction->paymentType=='winWallet')
            {      
            	$paymentType = '<a class="label label-danger">'.ucfirst($transaction->paymentType).'</a>';
            }elseif($transaction->paymentType =='Manually'){
            	$paymentType = '<a class="label label-success">'.ucfirst($transaction->paymentType).'</a>';
			}elseif($transaction->paymentType=='bank'){
            	$paymentType = '<a class="label label-info">'.ucfirst($transaction->paymentType).'</a>';
			}else{
				$paymentType = 'NA';
			}

			if($transaction->status=="Approved"){
				$status = "<label class='btn btn-xs btn-info'>".$transaction->status."</label>";
			}else if($transaction->status=="Pending"){
				$status = "<label class='btn btn-xs btn-danger'>".$transaction->status."</label>";
			}else if($transaction->status=="Rejected"){
				$status = "<label class='btn btn-xs btn-warning'>".$transaction->status."</label>";
			}else if($transaction->status=="Success"){
				$status = "<label class='btn btn-xs btn-success'>".$transaction->status."</label>";
			}else if($transaction->status=="Failed"){
				$status = "<label class='btn btn-xs btn-warning'>".$transaction->status."</label>";
			}else if($transaction->status=="Process"){
				$status = "<label class='btn btn-xs btn-success'>".$transaction->status."</label>";
			}else{
				$status = "<label class='btn btn-xs btn-info'>".$transaction->status."</label>";
			}

			
			if(!empty($transaction->orderId)){ $orderId = $transaction->orderId; }else{ $orderId = 'NA'; }
			if(!empty($transaction->user_name)){ $user_name = $transaction->user_name; }else{ $user_name = 'NA'; }
			if(!empty($transaction->mobileNo)){ $mobileNo = $transaction->mobileNo; }else{ $mobileNo = 'NA'; }
			if(!empty($transaction->mobile)){ $mobile = $transaction->mobile; }else{ $mobile = 'NA'; }
			if(!empty($transaction->balance)){ $balance = $transaction->balance; }else{ $balance = '0'; }
			if(!empty($transaction->mainWallet)){ $mainWallet = $transaction->mainWallet; }else{ $mainWallet = '0'; }
			if(!empty($transaction->winWallet)){ $winWallet = $transaction->winWallet; }else{ $winWallet = '0'; }
			if(!empty($transaction->amount)){ $amount = $transaction->amount; }else{ $amount = '0'; }

			if(!empty($transaction->created) && $transaction->created !="0000-00-00 00:00:00"){ $created = date('d M Y H:i A', strtotime($transaction->created)); }else{ $created = '0000-00-00 00:00:00'; }
		 
			$no++;
			$nestedData = array();
		    $nestedData[] = $no;
		    $nestedData[] = $orderId;
		    $nestedData[] = ucfirst($user_name);
		    $nestedData[] = $mobile;
		    $nestedData[] = $mobileNo;
         	$nestedData[] = $amount;
         	$nestedData[] = $winWallet;
		    $nestedData[] =$mainWallet;
         	$nestedData[] =$created;
		    $nestedData[] = $type;
         	$nestedData[] = $paymentType;
         	$nestedData[] = $status;
		    
		    $data[] = $nestedData;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->PaymentTransaction_model->count_all('user_account ual',$condition),
					"recordsFiltered" => $this->PaymentTransaction_model->count_filtered('user_account ual',$condition),
					"data" => $data,
					"csrfHash" => $this->security->get_csrf_hash(),
					"csrfName" => $this->security->get_csrf_token_name(),
				);
		echo json_encode($output);
	}

	public function exportAction() {
		$SearchData2 = $this->input->post('SearchData2');
		$SearchData3 = $this->input->post('SearchData3');
		$condition='ual.id!=""';
		if(!empty($SearchData2)){
			$condition .= " and ual.type='".$SearchData2."'";
		}
		if(!empty($SearchData3)){
			$condition .= " and ual.status='".$SearchData3."'";
		}
		if(!empty($this->input->post('fromDate')) && !empty($this->input->post('toDate'))) {
            $condition .= " and date(ual.created) between '".date("Y-m-d",strtotime($this->input->post('fromDate')))."' and '".date("Y-m-d",strtotime($this->input->post('toDate')))."' ";
        }else if(!empty($this->input->post('fromDate'))) {
            $condition .= " and date(ual.created) = '".date("Y-m-d",strtotime($this->input->post('fromDate')))."'";
        }else if(!empty($this->input->post('toDate'))) {
            $condition .= " and date(ual.created) = '".date("Y-m-d",strtotime($this->input->post('toDate')))."'";
        }
		$getPaymentTransData = $this->PaymentTransaction_model->getPaymentTransData('user_account ual',$condition);
		// echo "<pre>"; print_r($getPaymentTransData);exit;

		if(!empty($getPaymentTransData)) {
			$filename = "PaymentTrans".date('d-m-Y H:i').".csv";
			$fp = fopen('php://output', 'w');
			$header = array('OrderId','User Name','Mobile','Tax Amount(Rs)','Win Wallet(Rs)','Main Wallet(Rs)','Date','Type','Payment Mode','Status','Reg Mobile');
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename='.$filename);
			fputcsv($fp, $header);
			$sr=1;

			foreach ($getPaymentTransData as $report) {
				if(!empty($report->orderId)){ $orderId = $report->orderId; }else{ $orderId = 'NA'; }

				if(!empty($report->user_name)){ $user_name = $report->user_name; }else{ $user_name = 'NA'; }

				if(!empty($report->mobile)){ $mobile = $report->mobile; }else{ $mobile = 'NA'; }

				if(!empty($report->mobileNo)){ $mobileNo = $report->mobileNo; }else{ $mobileNo = 'NA'; }

				if(!empty($report->amount)){ $amount = $report->amount; }else{ $amount = '0'; }

				if(!empty($report->winWallet)){ $winWallet = $report->winWallet; }else{ $winWallet = '0'; }

				if(!empty($report->mainWallet)){ $mainWallet = $report->mainWallet; }else{ $mainWallet = '0'; }

				if(!empty($report->created)){ $created = date('d/m/Y', strtotime($report->created)); }else{ $created = 'NA'; }

				if(!empty($report->type)){ $type = $report->type; }else{ $type = 'NA'; }

				if(!empty($report->paymentType)){ $paymentType = $report->paymentType; }else{ $paymentType = 'NA'; }

				if(!empty($report->status)){ $status = $report->status; }else{ $status = 'NA'; }


				fputcsv($fp, array(ucfirst($user_name),$mobileNo,$amount,$winWallet,$mainWallet,$created,$type,$paymentType,$status,$mobile));
				$sr++;

			}
			
		} else {
			$this->session->set_flashdata('message', 'Record not avaliable.');
			redirect(PAYMENTTRANSACTION);
		}
	}
}
