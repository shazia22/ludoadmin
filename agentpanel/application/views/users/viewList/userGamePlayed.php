 <div class="col-md-9">
   <div class="row">
      <div class="col-md-12" style="padding-left: 3px !important;">
         <div class="nav-tabs-custom bShow" style="border-top:2px solid #00c0ef; margin:0px 0px 15px 0; padding:0 10px 0 10px;">
            <div class="col-md-2" style="float:right;margin-top:5px;">
              <a href="<?= site_url(USERGAMEPLAYEDEXPORT.'/'.base64_encode($userId)); ?>" style="float:right;" class="btn btn-success">Export</a>&nbsp;
            </div>
            <div>
               <h4>Game Played</h4>
               <input type="hidden" class="filter_search_data" value="<?= $userId;?>">
               <div class="table-responsive">
                  <table class="table table-bordered table-striped GamePlayed_table" style="width: 100%;">
                   <thead>
                   <tr>
                      <th>#</th>
                        <th>Table Id</th>
                        <th>Game Type</th>
                        <th>Bet Value</th>
                        <th>User Name</th>
                        <th>Is Win</th>
                        <th>Win/Loss Coins</th>
                        <th>Admin Commission</th>
                        <th>Admin Amount</th>
                        <th>Date & Time</th>
                        <th>Players In Table</th>
                     </tr>
                   </thead>
                   <tbody>

                   </tbody>
                 </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
  var url = '<?= site_url('Users/ajaxGamePlayed'); ?>';
  var actioncolumn=10;
  var  pageLength='';
</script>

<script type="text/javascript">
    var table = $('.GamePlayed_table').DataTable({
        "oLanguage": { 
        //"sProcessing": "<img src='<?= base_url()?>assets/images/loader.gif'>" 
       },
    
         //"scrollX":false,
          "scrollX":true,
          "processing": false, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "stateSave": true,
           "order": [], //Initial no order.
           "lengthMenu" : [[10,25, 100,200,500,1000,2000], [10,25, 100,200,500,1000,2000 ]],"pageLength" : 10,
           
           "ajax": {
               "url": url,
               "type": "POST",
           "data": function(d) {
                      d.Foo = 'gmm';
                      d.SearchData = $(".filter_search_data").val();
                      d.SearchData1 = $(".filter_search_data1").val();
                      d[csrfName] = csrfHash;
                      d.FormData = $(".filter_data_form").serializeArray();
                   },
                   "error": function(){
                     console.log("hiii");
                    $.ajax({
                    url: $("#site_url").val()+"/Csrfdata",
                    type: "GET",
                    success: function(response) {
                      $("#csrf_token").val(response);
                        }
                      });
                    }
                    
           },
             "fnDrawCallback": function( ) {
                var api = this.api();
                var json = api.ajax.json();
                csrfName =json.csrfName;
                csrfHash =json.csrfHash;
              },
         
           "columnDefs": [
           { 
               "targets": [ 0,actioncolumn ], //first column / numbering column
               "orderable": false, //set not orderable
           },
           ],
      
   
       })
     $(".filter_search_data").change(function(){
       table.draw();
     });

     $(".filter_search_data1").change(function(){
       table.draw();
     });
</script>