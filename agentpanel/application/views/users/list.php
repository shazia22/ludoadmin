<!-- Load common header -->
<?php $this->load->view('common/header'); ?>
<!-- Load common left panel -->
<?php $this->load->view('common/left_panel'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
	 <h1><?= $heading; ?></h1>
</section>
<!-- Main content -->
<section class="content">
	 <div class="row">
			<div class="col-xs-12">
				<form method="post" action="<?= site_url(USEREXPORT); ?>">
					<div class="box bShow">
						 <div class="box-header">
							 <input type="hidden" class="filter_search_data" name="type">
							 &nbsp;
							 <button type="button" id="activeAll" class="btn btn-primary col-xs-1 inactiveClass" onclick="getStatus('All');">All</button>
							 &nbsp;
							 <button type="button" id="activecustom" class="btn btn-default col-xs-1 inactiveClass" onclick="getStatus('custom');">Custom</button>
							 &nbsp;
							 <button type="button" id="activefacebook" class="btn btn-default col-xs-1 inactiveClass" onclick="getStatus('facebook');">Facebook</button>
							 &nbsp;
                                                         
<!--       							 <button type="button" id="register" class="btn btn-default  inactiveClass">Register New User</button>-->
							 &nbsp;
            <a href="<?= site_url(ROLEACCESSCREATE); ?>" class="btn btn-default">Register New User</a>

                                                  
						 </div>
					</div>
					<div class="box bShow">
						 <div class="box-header col-md-12">
							<div class="col-md-1" id="msgHide"><?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?></div>
							<div class="col-md-6">
								 <div class="col-md-4 pull-right paddRight">
										<input type="text" class="form-control datepicker filter_search_data2" name="toDate" id="toDate" placeholder="Select To Date" autocomplete="off">
								 </div>
								 <div class="col-md-4 pull-right paddRight">
										<input type="text" class="form-control datepicker filter_search_data3" name="fromDate" id="fromDate" placeholder="Select From Date" autocomplete="off">
								 </div>
							</div>
							<div class="col-md-3">
								 <select class="form-control filter_search_data4" name="SearchData4">
								 	<option value="">Select Version</option>
								 	<option value="oldVersion">Old Version</option>
								 	<option value="newVersion">New Version</option>
								 </select>
							</div>
							<div class="col-md-1">
								 <button type="reset" class="btn btn-warning resetBtn" name="reset" id="reset">Reset</button>
							</div>
							<div class="col-md-1 box-title paddLeft"> 
								<button type="submit" class="btn btn-success">Export</button>
						    </div>
						 </div>
						 <!-- /.box-header -->
						 <div class="box-body">
							<input type="hidden" name="flag" id="flag" value="<?= $flag; ?>">
								<table class="table table-bordered table-striped display" id="example_datatable" style="width: 100%;">
									 <thead>
											<tr>
												 <th>#</th>
												 <th>Username</th>
												 <th>Agend Name</th>
												 <th>Mobile</th>
												 <th>Game Played</th>
												 <th>Main Wallet</th>
												 <th>Win Wallet</th>
												 <th>Referral Code</th>
												 <th>Reg Date</th>
												 <th>Last Login</th>
												 <th>Is-User-Connect</th>
												 <th>Block Users</th>
												 <th>kyc Status</th>
												 <th>Status</th>
												 <th>Action</th>
											</tr>
									 </thead>
									 <tbody>
                    
                            <?php
//                            foreach($data as $row):
                                ?>
                    <!--<tr>-->
<!--                        <td><?php echo $row->user_name;?></td>-->
<!--                    <td><?php echo $row->name; ?></td>-->
<!--                    <td><?php echo $row->mobile;?></td>-->
<!--                    <td><?php echo $row->blockuser;?></td>-->
<!--                    <td><?php echo $row->status;?></td>-->
<!--                    <td></td>-->
<!--                    </tr>-->
                                

                        <?php //  endforeach;?>
                </tbody>
								</table>
						 </div>
						 <!-- /.box-body -->
					</div>
				</form> 
                        </div></div>
			<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
	 var url = '<?= site_url("Users/ajax_manage_page/"); ?>';
	 var actioncolumn=12;
	 var pageLength='';
</script>
<!-- Load common footer -->
<?php $this->load->view('common/footer'); ?>
<script type="text/javascript">
	$(function() {
		var flag =$("#flag").val();
		if(flag != ''){
			setTimeout(function(){
				getStatus(flag);				
			},70)
		}
	});


	function getStatus(status)
	{
		$('.filter_search_data').val(status);
		$('.inactiveClass').removeClass("btn-default btn-primary");
		$('.inactiveClass').addClass("btn-default");
		$('#active'+status).removeClass("btn-default").addClass("btn-primary");
		table.draw();
	}    


	 function blockuserChange(id)
	 { 
		 $("#Statusmodal").modal('show');
		 $("#statusSuccBtn").click(function(){
		 var site_url = $("#site_url").val();
		 var url = site_url+"/Users/blockUserChange";
			 var datastring = "id="+id+"&"+csrfName+"="+csrfHash;
			 $.post(url,datastring,function(data){
				 $("#Statusmodal").modal('hide');
				 $("#Statusmodal").load(location.href+" #Statusmodal>*","");
				 var obj = JSON.parse(data);
				 csrfName = obj.csrfName;
				 csrfHash = obj.csrfHash;
				 table.draw();
				 $("#msgData").val(obj.msg);
				 $("#toast-fade").click();
			 });
		 });
	 }
	 function blockuserChangeConnect(id)
	 { 
		 $("#Statusmodal").modal('show');
		 $("#statusSuccBtn").click(function(){
		 var site_url = $("#site_url").val();
		 var url = site_url+"/Users/blockuserChangeConnect";
			 var datastring = "id="+id+"&"+csrfName+"="+csrfHash;
			 $.post(url,datastring,function(data){
				 $("#Statusmodal").modal('hide');
				 $("#Statusmodal").load(location.href+" #Statusmodal>*","");
				 var obj = JSON.parse(data);
				 csrfName = obj.csrfName;
				 csrfHash = obj.csrfHash;
				 table.draw();
				 $("#msgData").val(obj.msg);
				 $("#toast-fade").click();
			 });
		 });
	 }
</script>
<script type="text/javascript">
	 function change_status(id)
	 { 
		 $("#Statusmodal").modal('show');
		 $("#statusSuccBtn").click(function(){
		 var site_url = $("#site_url").val();
		 var url = site_url+"/Users/change_status";
			 var datastring = "id="+id+"&"+csrfName+"="+csrfHash;
			 $.post(url,datastring,function(data){
				 $("#Statusmodal").modal('hide');
				 $("#Statusmodal").load(location.href+" #Statusmodal>*","");
				 var obj = JSON.parse(data);
				 csrfName = obj.csrfName;
				 csrfHash = obj.csrfHash;
				 table.draw();
				 $("#msgData").val(obj.msg);
				 $("#toast-fade").click();
			 });
		 });
	 }
</script>
<script type="text/javascript">  
	 function User_Validation()
	 { 
		 var import_user = $("#import_user").val(); 
		 if(import_user == "")
		 {  
			 $("#errorUser").html("<span style='color:red;'>Please upload excel</span>").fadeIn();
			 setTimeout(function(){$("#errorUser").fadeOut()},3000);
			 return false; 
		 }
	 }
	 
	 
	 function deleteUser(id) {
			 $("#Deletemodal").modal('show');
			 $("#deleteSuccBtn").click(function(){
				 var site_url   = $("#site_url").val();
				 var url        =  site_url+"/<?= DELUSER; ?>";
				 var datastring =  'id='+id+"&"+csrfName+"="+csrfHash;
				 $.post(url,datastring,function(response){
					 $("#Deletemodal").modal('hide');
					 $("#Deletemodal").load(location.href+" #Deletemodal>*","");
						 var obj   = JSON.parse(response);
						 csrfName = obj.csrfName;
						 csrfHash = obj.csrfHash;
						 table.draw();
						 $("#msgData").val(obj.msg);
						 $("#toast-fade").click();
					 });
			 });
		 }
</script>