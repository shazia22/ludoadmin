<!-- Load common header -->
<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/left_panel'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Register New User
      </h1>
<!--      <ol class="breadcrumb">
        <li><a href="<?= site_url(DASHBOARD); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?= site_url(ROLEACCESS); ?>"><?= $breadhead; ?></a></li>
        <li><?= $bread; ?></li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box bShow">
            <div class="box-header">
              <div class="col-md-10 box-title">New User Details</div>
              <div class="col-md-2 text-right text-danger">* Fields are required</div>
            </div>
            <!-- /.box-header -->
            <?php 
            echo form_open($action); 
            ?>
              <div class="box-body">
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="hidden" name="id" value="<?= $id; ?>">
              <input type="hidden" name="user_id" value="<?= $user_id; ?>">

                    <label>Name<span class="text-danger"></span> <span class="text-danger">* </span><span id="errname" class="text-danger"></span></label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="<?= $name; ?>" autocomplete="off">
                  </div>
                </div>
                  
<!--                  <div class="col-md-4">
                  <div class="form-group">
                    <label>User Id<span class="text-danger"></span> <span class="text-danger">* </span><span id="erremail" class="text-danger"></span></label>
                    <input type="text" class="form-control" name="user_id" id="user_id" placeholder="Enter User Id"  value="<?= $user_id; ?>" autocomplete="off">
                  </div>
                </div>-->
                  
                  <div class="col-md-4">
                  <div class="form-group">
                    <label>Username<span class="text-danger"></span> <span class="text-danger">* </span><span id="erremail" class="text-danger"></span></label>
                    <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Enter Username"  value="<?= $user_name; ?>" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Email ID<span class="text-danger"></span> <span class="text-danger">* </span><span id="erremail" class="text-danger"></span></label>
                    <input type="text" class="form-control" name="email_id" id="email_id" placeholder="Enter Email"  value="<?= $email_id; ?>" autocomplete="off">
                  </div>
                </div>
                
                <?php
                if($button=="Create"){
                    ?>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Password<span class="text-danger"></span> <span class="text-danger">* </span><span id="errpassword" class="text-danger"></span></label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password"  value="<?= $password; ?>" autocomplete="off">
                  </div>
                </div>
               <?php }?>
                  <div class="col-md-4">
                  <div class="form-group">
                    <label>Mobile<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Enter mobile no"  value="<?= $mobile; ?>" autocomplete="off">
                  </div>
                </div>
                  
                  <div class="col-md-4">
                  <div class="form-group">
                    <label>Player Id<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
                    <input type="text" class="form-control" name="playerId" id="playerId" placeholder="Enter Player Id"  value="<?= $playerId; ?>" autocomplete="off">
                  </div>
                </div>
                  
                
                  
                   <div class="col-md-4">
                  <div class="form-group">
                    <label>Player Type<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>

<select class="form-control"  name="playerType" id="playerType">
  <option value="Real">Select Player Type</option>

  <option value="Real">Real</option>
  <option value="Bot">Bot</option>
</select>
                  </div></div>
                  
                    <div class="col-md-4">
                  <div class="form-group">
                    <label>Registration Type<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
                    <input type="text" class="form-control" name="registrationType" id="registrationType" placeholder="Enter Registration Type"  value="<?= $registrationType; ?>" autocomplete="off">
                  </div>
                </div>
                  
                  <div class="col-md-4">
                  <div class="form-group">
                    <label>Social Id<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
                    <input type="text" class="form-control" name="socialId" id="socialId" placeholder="Enter social Id"  value="<?= $socialId; ?>" autocomplete="off">
                  </div>
                </div>
                  
      <div class="col-md-4">
      <div class="form-group">
      <label>Country Name<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="text" class="form-control" name="country_name" id="country_name" placeholder="Enter County Name"  value="<?= $country_name; ?>" autocomplete="off">
      </div>
      </div>
                  
      <div class="col-md-4">
      <div class="form-group">
      <label>Profile Image<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="file" class="form-control" name="profile_img" id="profile_img" accept="image/*">
      </div>
      </div>
                  
      <div class="col-md-4">
      <div class="form-group">
      <label>Adhar Username<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="text" class="form-control" name="adharUserName" id="adharUserName" placeholder="Enter County Name"  value="<?= $adharUserName; ?>" autocomplete="off">
      </div>
      </div>
                  
                  
    <div class="col-md-4">
      <div class="form-group">
      <label>Adhar Front Image<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="file" class="form-control" name="adharFron_img" id="adharFron_img" accept="image/*">
      </div>
      </div>
                  
                  
    <div class="col-md-4">
      <div class="form-group">
      <label>Adhar Back Image<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="file" class="form-control" name="adharBack_img" id="adharBack_img" accept="image/*">
      </div>
      </div>
                  
    <div class="col-md-4">
      <div class="form-group">
      <label>Pan Card Username<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="text" class="form-control" name="panUserName" id="panUserName" placeholder="Enter Pan Card Username"  value="<?= $panUserName; ?>" autocomplete="off">
      </div>
      </div> 
    
    <div class="col-md-4">
      <div class="form-group">
      <label>Pan Card Image<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="file" class="form-control" name="pan_img" id="pan_img" accept="image/*">
      </div>
      </div>              
                  
   <div class="col-md-4">
      <div class="form-group">
      <label>Adhar Card Number<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="text" class="form-control" name="adharCard_no" id="adharCard_no" placeholder="Enter Adhar Card Number"  value="<?= $adharCard_no; ?>">
      </div>
      </div>
                  
      <div class="col-md-4">
      <div class="form-group">
      <label>Pan Card Number<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="text" class="form-control" name="panCard_no" id="panCard_no" placeholder="Enter Pan Card Number"  value="<?= $panCard_no; ?>">
      </div>
      </div>
                  
    <div class="col-md-4">
    <div class="form-group">
    <label>Kyc Status<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>

<select class="form-control"  name="kyc_status" id="playerType">
  <option value="Real">Select KYC Status</option>

  <option value="Pending">Pending</option>
  <option value="Verified">Verified</option>
    <option value="Rejected">Rejected</option>

</select></div></div>   
                  
                  <div class="col-md-4">
      <div class="form-group">
      <label>KYC Date<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="date" class="form-control" name="kycDate" id="kycDate">
      </div>
      </div>
                  
    <div class="col-md-4">
    <div class="form-group">
    <label>Status<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>

<select class="form-control"  name="status" id="status">
  <option value="Real">Select Status</option>

  <option value="Active">Active</option>
  <option value="Inactive">Inactive</option>

</select></div></div>
                  
                  
    <div class="col-md-4">
      <div class="form-group">
      <label>OTP<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="text" class="form-control" name="otp" id="otp" placeholder="Enter OTP"  value="<?= $otp; ?>">
      </div>
      </div>             
                  
    <div class="col-md-4">
    <div class="form-group">
    <label>OTP Verify<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>

<select class="form-control"  name="otp_verify" id="otp_verify">

  <option value="No">No</option>
  <option value="Yes">Yes</option>

</select></div></div>
                  
  
                  <div class="col-md-4">
    <div class="form-group">
    <label>Block User<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>

<select class="form-control"  name="blockuser" id="blockuser">

  <option value="No">No</option>
  <option value="Yes">Yes</option>

</select></div></div>
                  
                  
 <div class="col-md-4">
      <div class="form-group">
      <label>Sign Up Date<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="date" class="form-control" name="signup_date" id="signup_date">
      </div>
      </div>

      <div class="col-md-4">
      <div class="form-group">
      <label>Last Login<span class="text-danger"></span> <span class="text-danger">* </span><span id="errcommission" class="text-danger"></span></label>
      <input type="time" class="form-control" name="last_login" id="last_login" />
      </div>
      </div>                  
                  
                  
                  
                <div class="col-md-12" style="margin-top: 10px;">
                  <div class="form-group">
                    <input type="hidden" name="button" id="button" value="<?= $button; ?>">
                    <button type="submit" class="btn btn-primary"><?= $button; ?></button>&nbsp;
<!--                    <a href="<?= site_url(ROLEACCESS); ?>"><button type="button" class="btn btn-danger">Cancel</button></a>-->
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            <?php echo form_close(); ?>
            <!-- </form> -->
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Load common footer -->
<?php $this->load->view('common/footer'); ?>
<script type="text/javascript">
  function validateUser() {
    var name = $("#name").val();
    var password = $("#password").val();
    var email = $("#email").val();
    var email_filter = /^[a-z0-9._-]+@[a-z]+.[a-z]{2,5}$/i;
     
    if(name.trim() == '')
    {
          $("#errname").fadeIn().html("Please enter name.");
          setTimeout(function(){ $("#errname").removeClass('error'); $("#errmsg").html("&nbsp;");},3000)
          $("#name").focus();
          return false; 
    }
    if(email.trim() == '')
    {
          $("#erremail").fadeIn().html("Please enter email.");
          setTimeout(function(){ $("#erremail").removeClass('error'); $("#errmsg").html("&nbsp;");},3000)
          $("#email").focus();
          return false; 
    }
    else if(!email_filter.test(email))
    {
          $("#erremail").fadeIn().html("Please enter valid email.");
          setTimeout(function(){ $("#erremail").removeClass('error'); $("#errmsg").html("&nbsp;");},3000)
          $("#email").focus();
          return false; 
    }

    if(password.trim() == '')
    {
          $("#errpassword").fadeIn().html("Please enter password.");
          setTimeout(function(){ $("#errpassword").removeClass('error'); $("#errmsg").html("&nbsp;");},3000)
          $("#password").focus();
          return false; 
    }
    if(commission.trim() == '')
    {
          $("#errcommission").fadeIn().html("Please enter commission.");
          setTimeout(function(){ $("#errcommission").removeClass('error'); $("#errmsg").html("&nbsp;");},3000)
          $("#commission").focus();
          return false; 
    }
  }

  function only_number(event)
{
  var x = event.which || event.keyCode;
  console.log(x);
  if((x >= 48 ) && (x <= 57 ) || x == 8 | x == 9 || x == 13)
  {
    return;
  }else{
    event.preventDefault();
  }    
}
  
</script>
