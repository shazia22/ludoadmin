<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("Asia/Calcutta");

class Paytm extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();
        $this->load->library('Custom');
    }

    public function payout(){
    	$userData =$this->Crud_model->GetData("user_account","id,mobileNo,orderId,paymentType","id='".$_POST['id']."'","","","","1");
        $userDetails =$this->Crud_model->GetData("user_details","*,bank_account_no as accno, bank_account_holder_name as acc_holderName, bank_ifsc as ifsc","id='".$_POST['userId']."'","","","","1");
        if($userData){
    		$getSettData = $this->Crud_model->GetData("mst_settings","id,adminPercent","id='4'",'','','','1');
			$admin_rs=($_POST['withAmt']*$getSettData->adminPercent)/100;
			$getAdminData=$this->Crud_model->GetData("admin_login","id,adminBalance","id='".$_SESSION[SESSION_NAME]['id']."'",'','','','1');

			$userAmt = $_POST['withAmt'] - $admin_rs;
		   	// $userBankData =$this->Crud_model->GetData("bank_details","","user_detail_id='".$_POST['userId']."'","","","","1"); 
			$order_id =$userData->orderId;
			if(!empty($userDetails->accno)){
				$beneficiaryAccount= $userDetails->accno;
				$beneficiaryIFSC= $userDetails->ifsc;
				$beneficiaryName= $userDetails->bank_account_holder_name;
			}else{
				$beneficiaryAccount= $userDetails->accno;
				$beneficiaryIFSC= $userDetails->ifsc;
				$beneficiaryName= $userDetails->bank_account_holder_name;
			}
			
			$amount=$userAmt;
			$userAccId=$this->input->post('id');
			$userId=$this->input->post('userId');
			$withAmt=$this->input->post('withAmt');
			$this->easebuzzPay($order_id,$amount,$beneficiaryAccount,$beneficiaryIFSC,$userAccId,$userId,$withAmt,$beneficiaryName);
		}
		$this->session->set_flashdata('message', 'No racord found user'); 
		redirect(site_url(WITHDRAWALDISTRIBUTE.'/'.base64_encode($_POST['id'])));
    }

    public function easebuzzPay($order_id,$amt,$beneficiaryAccount,$beneficiaryIFSC,$userAccId,$userId,$withAmt,$beneficiaryName)
    {
    	header("Pragma: no-cache");
 		header("Cache-Control: no-cache");
  		header("Expires: 0");
   
		$date = date('Y-m-d');
		$time = date('H:i:s');

		$key = "36DB56ECEA1";
		$salt_key = "DAF7B3EBE91";
		$amount = (float)$amt;
		
		$posted = array(
		    'key'=>$key,
		    'account_number'=>$beneficiaryAccount,
		    'ifsc'=>$beneficiaryIFSC,
		    'upi_handle'=>"",
		    'unique_request_number'=>$order_id,
		    'amount'=>$amount,
		    'salt'=>$salt_key,
		);

		$posted1 = array(
		    'key'=>$key,
		    'beneficiary_type'=>"bank_account",
		    'beneficiary_name'=>$beneficiaryName,
		    'account_number'=>$beneficiaryAccount,
		    'ifsc'=>$beneficiaryIFSC,
		    'upi_handle'=>"",
		    'unique_request_number'=>$order_id,
		    'payment_mode'=>"IMPS",
		    'amount'=>$amount,
		    'narration'=>"TEST",
		);
		
		$hash_sequence = "key|account_number|ifsc|upi_handle|unique_request_number|amount|salt";

		// make an array or split into array base on pipe sign.
		$hash_sequence_array = explode('|', $hash_sequence);
		$hash = null;

		// prepare a string based on hash sequence from the $params array.
		foreach ($hash_sequence_array as $value) {
		    $hash .= isset($posted[$value]) ? $posted[$value]."|" : '';
		    // $hash .= '|';
		}
		$hash = str_replace($salt_key."|", $salt_key, $hash);

		// generate hash key using hash function(predefine) and return
		$hash_code =  strtolower(hash('sha512', $hash));
		$x_checksum = $hash_code;
		$postdata = json_encode($posted1);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://wire.easebuzz.in/api/v1/quick_transfers/initiate/',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>$postdata,
		  CURLOPT_HTTPHEADER => array(
		    'Authorization:'.$hash_code,
		    'Content-Type: application/json'
		  ),
		));

		$respo = curl_exec($curl);
		curl_close($curl);
		$response = json_decode($respo);
		
		$userBal =$this->Crud_model->GetData("user_details","","id='".$userId."'","","","","1");

		if($response->success==1)
		{
			$dataString = $response->data->transfer_request;
			$resStatus ='Approved';
			// $type ='easebuzz';
			$statusMessage="success";
			$statusCode=1;
			$saveRefundData = array(
	    		'orderId'=>$order_id,
	    		'user_detail_id'=>$userId,
	    		'paytmStatus'=>$statusCode,
	    		'statusCode'=>$statusCode,
	    		'statusMessage'=>$statusMessage,
	    		'checkSum'=>$x_checksum,
	    		'type'=>'Withdraw',
	    		'paymentType'=>'bank',
	            'status'=>$resStatus,
	    		'modified'=>date('Y-m-d H:i:s'),
	        );
	        $saveData = $this->Crud_model->SaveData("user_account",$saveRefundData,'id="'.$userAccId.'"');

	        $saveRefundDataLog = array(
	        		'user_account_id'=>$userAccId,
	        		'orderId'=>$order_id,
	    			'amount'=>$withAmt,
	    			'balance'=>$userBal->balance,
	        		'user_detail_id'=>$userId,
	        		'paytmType'=>'byBank',
	        		'paytmStatus'=>$statusCode,
	        		'statusCode'=>$statusCode,
	        		'statusMessage'=>$statusMessage,
	        		'checkSum'=>$x_checksum,
	        		'type'=>'Withdraw',
	        		'paymentType'=>'bank',
	                'status'=>$resStatus,
	        		'created'=>date('Y-m-d H:i:s'),
	        		'modified'=>date('Y-m-d H:i:s'),
	        	);
	        $saveData = $this->Crud_model->SaveData("user_account_logs",$saveRefundDataLog);

	        $sms_body=$this->Crud_model->GetData("mst_sms_body","","smsType='withdraw_amt_msg_bank'",'','','','1');

	        $sms_body->smsBody=str_replace("{user_name}",ucfirst($userBal->user_name),$sms_body->smsBody); 
	        $sms_body->smsBody=str_replace("{amt}",$withAmt,$sms_body->smsBody); 
			$body=$sms_body->smsBody;
			$mobileNo=$userBal->mobile;
	        $this->custom->sendSms($mobileNo,$body);

	        $this->session->set_flashdata('message', 'Withdraw Amount Successfully');
		}else{
			$resStatus ='Failed';
			$type ='easebuzz';
			$statusMessage=$response->message;
			$statusCode=0;
			$saveRefundData = array(
	    		'orderId'=>$order_id,
	    		'user_detail_id'=>$userId,
	    		'paytmStatus'=>$statusCode,
	    		'statusCode'=>$statusCode,
	    		'statusMessage'=>$statusMessage,
	    		'checkSum'=>$x_checksum,
	    		'type'=>'Withdraw',
	    		'paymentType'=>'bank',
	            'status'=>$resStatus,
	    		'modified'=>date('Y-m-d H:i:s'),
	        );
	        $saveData = $this->Crud_model->SaveData("user_account",$saveRefundData,'id="'.$userAccId.'"');

	        $saveRefundDataLog = array(
	        		'user_account_id'=>$userAccId,
	        		'orderId'=>$order_id,
	    			'amount'=>$withAmt,
	    			'balance'=>$userBal->balance,
	        		'user_detail_id'=>$userId,
	        		'paytmType'=>'byBank',
	        		'paytmStatus'=>$statusCode,
	        		'statusCode'=>$statusCode,
	        		'statusMessage'=>$statusMessage,
	        		'checkSum'=>$x_checksum,
	        		'type'=>'Withdraw',
	        		'paymentType'=>'bank',
	                'status'=>$resStatus,
	        		'created'=>date('Y-m-d H:i:s'),
	        		'modified'=>date('Y-m-d H:i:s'),
	        	);
	        $saveData = $this->Crud_model->SaveData("user_account_logs",$saveRefundDataLog);

			$con = "id='".$userId."'";
            $getData = $this->Crud_model->GetData('user_details','',$con,'','','','1');
            
            $totalBal = $getData->balance + $withAmt;
            $winWallet = $getData->winWallet + $withAmt;
            $mainWallet = $getData->mainWallet;

            $dataUser = array(
                'balance'=>$totalBal,
                'winWallet'=>$winWallet,
                'mainWallet'=>$mainWallet,
            );
            $this->Crud_model->SaveData('user_details',$dataUser,$con);

            $sms_body=$this->Crud_model->GetData("mst_sms_body","","smsType='refund-reedem-amount'",'','','','1');

	        $sms_body->smsBody=str_replace("{user_name}",ucfirst($getData->user_name),$sms_body->smsBody); 
	        $sms_body->smsBody=str_replace("{amt}",$withAmt,$sms_body->smsBody); 
			$sms_body->smsBody=str_replace("{reason}",$statusMessage,$sms_body->smsBody);
			$body=$sms_body->smsBody;
			$mobileNo=$getData->mobile;
	        $this->custom->sendSms($mobileNo,$body);

			$this->session->set_flashdata('message', '<span>'.$response->message.'</span>');
		}
		
		redirect(site_url(WITHDRAWALDISTRIBUTE.'/'.base64_encode($userAccId)));
    }













    


 //    public function saveAllDistributerRedeemBypaytm(){
 //        $userData =$this->Crud_model->GetData("user_account","id,mobileNo,orderId,paymentType","id='".$_POST['id']."'","","","","1");
 //        $getSettData = $this->Crud_model->GetData("mst_settings","id,adminPercent","id='4'",'','','','1');
	// 	$admin_rs=($_POST['withAmt']*$getSettData->adminPercent)/100;

	// 	$getAdminData=$this->Crud_model->GetData("admin_login","id,adminBalance","id='".$_SESSION[SESSION_NAME]['id']."'",'','','','1');
	// 	$userAmt = $_POST['withAmt'] - $admin_rs;
	// 	$order_id = $userData->orderId;
	// 	$amount=$userAmt;
	// 	$userAccId=$this->input->post('id');
	// 	$userId=$this->input->post('userId');
	// 	$withrawAmt=$this->input->post('withAmt');
	// 	$userMobileNo= $userData->mobileNo;
	// 	//print_r($userMobileNo);exit();
	// 	$this->wallet_transfer($order_id,$amount,$userMobileNo,$userAccId,$userId,$withrawAmt);
	// 	redirect(site_url(WITHDRAWALDISTRIBUTE.'/'.base64_encode($_POST['id'])));

 //    }
 //    public function saveAllDistributerRedeem(){
 //        $userData =$this->Crud_model->GetData("user_account","id,mobileNo,orderId,paymentType","id='".$_POST['id']."'","","","","1");
 //        $userDetails =$this->Crud_model->GetData("user_details","*,bank_account_no as accno, bank_account_holder_name as acc_holderName, bank_ifsc as ifsc","id='".$_POST['userId']."'","","","","1");
 //        //print_r($userData->paymentType);exit();
 //        if($userData){
 //        	//if($userData->paymentType=='bank'){
 //        		$getSettData = $this->Crud_model->GetData("mst_settings","id,adminPercent","id='4'",'','','','1');
	// 			$admin_rs=($_POST['withAmt']*$getSettData->adminPercent)/100;
	// 			$getAdminData=$this->Crud_model->GetData("admin_login","id,adminBalance","id='".$_SESSION[SESSION_NAME]['id']."'",'','','','1');

	// 			$userAmt = $_POST['withAmt'] - $admin_rs;
	// 		   	$userBankData =$this->Crud_model->GetData("bank_details","","user_detail_id='".$_POST['userId']."'","","","","1"); 
	// 			$order_id =$userData->orderId;
	// 			if(!empty($userDetails->accno)){
	// 				$beneficiaryAccount= $userDetails->accno;
	// 				$beneficiaryIFSC= $userDetails->ifsc;
	// 				$beneficiaryName= $userDetails->acc_holderName;
	// 			}else{
	// 				$beneficiaryAccount= $userBankData->accno;
	// 				$beneficiaryIFSC= $userBankData->ifsc;
	// 				$beneficiaryName= $userDetails->name;
	// 			}
				
	// 			$amount=$userAmt;
	// 			$userAccId=$this->input->post('id');
	// 			$userId=$this->input->post('userId');
	// 			$withAmt=$this->input->post('withAmt');
	// 			$this->disburseFund($order_id,$amount,$beneficiaryAccount,$beneficiaryIFSC,$userAccId,$userId,$withAmt,$beneficiaryName);

 //     //     	}else{
 //     //     		$getSettData = $this->Crud_model->GetData("mst_settings","id,adminPercent","id='4'",'','','','1');
	// 			// $admin_rs=($_POST['withAmt']*$getSettData->adminPercent)/100;

	// 			// $getAdminData=$this->Crud_model->GetData("admin_login","id,adminBalance","id='".$_SESSION[SESSION_NAME]['id']."'",'','','','1');
	// 			// $userAmt = $_POST['withAmt'] - $admin_rs;
	// 			// $order_id = $userData->orderId;
	// 			// $amount=$userAmt;
	// 			// $userAccId=$this->input->post('id');
	// 			// $userId=$this->input->post('userId');
	// 			// $withrawAmt=$this->input->post('withAmt');
	// 			// $userMobileNo= $userData->mobileNo;
	// 			// $this->wallet_transfer($order_id,$amount,$userMobileNo,$userAccId,$userId,$withrawAmt);
 //     //     	}
 //        }
   
 //       redirect(site_url(WITHDRAWALDISTRIBUTE.'/'.base64_encode($_POST['id'])));
 //    }
 //    public function test(){
 //    	header("Pragma: no-cache");
 // 		header("Cache-Control: no-cache");
 //  		header("Expires: 0");
	// 	require_once(APPPATH . "/third_party/paytmlib/encdec_paytm.php");
	// 	$paytmParams = array();

	// 	$paytmParams["subwalletGuid"] = guid;//"97d2a3d6-f3bd-44e9-80de-c8a6dc89e7bc"; //GUID of AJAY

	// 	$date = date('Y-m-d');
	// 	$time = date('H:i:s');
	// 	$order_id = rand(000000000,999999999);
	// 	$amount = 1;
	// 	$paytmParams["orderId"] = $order_id;	
	// 	/* Amount in INR payable to beneficiary */
	// 	$paytmParams["beneficiaryAccount"] = '919899996782';
	// 	$paytmParams["beneficiaryIFSC"] = 'PYTM0123456';
	// 	$paytmParams["amount"] = $amount;
	// 	$paytmParams["purpose"] = 'OTHERS';//'BONUS';
	// 	// $paytmParams["date"] = $date;
	// 	// $paytmParams["requestTimestamp"] = $time;


	// 	$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
	// 	$x_checksum = getChecksumFromString($post_data, key);
	// 	$x_mid = mid;
	//     $url = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/bank";
	// 	$ch = curl_init($url);
	// 	curl_setopt($ch, CURLOPT_POST, true);
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "x-mid: " . $x_mid, "x-checksum: " . $x_checksum)); 
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	// 	$response = curl_exec($ch);	
	// 	$response = curl_error($ch) ? curl_error($ch) : $response;
	// 	$response = json_decode($response);
	// 	if($response->status=='ACCEPTED'){
	// 		$this->checkQuery($order_id,'12','1',$amount);
	// 		//echo "<pre>";  print_r("OK");exit();
	// 	}else{
	// 		//echo "<pre>";  print_r($response->status);
	// 	}
		
 //    }
 //    public function checkQuery($order_id,$userAccId,$userId,$amount)	// Check disburse bank status API
	// {
	// 	header("Pragma: no-cache");
	// 	header("Cache-Control: no-cache");
	// 	header("Expires: 0");		
	// 	require_once(APPPATH . "/third_party/paytmlib/encdec_paytm.php");	
	// 	/* initialize an array */
	// 	$paytmParams = array();
	// 	$getPaytmData = $this->Crud_model->GetData("user_account",'','id="'.$userAccId.'"','','','','1');
	// 	$paytmParams["orderId"] = $order_id;
	// 	$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
	// 	$checksum = getChecksumFromString($post_data, key);//iwpS9miFa%K0!x1L
	// 	$x_mid = mid;//"E15178612468400"
	// 	$x_checksum = $checksum;
	// 	$url = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/query";
	// 	$ch = curl_init($url);
	// 	curl_setopt($ch, CURLOPT_POST, true);
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "x-mid: " . $x_mid, "x-checksum: " . $x_checksum)); 
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	// 	$response = curl_exec($ch);
	// 	$response = curl_error($ch) ? curl_error($ch) : $response;
	// 	$response = json_decode($response);
	//     curl_close($ch);
	//     print_r($response);exit();
	// }



	// Old Function
    public function disburseFund_old($order_id,$amt,$beneficiaryAccount,$beneficiaryIFSC,$userAccId,$userId,$withAmt)// Creation of disburse Bank Transfer API.
	{	



		header("Pragma: no-cache");
 		header("Cache-Control: no-cache");
  		header("Expires: 0");
   
	    //require_once(APPPATH . "/third_party/paytmlib/config_paytm.php");
		require_once(APPPATH . "/third_party/paytmlib/encdec_paytm.php");	
		//require_once("encdec_paytm.php");

	    /* initialize an array */
		$paytmParams = array();

		$paytmParams["subwalletGuid"] = guid;//"97d2a3d6-f3bd-44e9-80de-c8a6dc89e7bc"; //GUID of AJAY


		$date = date('Y-m-d');
		$time = date('H:i:s');
		$paytmParams["orderId"] = $order_id;
		    
		/* Enter Beneficiary Phone Number against which the disbursal needs to be made */
		//$paytmParams["beneficiaryPhoneNo"] = 8421491235;

		/* Amount in INR payable to beneficiary */
		//$paytmParams["beneficiaryAccount"] = 919890800533;
		//$paytmParams["beneficiaryIFSC"] = 'HDFC0002746';
		//$paytmParams["beneficiaryAccount"] = 300000002448;	Invalid Account details
		//$paytmParams["beneficiaryIFSC"] = 'PYTM0123456';		Invalid Account details
		// if(isLivePaytm=='Yes'){
		// 	$paytmParams["beneficiaryAccount"] = '919899996782';
		// 	$paytmParams["beneficiaryIFSC"] = 'PYTM0123456';
		// }else{

		// }
		$paytmParams["beneficiaryIFSC"] 	= $beneficiaryIFSC;//'MAHB0000303';
		$paytmParams["beneficiaryAccount"]	= $beneficiaryAccount;//20195656312;
		$amount 							= $amt;//23;
		$paytmParams["amount"] 				= $amount;
		$paytmParams["purpose"] 			= 'OTHERS';//'BONUS';
		// $paytmParams["date"] 				= $date;
		// $paytmParams["requestTimestamp"] 	= $time;
		//echo "<pre>";print_r($paytmParams);exit();
		/* prepare JSON string for request body */
		$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

		//echo "post_data <pre>"; print_r($post_data);echo '<br/>';
		/**
		* Generate checksum by parameters we have in body
		*/
		$checksum = getChecksumFromString($post_data, key);//iwpS9miFa%K0!x1L
		//echo "checksum <pre>"; print_r($checksum);echo '<br/>';

		/* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
		//$x_mid = "E55778795707048";
		$x_mid = mid;//"E15178612468400";

		/* put generated checksum value here */
		$x_checksum = $checksum;
		// echo "x_checksum <pre>"; print_r($x_checksum);echo '<br/>';

		/* Solutions offered are: food, gift, gratification, loyalty, allowance, communication */

		/* for Staging */
		//$url = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/bank";
		

		/* for Production */
		$url = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/bank";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "x-mid: " . $x_mid, "x-checksum: " . $x_checksum)); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$response = curl_exec($ch);	
		$response = curl_error($ch) ? curl_error($ch) : $response;
		$response = json_decode($response);
		
		$userBal =$this->Crud_model->GetData("user_details","id,status,balance","id='".$_POST['userId']."'","","","","1");
		//print_r($response);exit;
		if($response->status=='ACCEPTED'){
			$resStatus ='Approved';
			$type ='Gratification';
			$statusMessage=$response->statusMessage;
			$statusCode=$response->statusCode;
			$saveRefundData = array(
	    		'orderId'=>$order_id,
	    		'user_detail_id'=>$userId,
	    		'paytmStatus'=>$response->status,
	    		'statusCode'=>$response->statusCode,
	    		'statusMessage'=>$response->statusMessage,
	    		'checkSum'=>$x_checksum,
	    		'type'=>$type,
	    		'paymentType'=>'bank',
	            'status'=>$resStatus,
	    		'created'=>date('Y-m-d H:i:s'),
	    		'modified'=>date('Y-m-d H:i:s'),
	        );
	        $saveData = $this->Crud_model->SaveData("user_account",$saveRefundData,'id="'.$userAccId.'"');

	        $saveRefundDataLog = array(
	        		'user_account_id'=>$userAccId,
	        		'orderId'=>$order_id,
	    			'amount'=>$withAmt,
	    			'balance'=>$userBal->balance,
	        		'user_detail_id'=>$userId,
	        		'paytmType'=>'byBank',
	        		'paytmStatus'=>$response->status,
	        		'statusCode'=>$response->statusCode,
	        		'statusMessage'=>$response->statusMessage,
	        		'checkSum'=>$x_checksum,
	        		'type'=>$type,
	        		'paymentType'=>'bank',
	                'status'=>$resStatus,
	        		'created'=>date('Y-m-d H:i:s'),
	        		'modified'=>date('Y-m-d H:i:s'),
	        	);
	        $saveData = $this->Crud_model->SaveData("user_account_logs",$saveRefundDataLog);
			$this->checkDisburseStatus($order_id,$userAccId,$userId,$amount);
		}elseif($response->status=='FAILURE'){
			$getUserDatadetails = $this->Crud_model->GetData('user_details','email_id,user_name,mobile,balance,winWallet',"id='".$userId."'",'','','','1');
			
			$updateBal =  $getUserDatadetails->balance + $getPaytmData->amount;
			$updatewinWallet =  $getUserDatadetails->winWallet + $getPaytmData->amount;
            $updateUserBal = array(
                'balance'=> $updateBal,
                'winWallet'=> $updatewinWallet,
                );
			$this->Crud_model->SaveData('user_details',$updateUserBal,'id="'.$userId.'"');


			/*  Sms Code  */
			// $sms_body=$this->Crud_model->GetData("mst_sms_body","","smsType='refund-reedem-amount'",'','','','1');
	  //       $sms_body->smsBody=str_replace("{user_name}",ucfirst($getUserDatadetails->user_name),$sms_body->smsBody); 
	  //       $sms_body->smsBody=str_replace("{amt}",$withAmt,$sms_body->smsBody); 
			// $sms_body->smsBody=str_replace("{reason}",$response->statusMessage,$sms_body->smsBody);
			// $body=$sms_body->smsBody;
			// $mobileNo=$getUserDatadetails->mobile;
	  //       $this->custom->sendSms($mobileNo,$body);
			/*  /.Sms Code  */

			$resStatus="Failed";
			$type ='Withdraw';
			$statusMessage=$response->statusMessage;
			$statusCode=$response->statusCode;
			$status='Failed';
			$statusMail ='failed';
			$content='Your withdrawal request of Rs '.$withrawAmt.' has been processed '.$statusMail.' due to '.$response->statusMessage.' ,please contact us at ';
           
		}else{
			$resStatus ='Pending';
			$type ='Withdraw';
			$statusMessage=$response->statusMessage;
			$statusCode=$response->statusCode;
			$status='Pending';
			$statusMail ='pending';
			$content='Your withdrawal request of Rs '.$withrawAmt.' has been processed '.$statusMail.' due to '.$response->statusMessage.' ,please contact us at ';
		}
		$saveRefundData = array(
    		'orderId'=>$order_id,
    		'user_detail_id'=>$userId,
    		'paytmStatus'=>$response->status,
    		'statusCode'=>$response->statusCode,
    		'statusMessage'=>$response->statusMessage,
    		'checkSum'=>$x_checksum,
    		'type'=>$type,
            'status'=>$resStatus,
            'paymentType'=>'bank',
    		'created'=>date('Y-m-d H:i:s'),
    		'modified'=>date('Y-m-d H:i:s'),
        );
        $saveData = $this->Crud_model->SaveData("user_account",$saveRefundData,'id="'.$userAccId.'"');

        $saveRefundDataLog = array(
        		'user_account_id'=>$userAccId,
        		'orderId'=>$order_id,
    			'amount'=>$withAmt,
    			'balance'=>$userBal->balance,
        		'user_detail_id'=>$userId,
        		'paytmType'=>'byBank',
        		'paytmStatus'=>$response->status,
        		'statusCode'=>$response->statusCode,
        		'statusMessage'=>$response->statusMessage,
        		'checkSum'=>$x_checksum,
        		'type'=>$type,
                'status'=>$resStatus,
                'paymentType'=>'bank',
        		'created'=>date('Y-m-d H:i:s'),
        		'modified'=>date('Y-m-d H:i:s'),
        	);
        $saveData = $this->Crud_model->SaveData("user_account_logs",$saveRefundDataLog);
	   	// echo "<pre>"; print_r($response);echo '<br/>';
	    //exit();
	   if($response->status=='FAILURE'){
	   		$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>'); 
	   }elseif($response->status=='PENDING'){
	   		$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>'); 	
	   }else{
	   		$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>'); 
	   }
	   curl_close($ch);
	   redirect(site_url(WITHDRAWALDISTRIBUTE.'/'.base64_encode($userAccId)));
	}

	// New Transfer to bank function
	public function disburseFund($order_id,$amt,$beneficiaryAccount,$beneficiaryIFSC,$userAccId,$userId,$withAmt,$beneficiaryName)// Creation of disburse Bank Transfer API.
	{	

		header("Pragma: no-cache");
 		header("Cache-Control: no-cache");
  		header("Expires: 0");
   
	  
		$date = date('Y-m-d');
		$time = date('H:i:s');

		$key = "36DB56ECEA";
		$salt_key = "DAF7B3EBE9";
		$amount = (float)$amt;
		
		// $order_id = "TST".rand();
		// $beneficiaryName = "Pradip Shende";
		
		$posted = array(
		    'key'=>$key,
		    'account_number'=>$beneficiaryAccount,
		    'ifsc'=>$beneficiaryIFSC,
		    'upi_handle'=>"",
		    'unique_request_number'=>$order_id,
		    'amount'=>$amount,
		    'salt'=>$salt_key,
		);

		$posted1 = array(
		    'key'=>$key,
		    'beneficiary_type'=>"bank_account",
		    'beneficiary_name'=>$beneficiaryName,
		    'account_number'=>$beneficiaryAccount,
		    'ifsc'=>$beneficiaryIFSC,
		    'upi_handle'=>"",
		    'unique_request_number'=>$order_id,
		    'payment_mode'=>"IMPS",
		    'amount'=>$amount,
		    'narration'=>"TEST",
		);
	
		
		$hash_sequence = "key|account_number|ifsc|upi_handle|unique_request_number|amount|salt";

		// make an array or split into array base on pipe sign.
		$hash_sequence_array = explode('|', $hash_sequence);
		$hash = null;

		// prepare a string based on hash sequence from the $params array.
		foreach ($hash_sequence_array as $value) {
		    $hash .= isset($posted[$value]) ? $posted[$value]."|" : '';
		    // $hash .= '|';
		}
		$hash = str_replace($salt_key."|", $salt_key, $hash);
		// print_r($hash);exit;
		// $hash .= $salt_key;
		#echo $hash;
		#echo " ";
		#echo strtolower(hash('sha512', $hash));
		// generate hash key using hash function(predefine) and return
		$hash_code =  strtolower(hash('sha512', $hash));
		// print_r($hash_code);
		$x_checksum = $hash_code;
		$postdata = json_encode($posted1);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://wire.easebuzz.in/api/v1/quick_transfers/initiate/',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>$postdata,
		  CURLOPT_HTTPHEADER => array(
		    'Authorization:'.$hash_code,
		    'Content-Type: application/json'
		  ),
		));

		$respo = curl_exec($curl);
		curl_close($curl);
		$response = json_decode($respo);
		


		
		// print_r($response->data);echo "<br>";
		
		$userBal =$this->Crud_model->GetData("user_details","id,status,balance","id='".$userId."'","","","","1");
		// print_r($response);//exit;
		if($response->success==1){
			$dataString = $response->data->transfer_request;
			// print_r($dataString);echo "<br>";//exit;
			$resStatus ='Approved';
			$type ='Gratification';
			$statusMessage="success";
			$statusCode=1;
			$saveRefundData = array(
	    		'orderId'=>$order_id,
	    		'user_detail_id'=>$userId,
	    		'paytmStatus'=>$statusCode,
	    		'statusCode'=>$statusCode,
	    		'statusMessage'=>$statusMessage,
	    		'checkSum'=>$x_checksum,
	    		'type'=>$type,
	    		'paymentType'=>'bank',
	            'status'=>$resStatus,
	    		'created'=>date('Y-m-d H:i:s'),
	    		'modified'=>date('Y-m-d H:i:s'),
	        );
	        $saveData = $this->Crud_model->SaveData("user_account",$saveRefundData,'id="'.$userAccId.'"');

	        $saveRefundDataLog = array(
	        		'user_account_id'=>$userAccId,
	        		'orderId'=>$order_id,
	    			'amount'=>$withAmt,
	    			'balance'=>$userBal->balance,
	        		'user_detail_id'=>$userId,
	        		'paytmType'=>'byBank',
	        		'paytmStatus'=>$statusCode,
	        		'statusCode'=>$statusCode,
	        		'statusMessage'=>$statusMessage,
	        		'checkSum'=>$x_checksum,
	        		'type'=>$type,
	        		'paymentType'=>'bank',
	                'status'=>$resStatus,
	        		'created'=>date('Y-m-d H:i:s'),
	        		'modified'=>date('Y-m-d H:i:s'),
	        	);
	        $saveData = $this->Crud_model->SaveData("user_account_logs",$saveRefundDataLog);
			$this->checkDisburseStatus($order_id,$userAccId,$userId,$amount);
		}elseif($response->success==0){
			$getUserDatadetails = $this->Crud_model->GetData('user_details','email_id,user_name,mobile,balance,winWallet',"id='".$userId."'",'','','','1');
			
			// $updateBal =  $getUserDatadetails->balance + $amount;
			// $updatewinWallet =  $getUserDatadetails->winWallet + $amount;
   //          $updateUserBal = array(
   //              'balance'=> $updateBal,
   //              'winWallet'=> $updatewinWallet,
   //              );
			$this->Crud_model->SaveData('user_details',$updateUserBal,'id="'.$userId.'"');


			/*  Sms Code  */
			// $sms_body=$this->Crud_model->GetData("mst_sms_body","","smsType='refund-reedem-amount'",'','','','1');
	  //       $sms_body->smsBody=str_replace("{user_name}",ucfirst($getUserDatadetails->user_name),$sms_body->smsBody); 
	  //       $sms_body->smsBody=str_replace("{amt}",$withAmt,$sms_body->smsBody); 
			// $sms_body->smsBody=str_replace("{reason}",$response->statusMessage,$sms_body->smsBody);
			// $body=$sms_body->smsBody;
			// $mobileNo=$getUserDatadetails->mobile;
	  //       $this->custom->sendSms($mobileNo,$body);
			/*  /.Sms Code  */

			$resStatus="Failed";
			$type ='Withdraw';
			$statusMessage="FAILURE";
			$statusCode=0;
			$status='Failed';
			$statusMail ='failed';
			$content='Your withdrawal request of Rs '.$amount.' has been processed '.$statusMail.' due to '.$response->message.' ,please contact us at ';
           
		}else{
			$resStatus ='Pending';
			$type ='Withdraw';
			$statusMessage=$response->message;
			$statusCode=0;
			$status='Pending';
			$statusMail ='pending';
			$content='Your withdrawal request of Rs '.$amount.' has been processed '.$statusMail.' due to '.$response->message.' ,please contact us at ';
		}
		$saveRefundData = array(
    		'orderId'=>$order_id,
    		'user_detail_id'=>$userId,
    		'paytmStatus'=>"FAILURE",
    		'statusCode'=>0,
    		'statusMessage'=>$response->message,
    		'checkSum'=>$x_checksum,
    		'type'=>$type,
            'status'=>$resStatus,
            'paymentType'=>'bank',
    		'created'=>date('Y-m-d H:i:s'),
    		'modified'=>date('Y-m-d H:i:s'),
        );
        $saveData = $this->Crud_model->SaveData("user_account",$saveRefundData,'id="'.$userAccId.'"');

        $saveRefundDataLog = array(
        		'user_account_id'=>$userAccId,
        		'orderId'=>$order_id,
    			'amount'=>$withAmt,
    			'balance'=>$userBal->balance,
        		'user_detail_id'=>$userId,
        		'paytmType'=>'byBank',
        		'paytmStatus'=>"FAILURE",
        		'statusCode'=>0,
        		'statusMessage'=>$response->message,
        		'checkSum'=>$x_checksum,
        		'type'=>$type,
                'status'=>$resStatus,
                'paymentType'=>'bank',
        		'created'=>date('Y-m-d H:i:s'),
        		'modified'=>date('Y-m-d H:i:s'),
        	);
        $saveData = $this->Crud_model->SaveData("user_account_logs",$saveRefundDataLog);
	   	// echo "<pre>"; print_r($response);echo '<br/>';
	    //exit();
	   if($response->success=='FAILURE'){
	   		$this->session->set_flashdata('message', '<span>'.$response->message.'</span>'); 
	   }elseif($response->success=='PENDING'){
	   		$this->session->set_flashdata('message', '<span>'.$response->message.'</span>'); 	
	   }else{
	   		$this->session->set_flashdata('message', '<span>'.$response->message.'</span>'); 
	   }
	   // curl_close($ch);
	   redirect(site_url(WITHDRAWALDISTRIBUTE.'/'.base64_encode($userAccId)));
	}


	public function wallet_transfer($order_id,$amount,$userMobileNo,$userAccId,$userId,$withrawAmt){
		/*print_r($order_id);echo "<br>";
		print_r($amount);echo "<br>";
		print_r($withrawAmt);echo "<br>";
		print_r($userMobileNo);echo "<br>";
		print_r($userAccId);echo "<br>";
		print_r($userId);echo "<br>";
		exit;*/

		/**
		* import checksum generation utility
		* You can get this utility from https://developer.paytm.com/docs/checksum/
		*/
		//print_r($customer_id);exit()
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");
	    // following files need to be included
	   // require_once(APPPATH . "/third_party/paytmlib/config_paytm.php");
	    require_once(APPPATH . "/third_party/paytmlib/encdec_paytm.php");
	    $paytmChecksum = "";

		/* initialize an array */
		$paytmParams = array();

		/* Find Sub Wallet GUID in your Paytm Dashboard at https://dashboard.paytm.com */
		$paytmParams["subwalletGuid"] = gratificationGuId;
		//$paytmParams["subwalletGuid"] = '6814a09b-1150-41a3-9f96-7e565b21c21c';
		//$paytmParams["subwalletGuid"] = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
		//$order_id = rand(00000000000000,99999999999999);
		/* Enter your unique order id, this should be unique for every disbursal */
		$paytmParams["orderId"] = $order_id;
		//$paytmParams["orderId"] = "190202";
		    
		/* Enter Beneficiary Phone Number against which the disbursal needs to be made */
		$paytmParams["beneficiaryPhoneNo"] = $userMobileNo;
		//$paytmParams["beneficiaryPhoneNo"] = '917777777777';

		/* Amount in INR payable to beneficiary */
		$paytmParams["amount"] = $amount;
		$paytmParams["purpose"] = 'OTHERS';
		//$paytmParams["timestamp"] = date("Y-m-d h:i:s");

		/* prepare JSON string for request body */
		$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
		//echo "<pre>";print_r($post_data);
		/*print_r("https://dashboard.paytm.com/bpay/api/v1/disburse/order/wallet/Gratification");print_r("<br/>");
		print_r($post_data);print_r("<br/>");*/
		/**
		* Generate checksum by parameters we have in body
		* Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
		*/
		$checksum = getChecksumFromString($post_data, gratificationMerchantKey);
		//$checksum = getChecksumFromString($post_data, "uF9cZaNpABsC&Xxa");

		/* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
		$x_mid = gratificationMerchantID;
		
		// echo "<pre> key ";print_r(key);
		// echo "<pre> mid ";print_r(mid);
		//$x_mid = "12966438680092";
		//print_r("x-mid : ".$x_mid);print_r("<br/>");	
		/* put generated checksum value here */
		$x_checksum = $checksum;
		//print_r("x-checksum : ".$x_checksum );print_r("<br/>");		
		/* Solutions offered are: food, gift, gratification, loyalty, allowance, communication */

		/* for Staging */
		//$url = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/wallet/{solution}";

		/* for Production */
		 //$url = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/wallet/Gratification";
		$url = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/wallet/gratification";
		//$url = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/bank";
		//echo "<pre> url ";print_r($url);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "x-mid: " . $x_mid, "x-checksum: " . $x_checksum)); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$resData = curl_exec($ch);
		//echo "<pre>";print_r($resData);
		$response = json_decode($resData);
			//echo "<pre>"; print_r($response);exit();
		$userBal =$this->Crud_model->GetData("user_details","id,status,balance","id='".$userId."'","","","","1");
		$getPaytmData = $this->Crud_model->GetData("user_account",'','id="'.$userAccId.'"','','','','1');
		//print_r($response);exit;
		if($response->status=='ACCEPTED'){
			$resStatus ='Approved';
			$status ='Approved';
			$statusMail ='successfully';
			$content='Congratulations! Your withdrawal request of Rs '.$withrawAmt.' has been processed '.$statusMail.'. Amount will reflect in your account within 24 working hours, if not then please contact us at ';
			if(!empty($getPaytmData) && $getPaytmData->isAdminReedem=='No'){
				$userData =$this->Crud_model->GetData("user_details","id,status,balance,user_name,mobile","id='".$userId."'","","","","1"); 
				$getSettData = $this->Crud_model->GetData("mst_settings","id,adminPercent","id='4'",'','','','1');
				$admin_rs=($amount*$getSettData->adminPercent)/100;
				$getAdminData=$this->Crud_model->GetData("admin_login","id,adminBalance","id='".$_SESSION[SESSION_NAME]['id']."'",'','','','1');
				if (!empty($getAdminData->adminBalance)) {
					$adminTotalAmt = $getAdminData->adminBalance + $admin_rs;
				} else {
					$adminTotalAmt = $admin_rs; 
				}
				$updateAdminData = array(
					'adminBalance'=>$adminTotalAmt,
				);

				$this->Crud_model->SaveData("admin_login",$updateAdminData,"id='".$_SESSION[SESSION_NAME]['id']."'");
				$saveAdminLogData = array(
						'user_account_id'=>$userAccId,
						'from_user_details_id'=>$userId,
						'to_admin_login_id'=>$getAdminData->id,
						'percent'=>$getSettData->adminPercent,
						'total_amount'=>$admin_rs,
						'type'=>'deposit',
					);
				$this->Crud_model->SaveData("admin_account_log",$saveAdminLogData);
				/***** Admin Data *****/
				 $approveData = array(
	                'status'=>$status,
	                'isAdminReedem'=>'Yes',
	                'paymentType'=>'paytm',
	                'paytmStatus'=>$response->status,
		    		'statusCode'=>$response->statusCode,
		    		'statusMessage'=>$response->statusMessage,
		    		'checkSum'=>$x_checksum,
	                'modified'=>date("Y-m-d H:i:s")
	            );
				 $approveDataLog = array(
	                'orderId'=>$order_id,
	                'user_account_id'=>$userAccId,
	                'user_detail_id'=>$userId,
	                'amount'=>$amount,
	                'balance'=>$userBal->balance,
	                'type'=>'Withdraw',
	                'paymentType'=>'paytm',
	                'paytmStatus'=>$response->status,
		    		'statusCode'=>$response->statusCode,
		    		'statusMessage'=>$response->statusMessage,
		    		'checkSum'=>$x_checksum,
	                'status'=>$status,
	                'created'=>date("Y-m-d H:i:s")
	            );
				$this->Crud_model->SaveData('user_account',$approveData,'id="'.$userAccId.'"');
				//$this->Crud_model->SaveData('user_details',$updateUserBal,'id="'.$userId.'"');
				$this->Crud_model->SaveData('user_account_logs',$approveDataLog);
				//------------------------------------------------------------------------------------------------
				$sms_body=$this->Crud_model->GetData("mst_sms_body","","smsType='withdraw_amt_msg_paytm'",'','','','1');
		        $sms_body->smsBody=str_replace("{user_name}",ucfirst($userData->user_name),$sms_body->smsBody); 
		        $sms_body->smsBody=str_replace("{amt}",$amount,$sms_body->smsBody); 
				$body=$sms_body->smsBody;
				$mobileNo=$userData->mobile;
		        $this->custom->sendSms($mobileNo,$body);
		        //------------------------------------------------------------------------------------------------
			}
			
		}elseif($response->status=='FAILURE'){
			$getUserFail = $this->Crud_model->GetData('user_details','email_id,user_name,mobile,balance,winWallet',"id='".$userId."'",'','','','1');
			
			$updateBal =  $getUserFail->balance + $getPaytmData->amount;
			$updatewinWallet =  $getUserFail->winWallet + $getPaytmData->amount;
            $updateUserBal = array(
                'balance'=> $updateBal,
                'winWallet'=> $updatewinWallet,
                );
			$this->Crud_model->SaveData('user_details',$updateUserBal,'id="'.$userId.'"');
			/*  Sms Code  */
			/*$sms_body=$this->Crud_model->GetData("mst_sms_body","","smsType='refund-reedem-amount'",'','','','1');
	        $sms_body->smsBody=str_replace("{user_name}",ucfirst($getUserFail->user_name),$sms_body->smsBody); 
	        $sms_body->smsBody=str_replace("{amt}",$getPaytmData->amount,$sms_body->smsBody); 
			$sms_body->smsBody=str_replace("{reason}",$response->statusMessage,$sms_body->smsBody);
			$body=$sms_body->smsBody;
			$mobileNo=$getUserFail->mobile;
	        $this->custom->sendSms($mobileNo,$body);*/
			$resStatus ='Failed';
			$status='Failed';
			$statusMail ='failed';
			$content='Your withdrawal request of Rs '.$withrawAmt.' has been processed '.$statusMail.' due to '.$response->statusMessage.' ,please contact us at ';
		}else{
			$getUserFail = $this->Crud_model->GetData('user_details','email_id,user_name,mobile,balance,winWallet',"id='".$userId."'",'','','','1');
			
			$updateBal =  $getUserFail->balance + $getPaytmData->amount;
			$updatewinWallet =  $getUserFail->winWallet + $getPaytmData->amount;
            $updateUserBal = array(
                'balance'=> $updateBal,
                'winWallet'=> $updatewinWallet,
                );
			$this->Crud_model->SaveData('user_details',$updateUserBal,'id="'.$userId.'"');
			$resStatus ='Failed';
			$status='Failed';
			$statusMail ='failed';
			$content='Your withdrawal request of Rs '.$withrawAmt.' has been processed '.$statusMail.' due to '.$response->statusMessage.' ,please contact us at ';
		}
		$saveRefundUpdate = array(
        		'orderId'=>$order_id,
        		'user_detail_id'=>$getPaytmData->user_detail_id,
        		'paytmStatus'=>$resStatus,
        		'statusCode'=>$response->statusCode,
        		'statusMessage'=>$response->statusMessage,
        		'checkSum'=>$x_checksum,
        		'type'=>'Withdraw',
                'status'=>$status,
                'paymentType'=>'paytm',
        		'modified'=>date('Y-m-d H:i:s'),
        	);
        $saveData = $this->Crud_model->SaveData("user_account",$saveRefundUpdate,'id="'.$userAccId.'"');
        $saveRefundUpdateLog = array(
        		'user_account_id'=>$userAccId,
        		'orderId'=>$order_id,
        		'amount'=>$getPaytmData->amount,
        		'balance'=>$getPaytmData->balance,
        		'user_detail_id'=>$getPaytmData->user_detail_id,
        		'paytmStatus'=>$response->status,
        		'statusCode'=>$response->statusCode,
        		'statusMessage'=>$response->statusMessage,
        		'checkSum'=>$x_checksum,
        		'type'=>'Withdraw',
        		'paymentType'=>'paytm',
                'status'=>$status,
        		'created'=>date('Y-m-d H:i:s'),
        		'modified'=>date('Y-m-d H:i:s'),
        	);
        $saveData = $this->Crud_model->SaveData("user_account_logs",$saveRefundUpdateLog);
        $getUserDAta=$this->Crud_model->GetData('user_details','id,user_name,email_id,mobile',"id='".$userId."'", '', '', '', '1');
        if($getUserDAta){
        	$siteTitle="ludo  cash";
            $mail_to=$getUserDAta->email_id;
            $subject='Redeem on ludo  cash';
            $mail_body ='<html>
				<head>
					<title></title>
				</head>
				<body>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
				        <tbody><tr>
				            <td valign="top" align="left">
				                <center>
				                    <table cellspacing="0" cellpadding="0" width="600">
				                        <tbody><tr>

				                            <td>

				                                <table cellspacing="0" cellpadding="0" width="100%">
				                                    <tbody><tr>
				                                       <td style="padding: 30.0px 0 10.0px 0;"><img src="http://3.20.220.191/admin/assets/images/profile/AT_8033logo.png" id="" alt="logo" width="120"><br></td>
				                                    </tr>
				                                    <tr>
				                                        <td height="150" valign="top">
				                                            <b>
				                                                <span>'.$siteTitle.'</span>
				                                            </b>
				                                            <br>
				                                            <span><small>'.$siteTitle.' is a dream game project of  Games Pvt. Ltd.</small></span>
				                                        </td>
				                                    </tr>

				                                    <tr>
				                                        <td style="height: 180.0px;width: 299.0px;">
				                                        </td>
				                                    </tr>
				                                </tbody></table>
				                            </td>

				                            <td valign="top">
				                                <table cellspacing="0" cellpadding="0" width="100%">
				                                    <tbody><tr>
				                                        <td>
				                                            <table cellspacing="0" cellpadding="0" width="100%">
				                                                <tbody><tr>
				                                                    <td>
				                                                        <table cellspacing="0" cellpadding="10" width="100%">
				                                                            <tbody><tr>
				                                                                <td>
				                                                                    <b>Dear '.$getUserDAta->user_name.',</b>
				                                                                </td>
				                                                            </tr>
				                                                        </tbody></table>

				                                                        <table cellspacing="0" cellpadding="10" width="100%">
				                                                            <tbody><tr>
				                                                                <td>
				                                                                    '.$content.' <a href="mailto:support@ludocash.com" target="_blank">support@ludocash.com</a>
				                                                                    <p><b>Thank you,</b></p>
				                                                                    <p><b><i>Team '.$siteTitle.'</i></b></p>
				                                                                </td>
				                                                            </tr>
				                                                        </tbody></table>
				                                                        
				                                                        <table cellspacing="0" cellpadding="0" width="100%">
				                                                            <tbody><tr>
				                                                                <td style="text-align: center;padding-top: 30.0px;"><img src="http://3.20.220.191/admin/uploads/settings/thank-you.png" id="" alt="signature" width="80px"><br>
				                                                                </td>
				                                                            </tr>
				                                                        </tbody></table>
				                                                        <table cellspacing="0" cellpadding="0" width="100%">
				                                                            <tbody><tr>
				                                                                <td>
				                                                                    <b>
				                                                                        <span>'.$siteTitle.'</span>
				                                                                    </b>
				                                                                    <br>
				                                                                    <span><small>'.$siteTitle.' is a dream game project of  Games Pvt. Ltd.</small></span>
				                                                                </td>
				                                                            </tr>
				                                                    </tbody></table></td>
				                                                </tr>
				                                            </tbody></table>
				                                        </td>
				                                    </tr>
				                                </tbody></table>
				                            </td>
				                        </tr>
				                    </tbody></table>
				                </center>
				            </td>
				        </tr>
				    </tbody></table>
				</body>
			</html>';
			//print_r($mail_body);exit;
			$this->load->library("Custom");
    		$this->custom->sendEmailSmtp($subject,$mail_body,$mail_to);
        }

    	if($response->status=='ACCEPTED'){
	    	$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>'); 	
	   }elseif($response->status=='FAILURE'){
	   		$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>'); 
	   }else{
	   		$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>');
	   }
	   redirect(site_url(WITHDRAWALDISTRIBUTE.'/'.base64_encode($userAccId)));
	}
	public function checkDisburseStatus($order_id,$userAccId,$userId,$amount)	// Check disburse bank status API
	{
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");		
		require_once(APPPATH . "/third_party/paytmlib/encdec_paytm.php");	

		/* initialize an array */
		$paytmParams = array();
		$getPaytmData = $this->Crud_model->GetData("user_account",'','id="'.$userAccId.'"','','','','1');
		/* Enter your order id which needs to be check disbursal status for */
		//$order_id = $_REQUEST['order_id'];
		$paytmParams["orderId"] = $order_id;
		/* prepare JSON string for request body */
		$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
		//echo "checksum <pre>"; print_r($post_data);echo '<br/>';
		/**
		* Generate checksum by parameters we have in body
		*/
		$checksum = getChecksumFromString($post_data, key);//iwpS9miFa%K0!x1L
		//echo "checksum <pre>"; print_r($checksum);echo '<br/>';
		/* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
		$x_mid = mid;//"E15178612468400"

		/* put generated checksum value here */
		$x_checksum = $checksum;

		/* for Staging */
		//$url = "https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/query";

		/* for Production */
		$url = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/query";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "x-mid: " . $x_mid, "x-checksum: " . $x_checksum)); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$response = curl_exec($ch);
		$response = curl_error($ch) ? curl_error($ch) : $response;
		$response = json_decode($response);
	    curl_close($ch);
	    $userBal =$this->Crud_model->GetData("user_details","id,status,balance","id='".$_POST['userId']."'","","","","1");
	    if($response->status=='SUCCESS' || $response->status=='PENDING'){
	    	//print_r("if");exit;
			$resStatus ='Approved';
			$status='Approved';
			if(!empty($getPaytmData) && $getPaytmData->isAdminReedem=='No'){
				$userData =$this->Crud_model->GetData("user_details","id,status,balance,user_name,mobile","id='".$userId."'","","","","1"); 
				$getSettData = $this->Crud_model->GetData("mst_settings","id,adminPercent","id='4'",'','','','1');
				$admin_rs=($amount*$getSettData->adminPercent)/100;

				$getAdminData=$this->Crud_model->GetData("admin_login","id,adminBalance","id='".$_SESSION[SESSION_NAME]['id']."'",'','','','1');
				
				if (!empty($getAdminData->adminBalance)) {
					$adminTotalAmt = $getAdminData->adminBalance + $admin_rs;
				} else {
					$adminTotalAmt = $admin_rs; 
				}
				$updateAdminData = array(
					'adminBalance'=>$adminTotalAmt,
				);
				$this->Crud_model->SaveData("admin_login",$updateAdminData,"id='".$_SESSION[SESSION_NAME]['id']."'");
				$saveAdminLogData = array(
						'user_account_id'=>$userAccId,
						'from_user_details_id'=>$userId,
						'to_admin_login_id'=>$getAdminData->id,
						'percent'=>$getSettData->adminPercent,
						'total_amount'=>$admin_rs,
						'type'=>'deposit',
					);
				$this->Crud_model->SaveData("admin_account_log",$saveAdminLogData);
				/***** Admin Data *****/
				 $approveData = array(
	                'status'=>'Approved',
	                'isAdminReedem'=>'Yes',
	                'paymentType'=>'bank',
	                'modified'=>date("Y-m-d H:i:s")
	            );

				 $approveDataLog = array(
	                'orderId'=>$order_id,
	                'user_account_id'=>$userAccId,
	                'user_detail_id'=>$userId,
	                'amount'=>$amount,
	                'balance'=>$userBal->balance,
	                'type'=>'Withdraw',
	                'paymentType'=>'bank',
	                'status'=>'Approved',
	                'created'=>date("Y-m-d H:i:s")
	            );
				
				$this->Crud_model->SaveData('user_account',$approveData,'id="'.$userAccId.'"');
				//$this->Crud_model->SaveData('user_details',$updateUserBal,'id="'.$userId.'"');
				$this->Crud_model->SaveData('user_account_logs',$approveDataLog);
				//------------------------------------------------------------------------------------------------
				$sms_body=$this->Crud_model->GetData("mst_sms_body","","smsType='withdraw_amt_msg_bank'",'','','','1');
		        $sms_body->smsBody=str_replace("{user_name}",ucfirst($userData->user_name),$sms_body->smsBody); 
		        $sms_body->smsBody=str_replace("{amt}",$amount,$sms_body->smsBody); 
				$body=$sms_body->smsBody;
				$mobileNo=$userData->mobile;
		        $this->custom->sendSms($mobileNo,$body);
		        //------------------------------------------------------------------------------------------------
			}
		}elseif($response->status=='PENDING'){
			$resStatus ='Process';
			$status='Process';
		}elseif($response->status=='FAILURE'){
			//print_r("query");echo "<prev>";
			$getUserDatadetails = $this->Crud_model->GetData('user_details','email_id,user_name,mobile,balance,winWallet',"id='".$userId."'",'','','','1');
			
			$updateBal =  $getUserDatadetails->balance + $getPaytmData->amount;
			$updatewinWallet =  $getUserDatadetails->winWallet + $getPaytmData->amount;
            $updateUserBal = array(
                'balance'=> $updateBal,
                'winWallet'=> $updatewinWallet,
                );
			$this->Crud_model->SaveData('user_details',$updateUserBal,'id="'.$userId.'"');
			/*  Sms Code  */
			/*$sms_body=$this->Crud_model->GetData("mst_sms_body","","smsType='refund-reedem-amount'",'','','','1');
	        $sms_body->smsBody=str_replace("{user_name}",ucfirst($getUserFail->user_name),$sms_body->smsBody); 
	        $sms_body->smsBody=str_replace("{amt}",$getPaytmData->amount,$sms_body->smsBody); 
			$sms_body->smsBody=str_replace("{reason}",$response->statusMessage,$sms_body->smsBody);
			$body=$sms_body->smsBody;
			$mobileNo=$getUserFail->mobile;
	        $this->custom->sendSms($mobileNo,$body);*/
			$resStatus ='Failed';
			$status='Failed';
		}else{
			$resStatus ='Pending';
			$status='Pending';
		}

		$saveRefundUpdate = array(
        		'orderId'=>$order_id,
        		'user_detail_id'=>$getPaytmData->user_detail_id,
        		'paytmStatus'=>$resStatus,
        		'statusCode'=>$response->statusCode,
        		'statusMessage'=>$response->statusMessage,
        		'checkSum'=>$x_checksum,
        		'type'=>'Withdraw',
                'status'=>$status,
                'paymentType'=>'bank',
        		'modified'=>date('Y-m-d H:i:s'),
        	);
        $saveData = $this->Crud_model->SaveData("user_account",$saveRefundUpdate,'id="'.$userAccId.'"');
        $saveRefundUpdateLog = array(
        		'user_account_id'=>$userAccId,
        		'orderId'=>$order_id,
        		'amount'=>$getPaytmData->amount,
        		'balance'=>$getPaytmData->balance,
        		'user_detail_id'=>$getPaytmData->user_detail_id,
        		'paytmType'=>'byQuery',
        		'paytmStatus'=>$response->status,
        		'statusCode'=>$response->statusCode,
        		'statusMessage'=>$response->statusMessage,
        		'checkSum'=>$x_checksum,
        		'type'=>'Withdraw',
        		'paymentType'=>'bank',
                'status'=>$status,
        		'created'=>date('Y-m-d H:i:s'),
        		'modified'=>date('Y-m-d H:i:s'),
        	);
        $saveData = $this->Crud_model->SaveData("user_account_logs",$saveRefundUpdateLog);
		//print_r($resStatus);exit();
       if($response->status=='SUCCESS'){
	    	$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>'); 	
	   }elseif($response->status=='PENDING'){
	   		$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>'); 
	   }elseif($response->status=='FAILURE'){
	   		$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>'); 
	   }else{
	   		$this->session->set_flashdata('message', '<span>'.$response->statusMessage.'</span>');
	   }
	   redirect(site_url(WITHDRAWALDISTRIBUTE.'/'.base64_encode($userAccId)));
	}
}