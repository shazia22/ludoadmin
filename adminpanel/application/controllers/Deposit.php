<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deposit extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->model('Deposit_model');
	} 

	public function index()
	{
		$data=array(
			'heading'=>"Manage Deposit",
			'bread'=>"Manage Deposit",

			);
		$this->load->view('deposit/list',$data);
	}

	public function ajax_manage_page()
	{
		$SearchData2 = $this->input->post('SearchData2');
		$cond='d.type="Deposit"';
		if(!empty($SearchData2)){
			$cond .=' and d.status="'.$SearchData2.'"';
		}

		if(!empty($this->input->post('SearchData')) && !empty($this->input->post('SearchData1'))) {
			$cond .= " and date(d.created) between '".date("Y-m-d",strtotime($this->input->post('SearchData')))."' and '".date("Y-m-d",strtotime($this->input->post('SearchData1')))."' ";
		}else if(!empty($this->input->post('SearchData'))) {
			$cond .= " and date(d.created) = '".date("Y-m-d",strtotime($this->input->post('SearchData')))."'";
		}else if(!empty($this->input->post('SearchData1'))) {
			$cond .= " and date(d.created) = '".date("Y-m-d",strtotime($this->input->post('SearchData1')))."'";
		}
		$getDeposit = $this->Deposit_model->get_datatables('user_account d',$cond);
	   // print_r($this->db->last_query());exit;

		if(empty($_POST['start']))
		{
			$no =0;   
		}else{
			 $no =$_POST['start'];
		}
		$data = array();
				  
		foreach ($getDeposit as $getDepositData) 
		{
			
			// $btn = '';
			// $btn = ''.anchor(site_url(USERVIEW.'/'.base64_encode($getDepositData->id)),'<span title="View" class="btn btn-primary btn-circle btn-xs"  data-placement="right" title="View"><i class="fa fa-eye"></i></span>');

			// $btn .="&nbsp;&nbsp;". anchor(site_url('users/delete/'.base64_encode($getDepositData->id)),"<button title='Delete' onclick='return confirm(\"Are you sure want to delete this record?\");' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i></button>");

			if(!empty($getDepositData->user_name)){ $user_name = $getDepositData->user_name; }else{ $user_name = 'NA'; }

			if(!empty($getDepositData->amount)){ $amount = $getDepositData->amount; }else{ $amount = 'NA'; }

			if(!empty($getDepositData->transactionId)){ $transactionId = $getDepositData->transactionId; }else{ $transactionId = 'NA'; }

			if(!empty($getDepositData->created)){ $created = $getDepositData->created; }else{ $created = 'NA'; }

			if($getDepositData->status=="Approved"){
				$status = "<label class='btn btn-xs btn-info'>".$getDepositData->status."</label>";
			}else if($getDepositData->status=="Pending"){
				$status = "<label class='btn btn-xs btn-danger'>".$getDepositData->status."</label>";
			}else if($getDepositData->status=="Rejected"){
				$status = "<label class='btn btn-xs btn-warning'>".$getDepositData->status."</label>";
			}else if($getDepositData->status=="Success"){
				$status = "<label class='btn btn-xs btn-success'>".$getDepositData->status."</label>";
			}else if($getDepositData->status=="Failed"){
				$status = "<label class='btn btn-xs btn-warning'>".$getDepositData->status."</label>";
			}else if($getDepositData->status=="Process"){
				$status = "<label class='btn btn-xs btn-success'>".$getDepositData->status."</label>";
			}else{
				$status = "<label class='btn btn-xs btn-info'>".$getDepositData->status."</label>";
			}

			$no++;
			$nestedData = array();
			$nestedData[] = $no;
			$nestedData[] = ucfirst($user_name);
			$nestedData[] = $getDepositData->email_id;
			$nestedData[] = $amount;
			$nestedData[] = date('d F Y h:i A', strtotime($created));
			$nestedData[] = $transactionId;
			$nestedData[] = $status;
			//$nestedData[] = $btn;
			
			$data[] = $nestedData;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->Deposit_model->count_all('user_account d',$cond),
					"recordsFiltered" => $this->Deposit_model->count_filtered('user_account d',$cond),
					"data" => $data,
					"csrfHash" => $this->security->get_csrf_hash(),
					"csrfName" => $this->security->get_csrf_token_name(),
				);
		echo json_encode($output);
	}

	public function exportAction() {
		$SearchData2 = $this->input->post('SearchData2');
		$cond='d.type="Deposit"';
		if(!empty($SearchData2)){
			$cond .=' and d.status="'.$SearchData2.'"';
		}

		if(!empty($this->input->post('fromDate')) && !empty($this->input->post('toDate'))) {
			$cond .= " and date(d.created) between '".date("Y-m-d",strtotime($this->input->post('fromDate')))."' and '".date("Y-m-d",strtotime($this->input->post('toDate')))."' ";
		}else if(!empty($this->input->post('fromDate'))) {
			$cond .= " and date(d.created) = '".date("Y-m-d",strtotime($this->input->post('fromDate')))."'";
		}else if(!empty($this->input->post('toDate'))) {
			$cond .= " and date(d.created) = '".date("Y-m-d",strtotime($this->input->post('toDate')))."'";
		}
		$getDepositData = $this->Deposit_model->getDepositDataExport('user_account d',$cond);
		if(!empty($getDepositData)) {
			$filename = "Withdrawal (Paytm)".date('d-m-Y H:i').".csv";
			$fp = fopen('php://output', 'w');
			$header = array('Name','Email','Deposit','Date','Transaction Id','Status');
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename='.$filename);
			fputcsv($fp, $header);
			$sr=1;
			foreach ($getDepositData as $report) {
				if(!empty($report->user_name)){ $user_name = $report->user_name; }else{ $user_name = 'NA'; }

				if(!empty($report->email_id)){ $email_id = $report->email_id; }else{ $email_id = 'NA'; }

				if(!empty($report->amount)){ $amount = $report->amount; }else{ $amount = '0'; }

				if(!empty($report->created)){ $created = date('d/m/Y', strtotime($report->created)); }else{ $created = 'NA'; }

				if(!empty($report->transactionId)){ $transactionId = $report->transactionId; }else{ $transactionId = 'NA'; }

				if(!empty($report->status)){ $status = $report->status; }else{ $status = 'NA'; }

				fputcsv($fp, array($user_name,$email_id,ucfirst($amount),$created,$transactionId,$status));
				$sr++;

			}
		} else {
			$this->session->set_flashdata('message', 'Record not avaliable.');
			redirect(DEPOSIT);
		}
	}
}
