<!-- Load common header -->
<?php $this->load->view('common/header'); ?>

<!-- Load common left panel -->
<?php $this->load->view('common/left_panel.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?= $heading; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= site_url(DASHBOARD); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><?= $bread; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box bShow">
            <div class="box-header col-md-12">
              <div class="col-md-4 box-title paddLeft"><?= $heading; ?></div>
              <div class="col-md-4"></div>
              <div class="col-md-4 text-right paddRight">
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <!--  <div class="table-responsive"> -->
                  <table class="table table-bordered table-striped" id="example_datatable" style="width: 100%;">
                    <thead>
                    <tr>
                      <th>Sr. No.</th>
                      <th>SMS Type</th>
                      <th>SMS Body</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                <!-- </div> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 
<script type="text/javascript">
  var url = '<?= site_url(SMSLOGLIST); ?>';
  var actioncolumn=2;
  var pageLength='';
</script>
<?php $this->load->view('common/footer'); ?>
