<!-- Load common header -->
<?php $this->load->view('common/header'); ?>

<!-- Load common left panel -->
<?php $this->load->view('common/left_panel'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?= $heading; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= site_url(DASHBOARD); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><?= $bread; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box bShow">
            <div class="box-header col-md-12">
              <div class="col-md-4 box-title paddLeft"><?= $heading; ?></div>
              <div class="col-md-4"></div>
              <div class="col-md-4 text-right paddRight">
                <a href="<?= site_url(ROLEACCESSCREATE); ?>" class="btn btn-primary">Create</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped" id="example_datatable" style="width: 100%;">
                <thead>
                <tr>
                   <th>Sr. No.</th>
                   <th>Name</th>
                   <th>Email</th>
                   <th>Commission %</th>
                   <th>Status</th>
                   <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    
                            <?php
                            foreach($data as $row):?>
                    <tr>
                        <td><?php echo $row->id;?></td>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->email;?></td>
                    <td></td>
                    <td><?php echo $row->status;?></td>
                    <td></td>
                    </tr>
                                
<!--   echo "<tr><td>".$row->id."</td>";
  echo "<td>".$row->name."</td>";
  echo "<td>".$row->email."</td>";
  echo "<td>"." "."</td>";
  echo "<td>".$row->status."</td>";
    echo "<td>"." "."</td>";-->

<!--  }-->
<!--//                        foreach($data as $row):
//                            print_r($row);-->
<!--                            ?>-->
                        <?php  endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 
<script type="text/javascript">
  var url = '<?= site_url(ROLEACCESSLIST);?>';
  var actioncolumn=5;
  var pageLength='';
</script>
<?php $this->load->view('common/footer.php'); ?>



<script type="text/javascript">  
	 function Delete(id) {
			 $("#Deletemodal").modal('show');
			 $("#deleteSuccBtn").click(function(){
				 var site_url   = $("#site_url").val();
				 var url        =  site_url+"/<?= ROLEACCESSDELETE; ?>";
				 var datastring =  'id='+id+"&"+csrfName+"="+csrfHash;
				 $.post(url,datastring,function(response){
					 $("#Deletemodal").modal('hide');
					 $("#Deletemodal").load(location.href+" #Deletemodal>*","");
						 var obj   = JSON.parse(response);
						 csrfName = obj.csrfName;
						 csrfHash = obj.csrfHash;
						 table.draw();
						 $("#msgData").val(obj.msg);
						 $("#toast-fade").click();
					 });
			 });
		 }
</script>