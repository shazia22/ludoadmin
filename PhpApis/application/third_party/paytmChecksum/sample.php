<?php

require_once("./PaytmChecksum.php");

/* initialize an array */
$paytmParams = array();

/* add parameters in Array */
$paytmParams["MID"] = "GAMERZ85626304836609";
$paytmParams["ORDERID"] = "ORDERID_987651";

/**
* Generate checksum by parameters we have
* Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
*/
$paytmChecksum = PaytmChecksum::generateSignature($paytmParams, 'qBmBRzgxh#4YT9qB');
$verifySignature = PaytmChecksum::verifySignature($paytmParams, 'qBmBRzgxh#4YT9qB', $paytmChecksum);
echo sprintf("generateSignature Returns: %s\n", $paytmChecksum);
echo sprintf("verifySignature Returns: %b\n\n", $verifySignature);


/* initialize JSON String */  
$body = "{\"mid\":\"GAMERZ85626304836609\",\"orderId\":\"ORDERID_987651\"}";

/**
* Generate checksum by parameters we have in body
* Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
*/
$paytmChecksum = PaytmChecksum::generateSignature($body, 'qBmBRzgxh#4YT9qB');
$verifySignature = PaytmChecksum::verifySignature($body, 'qBmBRzgxh#4YT9qB', $paytmChecksum);
echo sprintf("generateSignature Returns: %s\n", $paytmChecksum);
echo sprintf("verifySignature Returns: %b\n\n", $verifySignature);