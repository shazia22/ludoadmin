<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

require APPPATH . '/libraries/REST_Controller.php';

class Withdraw_new extends REST_Controller 
{

 	function __construct()
    {
        parent::__construct();
        $this->load->helper('custom_helper');
    }

    public function addWithdrawAmount_post() 
    {
        headers();
        $this->_request =  file_get_contents("php://input");
        $jsonDecodeData =json_decode($this->_request, true);

        $userId = $jsonDecodeData['userId'];
        $withdrawAmount = $jsonDecodeData['withdrawAmount'];
        $userMobileNo = $jsonDecodeData['userMobileNo'];
        $paymentType = $jsonDecodeData['paymentType'];
        $orderId 	= $jsonDecodeData['orderId'];

        if(!empty($userId) && !empty($withdrawAmount)  && !empty($userMobileNo) && !empty($paymentType) && !empty($orderId))
        {
           	$userData =$this->Crud_model->GetData("user_details","id,status,blockuser,balance,winWallet,kyc_status,is_aadharVerified,is_panVerified","id='".$userId."'","","","","1");

	            if(!empty($userData)){
	            	if($userData->status!='Active'){
		            	 $response = array('status' => FALSE, 'success' => "0",'msg' => "Your account is inactive.");
		            }elseif($userData->blockuser!='No'){
		            	$response = array('status' => FALSE, 'success' => "0",'msg' => "Your account is blocked by admin.");
		            }elseif($userData->balance < $withdrawAmount){
		                $response = array('status' => FALSE, 'success' => "3",'msg' => "You have insufficient available balance");
		            }elseif($userData->winWallet < $withdrawAmount){
		            	 $response = array('status' => FALSE, 'success' => "3",'msg' => "You have insufficient available balance in win wallet");
		            }elseif($paymentType=='bank' && ($userData->kyc_status=='Pending' || $userData->kyc_status=='Rejected')){
		            		$response = array('status' => FALSE, 'success' => "0",'msg' => "Your kyc is not verified so you cant send withdraw request");
		            }elseif($paymentType=='paytm' && ($userData->is_aadharVerified=='Pending' || $userData->is_aadharVerified=='Rejected' || $userData->is_panVerified=='Pending' || $userData->is_panVerified=='Rejected')){
		            		$response = array('status' => FALSE, 'success' => "0",'msg' => "Your kyc is not verified so you cant send withdraw request");
		            }else{
		            	$userRecord =$this->Crud_model->GetData("user_account","id,orderId","orderId='".$orderId."'");
		            	if(!empty($userRecord)){
		            	 $response = array('status' => FALSE, 'success' => "2",'msg' => "Order Id already exists");
		            	}else{
			            	$con = "id='".$userId."'";
			                $getData = $this->Crud_model->GetData('user_details','',$con,'','','','1');
			                
			                $totalBal = $getData->balance - $withdrawAmount;
			                $winWallet = $getData->winWallet - $withdrawAmount;
			                $mainWallet = $getData->mainWallet;

			                $dataUser = array(
			                    'balance'=>$totalBal,
			                    'winWallet'=>$winWallet,
			                    'mainWallet'=>$mainWallet,
			                );
			                $this->Crud_model->SaveData('user_details',$dataUser,$con);

			                $data = array(
			                    'user_detail_id'=>$userId,
			                    'orderId'=>$orderId,
			                    'type'=>'Withdraw',
			                    'mobileNo'=>$userMobileNo,
			                    'paymentType'=>$paymentType,
			                    'amount'=>$withdrawAmount,
			                    'balance'=>$totalBal,
			                    'winWallet'=>$winWallet,
			                    'mainWallet'=>$mainWallet,
			                    'isReadNotification'=>'No',
			                    'status'=>'Pending',
			                    'created'=>date("Y-m-d H:i:s"),
			                );
			                $this->Crud_model->SaveData('payments',$data);
			                $last_id = $this->db->insert_id();

			               /* $dataLog = array(
			                    'user_account_id'=>$last_id,
			                    'orderId'=>$orderId,
			                    'user_detail_id'=>$userId,
			                    'mobileNo'=>$userMobileNo,
			                    'type'=>'Withdraw',
			                    'amount'=>$withdrawAmount,
			                    'balance'=>$totalBal,
			                    'winWallet'=>$winWallet,
			                    'mainWallet'=>$mainWallet,
			                    'paymentType'=>$paymentType,
			                    'isReadNotification'=>'No',
			                    'status'=>'Pending',
			                     'created'=>date("Y-m-d H:i:s"),
			                );
			                $this->Crud_model->SaveData('payment_logs',$dataLog);*/

			                $response = array('status' => TRUE, 'success' => "1",'msg' => "Withdraw amount successfully");
		            	}
		        	}
		        }else{
		        	$response = array('status' => FALSE, 'success' => "0", 'msg' => "No user found");
		        }
        }
        else
        {
            $response = array('status' => FALSE, 'success' => "0", 'msg' => "All fields are required");
        }
        $this->response($response,REST_Controller::HTTP_CREATED);
    }
}