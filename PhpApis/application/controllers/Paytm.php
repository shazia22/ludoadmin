<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("Asia/Calcutta");
class Paytm extends CI_Controller 
{
	var $controller = "Paytm";
	public function __construct()
	{
		parent::__construct();
	}
	public function testpay($orderId,$user_id,$amount)
	{


		//76162
		if(!empty($orderId) && !empty($user_id) && !empty($amount) && $amount>=0){
		// if(!empty($orderId) && !empty($user_id) && !empty($amount)){
			

			$getOrderId = $this->Crud_model->GetData('payment_logs','','orderId="'.$orderId.'"');

			if(!empty($getOrderId)){
				redirect($this->controller.'/paymentDone/'.base64_encode('2'));
				return;
			}else 
			if(!empty($orderId) && !empty($user_id) && !empty($amount)){
				$getUser = $this->Crud_model->GetData('user_details','user_name,email_id,mobile','id="'.$user_id.'"','','','','1');
 				
				
				if(empty($getUser)){
					redirect($this->controller.'/paymentDone/'.base64_encode('3'));return;
				}else{          
					
					$Data = array(
						'user_detail_id' => $user_id,
						'userName' => $getUser->user_name,
						'userEmail' => $getUser->email_id,
						'userPhone' => $getUser->mobile,
						'orderId'        => $orderId,
						'amount'     => $amount,
						'paymentBy'    => 'Paytm',  
						'created'      => date('Y-m-d H:i:s'),
						'action'      => site_url("Paytm/easebuzz")."?api_name=initiate_payment",
					);
					$saveData = $this->Crud_model->SaveData('payment_logs',$Data);
					$_POST["user_id"] = $user_id;
					header("Pragma: no-cache");
					header("Cache-Control: no-cache");
					header("Expires: 0");
					$this->load->view("payment",$Data);
				 }
		    }
		    else
		    {
		    	print_r('Insufficient parameters, Kindly uppdate with parameters');exit;
		    }
		}
		else
		{
			print_r('Required parameters missing, Kindly uppdate with parameters');exit;
		}
		
	}
	public function pay_by_paytm($orderId,$user_id,$amount)
	{

		//76162
		if(!empty($orderId) && !empty($user_id) && !empty($amount) && $amount>=25){
		// if(!empty($orderId) && !empty($user_id) && !empty($amount)){
			

			$getOrderId = $this->Crud_model->GetData('payment_logs','','orderId="'.$orderId.'"');
			if(!empty($getOrderId)){
				redirect($this->controller.'/paymentDone/'.base64_encode('2'));
				return;
			}else 
			if(!empty($orderId) && !empty($user_id) && !empty($amount)){
				$getUser = $this->Crud_model->GetData('user_details','user_name,email_id,mobile','id="'.$user_id.'"','','','','1');
				$isGoToPayment="Yes";
				
				if(empty($getUser)){
					redirect($this->controller.'/paymentDone/'.base64_encode('3'));return;
				}else{   
					$user_name = str_replace(' ', '-', $getUser->user_name); // Replaces all spaces with hyphens.
		   			$user_name= preg_replace('/[^A-Za-z0-9\-]/', '', $user_name); // Removes special chars.
		   			if($getUser->email_id==''){
		   				$getUser->email_id = "test@gmail.com";	
		   			}
					$Data = array(
						'user_detail_id' => $user_id,
						'userName' => $user_name,
						'userEmail' => $getUser->email_id,
						'userPhone' => $getUser->mobile,
						'orderId'        => $orderId,
						'amount'     => $amount,
						'paymentBy'    => 'Paytm',  
						'created'      => date('Y-m-d H:i:s'),
						'action'      => site_url("Paytm/easebuzz")."?api_name=initiate_payment",
					);
					$saveData = $this->Crud_model->SaveData('payment_logs',$Data);
					$_POST["user_id"] = $user_id;
					header("Pragma: no-cache");
					header("Cache-Control: no-cache");
					header("Expires: 0");
					$this->load->view("payment",$Data);
				 }
		    }
		    else
		    {
		    	print_r('Insufficient parameters, Kindly uppdate with parameters');exit;
		    }
		}
		else
		{
			print_r('Required parameters missing, Kindly uppdate with parameters');exit;
		}
		
	}

	function clean($string='') {
		$string="ashish33$$";
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string= preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	   print_r($string);exit();
	}
	function easebuzz()
	{
		include_once(APPPATH .'/third_party/easebuzz-lib/easebuzz_payment_gateway.php');
		if(!empty($_POST) && (sizeof($_POST) > 0)){
			//echo "<pre>"; print_r($_POST);exit();
			$apiname = trim(htmlentities($_GET['api_name'], ENT_QUOTES));
			// $MERCHANT_KEY = "2PBP7IABZ2"; //testing
			$MERCHANT_KEY = "OHZH1XX9JK1";
			// $SALT = "DAH88E3UWQ"; //testing
			$SALT = "STBY1KOBU1M";
			// $ENV = "test";    // setup test enviroment (testpay.easebuzz.in).
			$ENV = "prod";   // setup production enviroment (pay.easebuzz.in).
			$easebuzzObj = new Easebuzz($MERCHANT_KEY, $SALT, $ENV);
			if($_POST['email']==''){
				$_POST['email']= 'test@gmail.com';
			}
			// print_r($apiname);exit;
        	if($apiname === "initiate_payment"){
				$easebuzzObj->initiatePaymentAPI($_POST);

			}else if($apiname === "transaction"){ 
				$result = $easebuzzObj->transactionAPI($_POST);
	            $this->checkPayment($result);
	     
	        }else if($apiname === "transaction_date" || $apiname === "transaction_date_api"){ 
	            $result = $easebuzzObj->transactionDateAPI($_POST);
	            $this->checkPayment($result);
	                       
	        }else if($apiname === "refund"){
	            $result = $easebuzzObj->refundAPI($_POST);
	            $this->checkPayment($result);
	                       
	        }else if($apiname === "payout"){
	            $result = $easebuzzObj->payoutAPI($_POST);
	            $this->checkPayment($result);
	        }else{
	            echo '<h1>You called wrong API, Pleae try again</h1>';
	        }
	    }else{
	        echo '<h1>Please fill all mandatory fields.</h1>';
	    }

	}

	function checkPayment($data)
	{
		print_r($data);exit;
	}

	function response()
	{
		include_once(APPPATH .'/third_party/easebuzz-lib/easebuzz_payment_gateway.php');
	    // salt for testing env
	    $SALT = "STBY1KOBUM";

	    $easebuzzObj = new Easebuzz($MERCHANT_KEY = null, $SALT, $ENV = null);
    
	    $result = $easebuzzObj->easebuzzResponse( $_POST );
	 
	    $jd = json_decode($result);
	    	// echo "<pre>"; print_r($jd);exit;
	   	$response = $jd->data;
	   	if($response->status == 'success'){
	   		$this->paymetSucces($response);
    		redirect($this->controller.'/paymentDone/'.base64_encode('1'));
	   	}else if($response->status == 'userCancelled'){
	   		redirect($this->controller.'/paymentDone/'.base64_encode('4'));
	   	}else{
	   		$this->paymentFail($response);
    		redirect($this->controller.'/paymentDone/'.base64_encode('3'));
	   	}

	}

	public function paymentDone($successData)
	{   
		$success = base64_decode($successData);
		if($success== '1'){
			$msg = "Payment Success";
		}else if($success=='2'){
			$msg = "Order Id Already Exits.";
		}else if($success== '4'){
			$msg = "Payment Failed";
		}else if($success== '5'){
			$msg = "Payment Already Done";
		}else{
			$msg = "Something went wrong";
		}

		echo "<h2><center>".$msg."</center></h2>";
	}

	public function paymetSucces($paytmResponse)
	{
		if(empty($paytmResponse)){
			$this->paymentFail();
			return;
		}
		$json_data    = json_encode($paytmResponse);
		
		$amount    = $paytmResponse->amount;
		$statusMessage    = $paytmResponse->status;
		$paymentMode    = $paytmResponse->payment_source;
		$transactionId    = $paytmResponse->easepayid;
		$bankName    = $paytmResponse->issuing_bank;
		$orderId    = $paytmResponse->txnid;

		$getLogData    = $this->Crud_model->GetData("payment_logs","","orderId='".$orderId."'","","","","1");
		$user_id = $getLogData->user_detail_id;
	    // Get User Data and Update Record
		$getUserData    = $this->Crud_model->GetData("user_details","","user_id='".$user_id."'","","","","1");
		$balance        = $getUserData->balance + $amount;
		$mainWallet     = $getUserData->mainWallet + $amount;
		$userDetailsSavedata=array(
			"balance"   => $balance,
			"mainWallet"=> $mainWallet,
		);
		// echo "<pre>"; print_r($userDetailsSavedata);exit;
		$this->Crud_model->SaveData("user_details",$userDetailsSavedata, "user_id='".$user_id."'");
		
	    // Update payement record 
		$saveuserData   = array(
			'transactionId' => $transactionId,
			'orderId'   => $orderId,
			'amount'    => $amount,
			'balance'   => $balance,
			'mainWallet'  => $mainWallet,
			'user_detail_id'=> $user_id,
			'paymentMode' => $paymentMode,
			'bankName'    => $bankName,
			'json_data'   => $json_data,
			'type'      => 'Deposit',
			'status'    => 'Success',
			'paymentType' => 'Paytm',
			'created'   => date('Y-m-d H:i:s'),
			'modified'    => date('Y-m-d H:i:s'),
		);
		$this->Crud_model->SaveData("payments",$saveuserData);

		$lastId= $this->db->insert_id();
		$saveuserDataLog = array(
			'transactionId'  => $transactionId,
			'payment_id'=> $lastId,
			'orderId'        => $orderId,
			'amount'         => $amount,
			'balance'    => $getUserData->balance,
			'mainWallet'   => $getUserData->mainWallet,
			'user_detail_id' => $user_id,
			'paymentMode'  => $paymentMode,
			'isPayment'    => 'Yes',
			'json_data'    => $json_data,
			'type'       => 'Deposit',
			'status'     => 'Success',
			'paytmStatus'  => $paytmStatus,
			// 'statusCode'   => $statusCode,
			'statusMessage'  => $statusMessage,
			'paymentType'  => 'paytm',
			'created'    => date('Y-m-d H:i:s'),
		);
		$this->Crud_model->SaveData("payment_logs",$saveuserDataLog,"id='".$payment_log_id."'");
	}

	public function paymentFail($paytmResponse)
	{
		$json_data    = json_encode($paytmResponse);
		
		$amount    = $paytmResponse->amount;
		$statusMessage    = $paytmResponse->status;
		$paymentMode    = $paytmResponse->payment_source;
		$transactionId    = $paytmResponse->easepayid;
		$bankName    = $paytmResponse->issuing_bank;
		$orderId    = $paytmResponse->txnid;
		
		$getLogData    = $this->Crud_model->GetData("payment_logs","","orderId='".$orderId."'","","","","1");
		$user_id = $getLogData->user_detail_id;
		

	      // Get User Data and Update Record
		$getUserData    = $this->Crud_model->GetData("user_details","","user_id='".$user_id."'","","","","1");
		$balance        = $getUserData->balance;
		$mainWallet     = $getUserData->mainWallet;
		

	      // Update payement record 
		$saveuserData   = array(
			'transactionId' => $transactionId,
			'orderId'   => $orderId,
			'amount'    => $amount,
			'balance'   => $balance,
			'mainWallet'  => $mainWallet,
			'user_detail_id'=> $user_id,
			'paymentMode' => $paymentMode,
			'bankName'    => $bankName,
			'json_data'   => $json_data,
			'type'      => 'Deposit',
			'status'    => 'Failed',
			'paymentType' => 'paytm',
			'created'   => date('Y-m-d H:i:s'),
			'modified'    => date('Y-m-d H:i:s'),
		);
		$this->Crud_model->SaveData("payments",$saveuserData);

		$lastId= $this->db->insert_id();
		$saveuserDataLog = array(
			'transactionId'  => $transactionId,
			'payment_id'=> $lastId,
			'orderId'        => $orderId,
			'amount'         => $amount,
			'balance'    => $getUserData->balance,
			'mainWallet'   => $getUserData->mainWallet,
			'user_detail_id' => $user_id,
			'paymentMode'  => $paymentMode,
			'bankName'     => $bankName,
			'isPayment'    => 'Yes',
			'json_data'    => $json_data,
			'type'       => 'Deposit',
			'status'     => 'Failed',
			'paytmStatus'  => $paytmStatus,
			'statusCode'   => $statusCode,
			'statusMessage'  => $statusMessage,
			'paymentType'  => 'paytm',
			'created'    => date('Y-m-d H:i:s'),
		);
		$this->Crud_model->SaveData("payment_logs",$saveuserDataLog,"id='".$payment_log_id."'");
	}

}
?>