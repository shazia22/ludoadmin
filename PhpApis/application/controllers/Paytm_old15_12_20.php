<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("Asia/Calcutta");
class Paytm extends CI_Controller 
{
	var $controller = "Paytm";
	public function __construct()
	{
		parent::__construct();
	}

	public function pay_by_paytm($orderId,$user_id,$amount)
	{

		
		if(!empty($orderId) && !empty($user_id) && !empty($amount) && $amount>=25){

			$getOrderId = $this->Crud_model->GetData('payment_logs','','orderId="'.$orderId.'"');
			if(!empty($getOrderId)){
				redirect($this->controller.'/paymentDone/'.base64_encode('2'));
				return;
			}else 
			if(!empty($orderId) && !empty($user_id) && !empty($amount)){
				$Data = array(
					'user_detail_id' => $user_id,
					'orderId'        => $orderId,
					'amount'     => $amount,
					'paymentBy'    => 'Paytm',  
					'created'      => date('Y-m-d H:i:s'),
				);

				$isGoToPayment="Yes";

				
				if($isGoToPayment=="No"){
          // Getting Some Error
					redirect($this->controller.'/paymentDone/'.base64_encode('3'));return;
				}else{          
					
					$saveData = $this->Crud_model->SaveData('payment_logs',$Data);
					$_POST["user_id"] = $user_id;
					header("Pragma: no-cache");
					header("Cache-Control: no-cache");
					header("Expires: 0");
					
              // following files need to be included
					require_once(APPPATH ."/third_party/paytmlib/config_paytm.php");
					require_once(APPPATH ."/third_party/paytmlib/encdec_paytm.php");  
					
					$paramList["MID"] = PAYTM_MERCHANT_MID_NEW;
					
					$paramList["ORDER_ID"]      = $orderId;
					$paramList["CUST_ID"]       = $user_id;
            $paramList["INDUSTRY_TYPE_ID"]  = 'Retail';// For Live Retail109
            $paramList["CHANNEL_ID"]    = 'WEB';
            $paramList["TXN_AMOUNT"]     = $amount; 
            
            $paramList["WEBSITE"]       = PAYTM_MERCHANT_WEBSITE_NEW;
            $paramList["CALLBACK_URL"]    = site_url($this->controller.'/checkPayment');
            $paramList["MSISDN"]      = ''; //Mobile number of customer
            $paramList["EMAIL"]       = '';//Email ID of customer
            $checkSum             = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY_NEW);
            //$action = PAYTM_TXN_URL;

            echo "<html>
            <head>
            <title>Merchant Check Out Page</title>
            </head>
            <body>
            <center><h1>Please do not refresh this page...</h1></center>
            <form method='post' action='".PAYTM_TXN_URL_NEW."' name='f1'>
            <table border='1'>
            <tbody>";

            foreach($paramList as $name => $value) {
            	echo '<input type="hidden" name="'. $name .'" value="'. $value .'">';
            }

            echo "<input type='hidden' name='CHECKSUMHASH' value='". $checkSum ."'>
            </tbody>
            </table>
            <script type='text/javascript'>
            document.f1.submit();
            </script>
            </form>
            </body>
            </html>";
        }
    }
    else
    {
    	print_r('Insufficient parameters, Kindly uppdate with parameters');exit;
    }
}
else
{
	print_r('Required parameters missing, Kindly uppdate with parameters');exit;
}
}

  // Get Payment Response
public function checkPayment()
{
	
	header("Pragma: no-cache");
	header("Cache-Control: no-cache");
	header("Expires: 0");
      // following files need to be included
	require_once(APPPATH . "/third_party/paytmlib/config_paytm.php");
	require_once(APPPATH . "/third_party/paytmlib/encdec_paytm.php");
	$paytmChecksum    = "";
	$paramList      = array();
	$isValidChecksum  = "FALSE";
	$paramList      = $_POST;
	$paytmChecksum    = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : "";
    $isValidChecksum  = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY_NEW, $paytmChecksum); //will return TRUE or FALSE string.
    $orderData      = $this->Crud_model->GetData('payment_logs','','orderId="'.$_POST['ORDERID'].'"',"","","","1");
    if(empty($orderData)){
      // Record Not Found
    	redirect($this->controller.'/paymentDone/'.base64_encode('4'));
    }
    if($orderData->isPayment=="Yes"){
    	redirect($this->controller.'/paymentDone/'.base64_encode('5'));         
    }
    $paytmResponse = $_POST;
    if($isValidChecksum == "TRUE") 
    { 
    	if($_POST["STATUS"] == "TXN_SUCCESS"){

    		$this->paymetSucces($paytmResponse,$orderData);
    		redirect($this->controller.'/paymentDone/'.base64_encode('1'));

    	}else if($_POST["STATUS"] == "PENDING"){
    		$this->paymentFail($paytmResponse,$orderData);
    		redirect($this->controller.'/paymentDone/'.base64_encode('3'));
    	}else{  
    		$this->paymentFail($paytmResponse,$orderData);
    		redirect($this->controller.'/paymentDone/'.base64_encode('3'));
    		
    	} 
    }else{
    	$this->paymentFail($paytmResponse,$orderData);
    	redirect($this->controller.'/paymentDone/'.base64_encode('3'));
    } 
}

public function paymetSucces($paytmResponse,$orderData)
{
	if(empty($paytmResponse)){
		$this->paymentFail();
		return;
	}
	$json_data    = json_encode($paytmResponse);
	
	$user_id    = $orderData->user_detail_id;
	$payment_log_id = $orderData->id;
	$amount     = $paytmResponse['TXNAMOUNT'];
	$orderId    = $paytmResponse['ORDERID'];
	$transactionId  = $paytmResponse['TXNID'];
	$paymentMode  = $paytmResponse['PAYMENTMODE'] ? $paytmResponse['PAYMENTMODE'] : '';
	if(empty($paytmResponse['BANKNAME']))
		$bankName = $paytmResponse['PAYMENTMODE'];
	else
		$bankName     = $paytmResponse['BANKNAME'] ? $paytmResponse['BANKNAME'] : '';
	$paytmStatus  = $paytmResponse['STATUS'] ? $paytmResponse['STATUS'] : '';
	$statusCode   = $paytmResponse['RESPCODE'] ? $paytmResponse['RESPCODE'] : '';
	$statusMessage  = $paytmResponse['RESPMSG'] ? $paytmResponse['RESPMSG'] : '';
	

      // Get User Data and Update Record
	$getUserData    = $this->Crud_model->GetData("user_details","","user_id='".$user_id."'","","","","1");
	$balance        = $getUserData->balance + $amount;
	$mainWallet     = $getUserData->mainWallet + $amount;
	$userDetailsSavedata=array(
		"balance"   => $balance,
		"mainWallet"=> $mainWallet,
	);
	$this->Crud_model->SaveData("user_details",$userDetailsSavedata, "user_id='".$user_id."'");
	

      // Update payement record 
	$saveuserData   = array(
		'transactionId' => $transactionId,
		'orderId'   => $orderId,
		'amount'    => $amount,
		'balance'   => $balance,
		'mainWallet'  => $mainWallet,
		'user_detail_id'=> $user_id,
		'paymentMode' => $paymentMode,
		'bankName'    => $bankName,
		'json_data'   => $json_data,
		'type'      => 'Deposit',
		'status'    => 'Success',
		'paymentType' => 'Paytm',
		'created'   => date('Y-m-d H:i:s'),
		'modified'    => date('Y-m-d H:i:s'),
	);
	$this->Crud_model->SaveData("payments",$saveuserData);

	$lastId= $this->db->insert_id();
	$saveuserDataLog = array(
		'transactionId'  => $transactionId,
		'payment_id'=> $lastId,
		'orderId'        => $orderId,
		'amount'         => $amount,
		'balance'    => $getUserData->balance,
		'mainWallet'   => $getUserData->mainWallet,
		'user_detail_id' => $user_id,
		'paymentMode'  => $paymentMode,
		'isPayment'    => 'Yes',
		'json_data'    => $json_data,
		'type'       => 'Deposit',
		'status'     => 'Success',
		'paytmStatus'  => $paytmStatus,
		'statusCode'   => $statusCode,
		'statusMessage'  => $statusMessage,
		'paymentType'  => 'paytm',
		'created'    => date('Y-m-d H:i:s'),
	);
	$this->Crud_model->SaveData("payment_logs",$saveuserDataLog,"id='".$payment_log_id."'");
	
	
	
}
public function paymentFail($paytmResponse,$orderData)
{
	
	$json_data    = json_encode($paytmResponse);
	
	$user_id    = $orderData->user_detail_id;
	$payment_log_id = $orderData->id;
	$amount     = $paytmResponse['TXNAMOUNT']   ? $paytmResponse['TXNAMOUNT']   : '';
	$orderId    = $paytmResponse['ORDERID']   ? $paytmResponse['ORDERID']   : '';
	$transactionId  = $paytmResponse['TXNID']     ? $paytmResponse['TXNID']     : '';
	$paymentMode  = $paytmResponse['PAYMENTMODE'] ? $paytmResponse['PAYMENTMODE'] : '';
	$bankName     = $paytmResponse['BANKNAME'] ? $paytmResponse['BANKNAME'] : '';
	$paytmStatus  = $paytmResponse['STATUS'] ? $paytmResponse['STATUS'] : '';
	$statusCode   = $paytmResponse['RESPCODE'] ? $paytmResponse['RESPCODE'] : '';
	$statusMessage  = $paytmResponse['RESPMSG'] ? $paytmResponse['RESPMSG'] : '';
	

      // Get User Data and Update Record
	$getUserData    = $this->Crud_model->GetData("user_details","","user_id='".$user_id."'","","","","1");
	$balance        = $getUserData->balance;
	$mainWallet     = $getUserData->mainWallet;
	

      // Update payement record 
	$saveuserData   = array(
		'transactionId' => $transactionId,
		'orderId'   => $orderId,
		'amount'    => $amount,
		'balance'   => $balance,
		'mainWallet'  => $mainWallet,
		'user_detail_id'=> $user_id,
		'paymentMode' => $paymentMode,
		'bankName'    => $bankName,
		'json_data'   => $json_data,
		'type'      => 'Deposit',
		'status'    => 'Failed',
		'paymentType' => 'paytm',
		'created'   => date('Y-m-d H:i:s'),
		'modified'    => date('Y-m-d H:i:s'),
	);
	$this->Crud_model->SaveData("payments",$saveuserData);

	$lastId= $this->db->insert_id();
	$saveuserDataLog = array(
		'transactionId'  => $transactionId,
		'payment_id'=> $lastId,
		'orderId'        => $orderId,
		'amount'         => $amount,
		'balance'    => $getUserData->balance,
		'mainWallet'   => $getUserData->mainWallet,
		'user_detail_id' => $user_id,
		'paymentMode'  => $paymentMode,
		'bankName'     => $bankName,
		'isPayment'    => 'Yes',
		'json_data'    => $json_data,
		'type'       => 'Deposit',
		'status'     => 'Failed',
		'paytmStatus'  => $paytmStatus,
		'statusCode'   => $statusCode,
		'statusMessage'  => $statusMessage,
		'paymentType'  => 'paytm',
		'created'    => date('Y-m-d H:i:s'),
	);
	$this->Crud_model->SaveData("payment_logs",$saveuserDataLog,"id='".$payment_log_id."'");
	
	
	
}

public function paymentDone($successData)
{   
	$success = base64_decode($successData);
	if($success== '1'){
		$msg = "Payment Success";
	}else if($success=='2'){
		$msg = "Order Id Already Exits.";
	}else if($success== '4'){
		$msg = "Payment Failed";
	}else if($success== '5'){
		$msg = "Payment Already Done";
	}else{
		$msg = "Something went wrong";
	}

	
	
	echo "<h2><center>".$msg."</center></h2>";
}





public function paymentStatus()
{
		$date = date("Y-m-d H:i:s", strtotime("+30 minutes"));
	$condition = "isPayment='No' AND created >= '".$date."' AND type='Deposit' ";
	if(!empty($_GET['ids'])){
		$condition .= " AND id IN(".$_GET['ids'].") ";
	}
	$getPaymentLog = $this->Crud_model->GetData("payment_logs","",$condition,"","id DESC","10","");
	// print_r($getPaymentLog);
	// print_r($this->db->last_query());exit;
	if (!empty($getPaymentLog)) {
		require_once(APPPATH."/third_party/paytmChecksum/PaytmChecksum.php");

		foreach ($getPaymentLog as $showPaymentLog) {

			$paytmParams = array();

			$paytmParams["MID"]     = PAYTM_MERCHANT_MID_NEW;
			$paytmParams["ORDERID"] = $showPaymentLog->orderId;
			$checksum = PaytmChecksum::generateSignature($paytmParams, PAYTM_MERCHANT_KEY_NEW);

			$paytmParams["CHECKSUMHASH"] = $checksum;

			$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

			/* for Staging */
			// $url = "https://securegw-stage.paytm.in/order/status";

			/* for Production */
        	$url = "https://securegw.paytm.in/order/status";

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));  
			$response = curl_exec($ch);
			$paytmResponse = json_decode($response, true);
			print_r($paytmResponse);
			if($paytmResponse["STATUS"] == "TXN_SUCCESS"){

				$this->paymetSucces($paytmResponse,$showPaymentLog);

			}else if($paytmResponse["STATUS"] == "PENDING"){
				$this->paymentFail($paytmResponse,$showPaymentLog);
			}else{  
				$this->paymentFail($paytmResponse,$showPaymentLog);
			} 

		}
	}
}
public function paymentStatusView()
{
	$condition = "type='Deposit' ";
	if(!empty($_GET['ids'])){
		$condition .= " AND id IN(".$_GET['ids'].") ";
	}
	$getPaymentLog = $this->Crud_model->GetData("payment_logs","",$condition,"","id DESC","10","");
	// print_r($getPaymentLog);
	// print_r($this->db->last_query());exit;
	if (!empty($getPaymentLog)) {
		require_once(APPPATH."/third_party/paytmChecksum/PaytmChecksum.php");

		foreach ($getPaymentLog as $showPaymentLog) {

			$paytmParams = array();

			$paytmParams["MID"]     = PAYTM_MERCHANT_MID_NEW;
			$paytmParams["ORDERID"] = $showPaymentLog->orderId;
			$checksum = PaytmChecksum::generateSignature($paytmParams, PAYTM_MERCHANT_KEY_NEW);

			$paytmParams["CHECKSUMHASH"] = $checksum;

			$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

			/* for Staging */
			// $url = "https://securegw-stage.paytm.in/order/status";

			/* for Production */
        	$url = "https://securegw.paytm.in/order/status";

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));  
			$response = curl_exec($ch);
			$paytmResponse = json_decode($response, true);
			print_r($paytmResponse);
			if($paytmResponse["STATUS"] == "TXN_SUCCESS"){

				$this->paymetSucces($paytmResponse,$showPaymentLog);

			}else if($paytmResponse["STATUS"] == "PENDING"){
				$this->paymentFail($paytmResponse,$showPaymentLog);
			}else{  
				$this->paymentFail($paytmResponse,$showPaymentLog);
			} 

		}
	}
}
}