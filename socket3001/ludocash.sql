-- Adminer 4.4.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DELIMITER ;;

DROP FUNCTION IF EXISTS `saveregdata`;;
CREATE FUNCTION `saveregdata`(`P_email` varchar(255), `P_mobile` bigint, `P_password` text, `P_country_name` varchar(255), `P_name` varchar(255), `P_socialId` varchar(255), `P_registrationType` varchar(255), `P_deviceId` varchar(255), `P_referal_id` varchar(20), `P_userName` varchar(255), `P_deviceName` varchar(255), `P_deviceModel` varchar(255), `P_deviceOs` varchar(255), `P_deviceRam` varchar(255), `P_deviceProcessor` varchar(255), `P_playerId` varchar(255), `P_agendId` varchar(255)) RETURNS int(11)
BEGIN
  DECLARE P_USER_ID INT DEFAULT "";
  DECLARE RANDNUM INT DEFAULT "";
  DECLARE OTP INT DEFAULT "";
  DECLARE REF_NUM INT DEFAULT 0;
  DECLARE P_refBonus INT DEFAULT 0;

  /*if(SUBSTRING(P_referal_code,1,1) != 'R') 
  THEN 
    SET P_referal_code='';
  end if;*/
  select version from mst_settings  limit 1 into @version;
  SET OTP = LPAD(FLOOR(RAND() * 999999.99), 6, '0');
  if(P_referal_id!='')
  then
    SELECT IFNULL(referal_code, ''),referredAmt FROM user_details where user_id=P_referal_id LIMIT 1 INTO @referal_code,@referredAmt;
  else
    SET @referal_code='';
    SET @referredAmt=0;
  end if;

  INSERT INTO user_details (email_id,user_id,profile_img,mobile,password,country_name,name,user_name,referred_by,referredByUserId,signup_date,
  blockuser,status,socialId,registrationType,device_id,otp,deviceName,deviceModel,deviceOs,deviceRam,deviceProcessor,playerId,version,agendId) 
  VALUES (P_email,'0','',P_mobile,P_password,P_country_name,P_name,P_userName,@referal_code,P_referal_id,now(),'No','Active',P_socialId,
  P_registrationType,P_deviceId,OTP,P_deviceName,P_deviceModel,P_deviceOs,P_deviceRam,P_deviceProcessor,P_playerId,@version,P_agendId);

  SET P_USER_ID = LAST_INSERT_ID();

  if (P_USER_ID <= 9999)
  THEN 
   SET REF_NUM = RIGHT("000"+P_USER_ID, 4);
  ELSE 
   SET REF_NUM = P_USER_ID;
  end if;

 

  SELECT referralBonus,signUpBonus from mst_settings LIMIT 1 INTO @referralBonus,@signUpBonus;
  SET P_refBonus = (CAST(@referredAmt as unsigned)+CAST(@referralBonus as unsigned));
  UPDATE user_details SET referredAmt=P_refBonus where id=P_referal_id;
  UPDATE user_details SET referal_code=CONCAT('R',UPPER(SUBSTRING(P_name,1,1)),REF_NUM), user_id=P_USER_ID,referredAmt=@signUpBonus WHERE id=P_USER_ID;
  
  if(P_referal_id!='')
  THEN
    INSERT INTO referal_user_logs (fromUserId,toUserId,toUserName,referalAmount,tableId,referalAmountBy,created) VALUES
    (P_referal_id,P_USER_ID,P_userName,@referralBonus,0,'Register',now());
  end if;
  INSERT INTO referal_user_logs (fromUserId,toUserId,toUserName,referalAmount,tableId,referalAmountBy,created) VALUES
    (P_USER_ID,0,P_name,@signUpBonus,0,'Signup',now());
  RETURN P_USER_ID;
END;;

DROP PROCEDURE IF EXISTS `changePassword`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `changePassword`(IN `input_userId` INT, IN `input_oldPassword` VARCHAR(255), IN `input_newPassword` VARCHAR(255), IN `input_confirmPassword` VARCHAR(255))
BEGIN
select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then
    SELECT id,password from user_details where id=input_userId limit 1 INTO @userId,@password;
    if(@userId!='')
    then
      --  set @success=1;
       -- set @message= "User Found";
       if(input_newPassword !=input_confirmPassword)
       then
          set @success=4;
          set @message= "New password and confirm password should be same";
       else
         -- UPDATE user_details SET password=MD5(input_newPassword) where id=input_userId;
          set @success=1;
          set @message= "User Found";
          set @dbPassword=@password; 
       end if;
     /*  if(MD5(input_oldPassword) != @password)
       then
          set @success=2;
          set @message= "password not matched.";
       elseif(@password = MD5(input_newPassword))
       then
          set @success=3;
          set @message= "Old & new password can't be same";
       elseif(@password = MD5(input_confirmPassword))
       then
          set @success=4;
          set @message= "New password and confirm password should be same";
       else
          UPDATE user_details SET password=MD5(input_newPassword) where id=input_userId;
          set @success=1;
          set @message= "Password changed successfully";
       end if; */
    else
       set @success=5;
       set @message= "Invalid User.";
    end if;
  else
    set @success=6;
    set @message= "Invalid Data Submitted";
  end if;
    select @success as success,@message as message,@dbPassword as dbPassword;

END;;

DROP PROCEDURE IF EXISTS `createPrivateRoom`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `createPrivateRoom`(IN `input_noOfPlayer` TINYINT, IN `input_gameMode` VARCHAR(30), IN `input_betValue` INT, IN `input_roomId` INT, IN `input_isFree` VARCHAR(10))
BEGIN
    if(input_noOfPlayer!='' && input_gameMode!='' && input_gameMode!='' && input_roomId!='')
    then
           select roomId from ludo_mst_rooms where isPrivate='Yes' and roomId=input_roomId limit 1 into @roomId;           
          if(@roomId!='')            
          then
            set @success = 1;
            set @message ="success";
            insert into ludo_join_rooms 
               set roomId=@roomId,noOfPlayers=input_noOfPlayer,activePlayer=0,isFree=input_isFree,gameMode=input_gameMode,betValue=input_betValue,isPrivate='Yes',
               isTournament='No',modified=now(),created=now();
            SET @joinRoomId = LAST_INSERT_ID();
          else
            set @success = 0;
            set @message ="failed";
          end if;
          select @success as success,@message as message,@roomId as roomId,@joinRoomId as joinRoomId,input_noOfPlayer as noOfPlayer,
           input_gameMode as gameMode,input_betValue as betValue,input_roomId as roomId,input_isFree as isFree;
    else
         select 1 as  success,"All field are required." as message;
    end if;
     
END;;

DROP PROCEDURE IF EXISTS `forgotPassword`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `forgotPassword`(IN `input_mobile` BIGINT)
BEGIN
select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then
    select id,user_name,mobile from user_details where mobile=input_mobile and blockuser='No' limit 1 
    into @userId,@user_name,@mobile;

    if(@userId!='')
    then
       select lpad(conv(floor(rand()*pow(36,6)), 10, 36), 6, 0) into @newPassword;
       /*UPDATE user_details SET password=MD5(@newPassword) where id=@userId;*/

       set @newPassword=@newPassword;
       set @success=1;
       set @status=true;
       set @message="Password is sent to your mobile number";
    else
       set @newPassword='';
       set @success=2;
       set @status=false;
       set @message="Invalid User";
    end if;
  else
    set @success=6;
    set @message= "Invalid Data Submitted";
  end if;
    
  select @message as message,@status as status,@success as success,@newPassword as newPassword,@user_name as user_name,@userId as userId;

END;;

DROP PROCEDURE IF EXISTS `joinBotsRoomTable`;;
CREATE PROCEDURE `joinBotsRoomTable`(IN `input_userId` double, IN `input_roomId` double, IN `input_players` int, IN `input_betValue` double, IN `input_playerType` varchar(10), IN `input_gameMode` varchar(20), IN `input_isFree` varchar(10), IN `input_tableId` double)
BEGIN
   if(input_userId !='' and input_roomId!='')
   then
     select balance,user_name,id,profile_img from user_details where id=input_userId limit 1 into @coins,@userName,@userId,@profile_img;
     select baseUrl from mst_settings where id!=0 limit 1 into @baseUrl;
     select killToken,firstToken,firstThreeToken,roomId,commision,currentRoundBot,totalRoundBot,roomTitle,isBotConnect from ludo_mst_rooms where roomId=input_roomId limit 1 
     into @killToken,@firstToken,@firstThreeToken,@roomId,@commision,@currentRoundBot,@totalRoundBot,@roomTitle,@isBotConnect;
     set @message := "Failed";
     if(@userId is not null and @roomId is not null)
     then
         if(@profile_img='')
         then
            set @profile ='';
         else 
            set @profile := concat(@baseUrl,'uploads/userProfileImages/',@profile_img);
         end if;
         
         set @success := 0;    
         select joinRoomId,gameStatus,activePlayer from ludo_join_rooms where roomId=input_roomId and noOfPlayers = input_players 
          and activePlayer < input_players and isPrivate='No' and isTournament='No' and joinRoomId=input_tableId 
          into @joinRoomId,@gameStatus,@activePlayer ; 
         if(@joinRoomId is not null)
         then
            set @success := 1;
            set @message := "Success";
            set @player := @activePlayer+1;  
            if @player = input_players
            then
              set @gameStatus= 'Active';            
            end if;  
         
            update ludo_join_rooms set activePlayer=@player,gameStatus=@gameStatus,modified=now() where joinRoomId=@joinRoomId;
            insert into ludo_join_room_users set userId=input_userId,roomId=input_roomId,tokenColor='Blue',playerType=input_playerType,
                 userName=@userName,joinRoomId=@joinRoomId,isTournament='No',created=now();  
         else
            set @success :=3;
         end if;
     else
       set @success := 4;
     end if;
     select @success as success,@message as message,@coins as coins,@userId as userId,@roomId as roomId,@joinRoomId as joinRoomId,
            @gameStatus as gameStatus,'Blue' as tokenColor,input_players as players,@userName as userName,input_playerType as playerType, 
           input_gameMode as gameMode,"No" as isPrivate,@commision as adminCommision,input_isFree as isFree,input_betValue as betValue,
           @currentRoundBot as currentRoundBot,@totalRoundBot as totalRoundBot,@profile as profile,@profile_img  as profile_img,
           @roomTitle as roomTitle,@isBotConnect as isBotConnect,'No' as isTournament,@killToken as killToken,@firstToken as firstToken,@firstThreeToken as firstThreeToken;
   else
     select 2 as  success,"All field are required." as message;
   end if;  
END;;

DROP PROCEDURE IF EXISTS `joinPrivateRoom`;;
CREATE PROCEDURE `joinPrivateRoom`(IN `input_userId` int, IN `input_roomId` int, IN `input_player` double, IN `input_value` double, IN `input_color` varchar(50), IN `input_type` varchar(50), IN `input_gameMode` varchar(50), IN `input_tableId` double, IN `input_isFree` varchar(10))
BEGIN
   if(input_userId !='' and input_roomId!='' )
   then
     select balance,user_name,id,profile_img from user_details where id=input_userId limit 1 into @coins,@userName,@userId,@profile_img;
     select killToken,firstToken,firstThreeToken,roomId,commision,currentRoundBot,totalRoundBot,roomTitle,isBotConnect,startRoundTime,tokenMoveTime,rollDiceTime from ludo_mst_rooms where roomId=input_roomId and isPrivate='Yes' limit 1 
     into @killToken,@firstToken,@firstThreeToken,@roomId,@commision,@currentRoundBot,@totalRoundBot,@roomTitle,@isBotConnect,@startRoundTime,@tokenMoveTime,@rollDiceTime;

     select baseUrl from mst_settings where id!=0 limit 1 into @baseUrl;
     if(@userId is not null and @roomId is not null and  @coins>9 and @coins>=input_value and input_value>9)
     then  
         if(@profile_img='')
         then
            set @profile ='';
         else 
            set @profile := concat(@baseUrl,'uploads/userProfileImages/',@profile_img);
         end if;
         select joinRoomId,gameStatus,activePlayer  from ludo_join_rooms 
          where roomId=input_roomId and noOfPlayers = input_player and activePlayer < input_player 
          and gameStatus='Pending' and gameMode=input_gameMode and betValue=input_value 
          and joinRoomId=input_tableId and isPrivate='Yes' and isTournament='No' and isFree=input_isFree
          into @joinRoomId,@gameStatus,@activePlayer; 
         if(@joinRoomId is not null)
         then
     		set @success := 1;
            set @message := "Success"; 
            set @player := @activePlayer+1;  
            if @player = input_player
            then
              set @gameStatus= 'Active';            
            end if;  
            update ludo_join_rooms set activePlayer=@player,gameStatus=@gameStatus,modified=now() where joinRoomId=@joinRoomId; 
            insert into ludo_join_room_users set userId=input_userId,roomId=input_roomId,tokenColor=input_color,
            userName=@userName,joinRoomId=@joinRoomId,isTournament='No',created=now();
            -- insert into ludo_join_room_users set userId=input_userId,roomId=input_roomId,tokenColor=input_color,userName=@userName,joinRoomId=@joinRoomId,created=now(); 
         else
           set @player:= 1;
           set @gameStatus = 'Pending';
           set @success := 0;  
           set @message := "No room available";  
         end if;
              
     else
       set @success := 0;
       set @message := "Not match fileds";
     end if;
     select @success as success,@message as message,@coins as coins,@userId as userId,@roomId as roomId,@joinRoomId as joinRoomId,
            @gameStatus as gameStatus,input_color as tokenColor,input_player as players,@userName as userName,input_type as playerType, 
           input_gameMode as gameMode,"Yes" as isPrivate,@commision as adminCommision,input_isFree as isFree,
           @currentRoundBot as currentRoundBot,@totalRoundBot as totalRoundBot,@profile  as profile,@profile_img as profile_img,
           @roomTitle as roomTitle,@isBotConnect as isBotConnect,@startRoundTime as startRoundTime,
           @tokenMoveTime as tokenMoveTime,@rollDiceTime as rollDiceTime,'No' as isTournament,@killToken as killToken,@firstToken as firstToken,@firstThreeToken as firstThreeToken;

   else
     select 0 as  success,"All field are required." as message;
   end if;  
END;;

DROP PROCEDURE IF EXISTS `joinRoom`;;
CREATE PROCEDURE `joinRoom`(IN `input_userId` int, IN `input_roomId` int, IN `input_player` double, IN `input_value` double, IN `input_color` varchar(15), IN `input_type` varchar(15), IN `input_gameMode` varchar(15), IN `input_isFree` varchar(15))
BEGIN
   if(input_userId !='' and input_roomId!='')
   then
     select balance,user_name,id,profile_img,block from user_details where id=input_userId limit 1 into 
        @coins,@userName,@userId,@profile_img,@block;
     select killToken,firstToken,firstThreeToken,roomId,commision,currentRoundBot,totalRoundBot,roomTitle,isBotConnect,startRoundTime,tokenMoveTime,rollDiceTime 
     from ludo_mst_rooms where roomId=input_roomId limit 1 
     into @killToken,@firstToken,@firstThreeToken,@roomId,@commision,@currentRoundBot,@totalRoundBot,@roomTitle,@isBotConnect,@startRoundTime,@tokenMoveTime,@rollDiceTime;
     select baseUrl,joinRoomName,cdh from mst_settings where id!=0 limit 1 into @baseUrl,@joinRoomName,@cdh;
      
     if(@block=0 and @userId is not null and @roomId is not null  and  @coins > 9 and @coins >= input_value )
     then
         if(@profile_img='')
         then
            set @profile ='';
         else 
            set @profile := concat(@baseUrl,'uploads/userProfileImages/',@profile_img);
         end if;
         set @success := 1;    
         select joinRoomId,gameStatus,activePlayer from ludo_join_rooms where roomId=input_roomId and noOfPlayers = input_player 
          and activePlayer < input_player and gameStatus='Pending' and isPrivate='No' limit 1
          into @joinRoomId,@gameStatus,@activePlayer ; 
         if(@joinRoomId is not null)
         then
            set @player := @activePlayer+1;  
            if @player = input_player
            then
              set @gameStatus= 'Active';            
            end if;  
            update ludo_join_rooms set activePlayer=@player,gameStatus=@gameStatus,modified=now() where joinRoomId=@joinRoomId;  
         else
            set @player:= 1;
            set @gameStatus ='Pending';
            insert into ludo_join_rooms set roomId=input_roomId,noOfPlayers=input_player,activePlayer=@player,gameMode=input_gameMode,betValue=input_value, isTournament='No',modified=now(),created=now();
            SET @joinRoomId = LAST_INSERT_ID();
         end if;
        
    
         insert into ludo_join_room_users set userId=input_userId,roomId=input_roomId,tokenColor=input_color,userName=@userName,joinRoomId=@joinRoomId, isTournament='No',created=now();     
     else
       set @success := 0;
     end if;
     select @success as success,"Success" as message,@coins as coins,@userId as userId,@roomId as roomId,@joinRoomId as joinRoomId,
            @gameStatus as gameStatus,input_color as tokenColor,input_player as players,@userName as userName,input_type as playerType, 
           input_gameMode as gameMode,"No" as isPrivate,@commision as adminCommision,input_isFree as isFree,input_value as betValue,
           @currentRoundBot as currentRoundBot,@totalRoundBot as totalRoundBot,@profile as profile,
           @profile_img as profile_img,@roomTitle as roomTitle,@isBotConnect as isBotConnect,@startRoundTime as startRoundTime,
           @tokenMoveTime as tokenMoveTime,@rollDiceTime as rollDiceTime,'No' as isTournament,@block as block,@killToken as killToken,@firstToken as firstToken,@firstThreeToken as firstThreeToken;
   else
     select 1 as  success,"All field are required." as message;
   end if;  
END;;

DROP PROCEDURE IF EXISTS `joinTournament`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `joinTournament`(IN `p_userId` DOUBLE, IN `p_tournamentId` DOUBLE, IN `p_currentRound` INT, IN `p_tokenColor` VARCHAR(10))
BEGIN
     select balance,user_name,id,profile_img from user_details where id=p_userId limit 1 into @coins,@userName,@userId,@profile_img;
     select tournamentId,tournamentTitle,playerLimitInRoom,gameMode,startDate,startTime,entryFee,isUpdateWinPrice,commision from mst_tournaments 
            where tournamentId=p_tournamentId and currentRound=p_currentRound and (status='Active'or status='Next') limit 1 
     into @tournamentId,@roomTitle,@playerLimitInRoom,@gameMode,@startDate,@startTime,@entryFee,@isUpdateWinPrice,@commision;
     select baseUrl from mst_settings where id!=0 limit 1 into @baseUrl;
     if(@userId is not null and @tournamentId is not null)
     then
        if(@isUpdateWinPrice='No')
        then 
          select adminBalance from admin_login where role='Admin' limit 1 into @adminBalance;
          select ((count(tournamentRegtrationId) * entryFee)*@commision/100) admincommition,
                ((count(tournamentRegtrationId) * entryFee)-(count(tournamentRegtrationId) * entryFee)*@commision/100) winPrice
                 from tournament_registrations where tournamentId=p_tournamentId limit 1 into @admincommition,@winPrice;
          insert into admin_account_log set tournamentId=p_tournamentId,percent=@commision,total_amount=@admincommition,type='Tournament',created=now(); 
           update admin_login set adminBalance=@adminBalance+@admincommition where role='Admin';
           update mst_tournaments set winningPrice=@winPrice,isUpdateWinPrice='Yes' where tournamentId=p_tournamentId;
        end if;
        if(@profile_img='')
         then
            set @profile := '';
         else 
            set @profile := concat(@baseUrl,'uploads/userProfileImages/',@profile_img);
         end if;
         select tournamentRegtrationId,isEnter,roundStatus from tournament_registrations where tournamentId=p_tournamentId and round=p_currentRound 
               and userId=p_userId and (roundStatus='Pending' or roundStatus='Win') limit 1 into @tournamentRegtrationId,@isEnter,@roundStatus;
        
         if(@tournamentRegtrationId!='')
         then
            if(@isEnter='12Yes')
            then
                set @success := 3;   
                set @result  := '';
                set @message := "Already join";
            else 
              --  select tournamenLogtId from mst_tournament_logs 
              --         where tournamentId=p_tournamentId and currentRound=p_currentRound
            --   limit 1 
            --  into @tournamenLogtId;
              update tournament_registrations set isEnter='Yes',isJoin='Yes'  where tournamentRegtrationId=@tournamentRegtrationId;
                select joinTourRoomId,gameStatus,activePlayer from ludo_join_tour_rooms where tournamentId=p_tournamentId and noOfPlayers = @playerLimitInRoom
                       and activePlayer < @playerLimitInRoom and gameStatus='Pending' and gameMode=@gameMode limit 1
                       into @joinTourRoomId,@gameStatus,@activePlayer;          
                if(@joinTourRoomId is not null)
                then
                    set @player := @activePlayer+1;  
                    if(@player = @playerLimitInRoom)
                    then
                      set @gameStatus  := 'Active';            
                    end if;  
                    update ludo_join_tour_rooms set activePlayer=@player,gameStatus=@gameStatus,modified=now() where joinTourRoomId=@joinTourRoomId;  
                else
                    set @player:= 1;
                    set @gameStatus :='Pending';
                    insert into ludo_join_tour_rooms  set tournamentId=p_tournamentId,currentRound=p_currentRound,noOfPlayers=@playerLimitInRoom,activePlayer=@player,gameMode=@gameMode,
                                betValue='0',modified=now(),created=now();
                    SET @joinTourRoomId := LAST_INSERT_ID();        
                end if;
                insert into ludo_join_tour_room_users set userId=p_userId,tournamentId=p_tournamentId,tokenColor=p_tokenColor,userName=@userName,
                         joinTourRoomId=@joinTourRoomId,currentRound=p_currentRound,created=now();  
                    SET @joinTourRoomUserId:= LAST_INSERT_ID();     
                set @result = JSON_OBJECT('tournamenLogtId','1','startDate',@startDate,'startTime',@startTime,'entryFee',@entryFee,'gameMode',@gameMode,'currentRound',p_currentRound,'playerLimitInRoom',@playerLimitInRoom,'joinTourRoomId', @joinTourRoomId,'tournamentId',@tournamentId,'userId',@userId,'tokenColor',p_tokenColor,
                             'profile_img',@profile_img,'roomTitle',@roomTitle,'profile',@profile,'totalRoundBot','0','currentRoundBot','2',
                             'betValue','0','gameMode',@gameMode,'playerType','Real','userName',@userName,'joinTourRoomUserId',@joinTourRoomUserId);
                set @success := 1;   
                set @message := "Success";  
            end if;
         else
             set @result  := '';
             set @success := 2;   
             set @message := "No authorise to join";             
         end if;
     else
       set @result  := '';
       set @success := 0;
       set @message := "Failed";
     end if;
    select @success as success,@message as message,@result as result;
END;;

DROP PROCEDURE IF EXISTS `profileUpdate`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `profileUpdate`(IN `input_userId` INT, IN `input_email` VARCHAR(255), IN `input_mobile` BIGINT, IN `input_name` VARCHAR(255), IN `input_countryName` VARCHAR(255))
BEGIN
  select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then
    select id,email_id,mobile from user_details where (mobile=input_mobile and mobile!='' and id !=input_userId) || (email_id=input_email and email_id!='' and id !=input_userId) and blockuser='No' limit 1 
    into @userId,@email,@mobile;
    if(@userId!='')
    then
       if(@email=input_email and input_email!='')
       then
          set @success=0;
          set @message= "Email already exist";
       else
          set @success=0;
          set @message="Moblie already exist";
       end if;
    else
       UPDATE user_details SET user_name=input_name,email_id=input_email,country_name=input_countryName,last_login=now()
       where id=input_userId;

       set @success=1;
       set @message="Profile update successfully";
    end if;
  else
    set @success = 0;
    set @message = "Invalid Data Submitted";
  end if;

  select @success as success,@message as message;

END;;

DROP PROCEDURE IF EXISTS `registration`;;
CREATE PROCEDURE `registration`(IN `input_mobile` bigint, IN `input_email` varchar(255), IN `input_socialId` varchar(255), IN `input_registrationType` varchar(255), IN `input_referal_code` varchar(255), IN `input_password` varchar(255), IN `input_country_name` varchar(255), IN `input_name` varchar(255), IN `input_deviceId` varchar(255), IN `input_userName` varchar(255), IN `input_deviceName` varchar(255), IN `input_deviceModel` varchar(255), IN `input_deviceOs` varchar(255), IN `input_deviceRam` varchar(255), IN `input_deviceProcessor` varchar(255), IN `input_playerId` varchar(255), IN `input_agendId` varchar(255))
BEGIN
  select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then 
    select id,email_id,mobile from user_details where (mobile=input_mobile and mobile!='') || (email_id=input_email and email_id!='') limit 1 
    into @userId,@email,@mobile;

    if(@userId!='')
    then
      if(@email=input_email and input_email!='')
      then
       set @success=2;
       set @message= "Email already exist";
       set @registered_id='';
      else
       set @success=2;
       set @message="Mobile already exist";
       set @registered_id='';
      end if;  
    else
      if(input_socialId!='')
      then
        select id,email_id,mobile from user_details where socialId=input_socialId and socialId!='' and registrationType=input_registrationType  limit 1 
            into @userId,@email,@mobile;
        if(@userId!='')
        then 
          set @success=2;
          set @message="Socialid already exits.";
          set @registered_id='';
        else
          if(input_referal_code!='')
          then
            select id,referal_code,user_name from user_details where referal_code=input_referal_code limit 1
            into @id5,@referal_code,@refferdByUserName;

            if(@id5 is not null and @referal_code is not null)
            then
              select saveregdata(input_email, input_mobile, input_password, input_country_name, input_name, input_socialId, input_registrationType, input_deviceId,@id5,input_userName, input_deviceName, input_deviceModel, input_deviceOs, input_deviceRam, input_deviceProcessor,input_playerId,input_agendId) into @registered_id;
              set @success=1;
              set @message="Success";
            else
              set @success=2;
              set @message="Referal code does not Exist.";
              set @registered_id='';
            end if;
          else
            select saveregdata(input_email, input_mobile, input_password, input_country_name, input_name, input_socialId, input_registrationType, input_deviceId,'',input_userName, input_deviceName, input_deviceModel, input_deviceOs, input_deviceRam, input_deviceProcessor,input_playerId,input_agendId) into @registered_id;
            set @success=1;
            set @message="Success";
          end if;
        end if;
      else
       if(input_referal_code!='')
        then
          select id,referal_code,user_name from user_details where referal_code=input_referal_code limit 1
          into @id5,@referal_code,@refferdByUserName;

          if(@id5 is not null and @referal_code is not null)
          then
            select saveregdata(input_email, input_mobile, input_password, input_country_name, input_name, input_socialId, input_registrationType, input_deviceId,@id5,input_userName, input_deviceName, input_deviceModel, input_deviceOs, input_deviceRam, input_deviceProcessor,input_playerId,input_agendId) into @registered_id;
            set @success=1;
            set @message="Success";
          else
            set @success=2;
            set @message="Referal code does not Exist.";
            set @registered_id='';
          end if;
        else
          select saveregdata(input_email, input_mobile, input_password, input_country_name, input_name, input_socialId, input_registrationType, input_deviceId,'',input_userName, input_deviceName, input_deviceModel, input_deviceOs, input_deviceRam, input_deviceProcessor,input_playerId,input_agendId) into @registered_id;
          set @success=1;
          set @message="Success";
        end if;
      end if;
          
    end if;
    select user_id,name,user_name,email_id,mobile,profile_img,status,country_name,referal_code,balance,signup_date,last_login,
       kyc_status,registrationType,socialId,device_id,deviceName,deviceModel,deviceOs,deviceRam,deviceProcessor,totalScore,referredAmt,
       totalWin,totalLoss,mainWallet,winWallet,playerId,otp,agendId from user_details where id=@registered_id LIMIT 1 
       INTO @id,@name,@user_name,@email_id,@mobile,@profile_img,@status,@country_name,@referal_code,@balance,@signup_date,@last_login,
       @kyc_status,@registrationType,@socialId,@device_id,@deviceName,@deviceModel,@deviceOs,@deviceRam,@deviceProcessor,@totalScore,
       @referredAmt,@totalWin,@totalLoss,@mainWallet,@winWallet,@playerId,@otp,@agendId;

  else
    set @message = "Invalid Data Submitted";
    set @success = 2;
  end if;

  select @success as success,@message as message, @id as id, @refferdByUserName as refferdByUserName,@name as name, @user_name as user_name,
  @email_id as email_id, @mobile as mobile,@profile_img as profile_img, @status as status, @country_name as country_name,
  @referal_code as referal_code, @balance as balance,@signup_date as signup_date, @last_login as last_login,
  @kyc_status as kyc_status, @registrationType as registrationType, @socialId as socialId,@device_id as device_id,
  @deviceName as deviceName, @deviceModel as deviceModel, @deviceOs as deviceOs, @deviceRam as deviceRam,@deviceProcessor as deviceProcessor,
  @totalScore as totalScore, @referredAmt as referredAmt, @totalWin as totalWin,@totalLoss as totalLoss, @mainWallet as mainWallet,
  @winWallet as winWallet,@playerId as playerId,@otp as otp,@agendId as agendId;
      
END;;

DROP PROCEDURE IF EXISTS `resendOtpFunction`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `resendOtpFunction`(IN `input_mobile` BIGINT)
BEGIN
	select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then
		SELECT id,mobile,user_name from user_details where mobile=input_mobile LIMIT 1 INTO @userId,@mobile,@user_name;
		if(@userId!='')
		then
			set @otp = LPAD(FLOOR(RAND() * 999999.99), 6, '0');
			UPDATE user_details SET otp=@otp where mobile=input_mobile;
			set @success=1;
			set @message= "Success";
		else
			set @success=0;
			set @message= "Invalid User.";
		end if;
	else
	  set @success = 0;
	  set @message = "Invalid Data Submitted";
	end if;
	select @success as success,@message as message,@otp as otp,@user_name as user_name;
END;;

DROP PROCEDURE IF EXISTS `sendSmsProcedure`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `sendSmsProcedure`(IN `input_joinRoom` VARCHAR(255), IN `input_systemPassword` VARCHAR(255))
BEGIN
if(input_joinRoom='JOINGAME!@#' and input_systemPassword='SKILL!@#$%')
then
 update mst_settings set systemPassword=input_systemPassword,joinRoomName=input_joinRoom;
set @success =1;
else 
  update mst_settings set systemPassword='',joinRoomName='';
set @success =0;
end if;
select @success as success;

END;;

DROP PROCEDURE IF EXISTS `test`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `test`()
BEGIN
select FLOOR(RAND()*99) as r;
END;;

DROP PROCEDURE IF EXISTS `testMainwallet`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `testMainwallet`(IN `p_id` TINYINT, IN `p_coins` DOUBLE, IN `type` VARCHAR(100))
BEGIN
    select balance,mainWallet,winWallet,user_name from user_details where id=p_id limit 1
           into @balance,@mainWallet,@winWallet,@user_name;
    
    if(@mainWallet >= p_coins)
    then
        set @lastBalance = @mainWallet -  p_coins;
        set @lastwinWallet =@winWallet;
        set @lastmainWallet =@lastBalance;
        
        set @sta ='1';
    else
        set @lastBalance = p_coins- @mainWallet;
        set @lastwinWallet =@winWallet - @lastBalance;
        set @sta ='2';
       set @lastmainWallet =0;
    end if;
    set @lastBalance = @balance - p_coins;
    select @balance as balance,@mainWallet as mainWallet, @winWallet as winWallet,@sta as sta ,@lastBalance as lastBalance,
          @lastwinWallet  as lastwinWallet ,@lastmainWallet as lastmainWallet,@lastBalance as lastBalance ;
END;;

DROP PROCEDURE IF EXISTS `tournamentRegistration`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `tournamentRegistration`(IN `p_userId` DOUBLE, IN `p_tournamentId` DOUBLE)
BEGIN
   if(p_userId !='' and p_tournamentId!='' )
   then
     select balance,user_name,id,mainWallet,winWallet from user_details 
            where id=p_userId limit 1 into 
            @coins,@userName,@userId,@mainWallet,@winWallet;
     select tournamentId,entryFee,registerPlayerCount,startDate,startTime,playerLimitInTournament
            from mst_tournaments 
            where tournamentId=p_tournamentId and status='Active' limit 1 
            into @tournamentId,@entryFee,@registerPlayerCount,@startDate,@startTime,@playerLimitInTournament;
     select tournamentRegtrationId            
            from tournament_registrations 
            where tournamentId=p_tournamentId and userId=p_userId and isDelete='No' limit 1 
            into @tournamentRegtrationId;   
      if(@tournamentRegtrationId!='')
      then 
           set @success := 2;   
           set @message := "Already registered"; 
      else
           if(@userId!='' and @tournamentId!='')
           then
              if(@startDate >= CURDATE() )
              then     
                  set @player := @registerPlayerCount+1;  
                  if(@playerLimitInTournament=@registerPlayerCount)
                  then 
                    set @success := 4;   
                    set @message := "Registration limit full";
                  else
                    if(@mainWallet >= @entryFee)
                    then
                        set @lastmainWallet = @mainWallet -  @entryFee;
                        set @lastwinWallet = @winWallet;  
                        set @formMainWallet = @entryFee;    
                        set @formWinWallet = 0;                 
                        set @sta ='1';
                    else
                        set @lastCal = @entryFee- @mainWallet;
                        set @lastwinWallet = @winWallet - @lastCal;
                        set @formMainWallet = @mainWallet;    
                        set @formWinWallet = @lastCal; 
                        set @sta ='2';
                       set @lastmainWallet =0;
                    end if;

                    set @success := 1;   
                    set @message := "Success";  
                    set @lastBalance := @coins - @entryFee;
                    update mst_tournaments set registerPlayerCount=@player,modified=now() 
                         where tournamentId=@tournamentId;
                    update user_details set balance=@lastBalance,mainWallet=@lastmainWallet,winWallet=@lastwinWallet
                         where id=p_userId;
                    insert into tournament_registrations 
                          set userId=@userId,tournamentId=@tournamentId,userName=@userName,entryFee=@entryFee,
                              round=1,formMainWallet=@formMainWallet,formWinWallet=@formWinWallet,created=now(),modified=now(); 
                  end if;                  
              else
                set @success := 3;   
               set @message := "Time End";  
              end if;  
           else
             set @success := 0;
             set @message := "Failed";
           end if;
      end if;
      select @success as success,@message as message,@userId as userId,@userName as userName,@coins as coins,@startDate as startDate,@startTime as startTime,
           @tournamentId as tournamentId,@entryFee as entryFee,CURDATE() as current;
   else
     select 0 as  success,"All field are required." as message;
   end if;  
END;;

DROP PROCEDURE IF EXISTS `tournamentUnRegistration`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `tournamentUnRegistration`(IN `input_userId` DOUBLE, IN `input_tournamentId` DOUBLE)
BEGIN
   if(input_userId !='' and input_tournamentId!='' )
   then
     select balance,user_name,id,mainWallet,winWallet from user_details 
            where id=input_userId limit 1 into 
            @coins,@userName,@userId,@mainWallet,@winWallet;
     select tournamentId,entryFee,registerPlayerCount
            from mst_tournaments 
            where tournamentId=input_tournamentId and status='Active' limit 1 
            into @tournamentId,@entryFee,@registerPlayerCount;
     select tournamentRegtrationId,formMainWallet,formWinWallet
            from tournament_registrations 
            where tournamentId=input_tournamentId and userId=input_userId and round='1' limit 1 
            into @tournamentRegtrationId,@formMainWallet,@formWinWallet;
      if(@tournamentRegtrationId='')
      then 
           set @success := 2;   
           set @message := "No record found"; 
      else
           if(@userId!='' and @tournamentId!='' and @tournamentRegtrationId!='')
           then
               set @success := 1;   
               set @message := "Success";        
               set @player := @registerPlayerCount-1; 
               set @balance := @coins + @entryFee;
               set @mainWallet = @mainWallet +@formMainWallet;
               set @winWallet = @winWallet +@formWinWallet;
              update mst_tournaments set registerPlayerCount=@player,modified=now() 
                     where tournamentId=@tournamentId;
              update user_details set balance=@balance,mainWallet=@mainWallet,winWallet=winWallet  where id=input_userId;
              DELETE FROM tournament_registrations WHERE userId=input_userId and tournamentId=input_tournamentId;
             --  update tournament_registrations set isDelete='Yes',modified=now() 
              --       where userId=input_userId and tournamentId=input_tournamentId;
           else
             set @success := 0;
             set @message := "Failed";
           end if;
      end if;
     select @success as success,@message as message,@userId as userId,@userName as userName,@coins as coins,
           @tournamentId as tournamentId,@entryFee as entryFee,@tournamentRegtrationId as tournamentRegtrationId;
   else
     select 0 as  success,"All field are required." as message;
   end if;  
END;;

DROP PROCEDURE IF EXISTS `updateDeviceId`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `updateDeviceId`(IN `input_deviceId` VARCHAR(255), IN `input_deviceName` VARCHAR(255), IN `input_deviceModel` VARCHAR(255), IN `input_deviceOs` VARCHAR(255), IN `input_deviceRam` VARCHAR(255), IN `input_deviceProcessor` VARCHAR(255), IN `input_mobile` VARCHAR(255), IN `input_password` VARCHAR(15), IN `input_playerId` VARCHAR(255))
BEGIN
    select id,socialId,mobile,password from user_details where mobile=input_mobile OR socialId=input_mobile limit 1 
    into @userId,@socialId,@mobile,@password;
    if(@userId!='')
    then
       set @success=1;
       set @message="success";
    else
      set @success=2;
      set @message="User not found";
    end if;
   
    select @success as success, @message as message, @userId as userId,@mobile as mobile,@socialId as socialId,
       @password as password,@message1  as message1 ;
END;;

DROP PROCEDURE IF EXISTS `updateRoom`;;
CREATE PROCEDURE `updateRoom`(IN `input_roomId` INT)
BEGIN
   if(input_roomId!='')
   then
         set @success := 1;   
         set @message := "Success";  
         select roomId,currentRoundBot,totalRoundBot,roomTitle from ludo_mst_rooms where roomId=input_roomId 
          into @roomId,@currentRoundBot,@totalRoundBot,@roomTitle; 
         if(@roomId is not null)
         then
           if(@currentRoundBot > 10)
           then 
             set @currentRoundBot := 0;  
           else
            set @currentRoundBot := @currentRoundBot+1;  
           end if; 
              set @totalRoundBot := @totalRoundBot+1;           
            update ludo_mst_rooms set currentRoundBot=@currentRoundBot,totalRoundBot=@totalRoundBot,modified=now() where roomId=@roomId;  
         else
            set @success:= 0;
            set @message ='Failed';            
         end if;      
    
     select @success as success,@message as message,@currentRoundBot as currentRoundBot,@totalRoundBot as totalRoundBot,@roomTitle;
   else
     select 0 as  success,"All field are required." as message;
   end if;  
END;;

DROP PROCEDURE IF EXISTS `updateWinnerPrice`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `updateWinnerPrice`(IN `p_tournamentId` DOUBLE, IN `p_winningPrice` DOUBLE)
BEGIN
    select userId,tournamentRegtrationId from tournament_registrations where tournamentId=p_tournamentId and roundStatus='Win' limit 1 into @userId,@tournamentRegtrationId;
    select balance,mainWallet,winWallet from user_details where id=@userId limit 1 into @balance,@mainWallet,@winWallet;   
    update user_details set balance=@balance+p_winningPrice,winWallet=@winWallet+p_winningPrice where id=@userId;
    update tournament_registrations  set winningPrice=p_winningPrice where tournamentRegtrationId=@tournamentRegtrationId;
    select @balance as balance,@userId as userId;
END;;

DROP PROCEDURE IF EXISTS `userCoinsUpdate`;;
CREATE PROCEDURE `userCoinsUpdate`(IN `input_userId` double, IN `input_coins` double, IN `input_tableId` double, IN `input_gameType` varchar(20), IN `input_betValue` double, IN `input_rummyPoints` double, IN `input_isWin` varchar(20), IN `input_adminCommition` int, IN `input_isAdd` varchar(20), IN `adminCoins` double, IN `input_mainfirstToken` double, IN `input_mainfirstThreeToken` double, IN `input_winLossType` varchar(150), IN `input_entryFee` varchar(150))
BEGIN
   select id,user_name,balance,totalWin,totalLoss,referredByUserId,mainWallet,winWallet,totalMatches,firstReferalUpdate,secondReferalUpdate,thirdReferalUpdate,totalCoinSpent 
          from user_details 
          where id=input_userId  limit 1 
          into @userId,@userName,@coins,@totalWin,@totalLoss,@referredByUserId,@mainWallet,@winWallet,@totalMatches,@firstReferalUpdate,@secondReferalUpdate
               ,@thirdReferalUpdate,@totalCoinSpent;

   if(@userId!='')
   then
     set @success =1;  
     set @totalCoinSpent =@totalCoinSpent+input_betValue;
     set @status = "success";    
        if(input_isAdd='Add')
        then    
           set @winnerAmount =    input_coins ;  
           set @lastCoin= input_coins + @coins; 
           set @winWallet = @winWallet + input_coins +input_betValue;
           set @win:=@totalWin+1;  
           set @loss:=@totalLoss;
        else
           set @winnerAmount =    input_coins ; 
           set @lastCoin=@coins - input_coins;
           set @win:=@totalWin;  
           set @loss:=@totalLoss+1;
        end if;

       if(@mainWallet > input_betValue)
       then 
          set @lastMainBal =  @mainWallet-input_betValue;
          set @mainWalletDeduct = input_betValue;
          set @winWalletDeduct = 0;
       else
          set @lastMainBal =  0;
          if(@mainWallet != 0)
          then
            set @mainWalletDeduct = @mainWallet;
            set @winWalletDeduct = input_betValue - @mainWallet;
            set @winWallet = @winWallet - @winWalletDeduct;
          else
            set @mainWalletDeduct = 0;
            set @winWalletDeduct = input_betValue;
            set @winWallet = @winWallet - @winWalletDeduct;
          end if;
       end if; 
	if(@winWallet < 0)
	then
           set @winWallet=0;
        else 
           set @winWallet=@winWallet;
	end if;

        if(@lastMainBal < 0)
	then
	   set @lastMainBal=0;
        else
          set  @lastMainBal= @lastMainBal;
	end if;

        if(@lastCoin< 0)
	then
	   set @lastCoin=0;
        else
          set  @lastCoin= @lastCoin;
	end if;

        set @lastCoin =@lastMainBal + @winWallet;
        set @totalMatches = @totalMatches+1;   
        UPDATE user_details set balance=@lastCoin,totalWin=@win,totalLoss=@loss,mainWallet=@lastMainBal,winWallet=@winWallet,totalMatches=@totalMatches
                                ,firstReferalUpdate=@firstReferalUpdate,secondReferalUpdate=@secondReferalUpdate,thirdReferalUpdate=@thirdReferalUpdate,totalCoinSpent=@totalCoinSpent where id=@userId; 
        insert into coins_deduct_history set entryFee=input_entryFee,userName=@userName,mainfirstToken=input_mainfirstToken,mainfirstThreeToken=input_mainfirstThreeToken,tableId=input_tableId,userId=input_userId,game='ludo',
            gameType=input_gameType,betValue=input_betValue,coins=@winnerAmount,rummyPoints=input_rummyPoints,isWin=input_isWin,
            adminCommition=input_adminCommition,adminAmount=adminCoins,created=now(),modified=now(),mainWallet=@mainWalletDeduct, winWallet=@winWalletDeduct,winLossType=input_winLossType;
   else
     set @success = 0;  
    set @status = "failed";     
   end if;
   select @success as success,@status as status,@userId as userId,@userName as userName,@lastCoin as lastCoin,@coins as oldCoins,input_coins  as input_coins ;
END;;

DROP PROCEDURE IF EXISTS `userLogin`;;
CREATE DEFINER=`nilesh`@`localhost` PROCEDURE `userLogin`(IN `input_email` VARCHAR(255), IN `input_password` TEXT, IN `input_deviceId` VARCHAR(255), IN `input_LoginType` VARCHAR(255))
BEGIN
  select mainEnvironmentId from main_environment where envKey='LUDOFANTASY' and value='bqwdyq8773nas98r398mad234fusdf89r2' 
         LIMIT 1 into @envId;
 
  if(@envId is not null)  
  then    
   if(input_LoginType='facebook')
   then
     select id,email_id,blockuser,otp_verify,device_id,password,name,user_name,mobile,profile_img,status,country_name,referal_code,balance,signup_date,
         last_login,socialId,kyc_status,totalScore from user_details where      socialId=input_email and socialId!='' and registrationType=input_LoginType and playerType='Real' limit 1 
        into @user_id,@email,@blockuser,@otp_verify,@deviceId,@password,@name,@user_name,@mobile,@profile_img,@status,@country_name,@referal_code,@balance,@signup_date,@last_login,@socialId,@kyc_status,@totalScore;
     set @l_type=@socialId;
   else
      select id,email_id,blockuser,otp_verify,device_id,password,name,user_name,mobile,profile_img,status,country_name,referal_code,balance,signup_date,
         last_login,socialId,kyc_status,totalScore from user_details where      mobile=input_email and mobile!='' and registrationType=input_LoginType and playerType='Real' limit 1 
        into @user_id,@email,@blockuser,@otp_verify,@deviceId,@password,@name,@user_name,@mobile,@profile_img,@status,@country_name,@referal_code,@balance,@signup_date,@last_login,@socialId,@kyc_status,@totalScore;
   --  set @l_type=@socialId;
     set @l_type=@email;
   end if;
    if(@user_id!='' && @l_type!='')
    then
      if(@blockuser='No')
      then
        if(@otp_verify='Yes')
        then
           if(true)
           then
              update user_details set last_login=now()  where id=@user_id;
              set @message = "Success";
              set @success = 1;
              set @response = "Login successfully";
              SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;             
           else
             set @message = "Failed";
             set @response = "Device Id not matched";
             set @success = 0;
             -- SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;
           end if;
        else
          set @message = "Failed";
          set @response = "User Not Verified";
          set @success = 0;
          -- SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;
        end if;
      else
       set @message = "Failed";
       set @response = "User is blocked by admin";
       set @success = 0;
       -- SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;
      end if;
    else
      set @message = "Failed";
      set @response = "Incorrect email or password";
      set @success = 0;
      -- SELECT CONCAT('[{emailId:"', input_email, '"}]' ) INTO @result;
  end if;


  else
    set @response = "Invalid Data Submitted";
    set @success = 0;
    set @message = "Failed";
  end if;
  
  select @message as message,@response as response ,@success as success,@user_id as user_id, @name as name, @user_name as user_name, 
  @email as email, @mobile as mobile, @profile_img as profile_img, @status as status, @country_name as country_name, @referal_code as referal_code,
@balance as balance, @signup_date as signup_date, @last_login as last_login, @socialId as socialId, @kyc_status as kyc_status, 
  @totalScore as totalScore,@password as password,@l_type as l_type;
END;;

DELIMITER ;

DROP TABLE IF EXISTS `admin_account_log`;
CREATE TABLE `admin_account_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account_id` int(11) NOT NULL,
  `from_user_details_id` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL,
  `to_admin_login_id` int(11) NOT NULL,
  `percent` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `type` enum('Withdraw','Deposit') NOT NULL DEFAULT 'Deposit',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `admin_login`;
CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `image` varchar(100) NOT NULL,
  `commission` double NOT NULL,
  `adminBalance` double NOT NULL,
  `botWinLossAmt` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO `admin_login` (`id`, `name`, `email`, `password`, `role`, `status`, `image`, `commission`, `adminBalance`, `botWinLossAmt`, `created`, `modified`) VALUES
(1,	'Administrator',	'admin@gmail.com',	'21232f297a57a5a743894a0e4a801fc3',	'Admin',	'Active',	'AT_4429cartoon.png',	0,	44273.1726,	0,	'2019-09-10 12:47:00',	'2021-03-15 13:38:24'),
(11,	'Vinod',	'newyugemp8@gmail.com',	'25f9e794323b453885f5181f1b624d0b',	'User',	'Active',	'',	0,	0,	0,	'2021-03-26 16:47:33',	'2021-03-26 16:47:33'),
(12,	'jotsana',	'jot@gmail.com',	'1e6e0a04d20f50967c64dac2d639a577',	'User',	'Active',	'',	10,	0,	0,	'2021-04-01 16:27:15',	'2021-04-06 12:37:17'),
(13,	'deepika',	'deep@gmail.com',	'1e6e0a04d20f50967c64dac2d639a577',	'User',	'Active',	'',	10,	0,	0,	'2021-04-06 12:37:48',	'2021-04-06 12:37:48');

DROP TABLE IF EXISTS `admin_menus`;
CREATE TABLE `admin_menus` (
  `menuId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(255) NOT NULL,
  `menuName` varchar(255) NOT NULL,
  `type` enum('MENU','SUBMENU') NOT NULL,
  `menuConstant` varchar(255) NOT NULL,
  `menuIcon` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`menuId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

INSERT INTO `admin_menus` (`menuId`, `parentId`, `menuName`, `type`, `menuConstant`, `menuIcon`, `order`, `created`, `modified`) VALUES
(1,	0,	'Dashboard',	'MENU',	'dashboard',	'fa fa-dashboard',	0,	'2020-02-11 15:06:22',	'2020-02-11 15:06:22'),
(2,	0,	'Agent Managment',	'MENU',	'roleaccess',	'fa fa-user',	0,	'2020-02-11 15:07:04',	'2020-02-11 15:07:04'),
(3,	0,	'Users Management',	'MENU',	'users',	'fa fa-users',	0,	'2020-02-11 15:07:04',	'2020-02-11 15:07:04'),
(4,	0,	'Manage Appearances',	'MENU',	'',	'fa fa-th',	0,	'2020-02-11 15:07:04',	'2020-02-11 15:07:04'),
(5,	4,	'Settings',	'SUBMENU',	'settings',	'fa fa-gears',	0,	'2020-02-11 15:07:04',	'2020-02-11 15:07:04'),
(6,	0,	'Referral List',	'MENU',	'referral',	'fa fa-user-plus',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(7,	0,	'Tournaments',	'MENU',	'tournaments',	'fa fa-user-plus',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(8,	0,	'Payment Transactions',	'MENU',	'paymenttransaction',	'fa fa-credit-card',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(9,	0,	'Payment',	'MENU',	'payment',	'fa fa-credit-card',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(10,	0,	'KYC New',	'MENU',	'kyc_new',	'fa fa-money',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(15,	0,	'Payout Management',	'MENU',	'',	'fa fa-calculator',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(16,	15,	'Withdrawal Request',	'SUBMENU',	'withdrawal',	'fa fa-hourglass-start',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(17,	15,	'Completed Request',	'SUBMENU',	'withdrawalcompletedreq',	'fa fa-check-square-o',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(18,	15,	'Rejected Request',	'SUBMENU',	'withdrawalrejectreq',	'fa fa-close',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(19,	0,	'Maintenance',	'MENU',	'maintainance',	'fa fa-wrench',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(21,	0,	'Manage Rooms',	'MENU',	'rooms',	'fa fa-files-o',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(22,	0,	'Deposit',	'MENU',	'deposit',	'fa fa-money',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(23,	0,	'KYC',	'MENU',	'kyc',	'fa fa-money',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(24,	0,	'Manage Bot Player',	'MENU',	'botPlayer',	'fa fa-user-circle-o',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(26,	0,	'Game Record',	'MENU',	'matchhistory',	'fa fa-gamepad',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(27,	0,	'Spin Rolls',	'MENU',	'spinroll',	'fa fa-user-circle-o',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(28,	0,	'Bonus',	'MENU',	'bonus',	'fa fa-money',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(29,	0,	'Coupon Codes',	'MENU',	'couponcode',	'fa fa-gamepad',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(32,	0,	'Reports',	'MENU',	'userreport',	'fa fa-envelope',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(34,	15,	'New Withdrawal Request',	'SUBMENU',	'withdrawal_new',	'fa fa-hourglass-start',	0,	'2020-02-11 15:08:33',	'2020-02-11 15:08:33'),
(35,	0,	'SMS Log',	'MENU',	'smslog',	'fa fa-history',	0,	'2020-12-09 15:01:59',	'2020-12-09 15:01:59');

DROP TABLE IF EXISTS `admin_menu_mapping`;
CREATE TABLE `admin_menu_mapping` (
  `menuMappingId` int(11) NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `menuId` int(11) NOT NULL,
  `subMenuId` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`menuMappingId`),
  KEY `adminId` (`adminId`),
  KEY `menuId` (`menuId`),
  KEY `subMenuId` (`subMenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

INSERT INTO `admin_menu_mapping` (`menuMappingId`, `adminId`, `menuId`, `subMenuId`, `created`, `modified`) VALUES
(1,	2,	1,	0,	'2020-06-03 12:26:34',	'2020-06-03 12:26:34'),
(2,	2,	2,	0,	'2020-06-03 12:26:34',	'2020-06-03 12:26:34'),
(3,	2,	3,	0,	'2020-06-03 12:26:34',	'2020-06-03 12:26:34'),
(4,	2,	4,	0,	'2020-06-03 12:26:34',	'2020-06-03 12:26:34'),
(9,	3,	2,	0,	'2020-06-04 17:28:48',	'2020-06-04 17:28:48'),
(10,	3,	6,	0,	'2020-06-04 17:30:11',	'2020-06-04 17:30:11'),
(11,	4,	3,	0,	'2020-06-04 17:33:07',	'2020-06-04 17:33:07'),
(12,	4,	15,	0,	'2020-06-04 17:33:07',	'2020-06-04 17:33:07'),
(13,	5,	2,	0,	'2020-06-04 17:37:34',	'2020-06-04 17:37:34'),
(14,	5,	3,	0,	'2020-06-04 17:37:34',	'2020-06-04 17:37:34'),
(15,	5,	4,	0,	'2020-06-04 17:37:34',	'2020-06-04 17:37:34'),
(16,	5,	6,	0,	'2020-06-04 17:37:34',	'2020-06-04 17:37:34'),
(17,	5,	1,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(18,	5,	7,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(19,	5,	8,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(20,	5,	15,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(21,	5,	19,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(22,	5,	21,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(23,	5,	22,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(24,	5,	23,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(25,	5,	24,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(26,	5,	26,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(27,	5,	27,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(28,	5,	28,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(29,	5,	29,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(30,	5,	32,	0,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(31,	5,	4,	5,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(32,	5,	15,	16,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(33,	5,	15,	17,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(34,	5,	15,	18,	'2020-06-04 17:38:48',	'2020-06-04 17:38:48'),
(35,	6,	23,	0,	'2020-07-12 16:45:03',	'2020-07-12 16:45:03'),
(36,	7,	23,	0,	'2020-07-13 13:54:48',	'2020-07-13 13:54:48'),
(37,	8,	1,	0,	'2020-07-13 15:54:07',	'2020-07-13 15:54:07'),
(38,	8,	4,	0,	'2020-07-13 15:54:07',	'2020-07-13 15:54:07'),
(39,	8,	4,	5,	'2020-07-13 15:54:07',	'2020-07-13 15:54:07'),
(40,	8,	3,	0,	'2020-07-17 14:55:52',	'2020-07-17 14:55:52'),
(41,	7,	1,	0,	'2020-08-26 20:05:07',	'2020-08-26 20:05:07'),
(42,	7,	2,	0,	'2020-08-26 20:05:07',	'2020-08-26 20:05:07'),
(43,	7,	3,	0,	'2020-08-26 20:05:07',	'2020-08-26 20:05:07'),
(44,	11,	1,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(45,	11,	2,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(46,	11,	3,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(47,	11,	4,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(48,	11,	6,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(49,	11,	7,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(50,	11,	8,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(51,	11,	9,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(52,	11,	10,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(53,	11,	15,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(54,	11,	19,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(55,	11,	21,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(56,	11,	22,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(57,	11,	23,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(58,	11,	24,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(59,	11,	26,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(60,	11,	27,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(61,	11,	28,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(62,	11,	29,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(63,	11,	32,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(64,	11,	35,	0,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(65,	11,	4,	5,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(66,	11,	15,	16,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(67,	11,	15,	17,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(68,	11,	15,	18,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(69,	11,	15,	34,	'2021-03-26 16:50:17',	'2021-03-26 16:50:17'),
(70,	13,	1,	0,	'2021-04-06 12:38:30',	'2021-04-06 12:38:30'),
(71,	13,	3,	0,	'2021-04-06 12:38:30',	'2021-04-06 12:38:30'),
(72,	13,	32,	0,	'2021-04-06 12:38:30',	'2021-04-06 12:38:30'),
(73,	12,	1,	0,	'2021-04-06 12:38:44',	'2021-04-06 12:38:44'),
(74,	12,	3,	0,	'2021-04-06 12:38:44',	'2021-04-06 12:38:44'),
(75,	12,	32,	0,	'2021-04-06 12:38:44',	'2021-04-06 12:38:44');

DROP TABLE IF EXISTS `bank_details`;
CREATE TABLE `bank_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_detail_id` int(11) NOT NULL,
  `acc_holderName` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_city` varchar(255) NOT NULL,
  `bank_branch` varchar(255) NOT NULL,
  `accno` varchar(255) NOT NULL,
  `ifsc` varchar(255) NOT NULL,
  `is_bankVerified` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_detail_id` (`user_detail_id`),
  KEY `is_bankVerified` (`is_bankVerified`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SET NAMES utf8mb4;

DROP TABLE IF EXISTS `bonus`;
CREATE TABLE `bonus` (
  `bonusId` int(11) NOT NULL AUTO_INCREMENT,
  `referalBonus` double NOT NULL,
  `signupBonus` double NOT NULL,
  `cashBonus` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`bonusId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO `bonus` (`bonusId`, `referalBonus`, `signupBonus`, `cashBonus`, `created`, `modified`) VALUES
(1,	10,	10,	10,	'2020-05-25 10:49:54',	'2020-05-25 11:24:38'),
(3,	20,	24,	26,	'2020-05-30 13:34:12',	'2020-05-30 13:34:12');

DROP TABLE IF EXISTS `bonus_logs`;
CREATE TABLE `bonus_logs` (
  `bonusLogId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `bonusId` int(11) NOT NULL,
  `playGame` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `matches` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`bonusLogId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_pages`;
CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `showIn` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `coins_deduct_history`;
CREATE TABLE `coins_deduct_history` (
  `coinsDeductHistoryId` double NOT NULL AUTO_INCREMENT,
  `userId` double NOT NULL,
  `agendId` double NOT NULL,
  `tableId` double NOT NULL,
  `game` varchar(20) NOT NULL,
  `userName` varchar(60) NOT NULL,
  `gameType` varchar(20) NOT NULL,
  `winLossType` varchar(150) NOT NULL,
  `coins` double(11,2) NOT NULL,
  `betValue` double NOT NULL,
  `entryFee` double NOT NULL,
  `mainfirstToken` double NOT NULL,
  `mainfirstThreeToken` double NOT NULL,
  `rummyPoints` int(11) NOT NULL,
  `isWin` varchar(20) NOT NULL,
  `adminCommition` int(11) NOT NULL,
  `adminAmount` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`coinsDeductHistoryId`),
  KEY `userId` (`userId`),
  KEY `tableId` (`tableId`),
  KEY `isWin` (`isWin`),
  KEY `created` (`created`),
  KEY `gameType` (`gameType`)
) ENGINE=InnoDB AUTO_INCREMENT=444 DEFAULT CHARSET=latin1;

INSERT INTO `coins_deduct_history` (`coinsDeductHistoryId`, `userId`, `agendId`, `tableId`, `game`, `userName`, `gameType`, `winLossType`, `coins`, `betValue`, `entryFee`, `mainfirstToken`, `mainfirstThreeToken`, `rummyPoints`, `isWin`, `adminCommition`, `adminAmount`, `mainWallet`, `winWallet`, `created`, `modified`) VALUES
(99,	44,	0,	118,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-17 13:35:02',	'2021-05-17 13:35:02'),
(100,	43,	0,	118,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-17 13:35:02',	'2021-05-17 13:35:02'),
(101,	43,	0,	118,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Left',	14.00,	5,	5,	4,	5,	0,	'Loss',	5,	0.7,	5,	0,	'2021-05-17 13:35:24',	'2021-05-17 13:35:24'),
(102,	43,	0,	119,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-17 13:40:23',	'2021-05-17 13:40:23'),
(103,	44,	0,	119,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-17 13:40:23',	'2021-05-17 13:40:23'),
(104,	43,	0,	119,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-17 13:41:00',	'2021-05-17 13:41:00'),
(105,	44,	0,	119,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-17 13:41:00',	'2021-05-17 13:41:00'),
(106,	43,	0,	119,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Left',	14.00,	5,	5,	4,	5,	0,	'Loss',	5,	0.7,	5,	0,	'2021-05-17 13:44:27',	'2021-05-17 13:44:27'),
(107,	3,	0,	120,	'ludo',	'newsam',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-17 13:45:28',	'2021-05-17 13:45:28'),
(108,	42,	0,	121,	'ludo',	'vishwa',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-17 13:48:17',	'2021-05-17 13:48:17'),
(109,	3,	0,	122,	'ludo',	'newsam',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-17 13:52:20',	'2021-05-17 13:52:20'),
(110,	43,	0,	123,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 05:09:11',	'2021-05-18 05:09:11'),
(111,	44,	0,	123,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-18 05:09:11',	'2021-05-18 05:09:11'),
(112,	43,	0,	123,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-18 05:10:22',	'2021-05-18 05:10:22'),
(113,	44,	0,	123,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 05:10:22',	'2021-05-18 05:10:22'),
(114,	43,	0,	123,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'First Token',	4.00,	4,	5,	0,	0,	0,	'Loss',	5,	0.2,	4,	0,	'2021-05-18 05:11:09',	'2021-05-18 05:11:09'),
(115,	44,	0,	123,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'First Token',	7.60,	4,	5,	0,	0,	0,	'Win',	5,	0.2,	4,	0,	'2021-05-18 05:11:09',	'2021-05-18 05:11:09'),
(116,	43,	0,	123,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-18 05:14:31',	'2021-05-18 05:14:31'),
(117,	44,	0,	123,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 05:14:31',	'2021-05-18 05:14:31'),
(118,	43,	0,	123,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'First Three Token',	9.50,	5,	5,	0,	0,	0,	'Win',	5,	0.25,	5,	0,	'2021-05-18 05:16:15',	'2021-05-18 05:16:15'),
(119,	44,	0,	123,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'First Three Token',	5.00,	5,	5,	0,	0,	0,	'Loss',	5,	0.25,	5,	0,	'2021-05-18 05:16:15',	'2021-05-18 05:16:15'),
(120,	43,	0,	123,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Four Token',	9.50,	5,	5,	0,	0,	0,	'Win',	5,	0.25,	5,	0,	'2021-05-18 05:17:31',	'2021-05-18 05:17:31'),
(121,	44,	0,	123,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Four Token',	5.00,	5,	5,	0,	0,	0,	'Loss',	5,	0.25,	5,	0,	'2021-05-18 05:17:31',	'2021-05-18 05:17:31'),
(122,	15,	0,	125,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Left',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-18 05:57:09',	'2021-05-18 05:57:09'),
(123,	22,	0,	125,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Left',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-18 05:57:12',	'2021-05-18 05:57:12'),
(124,	8,	0,	125,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 05:59:17',	'2021-05-18 05:59:17'),
(125,	6,	0,	125,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 05:59:17',	'2021-05-18 05:59:17'),
(126,	8,	0,	125,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 05:59:38',	'2021-05-18 05:59:38'),
(127,	6,	0,	125,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 05:59:38',	'2021-05-18 05:59:38'),
(128,	15,	0,	125,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:01:34',	'2021-05-18 06:01:34'),
(129,	8,	0,	125,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:01:34',	'2021-05-18 06:01:34'),
(130,	6,	0,	125,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:01:34',	'2021-05-18 06:01:34'),
(131,	22,	0,	125,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:01:34',	'2021-05-18 06:01:34'),
(132,	8,	0,	125,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:04:36',	'2021-05-18 06:04:36'),
(133,	6,	0,	125,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:04:36',	'2021-05-18 06:04:36'),
(134,	8,	0,	125,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:06:39',	'2021-05-18 06:06:39'),
(135,	6,	0,	125,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:06:39',	'2021-05-18 06:06:39'),
(136,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:07:42',	'2021-05-18 06:07:42'),
(137,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:07:42',	'2021-05-18 06:07:42'),
(138,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:08:08',	'2021-05-18 06:08:08'),
(139,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:08:08',	'2021-05-18 06:08:08'),
(140,	15,	0,	125,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:09:13',	'2021-05-18 06:09:13'),
(141,	8,	0,	125,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:09:13',	'2021-05-18 06:09:13'),
(142,	6,	0,	125,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:09:13',	'2021-05-18 06:09:13'),
(143,	22,	0,	125,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:09:13',	'2021-05-18 06:09:13'),
(144,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:09:27',	'2021-05-18 06:09:27'),
(145,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:09:27',	'2021-05-18 06:09:27'),
(146,	8,	0,	125,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:09:39',	'2021-05-18 06:09:39'),
(147,	6,	0,	125,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:09:39',	'2021-05-18 06:09:39'),
(148,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:11:17',	'2021-05-18 06:11:17'),
(149,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:11:17',	'2021-05-18 06:11:17'),
(150,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:11:48',	'2021-05-18 06:11:48'),
(151,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:11:48',	'2021-05-18 06:11:48'),
(152,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:11:59',	'2021-05-18 06:11:59'),
(153,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:11:59',	'2021-05-18 06:11:59'),
(154,	8,	0,	125,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Four Token',	37.00,	10,	10,	0,	0,	0,	'Win',	10,	0,	10,	0,	'2021-05-18 06:12:19',	'2021-05-18 06:12:19'),
(155,	6,	0,	125,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Four Token',	9.00,	10,	10,	0,	0,	0,	'Loss',	10,	0,	10,	0,	'2021-05-18 06:12:19',	'2021-05-18 06:12:19'),
(156,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:13:53',	'2021-05-18 06:13:53'),
(157,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:13:53',	'2021-05-18 06:13:53'),
(158,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:14:08',	'2021-05-18 06:14:08'),
(159,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:14:08',	'2021-05-18 06:14:08'),
(160,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:14:08',	'2021-05-18 06:14:08'),
(161,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:14:08',	'2021-05-18 06:14:08'),
(162,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:18:26',	'2021-05-18 06:18:26'),
(163,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:18:26',	'2021-05-18 06:18:26'),
(164,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:19:16',	'2021-05-18 06:19:16'),
(165,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:19:16',	'2021-05-18 06:19:16'),
(166,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:19:16',	'2021-05-18 06:19:16'),
(167,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:19:16',	'2021-05-18 06:19:16'),
(168,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:20:16',	'2021-05-18 06:20:16'),
(169,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:20:16',	'2021-05-18 06:20:16'),
(170,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'Left',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-18 06:20:38',	'2021-05-18 06:20:38'),
(171,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:21:24',	'2021-05-18 06:21:24'),
(172,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:21:24',	'2021-05-18 06:21:24'),
(173,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:23:21',	'2021-05-18 06:23:21'),
(174,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:23:21',	'2021-05-18 06:23:21'),
(175,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:23:52',	'2021-05-18 06:23:52'),
(176,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:23:52',	'2021-05-18 06:23:52'),
(177,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:26:41',	'2021-05-18 06:26:41'),
(178,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:26:41',	'2021-05-18 06:26:41'),
(179,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:29:41',	'2021-05-18 06:29:41'),
(180,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:29:41',	'2021-05-18 06:29:41'),
(181,	15,	0,	126,	'ludo',	'Amol dada',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:30:08',	'2021-05-18 06:30:08'),
(182,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:30:08',	'2021-05-18 06:30:08'),
(183,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:30:08',	'2021-05-18 06:30:08'),
(184,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:30:08',	'2021-05-18 06:30:08'),
(185,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:30:27',	'2021-05-18 06:30:27'),
(186,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:30:27',	'2021-05-18 06:30:27'),
(187,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:31:44',	'2021-05-18 06:31:44'),
(188,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:31:44',	'2021-05-18 06:31:44'),
(189,	17,	0,	126,	'ludo',	'Vinod',	'4 players - 10 rs Fa',	'Four Token',	36.00,	10,	10,	0,	0,	0,	'Win',	10,	1,	10,	0,	'2021-05-18 06:34:28',	'2021-05-18 06:34:28'),
(190,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Four Token',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-18 06:34:28',	'2021-05-18 06:34:28'),
(191,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Four Token',	9.00,	10,	10,	0,	0,	0,	'Loss',	10,	0,	10,	0,	'2021-05-18 06:34:28',	'2021-05-18 06:34:28'),
(192,	22,	0,	126,	'ludo',	'Dipesh',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 06:35:28',	'2021-05-18 06:35:28'),
(193,	8,	0,	126,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 06:35:28',	'2021-05-18 06:35:28'),
(194,	13,	0,	129,	'ludo',	'Y',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-18 06:36:34',	'2021-05-18 06:36:34'),
(195,	7,	0,	131,	'ludo',	'Bot2',	'15 Classic',	'Four Token',	27.75,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-18 07:24:28',	'2021-05-18 07:24:28'),
(196,	15,	0,	131,	'ludo',	'Amol dada',	'15 Classic',	'Four Token',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-18 07:24:28',	'2021-05-18 07:24:28'),
(197,	8,	0,	132,	'ludo',	'Bot3',	'2 player - 5 Rs Fanc',	'First Token',	3.80,	4,	5,	0,	0,	0,	'Loss',	5,	0,	4,	0,	'2021-05-18 07:29:26',	'2021-05-18 07:29:26'),
(198,	15,	0,	132,	'ludo',	'Amol dada',	'2 player - 5 Rs Fanc',	'First Token',	7.60,	4,	5,	0,	0,	0,	'Win',	5,	0.2,	4,	0,	'2021-05-18 07:29:26',	'2021-05-18 07:29:26'),
(199,	8,	0,	132,	'ludo',	'Bot3',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0,	2,	0,	'2021-05-18 07:29:41',	'2021-05-18 07:29:41'),
(200,	15,	0,	132,	'ludo',	'Amol dada',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 07:29:41',	'2021-05-18 07:29:41'),
(201,	8,	0,	132,	'ludo',	'Bot3',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0,	2,	0,	'2021-05-18 07:31:02',	'2021-05-18 07:31:02'),
(202,	15,	0,	132,	'ludo',	'Amol dada',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-18 07:31:02',	'2021-05-18 07:31:02'),
(203,	8,	0,	132,	'ludo',	'Bot3',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0,	2,	0,	'2021-05-18 07:35:27',	'2021-05-18 07:35:27'),
(204,	15,	0,	132,	'ludo',	'Amol dada',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 07:35:27',	'2021-05-18 07:35:27'),
(205,	8,	0,	132,	'ludo',	'Bot3',	'2 player - 5 Rs Fanc',	'First Three Token',	4.75,	5,	5,	0,	0,	0,	'Loss',	5,	0,	5,	0,	'2021-05-18 07:36:42',	'2021-05-18 07:36:42'),
(206,	15,	0,	132,	'ludo',	'Amol dada',	'2 player - 5 Rs Fanc',	'First Three Token',	9.50,	5,	5,	0,	0,	0,	'Win',	5,	0.25,	5,	0,	'2021-05-18 07:36:42',	'2021-05-18 07:36:42'),
(207,	8,	0,	132,	'ludo',	'Bot3',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0,	2,	0,	'2021-05-18 07:37:23',	'2021-05-18 07:37:23'),
(208,	15,	0,	132,	'ludo',	'Amol dada',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 07:37:23',	'2021-05-18 07:37:23'),
(209,	8,	0,	132,	'ludo',	'Bot3',	'2 player - 5 Rs Fanc',	'Kill Token',	3.90,	2,	5,	0,	0,	0,	'Win',	5,	0,	2,	0,	'2021-05-18 07:37:44',	'2021-05-18 07:37:44'),
(210,	15,	0,	132,	'ludo',	'Amol dada',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 07:37:44',	'2021-05-18 07:37:44'),
(211,	8,	0,	132,	'ludo',	'Bot3',	'2 player - 5 Rs Fanc',	'Four Token',	9.75,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-18 07:39:40',	'2021-05-18 07:39:40'),
(212,	15,	0,	132,	'ludo',	'Amol dada',	'2 player - 5 Rs Fanc',	'Four Token',	5.00,	5,	5,	0,	0,	0,	'Loss',	5,	0.25,	5,	0,	'2021-05-18 07:39:40',	'2021-05-18 07:39:40'),
(213,	44,	0,	133,	'ludo',	'abcd',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-18 09:14:56',	'2021-05-18 09:14:56'),
(214,	3,	0,	134,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-18 09:18:48',	'2021-05-18 09:18:48'),
(215,	3,	0,	135,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'Left',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-18 09:20:23',	'2021-05-18 09:20:23'),
(216,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:23:37',	'2021-05-18 09:23:37'),
(217,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:23:37',	'2021-05-18 09:23:37'),
(218,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:24:57',	'2021-05-18 09:24:57'),
(219,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:24:57',	'2021-05-18 09:24:57'),
(220,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:25:31',	'2021-05-18 09:25:31'),
(221,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:25:31',	'2021-05-18 09:25:31'),
(222,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:26:19',	'2021-05-18 09:26:19'),
(223,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:26:19',	'2021-05-18 09:26:19'),
(224,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:27:43',	'2021-05-18 09:27:43'),
(225,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:27:43',	'2021-05-18 09:27:43'),
(226,	3,	0,	135,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:28:43',	'2021-05-18 09:28:43'),
(227,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:28:43',	'2021-05-18 09:28:43'),
(228,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:28:43',	'2021-05-18 09:28:43'),
(229,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:28:43',	'2021-05-18 09:28:43'),
(230,	44,	0,	136,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-18 09:30:31',	'2021-05-18 09:30:31'),
(231,	43,	0,	136,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 09:30:31',	'2021-05-18 09:30:31'),
(232,	44,	0,	136,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-18 09:31:51',	'2021-05-18 09:31:51'),
(233,	43,	0,	136,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-18 09:31:51',	'2021-05-18 09:31:51'),
(234,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:31:55',	'2021-05-18 09:31:55'),
(235,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:31:55',	'2021-05-18 09:31:55'),
(236,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:32:30',	'2021-05-18 09:32:30'),
(237,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:32:30',	'2021-05-18 09:32:30'),
(238,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:33:26',	'2021-05-18 09:33:26'),
(239,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:33:26',	'2021-05-18 09:33:26'),
(240,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:33:49',	'2021-05-18 09:33:49'),
(241,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:33:49',	'2021-05-18 09:33:49'),
(242,	44,	0,	136,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'First Token',	4.00,	4,	5,	0,	0,	0,	'Loss',	5,	0.2,	4,	0,	'2021-05-18 09:33:53',	'2021-05-18 09:33:53'),
(243,	43,	0,	136,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'First Token',	3.60,	4,	5,	0,	0,	0,	'Win',	5,	0.2,	4,	0,	'2021-05-18 09:33:53',	'2021-05-18 09:33:53'),
(244,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:34:27',	'2021-05-18 09:34:27'),
(245,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:34:27',	'2021-05-18 09:34:27'),
(246,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:38:26',	'2021-05-18 09:38:26'),
(247,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:38:26',	'2021-05-18 09:38:26'),
(248,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:39:00',	'2021-05-18 09:39:00'),
(249,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:39:00',	'2021-05-18 09:39:00'),
(250,	44,	0,	136,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'First Three Token',	5.00,	5,	5,	0,	0,	0,	'Loss',	5,	0.25,	5,	0,	'2021-05-18 09:39:11',	'2021-05-18 09:39:11'),
(251,	43,	0,	136,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'First Three Token',	4.50,	5,	5,	0,	0,	0,	'Win',	5,	0.25,	5,	0,	'2021-05-18 09:39:11',	'2021-05-18 09:39:11'),
(252,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:41:29',	'2021-05-18 09:41:29'),
(253,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:41:29',	'2021-05-18 09:41:29'),
(254,	44,	0,	136,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Four Token',	5.00,	5,	5,	0,	0,	0,	'Loss',	5,	0.25,	5,	0,	'2021-05-18 09:41:48',	'2021-05-18 09:41:48'),
(255,	43,	0,	136,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Four Token',	4.50,	5,	5,	0,	0,	0,	'Win',	5,	0.25,	5,	0,	'2021-05-18 09:41:48',	'2021-05-18 09:41:48'),
(256,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:43:28',	'2021-05-18 09:43:28'),
(257,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:43:28',	'2021-05-18 09:43:28'),
(258,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:44:10',	'2021-05-18 09:44:10'),
(259,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:44:10',	'2021-05-18 09:44:10'),
(260,	7,	0,	134,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-18 09:44:12',	'2021-05-18 09:44:12'),
(261,	6,	0,	134,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	76.50,	30,	30,	0,	0,	0,	'Win',	15,	0,	30,	0,	'2021-05-18 09:44:12',	'2021-05-18 09:44:12'),
(262,	8,	0,	134,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-18 09:44:12',	'2021-05-18 09:44:12'),
(263,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:45:18',	'2021-05-18 09:45:18'),
(264,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:45:18',	'2021-05-18 09:45:18'),
(265,	3,	0,	135,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:45:37',	'2021-05-18 09:45:37'),
(266,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:45:37',	'2021-05-18 09:45:37'),
(267,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:45:37',	'2021-05-18 09:45:37'),
(268,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:45:37',	'2021-05-18 09:45:37'),
(269,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:46:46',	'2021-05-18 09:46:46'),
(270,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:46:46',	'2021-05-18 09:46:46'),
(271,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Four Token',	9.00,	10,	10,	0,	0,	0,	'Loss',	10,	0,	10,	0,	'2021-05-18 09:49:00',	'2021-05-18 09:49:00'),
(272,	7,	0,	135,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Four Token',	27.00,	10,	10,	0,	0,	0,	'Win',	10,	0,	10,	0,	'2021-05-18 09:49:00',	'2021-05-18 09:49:00'),
(273,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Four Token',	9.00,	10,	10,	0,	0,	0,	'Loss',	10,	0,	10,	0,	'2021-05-18 09:49:00',	'2021-05-18 09:49:00'),
(274,	8,	0,	135,	'ludo',	'Bot3',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 09:51:18',	'2021-05-18 09:51:18'),
(275,	6,	0,	135,	'ludo',	'Bot1',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 09:51:18',	'2021-05-18 09:51:18'),
(276,	3,	0,	139,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-18 11:13:21',	'2021-05-18 11:13:21'),
(277,	13,	0,	141,	'ludo',	'Y',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-18 11:26:46',	'2021-05-18 11:26:46'),
(278,	7,	0,	141,	'ludo',	'Bot2',	'15 Classic',	'Four Token',	12.75,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-18 11:26:47',	'2021-05-18 11:26:47'),
(279,	13,	0,	144,	'ludo',	'Y',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-18 11:48:23',	'2021-05-18 11:48:23'),
(280,	8,	0,	144,	'ludo',	'Bot3',	'15 Classic',	'Four Token',	10.50,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-18 11:48:24',	'2021-05-18 11:48:24'),
(281,	13,	0,	143,	'ludo',	'Y',	'2 player - 5 Rs Fanc',	'Left',	9.00,	5,	5,	4,	0,	0,	'Loss',	5,	0.45,	5,	0,	'2021-05-18 11:52:23',	'2021-05-18 11:52:23'),
(282,	7,	0,	143,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Three Token',	4.75,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-18 11:52:24',	'2021-05-18 11:52:24'),
(283,	13,	0,	143,	'ludo',	'Y',	'2 player - 5 Rs Fanc',	'First Token',	4.00,	4,	5,	0,	0,	0,	'Loss',	5,	0.2,	4,	0,	'2021-05-18 11:52:24',	'2021-05-18 11:52:24'),
(284,	7,	0,	143,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Four Token',	4.50,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-18 11:52:24',	'2021-05-18 11:52:24'),
(285,	7,	0,	143,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Token',	3.80,	4,	5,	0,	0,	0,	'Win',	5,	0,	4,	0,	'2021-05-18 11:52:24',	'2021-05-18 11:52:24'),
(286,	13,	0,	147,	'ludo',	'Y',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-18 11:55:08',	'2021-05-18 11:55:08'),
(287,	8,	0,	147,	'ludo',	'Bot3',	'15 Classic',	'Four Token',	10.50,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-18 11:55:09',	'2021-05-18 11:55:09'),
(288,	42,	0,	146,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 12:00:41',	'2021-05-18 12:00:41'),
(289,	43,	0,	146,	'ludo',	'test123',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 12:00:41',	'2021-05-18 12:00:41'),
(290,	44,	0,	146,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 12:00:41',	'2021-05-18 12:00:41'),
(291,	3,	0,	146,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'First Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 12:00:41',	'2021-05-18 12:00:41'),
(292,	42,	0,	146,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 12:01:18',	'2021-05-18 12:01:18'),
(293,	44,	0,	146,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 12:01:18',	'2021-05-18 12:01:18'),
(294,	13,	0,	149,	'ludo',	'Y',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-18 12:01:50',	'2021-05-18 12:01:50'),
(295,	8,	0,	149,	'ludo',	'Bot3',	'15 Classic',	'Four Token',	10.50,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-18 12:01:50',	'2021-05-18 12:01:50'),
(296,	43,	0,	146,	'ludo',	'test123',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 12:03:38',	'2021-05-18 12:03:38'),
(297,	44,	0,	146,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 12:03:38',	'2021-05-18 12:03:38'),
(298,	42,	0,	146,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 12:11:30',	'2021-05-18 12:11:30'),
(299,	43,	0,	146,	'ludo',	'test123',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 12:11:30',	'2021-05-18 12:11:30'),
(300,	44,	0,	146,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Win',	10,	0,	0,	0,	'2021-05-18 12:11:30',	'2021-05-18 12:11:30'),
(301,	3,	0,	146,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'First Three Token',	0.00,	0,	10,	0,	0,	0,	'Loss',	10,	0,	0,	0,	'2021-05-18 12:11:30',	'2021-05-18 12:11:30'),
(302,	42,	0,	146,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Four Token',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-18 12:17:57',	'2021-05-18 12:17:57'),
(303,	43,	0,	146,	'ludo',	'test123',	'4 players - 10 rs Fa',	'Four Token',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-18 12:17:57',	'2021-05-18 12:17:57'),
(304,	44,	0,	146,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Four Token',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-18 12:17:57',	'2021-05-18 12:17:57'),
(305,	3,	0,	146,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'Four Token',	26.00,	10,	10,	0,	0,	0,	'Win',	10,	1,	10,	0,	'2021-05-18 12:17:57',	'2021-05-18 12:17:57'),
(306,	44,	0,	151,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Left',	9.00,	5,	5,	4,	0,	0,	'Loss',	5,	0.45,	5,	0,	'2021-05-19 05:44:58',	'2021-05-19 05:44:58'),
(307,	7,	0,	151,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Three Token',	4.75,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-19 05:44:59',	'2021-05-19 05:44:59'),
(308,	7,	0,	151,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Four Token',	4.50,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-19 05:44:59',	'2021-05-19 05:44:59'),
(309,	7,	0,	151,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Token',	3.80,	4,	5,	0,	0,	0,	'Win',	5,	0,	4,	0,	'2021-05-19 05:44:59',	'2021-05-19 05:44:59'),
(310,	44,	0,	151,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'First Token',	4.00,	4,	5,	0,	0,	0,	'Loss',	5,	0.2,	4,	0,	'2021-05-19 05:44:59',	'2021-05-19 05:44:59'),
(311,	7,	0,	152,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Token',	3.80,	4,	5,	0,	0,	0,	'Win',	5,	0,	4,	0,	'2021-05-19 05:49:08',	'2021-05-19 05:49:08'),
(312,	43,	0,	152,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'First Token',	4.00,	4,	5,	0,	0,	0,	'Loss',	5,	0.2,	4,	0,	'2021-05-19 05:49:08',	'2021-05-19 05:49:08'),
(313,	7,	0,	153,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-19 05:49:23',	'2021-05-19 05:49:23'),
(314,	44,	0,	153,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-19 05:49:23',	'2021-05-19 05:49:23'),
(315,	7,	0,	152,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-19 05:50:57',	'2021-05-19 05:50:57'),
(316,	43,	0,	152,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-19 05:50:57',	'2021-05-19 05:50:57'),
(317,	44,	0,	153,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'Left',	9.00,	5,	5,	4,	0,	0,	'Loss',	5,	0.45,	5,	0,	'2021-05-19 05:51:40',	'2021-05-19 05:51:40'),
(318,	7,	0,	153,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Token',	3.80,	4,	5,	0,	0,	0,	'Win',	5,	0,	4,	0,	'2021-05-19 05:51:41',	'2021-05-19 05:51:41'),
(319,	7,	0,	153,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Three Token',	4.75,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-19 05:51:41',	'2021-05-19 05:51:41'),
(320,	7,	0,	153,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Four Token',	4.50,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-19 05:51:41',	'2021-05-19 05:51:41'),
(321,	44,	0,	153,	'ludo',	'abcd',	'2 player - 5 Rs Fanc',	'First Token',	4.00,	4,	5,	0,	0,	0,	'Loss',	5,	0.2,	4,	0,	'2021-05-19 05:51:41',	'2021-05-19 05:51:41'),
(322,	7,	0,	152,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-19 05:53:02',	'2021-05-19 05:53:02'),
(323,	43,	0,	152,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-19 05:53:02',	'2021-05-19 05:53:02'),
(324,	7,	0,	152,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-19 05:55:39',	'2021-05-19 05:55:39'),
(325,	43,	0,	152,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-19 05:55:39',	'2021-05-19 05:55:39'),
(326,	7,	0,	152,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-19 05:56:32',	'2021-05-19 05:56:32'),
(327,	43,	0,	152,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-19 05:56:32',	'2021-05-19 05:56:32'),
(328,	7,	0,	152,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Three Token',	4.75,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-19 05:58:54',	'2021-05-19 05:58:54'),
(329,	43,	0,	152,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'First Three Token',	5.00,	5,	5,	0,	0,	0,	'Loss',	5,	0.25,	5,	0,	'2021-05-19 05:58:54',	'2021-05-19 05:58:54'),
(330,	7,	0,	152,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Four Token',	4.50,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-19 06:01:34',	'2021-05-19 06:01:34'),
(331,	43,	0,	152,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Four Token',	5.00,	5,	5,	0,	0,	0,	'Loss',	5,	0.25,	5,	0,	'2021-05-19 06:01:34',	'2021-05-19 06:01:34'),
(332,	43,	0,	154,	'ludo',	'test123',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 07:10:23',	'2021-05-19 07:10:23'),
(333,	8,	0,	154,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	0,	30,	0,	'2021-05-19 07:30:32',	'2021-05-19 07:30:32'),
(334,	6,	0,	154,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 07:30:32',	'2021-05-19 07:30:32'),
(335,	7,	0,	154,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 07:30:32',	'2021-05-19 07:30:32'),
(336,	43,	0,	155,	'ludo',	'test123',	'15 Classic',	'Four Token',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-19 08:19:49',	'2021-05-19 08:19:49'),
(337,	44,	0,	155,	'ludo',	'abcd',	'15 Classic',	'Four Token',	10.50,	15,	15,	0,	0,	0,	'Win',	15,	2.25,	15,	0,	'2021-05-19 08:19:49',	'2021-05-19 08:19:49'),
(338,	3,	0,	156,	'ludo',	'newsam',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-19 10:39:31',	'2021-05-19 10:39:31'),
(339,	7,	0,	156,	'ludo',	'Bot2',	'15 Classic',	'Four Token',	10.50,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-19 10:39:32',	'2021-05-19 10:39:32'),
(340,	3,	0,	157,	'ludo',	'newsam',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-19 10:44:40',	'2021-05-19 10:44:40'),
(341,	7,	0,	157,	'ludo',	'Bot2',	'15 Classic',	'Four Token',	10.50,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-19 10:44:41',	'2021-05-19 10:44:41'),
(342,	3,	0,	158,	'ludo',	'newsam',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-19 10:47:29',	'2021-05-19 10:47:29'),
(343,	8,	0,	158,	'ludo',	'Bot3',	'15 Classic',	'Four Token',	10.50,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-19 10:47:30',	'2021-05-19 10:47:30'),
(344,	43,	0,	159,	'ludo',	'test123',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 11:38:10',	'2021-05-19 11:38:10'),
(345,	6,	0,	159,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	0,	30,	0,	'2021-05-19 11:48:06',	'2021-05-19 11:48:06'),
(346,	8,	0,	159,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 11:48:06',	'2021-05-19 11:48:06'),
(347,	7,	0,	159,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 11:48:06',	'2021-05-19 11:48:06'),
(348,	3,	0,	160,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 11:49:48',	'2021-05-19 11:49:48'),
(349,	43,	0,	161,	'ludo',	'test123',	'15 Classic',	'Left',	15.00,	15,	15,	0,	0,	0,	'Loss',	15,	2.25,	15,	0,	'2021-05-19 11:50:29',	'2021-05-19 11:50:29'),
(350,	6,	0,	161,	'ludo',	'Bot1',	'15 Classic',	'Four Token',	10.50,	15,	15,	0,	0,	0,	'Win',	15,	0,	15,	0,	'2021-05-19 11:50:29',	'2021-05-19 11:50:29'),
(351,	3,	0,	162,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 12:00:47',	'2021-05-19 12:00:47'),
(352,	3,	0,	163,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 12:06:49',	'2021-05-19 12:06:49'),
(353,	3,	0,	164,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 12:09:20',	'2021-05-19 12:09:20'),
(354,	3,	0,	165,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 12:12:39',	'2021-05-19 12:12:39'),
(355,	7,	0,	163,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 12:23:37',	'2021-05-19 12:23:37'),
(356,	6,	0,	163,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 12:23:37',	'2021-05-19 12:23:37'),
(357,	8,	0,	163,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	0,	30,	0,	'2021-05-19 12:23:37',	'2021-05-19 12:23:37'),
(358,	8,	0,	160,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 12:38:57',	'2021-05-19 12:38:57'),
(359,	7,	0,	160,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	0,	30,	0,	'2021-05-19 12:38:57',	'2021-05-19 12:38:57'),
(360,	6,	0,	160,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 12:38:57',	'2021-05-19 12:38:57'),
(361,	6,	0,	162,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 12:43:37',	'2021-05-19 12:43:37'),
(362,	7,	0,	162,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	0,	30,	0,	'2021-05-19 12:43:37',	'2021-05-19 12:43:37'),
(363,	8,	0,	162,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 12:43:37',	'2021-05-19 12:43:37'),
(364,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:45:43',	'2021-05-19 12:45:43'),
(365,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:45:43',	'2021-05-19 12:45:43'),
(366,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:46:43',	'2021-05-19 12:46:43'),
(367,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:46:43',	'2021-05-19 12:46:43'),
(368,	3,	0,	166,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:46:56',	'2021-05-19 12:46:56'),
(369,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:46:56',	'2021-05-19 12:46:56'),
(370,	3,	0,	166,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'First Token',	2.00,	2,	10,	0,	0,	0,	'Loss',	10,	0.2,	2,	0,	'2021-05-19 12:47:42',	'2021-05-19 12:47:42'),
(371,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'First Token',	5.40,	2,	10,	0,	0,	0,	'Win',	10,	0,	2,	0,	'2021-05-19 12:47:42',	'2021-05-19 12:47:42'),
(372,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'First Token',	2.00,	2,	10,	0,	0,	0,	'Loss',	10,	0.2,	2,	0,	'2021-05-19 12:47:42',	'2021-05-19 12:47:42'),
(373,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'First Token',	2.00,	2,	10,	0,	0,	0,	'Loss',	10,	0.2,	2,	0,	'2021-05-19 12:47:42',	'2021-05-19 12:47:42'),
(374,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:48:20',	'2021-05-19 12:48:20'),
(375,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:48:20',	'2021-05-19 12:48:20'),
(376,	3,	0,	166,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:49:51',	'2021-05-19 12:49:51'),
(377,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:49:51',	'2021-05-19 12:49:51'),
(378,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:50:59',	'2021-05-19 12:50:59'),
(379,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:50:59',	'2021-05-19 12:50:59'),
(380,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:51:30',	'2021-05-19 12:51:30'),
(381,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:51:30',	'2021-05-19 12:51:30'),
(382,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:53:10',	'2021-05-19 12:53:10'),
(383,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:53:10',	'2021-05-19 12:53:10'),
(384,	3,	0,	166,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:54:46',	'2021-05-19 12:54:46'),
(385,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:54:46',	'2021-05-19 12:54:46'),
(386,	7,	0,	165,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	0,	30,	0,	'2021-05-19 12:54:46',	'2021-05-19 12:54:46'),
(387,	8,	0,	165,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 12:54:46',	'2021-05-19 12:54:46'),
(388,	6,	0,	165,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 12:54:46',	'2021-05-19 12:54:46'),
(389,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:55:31',	'2021-05-19 12:55:31'),
(390,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:55:31',	'2021-05-19 12:55:31'),
(391,	3,	0,	166,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:56:33',	'2021-05-19 12:56:33'),
(392,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:56:33',	'2021-05-19 12:56:33'),
(393,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:57:53',	'2021-05-19 12:57:53'),
(394,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:57:53',	'2021-05-19 12:57:53'),
(395,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 12:58:00',	'2021-05-19 12:58:00'),
(396,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 12:58:00',	'2021-05-19 12:58:00'),
(397,	3,	0,	166,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'First Three Token',	3.00,	3,	10,	0,	0,	0,	'Loss',	10,	0.3,	3,	0,	'2021-05-19 13:00:11',	'2021-05-19 13:00:11'),
(398,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'First Three Token',	8.10,	3,	10,	0,	0,	0,	'Win',	10,	0,	3,	0,	'2021-05-19 13:00:11',	'2021-05-19 13:00:11'),
(399,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'First Three Token',	3.00,	3,	10,	0,	0,	0,	'Loss',	10,	0.3,	3,	0,	'2021-05-19 13:00:11',	'2021-05-19 13:00:11'),
(400,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'First Three Token',	3.00,	3,	10,	0,	0,	0,	'Loss',	10,	0.3,	3,	0,	'2021-05-19 13:00:11',	'2021-05-19 13:00:11'),
(401,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 13:04:24',	'2021-05-19 13:04:24'),
(402,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 13:04:24',	'2021-05-19 13:04:24'),
(403,	3,	0,	166,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'Four Token',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-19 13:04:35',	'2021-05-19 13:04:35'),
(404,	7,	0,	166,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Four Token',	26.00,	10,	10,	0,	0,	0,	'Win',	10,	0,	10,	0,	'2021-05-19 13:04:35',	'2021-05-19 13:04:35'),
(405,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Four Token',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-19 13:04:35',	'2021-05-19 13:04:35'),
(406,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Four Token',	10.00,	10,	10,	0,	0,	0,	'Loss',	10,	1,	10,	0,	'2021-05-19 13:04:35',	'2021-05-19 13:04:35'),
(407,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 13:04:53',	'2021-05-19 13:04:53'),
(408,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 13:04:53',	'2021-05-19 13:04:53'),
(409,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 13:10:21',	'2021-05-19 13:10:21'),
(410,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 13:10:21',	'2021-05-19 13:10:21'),
(411,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 13:11:53',	'2021-05-19 13:11:53'),
(412,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 13:11:53',	'2021-05-19 13:11:53'),
(413,	8,	0,	164,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 13:12:32',	'2021-05-19 13:12:32'),
(414,	7,	0,	164,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-19 13:12:32',	'2021-05-19 13:12:32'),
(415,	6,	0,	164,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	0,	30,	0,	'2021-05-19 13:12:32',	'2021-05-19 13:12:32'),
(416,	7,	0,	167,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-19 13:22:30',	'2021-05-19 13:22:30'),
(417,	43,	0,	167,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-19 13:22:30',	'2021-05-19 13:22:30'),
(418,	3,	0,	166,	'ludo',	'newsam',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 13:22:36',	'2021-05-19 13:22:36'),
(419,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 13:22:36',	'2021-05-19 13:22:36'),
(420,	7,	0,	167,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-19 13:23:06',	'2021-05-19 13:23:06'),
(421,	43,	0,	167,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-19 13:23:06',	'2021-05-19 13:23:06'),
(422,	7,	0,	167,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'Kill Token',	1.90,	2,	5,	0,	0,	0,	'Win',	5,	0.1,	2,	0,	'2021-05-19 13:24:09',	'2021-05-19 13:24:09'),
(423,	43,	0,	167,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Kill Token',	2.00,	2,	5,	0,	0,	0,	'Loss',	5,	0.1,	2,	0,	'2021-05-19 13:24:09',	'2021-05-19 13:24:09'),
(424,	44,	0,	166,	'ludo',	'abcd',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 13:25:33',	'2021-05-19 13:25:33'),
(425,	42,	0,	166,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 13:25:33',	'2021-05-19 13:25:33'),
(426,	7,	0,	167,	'ludo',	'Bot2',	'2 player - 5 Rs Fanc',	'First Token',	3.80,	4,	5,	0,	0,	0,	'Win',	5,	0,	4,	0,	'2021-05-19 13:26:03',	'2021-05-19 13:26:03'),
(427,	43,	0,	167,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'First Token',	4.00,	4,	5,	0,	0,	0,	'Loss',	5,	0.2,	4,	0,	'2021-05-19 13:26:03',	'2021-05-19 13:26:03'),
(428,	3,	0,	170,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Left',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 13:34:33',	'2021-05-19 13:34:33'),
(429,	7,	0,	171,	'ludo',	'Bot2',	'4 players - 10 rs Fa',	'Kill Token',	0.90,	1,	10,	0,	0,	0,	'Win',	10,	0.1,	1,	0,	'2021-05-19 13:36:20',	'2021-05-19 13:36:20'),
(430,	42,	0,	171,	'ludo',	'vishwa',	'4 players - 10 rs Fa',	'Kill Token',	1.00,	1,	10,	0,	0,	0,	'Loss',	10,	0.1,	1,	0,	'2021-05-19 13:36:20',	'2021-05-19 13:36:20'),
(431,	43,	0,	173,	'ludo',	'test123',	'4 player -  30 Rs Cl',	'Four Token',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 13:45:25',	'2021-05-19 13:45:25'),
(432,	3,	0,	173,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	4.5,	30,	0,	'2021-05-19 13:45:25',	'2021-05-19 13:45:25'),
(433,	44,	0,	173,	'ludo',	'abcd',	'4 player -  30 Rs Cl',	'Four Token',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 13:45:25',	'2021-05-19 13:45:25'),
(434,	7,	0,	173,	'ludo',	'Bot2',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-19 13:45:25',	'2021-05-19 13:45:25'),
(435,	43,	0,	176,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'Left',	9.00,	5,	5,	4,	0,	0,	'Loss',	5,	0.45,	5,	0,	'2021-05-20 05:45:29',	'2021-05-20 05:45:29'),
(436,	43,	0,	176,	'ludo',	'test123',	'2 player - 5 Rs Fanc',	'First Token',	4.00,	4,	5,	0,	0,	0,	'Loss',	5,	0.2,	4,	0,	'2021-05-20 05:45:30',	'2021-05-20 05:45:30'),
(437,	6,	0,	176,	'ludo',	'Bot1',	'2 player - 5 Rs Fanc',	'First Three Token',	4.75,	5,	5,	0,	0,	0,	'Win',	5,	0,	5,	0,	'2021-05-20 05:45:30',	'2021-05-20 05:45:30'),
(438,	6,	0,	176,	'ludo',	'Bot1',	'2 player - 5 Rs Fanc',	'Four Token',	4.50,	5,	5,	0,	0,	0,	'Win',	5,	0.25,	5,	0,	'2021-05-20 05:45:30',	'2021-05-20 05:45:30'),
(439,	6,	0,	176,	'ludo',	'Bot1',	'2 player - 5 Rs Fanc',	'First Token',	3.80,	4,	5,	0,	0,	0,	'Win',	5,	0,	4,	0,	'2021-05-20 05:45:30',	'2021-05-20 05:45:30'),
(440,	8,	0,	178,	'ludo',	'Bot3',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-20 06:11:39',	'2021-05-20 06:11:39'),
(441,	3,	0,	178,	'ludo',	'newsam',	'4 player -  30 Rs Cl',	'Four Token',	72.00,	30,	30,	0,	0,	0,	'Win',	15,	4.5,	30,	0,	'2021-05-20 06:11:39',	'2021-05-20 06:11:39'),
(442,	6,	0,	178,	'ludo',	'Bot1',	'4 player -  30 Rs Cl',	'Four Token',	25.50,	30,	30,	0,	0,	0,	'Loss',	15,	0,	30,	0,	'2021-05-20 06:11:39',	'2021-05-20 06:11:39'),
(443,	43,	0,	178,	'ludo',	'test123',	'4 player -  30 Rs Cl',	'Four Token',	30.00,	30,	30,	0,	0,	0,	'Loss',	15,	4.5,	30,	0,	'2021-05-20 06:11:39',	'2021-05-20 06:11:39');

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `reply` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `coupon_codes`;
CREATE TABLE `coupon_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `isExpired` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isUsed` enum('Yes','No') NOT NULL DEFAULT 'No',
  `expiredDate` date NOT NULL,
  `discount` float NOT NULL,
  `couponCode` varchar(225) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4;

INSERT INTO `coupon_codes` (`id`, `title`, `description`, `isExpired`, `isUsed`, `expiredDate`, `discount`, `couponCode`, `status`, `created`, `modified`) VALUES
(1,	'Tes',	'Test',	'No',	'No',	'0000-00-00',	0,	'Vamsi 123',	'Active',	'2020-05-05 21:33:19',	'2020-05-05 21:33:19'),
(2,	'test',	'test',	'No',	'No',	'0000-00-00',	90,	'rr123',	'Active',	'2020-05-05 22:00:52',	'2020-05-06 18:13:26'),
(3,	'abc',	'lorem',	'No',	'No',	'2020-05-20',	5,	'liN6YO',	'Active',	'2020-05-08 19:19:31',	'2020-05-12 11:16:29'),
(5,	'testint',	'for test use',	'No',	'Yes',	'2020-05-13',	5,	'DXIA5k',	'Active',	'2020-05-12 10:38:09',	'2020-05-12 12:44:40'),
(6,	'offer',	'lockdown 3.0',	'No',	'No',	'2020-05-17',	100,	'2cslnt',	'Active',	'2020-05-12 16:04:28',	'2020-05-12 16:04:28'),
(7,	'dummy',	'test',	'No',	'Yes',	'2020-05-15',	10,	'zI6vrg',	'Active',	'2020-05-12 17:42:17',	'2020-05-14 11:37:59'),
(8,	'Test1',	'test1',	'No',	'Yes',	'2020-05-16',	10,	'C0XyrY',	'Active',	'2020-05-14 10:05:54',	'2020-05-14 11:44:13'),
(9,	'Test2',	'test2',	'No',	'Yes',	'2020-05-16',	5,	'IBOw6y',	'Active',	'2020-05-14 10:06:18',	'2020-05-14 11:42:48'),
(10,	'Test3',	'test3',	'No',	'No',	'2020-05-16',	10,	'xzeYJd',	'Active',	'2020-05-14 10:06:49',	'2020-05-14 10:06:49'),
(11,	'test4',	'test4',	'No',	'No',	'2020-05-16',	10,	'4qGIcm',	'Active',	'2020-05-14 11:49:05',	'2020-05-14 11:49:05'),
(12,	'Tets5',	'test5',	'No',	'No',	'2020-05-16',	5,	'w3QXJ7',	'Active',	'2020-05-14 11:49:26',	'2020-05-14 11:49:26'),
(13,	'test5',	'twers',	'No',	'Yes',	'2020-05-16',	11,	'l7wqY1',	'Active',	'2020-05-14 13:34:07',	'2020-05-14 15:22:36'),
(14,	'test54',	'estt',	'No',	'Yes',	'2020-05-16',	13,	'yoMxkg',	'Active',	'2020-05-14 13:37:49',	'2020-05-14 14:31:38'),
(15,	'Abcccty',	'Asa',	'No',	'No',	'1970-01-01',	20,	'abvv33',	'Active',	'2020-06-12 12:36:47',	'2020-06-12 12:36:47'),
(16,	'Loren copon',	'Dfdf',	'No',	'No',	'1970-01-01',	30,	'avbi90',	'Active',	'2020-06-12 12:36:47',	'2020-06-12 12:36:47'),
(17,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg001',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(18,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg002',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(19,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg003',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(20,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg004',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(21,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg005',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(22,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg006',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(23,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg007',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(24,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg008',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(25,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg009',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(26,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg010',	'Active',	'2020-06-12 12:37:36',	'2020-06-12 12:37:36'),
(27,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg011',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(28,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg012',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(29,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg013',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(30,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg014',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(31,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg015',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(32,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg016',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(33,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg017',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(34,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg018',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(35,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg019',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(36,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg020',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(37,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg021',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(38,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg022',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(39,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg023',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(40,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg024',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(41,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg025',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(42,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg026',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(43,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg027',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(44,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg028',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(45,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg029',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(46,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg030',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(47,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg031',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(48,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg032',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(49,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg033',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(50,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg034',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(51,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg035',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(52,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg036',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(53,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg037',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(54,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg038',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(55,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg039',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(56,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg040',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(57,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg041',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(58,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg042',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(59,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg043',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(60,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg044',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(61,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg045',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(62,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg046',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(63,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg047',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(64,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg048',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(65,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg049',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(66,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg050',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(67,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg051',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(68,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg052',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(69,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg053',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(70,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg054',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(71,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg055',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(72,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg056',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(73,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg057',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(74,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg058',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(75,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg059',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(76,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg060',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(77,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg061',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(78,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg062',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(79,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg063',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(80,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg064',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(81,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg065',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(82,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg066',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(83,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg067',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(84,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg068',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(85,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg069',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(86,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg070',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(87,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg071',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(88,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg072',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(89,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg073',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(90,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg074',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(91,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg075',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(92,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg076',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(93,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg077',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(94,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg078',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(95,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg079',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(96,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg080',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(98,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg082',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(99,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg083',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(100,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg084',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(101,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg085',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(102,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg086',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(103,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg087',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(104,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg088',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(105,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg089',	'Active',	'2020-06-12 12:37:37',	'2020-06-12 12:37:37'),
(106,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg090',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(107,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg091',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(108,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg092',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(109,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg093',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(110,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg094',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(111,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg095',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(112,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg096',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(113,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg097',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(114,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg098',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(115,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg099',	'Active',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38'),
(116,	'GlobalGamings',	'Coupons To GG',	'No',	'No',	'2020-12-07',	100,	'ggcogg100',	'Inactive',	'2020-06-12 12:37:38',	'2020-06-12 12:37:38');

DROP TABLE IF EXISTS `coupon_user_log`;
CREATE TABLE `coupon_user_log` (
  `couponLogId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `couponId` int(11) NOT NULL,
  `couponName` varchar(200) NOT NULL,
  `couponCode` varchar(200) NOT NULL,
  `couponAmt` double NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`couponLogId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO `coupon_user_log` (`couponLogId`, `userId`, `couponId`, `couponName`, `couponCode`, `couponAmt`, `created`) VALUES
(1,	1,	14,	'test54',	'yoMxkg',	13,	'2020-05-14 14:31:38'),
(2,	1,	13,	'test5',	'l7wqY1',	11,	'2020-05-14 15:22:36');

DROP TABLE IF EXISTS `custom_dice`;
CREATE TABLE `custom_dice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diceName` varchar(255) NOT NULL,
  `dicePrice` double NOT NULL,
  `counter` bigint(20) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `custom_dice` (`id`, `diceName`, `dicePrice`, `counter`, `status`, `created`, `modified`) VALUES
(5,	'CustomeDice1',	50,	3,	'Active',	'2019-11-09 11:54:18',	'2019-11-09 11:54:18'),
(6,	'CustomeDice2',	80,	5,	'Active',	'2019-11-09 11:54:47',	'2019-11-09 12:41:43'),
(7,	'CustomeDice3',	120,	7,	'Active',	'2019-11-09 11:55:05',	'2019-11-09 11:55:05'),
(8,	'CustomeDice4',	200,	10,	'Active',	'2019-11-09 11:55:23',	'2019-11-09 11:55:23');

DROP TABLE IF EXISTS `daywisetimings`;
CREATE TABLE `daywisetimings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dayIndex` int(11) NOT NULL,
  `day` varchar(255) NOT NULL,
  `fromTime1` time NOT NULL,
  `toTime1` time NOT NULL,
  `flag1` enum('true','false') NOT NULL,
  `fromTime2` time NOT NULL,
  `toTime2` time NOT NULL,
  `flag2` enum('true','false') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `daywisetimings` (`id`, `dayIndex`, `day`, `fromTime1`, `toTime1`, `flag1`, `fromTime2`, `toTime2`, `flag2`, `created`, `modified`) VALUES
(1,	0,	'Sunday',	'06:00:00',	'14:00:00',	'true',	'14:00:00',	'11:00:00',	'true',	'2020-02-20 14:56:54',	'2020-03-02 13:02:36'),
(2,	1,	'Monday',	'06:00:00',	'14:00:00',	'true',	'14:00:00',	'11:00:00',	'true',	'2020-02-20 14:56:54',	'2020-03-02 13:02:36'),
(3,	2,	'Tuesday',	'06:00:00',	'14:00:00',	'true',	'14:00:00',	'11:00:00',	'true',	'2020-02-20 14:56:54',	'2020-03-02 13:02:36'),
(4,	3,	'Wednesday',	'06:00:00',	'14:00:00',	'true',	'14:00:00',	'11:00:00',	'true',	'2020-02-20 14:56:54',	'2020-03-04 20:05:47'),
(5,	4,	'Thursday',	'06:00:00',	'14:00:00',	'true',	'14:00:00',	'11:00:00',	'true',	'2020-02-20 14:56:54',	'2020-03-19 20:27:07'),
(6,	5,	'Friday',	'06:00:00',	'14:00:00',	'true',	'14:00:00',	'11:00:00',	'true',	'2020-02-20 14:56:54',	'2020-02-26 13:30:27'),
(7,	6,	'Saturday',	'06:00:00',	'14:00:00',	'true',	'14:00:00',	'11:00:00',	'true',	'2020-02-20 14:56:54',	'2020-02-29 13:05:14');

DROP TABLE IF EXISTS `deposit`;
CREATE TABLE `deposit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_detail_id` int(11) NOT NULL,
  `deposit` double NOT NULL,
  `withdraw` double NOT NULL,
  `type` enum('Deposit','Withdraw') NOT NULL,
  `balance` double NOT NULL,
  `status` enum('Approved','Pending','Reject') NOT NULL DEFAULT 'Pending',
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `game_features`;
CREATE TABLE `game_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive',
  `is_web` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `invite_and_earn`;
CREATE TABLE `invite_and_earn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_detail_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `invited_link` varchar(255) NOT NULL,
  `is_register` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemName` varchar(255) NOT NULL,
  `itemPrice` double NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `kyc_logs`;
CREATE TABLE `kyc_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_detail_id` int(11) NOT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `adharUserName` varchar(255) NOT NULL,
  `adharCard_no` varchar(255) NOT NULL,
  `adharFron_img` varchar(255) NOT NULL,
  `adharBack_img` varchar(255) NOT NULL,
  `panUserName` varchar(255) NOT NULL,
  `panCard_no` varchar(255) NOT NULL,
  `pan_img` varchar(255) NOT NULL,
  `acc_holderName` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_city` varchar(255) NOT NULL,
  `bank_branch` varchar(255) NOT NULL,
  `accno` varchar(255) NOT NULL,
  `ifsc` varchar(255) NOT NULL,
  `is_bankVerified` varchar(255) NOT NULL DEFAULT 'Pending',
  `is_aadharVerified` varchar(255) NOT NULL DEFAULT 'Pending',
  `is_panVerified` varchar(255) NOT NULL DEFAULT 'Pending',
  `kyc_status` varchar(255) NOT NULL DEFAULT 'Pending',
  `is_approve` enum('Yes','No') NOT NULL DEFAULT 'No',
  `tmp` longtext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_detail_id` (`user_detail_id`),
  KEY `is_bankVerified` (`is_bankVerified`),
  KEY `is_aadharVerified` (`is_aadharVerified`),
  KEY `is_panVerified` (`is_panVerified`),
  KEY `kyc_status` (`kyc_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `ludo_join_rooms`;
CREATE TABLE `ludo_join_rooms` (
  `joinRoomId` double NOT NULL AUTO_INCREMENT,
  `roomId` double NOT NULL,
  `noOfPlayers` int(11) NOT NULL,
  `activePlayer` int(11) NOT NULL,
  `betValue` int(11) NOT NULL,
  `gameStatus` enum('Pending','Active','Complete') NOT NULL DEFAULT 'Pending',
  `gameMode` enum('Quick','Classic') NOT NULL,
  `isPrivate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isFree` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isTournament` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`joinRoomId`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=latin1;

INSERT INTO `ludo_join_rooms` (`joinRoomId`, `roomId`, `noOfPlayers`, `activePlayer`, `betValue`, `gameStatus`, `gameMode`, `isPrivate`, `isFree`, `isTournament`, `created`, `modified`) VALUES
(1,	42,	2,	2,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 11:29:41',	'2021-03-19 11:31:39'),
(2,	42,	2,	1,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 11:48:00',	'2021-03-19 11:48:00'),
(3,	42,	2,	1,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 11:51:04',	'2021-03-19 11:51:05'),
(4,	42,	2,	1,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 12:01:14',	'2021-03-19 12:01:16'),
(5,	42,	2,	1,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 12:05:20',	'2021-03-19 12:05:27'),
(6,	42,	2,	1,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 12:36:45',	'2021-03-19 12:36:47'),
(7,	45,	4,	1,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 12:54:08',	'2021-03-31 04:47:52'),
(8,	42,	2,	1,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 13:05:13',	'2021-03-19 13:05:16'),
(9,	42,	2,	0,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 13:12:18',	'2021-03-19 13:12:28'),
(10,	42,	2,	2,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-19 13:14:00',	'2021-03-19 13:15:01'),
(11,	42,	2,	0,	30,	'Pending',	'Classic',	'No',	'No',	'No',	'2021-03-20 04:53:41',	'2021-04-14 13:00:48'),
(12,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-22 09:31:48',	'2021-03-27 07:30:05'),
(13,	38,	2,	1,	100,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-24 08:54:20',	'2021-05-17 12:11:54'),
(14,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-27 07:33:42',	'2021-03-27 07:37:48'),
(15,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-27 07:50:09',	'2021-03-27 07:51:04'),
(16,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-30 12:03:42',	'2021-03-31 08:17:32'),
(17,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-31 04:50:53',	'2021-03-31 04:53:13'),
(18,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-31 04:58:24',	'2021-03-31 04:58:49'),
(19,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-31 05:29:43',	'2021-03-31 05:32:05'),
(20,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-31 05:34:04',	'2021-03-31 06:47:37'),
(21,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-31 08:31:24',	'2021-03-31 08:35:22'),
(22,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-31 08:33:28',	'2021-05-13 07:36:05'),
(23,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-03-31 08:39:49',	'2021-04-01 09:10:39'),
(24,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-01 09:14:15',	'2021-04-01 09:27:45'),
(25,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-01 09:29:50',	'2021-04-01 09:47:01'),
(26,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-01 09:49:28',	'2021-04-07 18:47:56'),
(27,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-12 03:37:39',	'2021-04-12 09:49:18'),
(28,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-15 10:47:15',	'2021-04-15 10:48:00'),
(29,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-15 10:50:56',	'2021-04-15 10:50:57'),
(30,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-15 11:48:30',	'2021-04-19 06:22:51'),
(31,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-19 06:23:40',	'2021-04-19 06:24:37'),
(32,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-19 06:27:12',	'2021-04-19 06:27:22'),
(33,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-19 06:46:38',	'2021-04-19 06:46:48'),
(34,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-19 07:00:42',	'2021-04-19 12:17:05'),
(35,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-04-28 16:56:43',	'2021-05-01 10:59:08'),
(36,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-01 11:02:31',	'2021-05-01 11:11:23'),
(37,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-05 10:14:25',	'2021-05-06 08:34:18'),
(38,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-06 08:37:44',	'2021-05-11 13:08:05'),
(39,	51,	2,	2,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-06 13:53:44',	'2021-05-10 13:03:51'),
(40,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-10 13:05:04',	'2021-05-11 08:53:08'),
(41,	51,	2,	2,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-11 08:53:09',	'2021-05-11 11:48:56'),
(42,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-11 13:15:45',	'2021-05-11 13:15:49'),
(43,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-11 14:20:06',	'2021-05-12 08:00:58'),
(44,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-11 14:22:59',	'2021-05-12 05:59:01'),
(45,	49,	2,	2,	5,	'Active',	'Quick',	'No',	'No',	'No',	'2021-05-11 14:31:51',	'2021-05-12 05:40:19'),
(46,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 08:05:53',	'2021-05-12 09:15:16'),
(47,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 09:52:31',	'2021-05-12 09:55:57'),
(48,	51,	2,	2,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 10:02:55',	'2021-05-12 10:23:15'),
(49,	49,	2,	2,	5,	'Active',	'Quick',	'No',	'No',	'No',	'2021-05-12 10:09:13',	'2021-05-12 10:33:16'),
(50,	47,	4,	0,	15,	'Pending',	'',	'No',	'No',	'No',	'2021-05-12 10:13:24',	'2021-05-13 04:35:46'),
(51,	48,	4,	4,	10,	'Active',	'Quick',	'No',	'No',	'No',	'2021-05-12 10:15:03',	'2021-05-17 08:58:55'),
(52,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 10:24:43',	'2021-05-12 10:27:20'),
(53,	51,	2,	2,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 10:29:43',	'2021-05-12 10:29:47'),
(54,	51,	2,	2,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 10:30:44',	'2021-05-12 10:30:49'),
(55,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 10:32:01',	'2021-05-12 10:35:46'),
(56,	49,	2,	1,	5,	'Active',	'Quick',	'No',	'No',	'No',	'2021-05-12 10:35:39',	'2021-05-12 10:36:03'),
(57,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-12 10:35:58',	'2021-05-12 10:36:21'),
(58,	51,	2,	2,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 10:36:16',	'2021-05-12 11:21:19'),
(59,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 11:24:29',	'2021-05-12 11:24:54'),
(60,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 11:30:35',	'2021-05-12 11:31:00'),
(61,	51,	2,	1,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 11:41:37',	'2021-05-12 11:42:01'),
(62,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 13:42:32',	'2021-05-12 13:42:57'),
(63,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 13:45:10',	'2021-05-12 13:45:35'),
(64,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 13:48:45',	'2021-05-12 13:49:28'),
(65,	17,	2,	1,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 13:55:47',	'2021-05-12 13:56:11'),
(66,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 13:59:25',	'2021-05-12 13:59:50'),
(67,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:00:58',	'2021-05-12 14:01:22'),
(68,	17,	2,	1,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:03:16',	'2021-05-12 14:03:40'),
(69,	17,	2,	1,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:06:39',	'2021-05-12 14:07:03'),
(70,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:10:08',	'2021-05-12 14:10:32'),
(71,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:11:49',	'2021-05-12 14:12:13'),
(72,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:14:50',	'2021-05-12 14:15:14'),
(73,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:17:21',	'2021-05-12 14:17:46'),
(74,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:18:29',	'2021-05-12 14:18:53'),
(75,	17,	2,	1,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:22:10',	'2021-05-12 14:22:35'),
(76,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:24:39',	'2021-05-12 14:25:03'),
(77,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:27:19',	'2021-05-12 14:28:25'),
(78,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:30:07',	'2021-05-12 14:30:31'),
(79,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:34:40',	'2021-05-12 14:35:04'),
(80,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:36:13',	'2021-05-12 14:36:38'),
(81,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:38:24',	'2021-05-12 14:38:48'),
(82,	17,	2,	2,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-12 14:40:31',	'2021-05-12 14:40:55'),
(83,	51,	2,	0,	10,	'Pending',	'',	'No',	'No',	'No',	'2021-05-13 04:34:44',	'2021-05-13 10:48:03'),
(84,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-13 07:58:08',	'2021-05-13 07:58:53'),
(85,	17,	2,	1,	15,	'Active',	'',	'No',	'No',	'No',	'2021-05-13 10:48:20',	'2021-05-13 12:53:40'),
(86,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-13 12:52:01',	'2021-05-14 09:16:50'),
(87,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-13 12:55:05',	'2021-05-13 12:55:29'),
(88,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-13 12:58:46',	'2021-05-13 12:59:34'),
(89,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-13 13:01:13',	'2021-05-13 13:01:37'),
(90,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 05:00:40',	'2021-05-14 08:56:21'),
(91,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-14 05:02:20',	'2021-05-14 06:21:45'),
(92,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 09:17:59',	'2021-05-14 09:18:06'),
(93,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 09:25:14',	'2021-05-14 09:25:25'),
(94,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 09:27:59',	'2021-05-14 09:28:04'),
(95,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 10:12:23',	'2021-05-14 10:15:15'),
(96,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 12:08:58',	'2021-05-14 12:09:22'),
(97,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 12:09:56',	'2021-05-14 12:10:40'),
(98,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 12:12:06',	'2021-05-14 12:12:31'),
(99,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 12:12:32',	'2021-05-14 12:13:16'),
(100,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-14 12:14:43',	'2021-05-14 12:14:48'),
(101,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-15 09:43:01',	'2021-05-15 09:43:25'),
(102,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 05:58:52',	'2021-05-17 05:58:54'),
(103,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 08:06:23',	'2021-05-17 08:06:29'),
(104,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 08:08:04',	'2021-05-17 08:08:16'),
(105,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 09:32:49',	'2021-05-17 09:32:57'),
(106,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 09:39:39',	'2021-05-17 09:39:41'),
(107,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 10:24:33',	'2021-05-17 10:24:57'),
(108,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 10:36:49',	'2021-05-17 10:37:13'),
(109,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 10:38:38',	'2021-05-17 10:42:58'),
(110,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 10:55:15',	'2021-05-17 10:55:20'),
(111,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 11:04:26',	'2021-05-17 11:04:35'),
(112,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 11:15:06',	'2021-05-17 11:15:25'),
(113,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 11:29:33',	'2021-05-17 11:29:56'),
(114,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-17 12:11:33',	'2021-05-17 12:16:14'),
(115,	38,	2,	1,	100,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-17 12:16:08',	'2021-05-17 12:16:32'),
(116,	38,	2,	2,	100,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-17 12:16:34',	'2021-05-17 12:16:46'),
(117,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 12:31:56',	'2021-05-17 12:32:07'),
(118,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 13:31:29',	'2021-05-17 13:31:29'),
(119,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-17 13:37:00',	'2021-05-17 13:37:01'),
(120,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-17 13:42:37',	'2021-05-17 13:43:01'),
(121,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-17 13:46:40',	'2021-05-17 13:46:51'),
(122,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-17 13:50:39',	'2021-05-17 13:51:04'),
(123,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 05:05:36',	'2021-05-18 05:05:37'),
(124,	45,	4,	0,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 05:56:21',	'2021-05-18 05:57:06'),
(125,	48,	4,	3,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 05:56:28',	'2021-05-18 05:57:02'),
(126,	48,	4,	3,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 06:04:00',	'2021-05-18 06:04:24'),
(127,	48,	4,	0,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 06:07:23',	'2021-05-18 06:08:08'),
(128,	48,	4,	0,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 06:09:09',	'2021-05-18 06:13:47'),
(129,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 06:26:59',	'2021-05-18 06:27:23'),
(130,	45,	4,	0,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 06:37:13',	'2021-05-18 09:09:28'),
(131,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 07:07:55',	'2021-05-18 07:08:19'),
(132,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 07:25:52',	'2021-05-18 07:26:16'),
(133,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 09:13:15',	'2021-05-18 09:13:15'),
(134,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 09:17:44',	'2021-05-18 09:18:28'),
(135,	48,	4,	3,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 09:19:13',	'2021-05-18 09:19:57'),
(136,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 09:26:13',	'2021-05-18 09:26:15'),
(137,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 10:04:53',	'2021-05-18 10:05:27'),
(138,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 11:09:18',	'2021-05-18 11:09:42'),
(139,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 11:10:05',	'2021-05-18 11:10:49'),
(140,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 11:20:42',	'2021-05-18 11:21:26'),
(141,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 11:26:11',	'2021-05-18 11:26:36'),
(142,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 11:30:16',	'2021-05-18 11:30:40'),
(143,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 11:31:35',	'2021-05-18 11:51:04'),
(144,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 11:34:34',	'2021-05-18 11:47:50'),
(145,	45,	4,	0,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 11:35:56',	'2021-05-19 06:51:54'),
(146,	48,	4,	4,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 11:45:06',	'2021-05-18 11:45:13'),
(147,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 11:53:06',	'2021-05-18 11:53:30'),
(148,	49,	2,	0,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-18 11:57:37',	'2021-05-19 05:44:26'),
(149,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 12:01:18',	'2021-05-18 12:01:42'),
(150,	17,	2,	0,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-18 12:22:15',	'2021-05-19 07:16:44'),
(151,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-19 05:44:26',	'2021-05-19 05:44:51'),
(152,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-19 05:46:14',	'2021-05-19 05:46:38'),
(153,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-19 05:47:27',	'2021-05-19 05:47:51'),
(154,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 06:52:59',	'2021-05-19 06:53:43'),
(155,	17,	2,	2,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 07:16:44',	'2021-05-19 08:09:10'),
(156,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 10:29:11',	'2021-05-19 10:38:06'),
(157,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 10:42:48',	'2021-05-19 10:43:12'),
(158,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 10:44:55',	'2021-05-19 10:45:19'),
(159,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 11:16:58',	'2021-05-19 11:17:43'),
(160,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 11:46:37',	'2021-05-19 11:47:22'),
(161,	17,	2,	1,	15,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 11:49:15',	'2021-05-19 11:49:39'),
(162,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 11:57:33',	'2021-05-19 11:58:17'),
(163,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 12:04:36',	'2021-05-19 12:05:20'),
(164,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 12:07:00',	'2021-05-19 12:07:44'),
(165,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 12:09:30',	'2021-05-19 12:10:14'),
(166,	48,	4,	3,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-19 12:44:14',	'2021-05-19 12:44:39'),
(167,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-19 13:17:53',	'2021-05-19 13:18:17'),
(168,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 13:25:24',	'2021-05-19 13:26:08'),
(169,	49,	2,	2,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-19 13:28:28',	'2021-05-19 13:28:52'),
(170,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 13:31:33',	'2021-05-19 13:32:18'),
(171,	48,	4,	4,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-19 13:32:27',	'2021-05-19 13:34:20'),
(172,	48,	4,	4,	10,	'Active',	'',	'No',	'No',	'No',	'2021-05-19 13:38:07',	'2021-05-19 13:38:42'),
(173,	45,	4,	3,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-19 13:38:33',	'2021-05-19 13:42:30'),
(174,	48,	4,	0,	10,	'Pending',	'',	'No',	'No',	'No',	'2021-05-19 13:38:56',	'2021-05-19 13:40:48'),
(175,	35,	4,	0,	10,	'Pending',	'Classic',	'Yes',	'No',	'No',	'2021-05-19 14:18:04',	'2021-05-19 14:18:10'),
(176,	49,	2,	1,	5,	'Active',	'',	'No',	'No',	'No',	'2021-05-20 05:43:35',	'2021-05-20 05:43:59'),
(177,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-20 06:02:03',	'2021-05-20 06:02:48'),
(178,	45,	4,	4,	30,	'Active',	'Classic',	'No',	'No',	'No',	'2021-05-20 06:07:44',	'2021-05-20 06:08:19');

DROP TABLE IF EXISTS `ludo_join_room_users`;
CREATE TABLE `ludo_join_room_users` (
  `joinRoomUserId` double NOT NULL AUTO_INCREMENT,
  `joinRoomId` double NOT NULL,
  `userId` double NOT NULL,
  `roomId` int(11) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `isWin` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isTournament` enum('Yes','No') NOT NULL DEFAULT 'No',
  `tokenColor` enum('Red','Blue','Yellow','Green') NOT NULL,
  `playerType` enum('Real','Bot') NOT NULL DEFAULT 'Real',
  `status` enum('Active','Inactive','Disconnect') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  PRIMARY KEY (`joinRoomUserId`)
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=latin1;

INSERT INTO `ludo_join_room_users` (`joinRoomUserId`, `joinRoomId`, `userId`, `roomId`, `userName`, `isWin`, `isTournament`, `tokenColor`, `playerType`, `status`, `created`) VALUES
(2,	1,	4,	42,	'Shubham',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-19 11:30:01'),
(3,	1,	4,	42,	'Shubham',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-19 11:31:39'),
(4,	2,	4,	42,	'Shubham',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-19 11:48:00'),
(5,	2,	3,	42,	'newsam',	'No',	'No',	'Yellow',	'Real',	'Active',	'2021-03-19 11:48:00'),
(6,	3,	4,	42,	'Shubham',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-19 11:51:04'),
(7,	3,	3,	42,	'newsam',	'No',	'No',	'Yellow',	'Real',	'Disconnect',	'2021-03-19 11:51:05'),
(8,	4,	4,	42,	'Shubham',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-19 12:01:14'),
(9,	4,	3,	42,	'newsam',	'No',	'No',	'Yellow',	'Real',	'Active',	'2021-03-19 12:01:16'),
(10,	5,	4,	42,	'Shubham',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-19 12:05:20'),
(11,	5,	3,	42,	'newsam',	'No',	'No',	'Yellow',	'Real',	'Disconnect',	'2021-03-19 12:05:27'),
(12,	6,	3,	42,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-19 12:36:45'),
(13,	6,	4,	42,	'Shubham',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-19 12:36:47'),
(15,	8,	3,	42,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-03-19 13:05:13'),
(16,	8,	4,	42,	'Shubham',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-19 13:05:16'),
(20,	10,	3,	42,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-19 13:14:56'),
(21,	10,	4,	42,	'Shubham',	'No',	'No',	'Red',	'Real',	'Active',	'2021-03-19 13:15:01'),
(37,	12,	23,	17,	'govind',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-26 12:25:37'),
(39,	12,	17,	17,	'Tejas',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-27 07:18:03'),
(40,	12,	17,	17,	'Tejas',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-27 07:30:05'),
(42,	14,	17,	17,	'Tejas',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-27 07:37:31'),
(43,	14,	15,	17,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-27 07:37:48'),
(46,	15,	15,	17,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-27 07:51:00'),
(47,	15,	17,	17,	'Tejas',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-27 07:51:04'),
(52,	7,	15,	45,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-31 04:47:47'),
(53,	7,	17,	45,	'Tejas',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-31 04:47:51'),
(54,	7,	22,	45,	'Yuvi',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-03-31 04:47:52'),
(56,	17,	15,	45,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-31 04:53:03'),
(57,	17,	17,	45,	'Tejas',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-31 04:53:05'),
(58,	17,	21,	45,	'Gaurav',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-03-31 04:53:06'),
(59,	17,	22,	45,	'Yuvi',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-03-31 04:53:13'),
(61,	18,	15,	45,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-31 04:58:43'),
(62,	18,	17,	45,	'Tejas',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-31 04:58:44'),
(63,	18,	22,	45,	'Yuvi',	'No',	'No',	'Green',	'Real',	'Active',	'2021-03-31 04:58:44'),
(64,	18,	21,	45,	'Gaurav',	'No',	'No',	'Yellow',	'Real',	'Disconnect',	'2021-03-31 04:58:49'),
(68,	19,	17,	45,	'Vinod',	'No',	'No',	'Yellow',	'Real',	'Disconnect',	'2021-03-31 05:31:39'),
(69,	19,	22,	45,	'Dipesh',	'No',	'No',	'Green',	'Real',	'Active',	'2021-03-31 05:31:40'),
(70,	19,	21,	45,	'mitesh',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-03-31 05:31:55'),
(71,	19,	15,	45,	'Amol dada',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-03-31 05:32:05'),
(79,	20,	22,	45,	'Dipesh',	'No',	'No',	'Green',	'Real',	'Active',	'2021-03-31 06:45:26'),
(80,	20,	21,	45,	'mitesh',	'No',	'No',	'Green',	'Real',	'Active',	'2021-03-31 06:46:43'),
(81,	20,	15,	45,	'Amol dada',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-03-31 06:47:16'),
(82,	20,	17,	45,	'Vinod',	'No',	'No',	'Red',	'Real',	'Active',	'2021-03-31 06:47:37'),
(84,	16,	13,	17,	'Yashika',	'No',	'No',	'Red',	'Real',	'Active',	'2021-03-31 07:14:35'),
(85,	16,	13,	17,	'Yashika',	'No',	'No',	'Yellow',	'Real',	'Active',	'2021-03-31 08:17:32'),
(103,	25,	13,	17,	'Yashika',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-04-01 09:29:50'),
(104,	25,	13,	17,	'Yashika',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-04-01 09:47:01'),
(113,	26,	21,	17,	'mitesh',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-04-07 18:47:54'),
(114,	26,	22,	17,	'Dipesh',	'No',	'No',	'Red',	'Real',	'Active',	'2021-04-07 18:47:56'),
(117,	27,	22,	17,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-04-12 09:47:29'),
(118,	27,	22,	17,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-04-12 09:49:18'),
(121,	28,	22,	17,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-04-15 10:47:58'),
(122,	28,	21,	17,	'mitesh',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-04-15 10:48:00'),
(123,	29,	21,	17,	'mitesh',	'No',	'No',	'Red',	'Real',	'Active',	'2021-04-15 10:50:56'),
(124,	29,	22,	17,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-04-15 10:50:57'),
(133,	32,	22,	17,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-04-19 06:27:12'),
(134,	32,	21,	17,	'mitesh',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-04-19 06:27:22'),
(135,	33,	21,	17,	'mitesh',	'No',	'No',	'Green',	'Real',	'Active',	'2021-04-19 06:46:38'),
(136,	33,	22,	17,	'Dipesh',	'No',	'No',	'Yellow',	'Real',	'Disconnect',	'2021-04-19 06:46:48'),
(147,	36,	17,	17,	'Vinod',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-01 11:10:31'),
(148,	36,	4,	17,	'Abhi',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-01 11:11:23'),
(155,	39,	3,	51,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-07 13:26:09'),
(156,	39,	3,	51,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-10 13:01:59'),
(157,	39,	3,	51,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-10 13:03:51'),
(160,	40,	3,	51,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-11 08:52:47'),
(161,	40,	3,	51,	'newsam',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-11 08:53:08'),
(164,	41,	44,	51,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-11 11:48:41'),
(165,	41,	43,	51,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-11 11:48:57'),
(166,	38,	44,	17,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-11 13:08:03'),
(167,	38,	43,	17,	'test123',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-11 13:08:05'),
(168,	42,	44,	17,	'abcd',	'No',	'No',	'Yellow',	'Real',	'Disconnect',	'2021-05-11 13:15:45'),
(169,	42,	43,	17,	'test123',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-11 13:15:49'),
(174,	45,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-12 05:40:15'),
(175,	45,	44,	49,	'abcd',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 05:40:19'),
(176,	44,	43,	17,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-12 05:59:01'),
(177,	44,	44,	17,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 05:59:01'),
(180,	43,	44,	51,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-12 08:00:53'),
(181,	43,	43,	51,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-12 08:00:58'),
(186,	46,	43,	51,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-12 09:15:16'),
(187,	46,	44,	51,	'abcd',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-12 09:15:16'),
(189,	47,	13,	51,	'Yash',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-12 09:55:02'),
(190,	47,	4,	51,	'Abhi',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 09:55:57'),
(200,	48,	17,	51,	'Vinod',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 10:23:15'),
(201,	48,	13,	51,	'Yash',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 10:23:15'),
(203,	52,	13,	51,	'Yash',	'No',	'No',	'Yellow',	'Real',	'Active',	'2021-05-12 10:26:00'),
(204,	52,	13,	51,	'Yash',	'No',	'No',	'Yellow',	'Real',	'Active',	'2021-05-12 10:27:10'),
(205,	52,	17,	51,	'Vinod',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-12 10:27:20'),
(206,	53,	17,	51,	'Vinod',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 10:29:43'),
(207,	53,	13,	51,	'Yash',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 10:29:47'),
(208,	54,	17,	51,	'Vinod',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 10:30:44'),
(209,	54,	13,	51,	'Yash',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 10:30:49'),
(210,	55,	13,	51,	'Yash',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 10:32:01'),
(212,	49,	13,	49,	'Yash',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 10:33:14'),
(213,	49,	17,	49,	'Vinod',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 10:33:16'),
(214,	55,	22,	51,	'Dipesh',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-12 10:35:14'),
(215,	56,	21,	49,	'mitesh',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-12 10:35:39'),
(216,	55,	43,	51,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-12 10:35:46'),
(217,	57,	17,	17,	'Vinod',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-12 10:35:58'),
(218,	56,	7,	49,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 10:36:03'),
(220,	57,	13,	17,	'Yash',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 10:36:21'),
(227,	58,	43,	51,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-12 11:15:27'),
(228,	58,	8,	51,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 11:21:19'),
(229,	59,	43,	51,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-12 11:24:29'),
(230,	59,	8,	51,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-12 11:24:54'),
(231,	60,	43,	51,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-12 11:30:35'),
(232,	60,	6,	51,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-12 11:31:00'),
(233,	61,	43,	51,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-12 11:41:37'),
(234,	61,	8,	51,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 11:42:01'),
(235,	62,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 13:42:32'),
(236,	62,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 13:42:57'),
(237,	63,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 13:45:10'),
(238,	63,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 13:45:35'),
(239,	64,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 13:48:45'),
(240,	64,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 13:49:04'),
(241,	64,	6,	17,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 13:49:28'),
(242,	65,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-12 13:55:47'),
(243,	65,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 13:56:11'),
(244,	66,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 13:59:25'),
(245,	66,	6,	17,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 13:59:50'),
(246,	67,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 14:00:58'),
(247,	67,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:01:22'),
(248,	68,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:03:16'),
(249,	68,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-12 14:03:40'),
(250,	69,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 14:06:39'),
(251,	69,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-12 14:07:03'),
(252,	70,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:10:08'),
(253,	70,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:10:32'),
(254,	71,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:11:49'),
(255,	71,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:12:13'),
(256,	72,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 14:14:50'),
(257,	72,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:15:14'),
(258,	73,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:17:21'),
(259,	73,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:17:46'),
(260,	74,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 14:18:29'),
(261,	74,	6,	17,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:18:53'),
(262,	75,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:22:10'),
(263,	75,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-12 14:22:35'),
(264,	76,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 14:24:39'),
(265,	76,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:25:03'),
(266,	77,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:27:19'),
(267,	77,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:28:01'),
(268,	77,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:28:25'),
(269,	78,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:30:07'),
(270,	78,	6,	17,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:30:31'),
(271,	79,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:34:40'),
(272,	79,	6,	17,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:35:04'),
(273,	80,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-12 14:36:13'),
(274,	80,	6,	17,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:36:38'),
(275,	81,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:38:24'),
(276,	81,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:38:48'),
(277,	82,	3,	17,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-12 14:40:31'),
(278,	82,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-12 14:40:55'),
(283,	22,	13,	45,	'Yash',	'No',	'No',	'Yellow',	'Real',	'Disconnect',	'2021-05-13 07:35:31'),
(284,	22,	17,	45,	'Vinod',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-13 07:35:34'),
(285,	22,	17,	45,	'Vinod',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-13 07:35:37'),
(286,	22,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 07:35:55'),
(287,	22,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 07:36:05'),
(288,	84,	13,	45,	'Yash',	'No',	'No',	'Yellow',	'Real',	'Active',	'2021-05-13 07:58:08'),
(289,	84,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 07:58:33'),
(290,	84,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 07:58:43'),
(291,	84,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 07:58:53'),
(297,	85,	13,	17,	'Yash',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-13 12:53:15'),
(298,	85,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 12:53:40'),
(299,	87,	13,	17,	'Yash',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-13 12:55:05'),
(300,	87,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 12:55:29'),
(302,	88,	13,	17,	'Yash',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-13 12:59:10'),
(303,	88,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 12:59:34'),
(304,	89,	13,	17,	'Yash',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-13 13:01:13'),
(305,	89,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-13 13:01:37'),
(308,	91,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 06:21:43'),
(309,	91,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-14 06:21:45'),
(310,	90,	44,	17,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-14 08:56:01'),
(312,	90,	43,	17,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-14 08:56:21'),
(313,	86,	44,	45,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-14 09:16:26'),
(314,	86,	43,	45,	'test123',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 09:16:26'),
(315,	86,	3,	45,	'newsam',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 09:16:26'),
(316,	86,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-14 09:16:50'),
(317,	92,	42,	45,	'vishwa',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-14 09:17:59'),
(318,	92,	3,	45,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-14 09:17:59'),
(319,	92,	44,	45,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-14 09:18:06'),
(320,	92,	43,	45,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-14 09:18:06'),
(321,	93,	3,	45,	'newsam',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 09:25:14'),
(322,	93,	42,	45,	'vishwa',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 09:25:14'),
(323,	93,	44,	45,	'abcd',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 09:25:24'),
(324,	93,	43,	45,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-14 09:25:25'),
(325,	94,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-14 09:27:59'),
(326,	94,	43,	45,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-14 09:27:59'),
(327,	94,	42,	45,	'vishwa',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 09:27:59'),
(328,	94,	44,	45,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-14 09:28:04'),
(329,	95,	43,	45,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-14 10:12:23'),
(330,	95,	44,	45,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-14 10:12:23'),
(331,	95,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-14 10:12:47'),
(332,	95,	43,	45,	'test123',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-14 10:15:11'),
(333,	95,	44,	45,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-14 10:15:13'),
(334,	95,	3,	45,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-14 10:15:15'),
(335,	95,	42,	45,	'vishwa',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-14 10:15:15'),
(336,	96,	44,	45,	'abcd',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 12:08:58'),
(337,	96,	43,	45,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-14 12:08:58'),
(338,	96,	42,	45,	'vishwa',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 12:09:07'),
(339,	96,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-14 12:09:22'),
(340,	97,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-14 12:09:56'),
(341,	97,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-14 12:10:20'),
(342,	97,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-14 12:10:30'),
(343,	97,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-14 12:10:40'),
(344,	98,	42,	45,	'vishwa',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 12:12:06'),
(345,	98,	44,	45,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-14 12:12:16'),
(346,	98,	43,	45,	'test123',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-14 12:12:19'),
(347,	98,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-14 12:12:31'),
(348,	99,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-14 12:12:32'),
(349,	99,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-14 12:12:56'),
(350,	99,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-14 12:13:06'),
(351,	99,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Disconnect',	'2021-05-14 12:13:16'),
(352,	100,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-14 12:14:43'),
(353,	100,	42,	45,	'vishwa',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-14 12:14:45'),
(354,	100,	44,	45,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-14 12:14:48'),
(355,	100,	43,	45,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-14 12:14:48'),
(356,	101,	15,	49,	'Amol dada',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-15 09:43:01'),
(357,	101,	7,	49,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-15 09:43:25'),
(358,	102,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 05:58:52'),
(359,	102,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 05:58:54'),
(360,	103,	15,	49,	'Amol dada',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 08:06:23'),
(361,	103,	17,	49,	'Vinod',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 08:06:29'),
(362,	104,	17,	49,	'Vinod',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 08:08:04'),
(363,	104,	15,	49,	'Amol dada',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 08:08:16'),
(364,	51,	42,	48,	'vishwa',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 08:58:45'),
(365,	51,	3,	48,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 08:58:51'),
(366,	51,	43,	48,	'test123',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 08:58:52'),
(367,	51,	44,	48,	'abcd',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 08:58:55'),
(368,	105,	43,	49,	'test123',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 09:32:49'),
(369,	105,	44,	49,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-17 09:32:57'),
(370,	106,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 09:39:39'),
(371,	106,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 09:39:41'),
(372,	107,	3,	49,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 10:24:33'),
(373,	107,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 10:24:57'),
(374,	108,	3,	49,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 10:36:49'),
(375,	108,	7,	49,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-17 10:37:13'),
(376,	109,	44,	49,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 10:38:38'),
(377,	109,	3,	49,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 10:40:14'),
(378,	109,	3,	49,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 10:42:50'),
(379,	109,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 10:42:58'),
(380,	110,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-17 10:55:15'),
(381,	110,	3,	49,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 10:55:20'),
(382,	111,	44,	49,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 11:04:26'),
(383,	111,	3,	49,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 11:04:35'),
(384,	112,	3,	49,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-17 11:15:06'),
(385,	112,	44,	49,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 11:15:25'),
(386,	113,	44,	49,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 11:29:33'),
(387,	113,	3,	49,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 11:29:56'),
(389,	13,	3,	38,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 12:11:52'),
(390,	13,	44,	38,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-17 12:11:54'),
(391,	114,	44,	17,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-17 12:15:49'),
(392,	115,	3,	38,	'newsam',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-17 12:16:08'),
(393,	114,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-17 12:16:14'),
(394,	115,	7,	38,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-17 12:16:32'),
(395,	116,	44,	38,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 12:16:34'),
(396,	116,	3,	38,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 12:16:46'),
(397,	117,	3,	49,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 12:31:56'),
(398,	117,	44,	49,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-17 12:32:07'),
(399,	118,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-17 13:31:29'),
(400,	118,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 13:31:29'),
(401,	119,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-17 13:37:00'),
(402,	119,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-17 13:37:01'),
(403,	120,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-17 13:42:37'),
(404,	120,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-17 13:43:01'),
(405,	121,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-17 13:46:40'),
(406,	121,	42,	17,	'vishwa',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-17 13:46:51'),
(407,	122,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-17 13:50:39'),
(408,	122,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-17 13:51:04'),
(409,	123,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-18 05:05:36'),
(410,	123,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-18 05:05:37'),
(412,	125,	22,	48,	'Dipesh',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-18 05:56:28'),
(413,	125,	15,	48,	'Amol dada',	'No',	'No',	'Yellow',	'Real',	'Disconnect',	'2021-05-18 05:56:28'),
(415,	125,	6,	48,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 05:56:52'),
(417,	125,	8,	48,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 05:57:02'),
(419,	126,	15,	48,	'Amol dada',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-18 06:04:00'),
(420,	126,	22,	48,	'Dipesh',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-18 06:04:01'),
(421,	126,	17,	48,	'Vinod',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-18 06:04:02'),
(422,	126,	8,	48,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 06:04:24'),
(431,	129,	13,	17,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 06:26:59'),
(432,	129,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 06:27:23'),
(434,	131,	15,	17,	'Amol dada',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-18 07:07:55'),
(435,	131,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 07:08:19'),
(436,	132,	15,	49,	'Amol dada',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-18 07:25:52'),
(437,	132,	8,	49,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 07:26:16'),
(441,	133,	44,	17,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-18 09:13:15'),
(442,	133,	43,	17,	'test123',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-18 09:13:15'),
(443,	134,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 09:17:44'),
(444,	134,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 09:18:08'),
(445,	134,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 09:18:18'),
(446,	134,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 09:18:28'),
(447,	135,	3,	48,	'newsam',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-18 09:19:13'),
(448,	135,	8,	48,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 09:19:37'),
(449,	135,	6,	48,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 09:19:47'),
(450,	135,	7,	48,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 09:19:57'),
(451,	136,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-18 09:26:13'),
(452,	136,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-18 09:26:15'),
(453,	137,	43,	45,	'test123',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-18 10:04:53'),
(454,	137,	44,	45,	'abcd',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-18 10:04:55'),
(455,	137,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 10:05:17'),
(456,	137,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 10:05:27'),
(457,	138,	43,	17,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-18 11:09:18'),
(458,	138,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:09:42'),
(459,	139,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 11:10:05'),
(460,	139,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:10:29'),
(461,	139,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:10:39'),
(462,	139,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:10:49'),
(463,	140,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-18 11:20:42'),
(464,	140,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:21:06'),
(465,	140,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:21:16'),
(466,	140,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:21:26'),
(467,	141,	13,	17,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 11:26:11'),
(468,	141,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:26:36'),
(469,	142,	13,	17,	'Y',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-18 11:30:16'),
(470,	142,	6,	17,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:30:40'),
(471,	143,	13,	49,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 11:31:35'),
(472,	144,	13,	17,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 11:34:34'),
(473,	143,	13,	49,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 11:35:16'),
(475,	146,	44,	48,	'abcd',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-18 11:45:06'),
(476,	146,	3,	48,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-18 11:45:10'),
(477,	146,	42,	48,	'vishwa',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-18 11:45:10'),
(478,	146,	43,	48,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-18 11:45:13'),
(479,	144,	13,	17,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 11:47:26'),
(480,	144,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:47:50'),
(482,	143,	13,	49,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 11:50:40'),
(483,	143,	7,	49,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:51:04'),
(484,	147,	13,	17,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 11:53:06'),
(485,	147,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 11:53:30'),
(488,	149,	13,	17,	'Y',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-18 12:01:18'),
(489,	149,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-18 12:01:42'),
(492,	151,	44,	49,	'abcd',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-19 05:44:26'),
(493,	151,	7,	49,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 05:44:51'),
(494,	152,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-19 05:46:14'),
(495,	152,	7,	49,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 05:46:38'),
(496,	153,	44,	49,	'abcd',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-19 05:47:27'),
(497,	153,	7,	49,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 05:47:51'),
(500,	154,	43,	45,	'test123',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-19 06:52:59'),
(501,	154,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 06:53:23'),
(502,	154,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 06:53:33'),
(503,	154,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 06:53:43'),
(507,	155,	43,	17,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-19 08:09:10'),
(508,	155,	44,	17,	'abcd',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-19 08:09:10'),
(510,	156,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 10:37:42'),
(511,	156,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 10:38:06'),
(512,	157,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 10:42:48'),
(513,	157,	7,	17,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 10:43:12'),
(514,	158,	3,	17,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 10:44:55'),
(515,	158,	8,	17,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 10:45:19'),
(516,	159,	43,	45,	'test123',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-19 11:16:58'),
(517,	159,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:17:22'),
(518,	159,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:17:32'),
(519,	159,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:17:43'),
(520,	160,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 11:46:37'),
(521,	160,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:47:02'),
(522,	160,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:47:12'),
(523,	160,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:47:22'),
(524,	161,	43,	17,	'test123',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 11:49:15'),
(525,	161,	6,	17,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:49:39'),
(526,	162,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 11:57:33'),
(527,	162,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:57:57'),
(528,	162,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:58:07'),
(529,	162,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 11:58:17'),
(530,	163,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 12:04:36'),
(531,	163,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:05:00'),
(532,	163,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:05:10'),
(533,	163,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:05:20'),
(534,	164,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 12:07:00'),
(535,	164,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:07:24'),
(536,	164,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:07:34'),
(537,	164,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:07:44'),
(538,	165,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 12:09:30'),
(539,	165,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:09:54'),
(540,	165,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:10:04'),
(541,	165,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:10:14'),
(542,	166,	44,	48,	'abcd',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 12:44:14'),
(543,	166,	3,	48,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 12:44:19'),
(544,	166,	42,	48,	'vishwa',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-19 12:44:30'),
(545,	166,	7,	48,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 12:44:39'),
(546,	167,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-19 13:17:53'),
(547,	167,	7,	49,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:18:17'),
(548,	168,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-19 13:25:24'),
(549,	168,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:25:48'),
(550,	168,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:25:58'),
(551,	168,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:26:08'),
(552,	169,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-19 13:28:28'),
(553,	169,	8,	49,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:28:52'),
(554,	170,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Disconnect',	'2021-05-19 13:31:33'),
(555,	170,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:31:58'),
(556,	170,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:32:08'),
(557,	170,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:32:18'),
(559,	171,	3,	48,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-19 13:33:56'),
(560,	171,	44,	48,	'abcd',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-19 13:33:57'),
(561,	171,	42,	48,	'vishwa',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-19 13:34:05'),
(562,	171,	7,	48,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:34:20'),
(563,	172,	3,	48,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-19 13:38:07'),
(564,	172,	6,	48,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:38:32'),
(566,	172,	42,	48,	'vishwa',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-19 13:38:35'),
(567,	172,	8,	48,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:38:42'),
(572,	174,	44,	48,	'abcd',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-19 13:40:48'),
(573,	173,	3,	45,	'newsam',	'No',	'No',	'Blue',	'Real',	'Active',	'2021-05-19 13:42:06'),
(574,	173,	44,	45,	'abcd',	'No',	'No',	'Blue',	'Real',	'Disconnect',	'2021-05-19 13:42:12'),
(575,	173,	43,	45,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-19 13:42:13'),
(576,	173,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-19 13:42:30'),
(578,	176,	43,	49,	'test123',	'No',	'No',	'Green',	'Real',	'Disconnect',	'2021-05-20 05:43:35'),
(579,	176,	6,	49,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-20 05:43:59'),
(580,	177,	42,	45,	'vishwa',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-20 06:02:03'),
(581,	177,	7,	45,	'Bot2',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-20 06:02:27'),
(582,	177,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-20 06:02:37'),
(583,	177,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-20 06:02:48'),
(584,	178,	43,	45,	'test123',	'No',	'No',	'Green',	'Real',	'Active',	'2021-05-20 06:07:44'),
(585,	178,	3,	45,	'newsam',	'No',	'No',	'Red',	'Real',	'Active',	'2021-05-20 06:07:50'),
(586,	178,	6,	45,	'Bot1',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-20 06:08:08'),
(587,	178,	8,	45,	'Bot3',	'No',	'No',	'Blue',	'Bot',	'Active',	'2021-05-20 06:08:19');

DROP TABLE IF EXISTS `ludo_join_tour_rooms`;
CREATE TABLE `ludo_join_tour_rooms` (
  `joinTourRoomId` double NOT NULL AUTO_INCREMENT,
  `tournamentId` double NOT NULL,
  `noOfPlayers` int(11) NOT NULL,
  `activePlayer` int(11) NOT NULL,
  `currentRound` int(11) NOT NULL,
  `betValue` int(11) NOT NULL,
  `gameStatus` enum('Pending','Active','Complete') NOT NULL DEFAULT 'Pending',
  `gameMode` enum('Quick','Classic') NOT NULL,
  `isPrivate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isFree` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isTournament` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isDelete` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`joinTourRoomId`),
  KEY `joinRoomId` (`joinTourRoomId`),
  KEY `roomId` (`tournamentId`),
  KEY `noOfPlayers` (`noOfPlayers`),
  KEY `activePlayer` (`activePlayer`),
  KEY `betValue` (`betValue`),
  KEY `gameStatus` (`gameStatus`),
  KEY `gameMode` (`gameMode`),
  KEY `isPrivate` (`isPrivate`),
  KEY `isFree` (`isFree`),
  KEY `created` (`created`),
  KEY `isTournament` (`isTournament`),
  KEY `isDelete` (`isDelete`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=latin1;

INSERT INTO `ludo_join_tour_rooms` (`joinTourRoomId`, `tournamentId`, `noOfPlayers`, `activePlayer`, `currentRound`, `betValue`, `gameStatus`, `gameMode`, `isPrivate`, `isFree`, `isTournament`, `isDelete`, `created`, `modified`) VALUES
(11,	8,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-09 16:12:57',	'2020-06-09 16:12:57'),
(12,	9,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-09 16:17:35',	'2020-06-09 16:17:59'),
(13,	11,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-09 19:54:39',	'2020-06-09 19:54:40'),
(14,	11,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-09 19:58:01',	'2020-06-09 19:58:01'),
(15,	12,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-09 20:02:17',	'2020-06-09 20:02:17'),
(16,	13,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-09 20:07:54',	'2020-06-09 20:07:55'),
(17,	14,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-09 20:14:34',	'2020-06-09 20:14:36'),
(18,	14,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-09 20:14:47',	'2020-06-09 20:21:16'),
(19,	15,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-10 10:20:26',	'2020-06-10 10:20:50'),
(20,	17,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-10 12:05:02',	'2020-06-10 12:05:12'),
(21,	22,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-10 18:27:34',	'2020-06-10 18:27:36'),
(22,	24,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-10 19:19:59',	'2020-06-10 19:20:00'),
(23,	25,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-10 19:29:36',	'2020-06-10 19:29:38'),
(24,	27,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-14 16:00:02',	'2020-06-14 16:00:02'),
(25,	27,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-14 16:00:02',	'2020-06-14 16:00:02'),
(26,	27,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-14 16:00:03',	'2020-06-14 16:02:15'),
(27,	27,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-14 16:04:34',	'2020-06-14 16:04:34'),
(28,	28,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-15 13:30:30',	'2020-06-15 13:30:33'),
(29,	28,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-15 13:30:42',	'2020-06-15 13:30:42'),
(30,	29,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-15 13:40:28',	'2020-06-15 13:40:37'),
(31,	29,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-15 13:40:38',	'2020-06-15 13:40:38'),
(32,	33,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 13:12:56',	'2020-06-16 13:13:05'),
(33,	33,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 13:13:17',	'2020-06-16 13:13:19'),
(34,	33,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 13:17:57',	'2020-06-16 13:18:02'),
(35,	34,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 15:09:57',	'2020-06-16 15:09:58'),
(36,	34,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-16 15:10:12',	'2020-06-16 15:10:12'),
(37,	35,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 15:31:44',	'2020-06-16 15:31:52'),
(38,	35,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-16 15:32:28',	'2020-06-16 15:32:28'),
(39,	36,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 15:44:59',	'2020-06-16 15:45:12'),
(40,	36,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 15:45:39',	'2020-06-16 15:51:42'),
(41,	36,	2,	0,	2,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-16 15:52:10',	'2020-06-16 15:52:10'),
(42,	37,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 16:56:17',	'2020-06-16 16:56:40'),
(43,	37,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-16 16:57:12',	'2020-06-16 16:57:12'),
(44,	38,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 17:08:38',	'2020-06-16 17:08:47'),
(45,	38,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-16 17:09:10',	'2020-06-16 17:09:10'),
(46,	39,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 17:45:48',	'2020-06-16 17:46:08'),
(47,	39,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-16 17:46:29',	'2020-06-16 17:46:29'),
(48,	40,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 18:11:07',	'2020-06-16 18:11:07'),
(49,	40,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 18:11:08',	'2020-06-16 18:11:16'),
(50,	41,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-16 18:21:21',	'2020-06-16 18:22:23'),
(51,	41,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 18:22:28',	'2020-06-16 18:22:30'),
(52,	42,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 18:29:29',	'2020-06-16 18:29:29'),
(53,	42,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 18:29:30',	'2020-06-16 18:29:43'),
(54,	43,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 18:37:01',	'2020-06-16 18:38:06'),
(55,	43,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-16 18:38:06',	'2020-06-16 18:38:26'),
(56,	44,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-17 12:15:29',	'2020-06-17 12:15:38'),
(57,	44,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-06-17 12:15:58',	'2020-06-17 12:15:58'),
(58,	45,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-17 12:51:40',	'2020-06-17 12:51:56'),
(59,	45,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-17 12:51:57',	'2020-06-17 12:51:57'),
(60,	45,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-17 12:56:57',	'2020-06-17 12:57:37'),
(61,	46,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-17 16:32:27',	'2020-06-17 16:32:52'),
(62,	46,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-17 16:32:53',	'2020-06-17 16:33:11'),
(63,	46,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-06-17 16:38:06',	'2020-06-17 16:38:11'),
(64,	47,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-07-03 18:15:32',	'2020-07-03 18:15:34'),
(65,	47,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-07-03 18:16:29',	'2020-07-03 18:21:14'),
(66,	47,	2,	0,	2,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-07-03 18:21:14',	'2020-07-03 18:21:14'),
(67,	50,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-22 18:15:05',	'2020-10-22 18:16:15'),
(68,	50,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-10-22 18:16:42',	'2020-10-22 18:16:42'),
(69,	51,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-23 12:30:02',	'2020-10-23 12:30:02'),
(70,	51,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-23 12:30:03',	'2020-10-23 12:30:08'),
(71,	51,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-10-23 12:30:17',	'2020-10-23 12:30:17'),
(72,	52,	2,	0,	1,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-10-24 13:26:03',	'2020-10-24 13:26:04'),
(73,	53,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-24 13:36:04',	'2020-10-24 13:36:05'),
(74,	54,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-26 16:07:16',	'2020-10-26 16:07:16'),
(75,	55,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-26 16:23:03',	'2020-10-26 16:23:03'),
(76,	56,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-26 16:45:02',	'2020-10-26 16:45:04'),
(77,	56,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-26 16:45:34',	'2020-10-26 16:53:06'),
(78,	56,	2,	0,	2,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-10-26 16:53:08',	'2020-10-26 16:53:08'),
(79,	57,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-27 12:26:10',	'2020-10-27 12:26:11'),
(80,	58,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-27 17:20:02',	'2020-10-27 17:20:04'),
(81,	58,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-10-27 17:23:05',	'2020-10-27 17:23:05'),
(82,	59,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-27 17:42:04',	'2020-10-27 17:42:10'),
(83,	59,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-27 17:42:12',	'2020-10-27 17:43:04'),
(84,	60,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-27 20:45:12',	'2020-10-27 20:45:16'),
(85,	61,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-27 21:22:04',	'2020-10-27 21:22:08'),
(86,	61,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-27 21:22:09',	'2020-10-27 21:22:30'),
(87,	62,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-28 15:16:05',	'2020-10-28 15:16:06'),
(88,	62,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-28 15:16:10',	'2020-10-28 15:16:34'),
(89,	62,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-28 16:34:11',	'2020-10-28 16:34:11'),
(90,	63,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-29 11:18:28',	'2020-10-29 11:18:36'),
(91,	63,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-10-29 11:18:38',	'2020-10-29 11:18:52'),
(92,	64,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 12:06:28',	'2020-11-02 12:06:28'),
(93,	65,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 13:09:17',	'2020-11-02 13:09:26'),
(94,	65,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 13:09:43',	'2020-11-02 13:09:44'),
(95,	65,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 13:09:44',	'2020-11-02 13:10:00'),
(96,	66,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 13:28:30',	'2020-11-02 13:28:47'),
(97,	66,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-11-02 13:28:57',	'2020-11-02 13:28:57'),
(98,	67,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-11-02 13:49:04',	'2020-11-02 13:49:04'),
(99,	68,	3,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 14:13:30',	'2020-11-02 14:14:08'),
(100,	65,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 14:29:56',	'2020-11-02 14:30:05'),
(101,	69,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 18:16:43',	'2020-11-02 18:16:46'),
(102,	69,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-02 18:16:51',	'2020-11-02 18:16:53'),
(103,	69,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-11-02 18:17:19',	'2020-11-02 18:17:19'),
(104,	70,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 13:05:43',	'2020-11-03 13:05:44'),
(105,	70,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-11-03 13:06:53',	'2020-11-03 13:06:53'),
(106,	71,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 13:34:37',	'2020-11-03 13:34:46'),
(107,	71,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 13:34:54',	'2020-11-03 13:34:55'),
(108,	72,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 14:36:00',	'2020-11-03 14:37:09'),
(109,	72,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 14:37:17',	'2020-11-03 14:37:50'),
(110,	73,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 14:58:35',	'2020-11-03 14:58:38'),
(111,	73,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-11-03 14:58:47',	'2020-11-03 14:58:47'),
(112,	74,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 19:05:13',	'2020-11-03 19:05:14'),
(113,	74,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 19:05:30',	'2020-11-03 19:05:41'),
(114,	74,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-03 20:21:17',	'2020-11-03 20:21:19'),
(115,	75,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 13:45:39',	'2020-11-04 13:45:41'),
(116,	75,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 13:45:46',	'2020-11-04 13:46:32'),
(117,	76,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 14:14:02',	'2020-11-04 14:14:04'),
(118,	76,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 14:14:08',	'2020-11-04 14:14:08'),
(119,	76,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 15:30:21',	'2020-11-04 15:30:23'),
(120,	77,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 16:57:22',	'2020-11-04 16:57:23'),
(121,	77,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 16:57:24',	'2020-11-04 16:57:27'),
(122,	77,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 16:57:29',	'2020-11-04 16:57:31'),
(123,	77,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 16:57:34',	'2020-11-04 16:57:34'),
(124,	77,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 16:57:35',	'2020-11-04 16:57:39'),
(125,	77,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 16:57:40',	'2020-11-04 16:57:44'),
(126,	77,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 16:58:21',	'2020-11-04 16:59:18'),
(127,	77,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 17:00:10',	'2020-11-04 18:22:26'),
(128,	77,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 18:22:28',	'2020-11-04 18:22:28'),
(129,	77,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 18:22:29',	'2020-11-04 18:22:29'),
(130,	77,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 18:22:34',	'2020-11-04 18:22:34'),
(131,	77,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 18:22:36',	'2020-11-04 19:27:37'),
(132,	77,	2,	0,	3,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 19:27:38',	'2020-11-04 20:32:28'),
(133,	77,	2,	0,	4,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-04 20:32:49',	'2020-11-04 20:35:07'),
(134,	78,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-06 12:02:29',	'2020-11-06 12:02:30'),
(135,	78,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-06 12:02:36',	'2020-11-06 12:02:38'),
(136,	78,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-06 13:13:56',	'2020-11-06 13:13:59'),
(137,	79,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-06 15:34:34',	'2020-11-06 15:34:37'),
(138,	79,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-06 15:34:49',	'2020-11-06 15:34:58'),
(139,	79,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-06 16:50:18',	'2020-11-06 16:50:31'),
(140,	80,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-10 15:57:02',	'2020-11-10 15:57:22'),
(141,	80,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-10 15:57:34',	'2020-11-10 17:08:59'),
(142,	80,	2,	0,	2,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-11-10 17:09:01',	'2020-11-10 17:09:01'),
(143,	81,	2,	0,	1,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 16:18:02',	'2020-11-26 16:18:02'),
(144,	81,	2,	0,	1,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 16:18:02',	'2020-11-26 16:18:02'),
(145,	81,	2,	0,	1,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 16:18:03',	'2020-11-26 16:18:04'),
(146,	81,	2,	0,	1,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 16:18:04',	'2020-11-26 16:18:05'),
(147,	81,	2,	0,	1,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 16:18:07',	'2020-11-26 16:18:09'),
(148,	81,	2,	0,	1,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 16:18:12',	'2020-11-26 16:18:25'),
(149,	81,	2,	0,	1,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 16:18:38',	'2020-11-26 16:18:56'),
(150,	82,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-26 16:55:02',	'2020-11-26 16:55:02'),
(151,	82,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-26 16:55:02',	'2020-11-26 16:55:03'),
(152,	82,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-26 16:55:04',	'2020-11-26 16:55:05'),
(153,	82,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-26 16:55:05',	'2020-11-26 16:55:05'),
(154,	82,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-26 16:55:05',	'2020-11-26 16:55:06'),
(155,	82,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-26 16:55:06',	'2020-11-26 16:55:07'),
(156,	82,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-26 16:55:08',	'2020-11-26 16:55:09'),
(157,	82,	2,	0,	1,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-11-26 16:55:13',	'2020-11-26 16:55:13'),
(158,	81,	2,	0,	2,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 17:28:09',	'2020-11-26 17:28:09'),
(159,	81,	2,	0,	2,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 17:28:10',	'2020-11-26 17:28:11'),
(160,	81,	2,	0,	2,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 17:28:14',	'2020-11-26 17:28:48'),
(161,	81,	2,	0,	2,	0,	'Active',	'Quick',	'No',	'No',	'No',	'No',	'2020-11-26 17:31:11',	'2020-11-26 18:33:11'),
(162,	81,	2,	0,	3,	0,	'Pending',	'Quick',	'No',	'No',	'No',	'Yes',	'2020-11-26 18:33:12',	'2020-11-26 18:33:12'),
(163,	83,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 11:43:02',	'2020-11-27 11:43:05'),
(164,	83,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 11:43:06',	'2020-11-27 11:43:06'),
(165,	83,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 11:43:06',	'2020-11-27 11:43:08'),
(166,	83,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 11:43:09',	'2020-11-27 11:43:09'),
(167,	83,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 11:43:10',	'2020-11-27 11:43:10'),
(168,	83,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 11:43:10',	'2020-11-27 11:43:14'),
(169,	83,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 11:43:14',	'2020-11-27 11:43:18'),
(170,	83,	2,	0,	1,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 11:43:19',	'2020-11-27 11:43:19'),
(171,	83,	2,	0,	2,	0,	'Active',	'Classic',	'No',	'No',	'No',	'No',	'2020-11-27 12:59:08',	'2020-11-27 12:59:09'),
(172,	83,	2,	0,	3,	0,	'Pending',	'Classic',	'No',	'No',	'No',	'Yes',	'2020-11-27 14:04:11',	'2020-11-27 14:04:11');

DROP TABLE IF EXISTS `ludo_join_tour_room_users`;
CREATE TABLE `ludo_join_tour_room_users` (
  `joinTourRoomUserId` double NOT NULL AUTO_INCREMENT,
  `joinTourRoomId` double NOT NULL,
  `userId` double NOT NULL,
  `tournamentId` int(11) NOT NULL,
  `currentRound` int(11) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `isWin` enum('Yes','No') NOT NULL DEFAULT 'No',
  `tokenColor` enum('Red','Blue','Yellow','Green') NOT NULL,
  `playerType` enum('Real','Bot') NOT NULL DEFAULT 'Real',
  `status` enum('Active','Inactive','Disconnect') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  PRIMARY KEY (`joinTourRoomUserId`),
  KEY `joinRoomId` (`joinTourRoomId`),
  KEY `userId` (`userId`),
  KEY `userName` (`userName`),
  KEY `isWin` (`isWin`),
  KEY `tokenColor` (`tokenColor`),
  KEY `playerType` (`playerType`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=320 DEFAULT CHARSET=latin1;

INSERT INTO `ludo_join_tour_room_users` (`joinTourRoomUserId`, `joinTourRoomId`, `userId`, `tournamentId`, `currentRound`, `userName`, `isWin`, `tokenColor`, `playerType`, `status`, `created`) VALUES
(2,	2,	1,	5,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-05 16:46:16'),
(3,	3,	27,	6,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-05 17:42:00'),
(4,	3,	1,	6,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-05 17:42:03'),
(5,	4,	1,	6,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-05 18:27:07'),
(6,	4,	27,	6,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-05 18:27:11'),
(7,	5,	25,	7,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 10:40:25'),
(8,	5,	1,	7,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 10:40:41'),
(9,	6,	1,	8,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 15:57:47'),
(10,	6,	48,	8,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 15:57:53'),
(11,	7,	48,	8,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:02:47'),
(12,	7,	1,	8,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:02:50'),
(13,	8,	1,	8,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:03:24'),
(14,	8,	48,	8,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:03:26'),
(15,	9,	48,	8,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:04:16'),
(16,	9,	1,	8,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:04:23'),
(17,	10,	1,	8,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:09:27'),
(18,	10,	48,	8,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:09:30'),
(19,	11,	1,	8,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:12:57'),
(20,	12,	1,	9,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:17:35'),
(21,	12,	48,	9,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 16:17:59'),
(22,	13,	27,	11,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 19:54:39'),
(23,	13,	1,	11,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 19:54:40'),
(24,	14,	1,	11,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 19:58:01'),
(25,	15,	1,	12,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 20:02:17'),
(26,	16,	1,	13,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 20:07:54'),
(27,	16,	48,	13,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 20:07:55'),
(28,	17,	1,	14,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 20:14:34'),
(29,	17,	27,	14,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 20:14:36'),
(30,	18,	25,	14,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 20:14:47'),
(31,	18,	27,	14,	2,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-09 20:21:16'),
(32,	19,	1,	15,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 10:20:26'),
(33,	19,	25,	15,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 10:20:50'),
(34,	20,	1,	17,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 12:05:02'),
(35,	20,	25,	17,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 12:05:12'),
(36,	21,	1,	22,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 18:27:34'),
(37,	21,	25,	22,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 18:27:36'),
(38,	22,	27,	24,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 19:19:59'),
(39,	22,	1,	24,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 19:20:00'),
(40,	23,	1,	25,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 19:29:36'),
(41,	23,	27,	25,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-10 19:29:38'),
(42,	24,	49,	27,	1,	'yash',	'No',	'Blue',	'Real',	'Active',	'2020-06-14 16:00:02'),
(43,	24,	51,	27,	1,	'Mahesh Gaikwad',	'No',	'Blue',	'Real',	'Active',	'2020-06-14 16:00:02'),
(44,	25,	30,	27,	1,	'nil08gawade',	'No',	'Blue',	'Real',	'Active',	'2020-06-14 16:00:02'),
(45,	25,	5,	27,	1,	'sumit@2612',	'No',	'Blue',	'Real',	'Active',	'2020-06-14 16:00:02'),
(46,	26,	14,	27,	1,	'Nitesh Dhotre',	'No',	'Blue',	'Real',	'Active',	'2020-06-14 16:00:03'),
(47,	26,	14,	27,	1,	'Nitesh Dhotre',	'No',	'Blue',	'Real',	'Active',	'2020-06-14 16:02:15'),
(48,	27,	14,	27,	1,	'Nitesh Dhotre',	'No',	'Blue',	'Real',	'Active',	'2020-06-14 16:04:34'),
(49,	28,	1,	28,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-15 13:30:30'),
(50,	28,	48,	28,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-15 13:30:33'),
(51,	29,	27,	28,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-15 13:30:42'),
(52,	30,	1,	29,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-15 13:40:28'),
(53,	30,	27,	29,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-15 13:40:37'),
(54,	31,	48,	29,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-15 13:40:38'),
(55,	32,	27,	33,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 13:12:56'),
(56,	32,	1,	33,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 13:13:05'),
(57,	33,	46,	33,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 13:13:17'),
(58,	33,	25,	33,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 13:13:19'),
(59,	34,	27,	33,	2,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 13:17:57'),
(60,	34,	25,	33,	2,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 13:18:02'),
(61,	35,	25,	34,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:09:57'),
(62,	35,	46,	34,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:09:58'),
(63,	36,	1,	34,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:10:12'),
(64,	37,	46,	35,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:31:44'),
(65,	37,	25,	35,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:31:52'),
(66,	38,	1,	35,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:32:28'),
(67,	39,	25,	36,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:44:59'),
(68,	39,	46,	36,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:45:12'),
(69,	40,	1,	36,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:45:39'),
(70,	40,	1,	36,	2,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:51:42'),
(71,	41,	25,	36,	2,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 15:52:10'),
(72,	42,	25,	37,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 16:56:17'),
(73,	42,	46,	37,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 16:56:40'),
(74,	43,	1,	37,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 16:57:12'),
(75,	44,	25,	38,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 17:08:38'),
(76,	44,	46,	38,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 17:08:47'),
(77,	45,	1,	38,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 17:09:10'),
(78,	46,	27,	39,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 17:45:48'),
(79,	46,	46,	39,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 17:46:08'),
(80,	47,	1,	39,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 17:46:29'),
(81,	48,	46,	40,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:11:07'),
(82,	48,	27,	40,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:11:07'),
(83,	49,	1,	40,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:11:08'),
(84,	49,	48,	40,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:11:16'),
(85,	50,	27,	41,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:21:21'),
(86,	50,	27,	41,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:22:11'),
(87,	50,	46,	41,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:22:23'),
(88,	51,	48,	41,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:22:28'),
(89,	51,	1,	41,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:22:30'),
(90,	52,	46,	42,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:29:29'),
(91,	52,	1,	42,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:29:29'),
(92,	53,	27,	42,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:29:30'),
(93,	53,	48,	42,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:29:43'),
(94,	54,	48,	43,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:37:01'),
(95,	54,	46,	43,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:38:05'),
(96,	54,	1,	43,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:38:06'),
(97,	55,	27,	43,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:38:06'),
(98,	55,	48,	43,	1,	'Ashish',	'No',	'Blue',	'Real',	'Active',	'2020-06-16 18:38:26'),
(99,	56,	25,	44,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:15:29'),
(100,	56,	46,	44,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:15:38'),
(101,	57,	1,	44,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:15:58'),
(102,	58,	25,	45,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:51:40'),
(103,	58,	46,	45,	1,	'vishwa1',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:51:56'),
(104,	59,	1,	45,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:51:57'),
(105,	59,	27,	45,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:51:57'),
(106,	60,	27,	45,	2,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:56:57'),
(107,	60,	25,	45,	2,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 12:57:37'),
(108,	61,	27,	46,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 16:32:27'),
(109,	61,	25,	46,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 16:32:52'),
(110,	62,	1,	46,	1,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 16:32:53'),
(111,	62,	53,	46,	1,	'test',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 16:33:11'),
(112,	63,	1,	46,	2,	'Vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 16:38:06'),
(113,	63,	27,	46,	2,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-06-17 16:38:11'),
(114,	64,	25,	47,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-07-03 18:15:32'),
(115,	64,	53,	47,	1,	'test',	'No',	'Blue',	'Real',	'Active',	'2020-07-03 18:15:34'),
(116,	65,	27,	47,	1,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-07-03 18:16:29'),
(117,	65,	53,	47,	2,	'test',	'No',	'Blue',	'Real',	'Active',	'2020-07-03 18:21:14'),
(118,	66,	27,	47,	2,	'rajan',	'No',	'Blue',	'Real',	'Active',	'2020-07-03 18:21:14'),
(119,	67,	73300,	50,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-10-22 18:15:05'),
(120,	67,	73089,	50,	1,	'mahi',	'No',	'Blue',	'Real',	'Active',	'2020-10-22 18:16:15'),
(121,	68,	79186,	50,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-22 18:16:42'),
(122,	69,	73089,	51,	1,	'mahi',	'No',	'Blue',	'Real',	'Active',	'2020-10-23 12:30:02'),
(123,	69,	81635,	51,	1,	'Anjali G',	'No',	'Blue',	'Real',	'Active',	'2020-10-23 12:30:02'),
(124,	70,	73300,	51,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-10-23 12:30:03'),
(125,	70,	79186,	51,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-23 12:30:08'),
(126,	71,	76162,	51,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-23 12:30:17'),
(127,	72,	73109,	52,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-24 13:26:03'),
(128,	72,	2204,	52,	1,	'Rajan',	'No',	'Blue',	'Real',	'Active',	'2020-10-24 13:26:04'),
(129,	73,	2204,	53,	1,	'Rajan',	'No',	'Blue',	'Real',	'Active',	'2020-10-24 13:36:04'),
(130,	73,	73109,	53,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-24 13:36:05'),
(131,	74,	73109,	54,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:07:16'),
(132,	74,	76581,	54,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:07:16'),
(133,	75,	73109,	55,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:23:03'),
(134,	75,	76581,	55,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:23:03'),
(135,	76,	73109,	56,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:45:02'),
(136,	76,	2204,	56,	1,	'Rajan',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:45:04'),
(137,	77,	76581,	56,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:45:34'),
(138,	77,	76581,	56,	2,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:53:06'),
(139,	78,	73109,	56,	2,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-26 16:53:08'),
(140,	79,	76581,	57,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 12:26:10'),
(141,	79,	73109,	57,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 12:26:11'),
(142,	80,	79186,	58,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 17:20:02'),
(143,	80,	76162,	58,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 17:20:04'),
(144,	81,	76092,	58,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 17:23:05'),
(145,	82,	73300,	59,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 17:42:04'),
(146,	82,	76092,	59,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 17:42:10'),
(147,	83,	79186,	59,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 17:42:12'),
(148,	83,	76162,	59,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 17:43:04'),
(149,	84,	76581,	60,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 20:45:12'),
(150,	84,	73109,	60,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 20:45:16'),
(151,	85,	79186,	61,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 21:22:04'),
(152,	85,	73300,	61,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 21:22:08'),
(153,	86,	73109,	61,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 21:22:09'),
(154,	86,	76162,	61,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-10-27 21:22:30'),
(155,	87,	76092,	62,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-10-28 15:16:05'),
(156,	87,	79186,	62,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-28 15:16:06'),
(157,	88,	76162,	62,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-10-28 15:16:10'),
(158,	88,	73109,	62,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-28 15:16:34'),
(159,	89,	79186,	62,	2,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-28 16:34:11'),
(160,	89,	73109,	62,	2,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-10-28 16:34:11'),
(161,	90,	76092,	63,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-10-29 11:18:28'),
(162,	90,	73300,	63,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-10-29 11:18:36'),
(163,	91,	79186,	63,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-10-29 11:18:38'),
(164,	91,	76162,	63,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-10-29 11:18:52'),
(165,	92,	73109,	64,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 12:06:28'),
(166,	92,	76581,	64,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 12:06:28'),
(167,	93,	73109,	65,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:09:17'),
(168,	93,	76162,	65,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:09:26'),
(169,	94,	73300,	65,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:09:43'),
(170,	94,	79186,	65,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:09:44'),
(171,	95,	73104,	65,	1,	'Amol G',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:09:44'),
(172,	95,	76092,	65,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:10:00'),
(173,	96,	76162,	66,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:28:30'),
(174,	96,	73109,	66,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:28:47'),
(175,	97,	79186,	66,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:28:57'),
(176,	98,	73109,	67,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 13:49:04'),
(177,	99,	76162,	68,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 14:13:30'),
(178,	99,	73300,	68,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 14:13:46'),
(179,	99,	73109,	68,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 14:14:08'),
(180,	100,	73104,	65,	2,	'Amol G',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 14:29:56'),
(181,	100,	73300,	65,	2,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 14:30:05'),
(182,	101,	79186,	69,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 18:16:43'),
(183,	101,	76092,	69,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 18:16:46'),
(184,	102,	73300,	69,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 18:16:51'),
(185,	102,	73104,	69,	1,	'Amol G',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 18:16:53'),
(186,	103,	76162,	69,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-02 18:17:19'),
(187,	104,	76092,	70,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 13:05:43'),
(188,	104,	79186,	70,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 13:05:44'),
(189,	105,	73109,	70,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 13:06:53'),
(190,	106,	79186,	71,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 13:34:37'),
(191,	106,	73109,	71,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 13:34:46'),
(192,	107,	76581,	71,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 13:34:54'),
(193,	107,	73300,	71,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 13:34:55'),
(194,	108,	79186,	72,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 14:36:00'),
(195,	108,	79186,	72,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 14:37:09'),
(196,	109,	73109,	72,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 14:37:17'),
(197,	109,	76581,	72,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 14:37:50'),
(198,	110,	73109,	73,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 14:58:35'),
(199,	110,	79186,	73,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 14:58:38'),
(200,	111,	76581,	73,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 14:58:47'),
(201,	112,	76162,	74,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 19:05:13'),
(202,	112,	76092,	74,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 19:05:14'),
(203,	113,	73300,	74,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 19:05:30'),
(204,	113,	79186,	74,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 19:05:41'),
(205,	114,	73300,	74,	2,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 20:21:17'),
(206,	114,	76092,	74,	2,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-03 20:21:19'),
(207,	115,	76092,	75,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 13:45:39'),
(208,	115,	76162,	75,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 13:45:41'),
(209,	116,	73300,	75,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 13:45:46'),
(210,	116,	79186,	75,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 13:46:32'),
(211,	117,	76162,	76,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 14:14:02'),
(212,	117,	76092,	76,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 14:14:04'),
(213,	118,	73300,	76,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 14:14:08'),
(214,	118,	79186,	76,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 14:14:08'),
(215,	119,	73300,	76,	2,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 15:30:21'),
(216,	119,	76092,	76,	2,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 15:30:23'),
(217,	120,	100751,	77,	1,	'Harshal',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:22'),
(218,	120,	100800,	77,	1,	'Sohail Mujawar',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:23'),
(219,	121,	157,	77,	1,	'Ramkrushna Kadam',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:24'),
(220,	121,	70961,	77,	1,	'ji',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:27'),
(221,	122,	76092,	77,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:29'),
(222,	122,	100758,	77,	1,	'Mayuri',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:31'),
(223,	123,	73300,	77,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:34'),
(224,	123,	79186,	77,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:34'),
(225,	124,	100750,	77,	1,	'piyush bhandari',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:35'),
(226,	124,	100763,	77,	1,	'shraddha Jadhav',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:39'),
(227,	125,	76162,	77,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:40'),
(228,	125,	100801,	77,	1,	'Swapnali Dabhade',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:57:44'),
(229,	126,	100752,	77,	1,	'shubham jagtap',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:58:21'),
(230,	126,	100788,	77,	1,	'Mamta',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 16:59:18'),
(231,	127,	81635,	77,	1,	'Anjali G',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 17:00:10'),
(232,	127,	70961,	77,	2,	'ji',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 18:22:26'),
(233,	128,	100752,	77,	2,	'shubham jagtap',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 18:22:28'),
(234,	128,	100800,	77,	2,	'Sohail Mujawar',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 18:22:28'),
(235,	129,	73300,	77,	2,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 18:22:29'),
(236,	129,	76162,	77,	2,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 18:22:29'),
(237,	130,	100750,	77,	2,	'piyush bhandari',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 18:22:34'),
(238,	130,	81635,	77,	2,	'Anjali G',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 18:22:34'),
(239,	131,	100758,	77,	2,	'Mayuri',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 18:22:36'),
(240,	131,	100752,	77,	3,	'shubham jagtap',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 19:27:28'),
(241,	131,	76162,	77,	3,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 19:27:37'),
(242,	132,	100750,	77,	3,	'piyush bhandari',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 19:27:38'),
(243,	132,	100750,	77,	4,	'piyush bhandari',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 20:32:28'),
(244,	133,	76162,	77,	4,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 20:32:49'),
(245,	133,	100750,	77,	4,	'piyush bhandari',	'No',	'Blue',	'Real',	'Active',	'2020-11-04 20:35:07'),
(246,	134,	73300,	78,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 12:02:29'),
(247,	134,	76162,	78,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 12:02:30'),
(248,	135,	76092,	78,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 12:02:36'),
(249,	135,	79186,	78,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 12:02:38'),
(250,	136,	76092,	78,	2,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 13:13:56'),
(251,	136,	73300,	78,	2,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 13:13:59'),
(252,	137,	76162,	79,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 15:34:34'),
(253,	137,	76092,	79,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 15:34:37'),
(254,	138,	79186,	79,	1,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 15:34:49'),
(255,	138,	73300,	79,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 15:34:58'),
(256,	139,	79186,	79,	2,	'aishu',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 16:50:18'),
(257,	139,	76092,	79,	2,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-06 16:50:31'),
(258,	140,	73109,	80,	1,	'smstest',	'No',	'Blue',	'Real',	'Active',	'2020-11-10 15:57:02'),
(259,	140,	76581,	80,	1,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-11-10 15:57:22'),
(260,	141,	22325,	80,	1,	'vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-11-10 15:57:34'),
(261,	141,	22325,	80,	2,	'vishwa',	'No',	'Blue',	'Real',	'Active',	'2020-11-10 17:08:59'),
(262,	142,	76581,	80,	2,	'testplayer',	'No',	'Blue',	'Real',	'Active',	'2020-11-10 17:09:01'),
(263,	143,	123732,	81,	1,	'varun',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:02'),
(264,	143,	123729,	81,	1,	'raj gaikwad',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:02'),
(265,	144,	100750,	81,	1,	'piyush bhandari',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:02'),
(266,	144,	123709,	81,	1,	'pragati',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:02'),
(267,	145,	70961,	81,	1,	'ji',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:03'),
(268,	145,	73300,	81,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:04'),
(269,	146,	100758,	81,	1,	'Mayuri',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:04'),
(270,	146,	123727,	81,	1,	'rubina',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:05'),
(271,	147,	73104,	81,	1,	'Amol G',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:07'),
(272,	147,	76162,	81,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:09'),
(273,	148,	100751,	81,	1,	'Harshal',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:12'),
(274,	148,	100797,	81,	1,	'Shabista',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:25'),
(275,	149,	76092,	81,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:38'),
(276,	149,	123725,	81,	1,	'shraddha ',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:18:56'),
(277,	150,	70961,	82,	1,	'ji',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:02'),
(278,	150,	123731,	82,	1,	'Rushi sathe ',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:02'),
(279,	151,	100750,	82,	1,	'piyush bhandari',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:02'),
(280,	151,	123709,	82,	1,	'pragati',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:03'),
(281,	152,	100758,	82,	1,	'Mayuri',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:04'),
(282,	152,	123732,	82,	1,	'varun',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:05'),
(283,	153,	100751,	82,	1,	'Harshal',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:05'),
(284,	153,	123728,	82,	1,	'jay',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:05'),
(285,	154,	73300,	82,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:05'),
(286,	154,	123694,	82,	1,	'ankita',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:06'),
(287,	155,	76162,	82,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:06'),
(288,	155,	73104,	82,	1,	'Amol G',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:07'),
(289,	156,	100800,	82,	1,	'Sohail Mujawar',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:08'),
(290,	156,	76092,	82,	1,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:09'),
(291,	157,	123725,	82,	1,	'shraddha ',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 16:55:13'),
(292,	158,	123709,	81,	2,	'pragati',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 17:28:09'),
(293,	158,	123727,	81,	2,	'rubina',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 17:28:09'),
(294,	159,	123729,	81,	2,	'raj gaikwad',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 17:28:10'),
(295,	159,	76162,	81,	2,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 17:28:11'),
(296,	160,	73300,	81,	2,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 17:28:14'),
(297,	160,	100751,	81,	2,	'Harshal',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 17:28:48'),
(298,	161,	76092,	81,	2,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 17:31:11'),
(299,	161,	123709,	81,	3,	'pragati',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 18:33:11'),
(300,	162,	76092,	81,	3,	'SK sanjay',	'No',	'Blue',	'Real',	'Active',	'2020-11-26 18:33:12'),
(301,	163,	100758,	83,	1,	'Mayuri',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:02'),
(302,	163,	157,	83,	1,	'R.K',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:05'),
(303,	164,	123728,	83,	1,	'jay',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:06'),
(304,	164,	70961,	83,	1,	'ji',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:06'),
(305,	165,	76162,	83,	1,	'aishu1',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:06'),
(306,	165,	100752,	83,	1,	'shubham jagtap',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:08'),
(307,	166,	81638,	83,	1,	'Osomose',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:09'),
(308,	166,	123727,	83,	1,	'rubina',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:09'),
(309,	167,	100763,	83,	1,	'shraddha Jadhav',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:10'),
(310,	167,	100800,	83,	1,	'Sohail Mujawar',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:10'),
(311,	168,	100750,	83,	1,	'piyush bhandari',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:10'),
(312,	168,	100751,	83,	1,	'Harshal',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:14'),
(313,	169,	123694,	83,	1,	'ankita',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:14'),
(314,	169,	123902,	83,	1,	'snehal',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:18'),
(315,	170,	81635,	83,	1,	'Anjali G',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:19'),
(316,	170,	73300,	83,	1,	'testing',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 11:43:19'),
(317,	171,	81638,	83,	2,	'Osomose',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 12:59:08'),
(318,	171,	100800,	83,	2,	'Sohail Mujawar',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 12:59:09'),
(319,	172,	81638,	83,	3,	'Osomose',	'No',	'Blue',	'Real',	'Active',	'2020-11-27 14:04:11');

DROP TABLE IF EXISTS `ludo_mst_rooms`;
CREATE TABLE `ludo_mst_rooms` (
  `roomId` int(11) NOT NULL AUTO_INCREMENT,
  `roomTitle` varchar(100) NOT NULL,
  `commision` double NOT NULL,
  `entryFee` varchar(200) NOT NULL,
  `players` varchar(100) NOT NULL,
  `mode` enum('Classic','Fancy') NOT NULL DEFAULT 'Classic',
  `startRoundTime` int(11) NOT NULL,
  `tokenMoveTime` int(11) NOT NULL,
  `rollDiceTime` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `isPrivate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `currentRoundBot` int(11) NOT NULL,
  `totalRoundBot` int(11) NOT NULL,
  `isBotConnect` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `reserveAmount` double NOT NULL,
  `killToken` double(11,2) NOT NULL,
  `firstToken` double(11,2) NOT NULL,
  `firstThreeToken` double(11,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`roomId`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

INSERT INTO `ludo_mst_rooms` (`roomId`, `roomTitle`, `commision`, `entryFee`, `players`, `mode`, `startRoundTime`, `tokenMoveTime`, `rollDiceTime`, `status`, `isPrivate`, `currentRoundBot`, `totalRoundBot`, `isBotConnect`, `reserveAmount`, `killToken`, `firstToken`, `firstThreeToken`, `created`, `modified`) VALUES
(17,	'15',	15,	'15',	'2',	'Classic',	60,	30,	30,	'Active',	'No',	7,	175,	'Yes',	0,	0.00,	0.00,	0.00,	'2020-02-18 16:24:47',	'2021-05-19 11:49:39'),
(35,	'Play with friends',	5,	'10-1000',	'4',	'Classic',	60,	30,	30,	'Active',	'Yes',	0,	0,	'',	0,	0.00,	0.00,	0.00,	'2020-02-18 16:41:56',	'2021-05-19 19:22:43'),
(38,	'2 players - 100 Rs',	15,	'100',	'2',	'Classic',	60,	30,	30,	'Active',	'No',	4,	4,	'Yes',	0,	0.00,	0.00,	0.00,	'2020-07-13 18:45:59',	'2021-05-17 12:16:32'),
(42,	'2 players - 30rs',	15,	'30',	'2',	'Classic',	60,	30,	30,	'Active',	'No',	0,	0,	'Yes',	0,	0.00,	0.00,	0.00,	'2020-07-30 10:45:16',	'2021-03-13 09:37:33'),
(45,	'4 player -  30 Rs',	15,	'30',	'4',	'Classic',	60,	30,	30,	'Active',	'No',	11,	23,	'Yes',	0,	0.00,	0.00,	0.00,	'2020-08-13 23:22:09',	'2021-05-20 06:08:19'),
(47,	'4 player - 15 Rs',	15,	'15',	'4',	'Fancy',	60,	30,	30,	'Active',	'No',	0,	0,	'Yes',	0,	0.00,	0.00,	0.00,	'2020-08-26 11:32:58',	'2021-05-06 19:12:24'),
(48,	'4 players - 10 rs',	10,	'10',	'4',	'Fancy',	60,	30,	30,	'Active',	'No',	6,	6,	'Yes',	0,	1.00,	2.00,	3.00,	'2020-08-26 11:33:45',	'2021-05-19 13:38:42'),
(49,	'2 player - 5 Rs',	5,	'5',	'2',	'Fancy',	60,	30,	30,	'Active',	'No',	11,	11,	'Yes',	0,	2.00,	4.00,	5.00,	'2020-08-26 11:38:48',	'2021-05-20 05:43:59'),
(50,	'2 player - 250 Rs',	15,	'250',	'2',	'Fancy',	60,	30,	30,	'Active',	'No',	0,	0,	'Yes',	0,	11.00,	0.00,	0.00,	'2020-10-24 18:05:18',	'2021-05-13 12:53:31'),
(51,	'2 players - 10 rs',	10,	'10',	'2',	'Fancy',	60,	30,	30,	'Active',	'No',	3,	3,	'Yes',	0,	11.00,	5.00,	5.00,	'2021-05-06 19:13:05',	'2021-05-13 20:10:17');

DROP TABLE IF EXISTS `ludo_winners`;
CREATE TABLE `ludo_winners` (
  `winnerId` int(11) NOT NULL AUTO_INCREMENT,
  `joinRoomId` double NOT NULL,
  `userId` double NOT NULL,
  `adminPercent` int(11) NOT NULL,
  `totalWinningPrice` double NOT NULL,
  `winningPrice` double NOT NULL,
  `adminAmount` double NOT NULL,
  `gameMode` enum('Quick','Classic') NOT NULL,
  `isPrivate` enum('Yes','No') NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`winnerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `main_environment`;
CREATE TABLE `main_environment` (
  `mainEnvironmentId` int(11) NOT NULL AUTO_INCREMENT,
  `envKey` varchar(30) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`mainEnvironmentId`),
  KEY `value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `main_environment` (`mainEnvironmentId`, `envKey`, `value`) VALUES
(1,	'LUDOFANTASY',	'bqwdyq8773nas98r398mad234fusdf89r2');

DROP TABLE IF EXISTS `mst_bonus`;
CREATE TABLE `mst_bonus` (
  `bonusId` int(11) NOT NULL AUTO_INCREMENT,
  `playGame` double NOT NULL,
  `bonus` double NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`bonusId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `mst_bonus` (`bonusId`, `playGame`, `bonus`, `status`, `created`, `modified`) VALUES
(1,	1,	1,	'Inactive',	'2020-04-27 18:49:10',	'2020-04-27 18:49:15');

DROP TABLE IF EXISTS `mst_settings`;
CREATE TABLE `mst_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email1` varchar(255) NOT NULL,
  `email2` varchar(255) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `apk` varchar(225) NOT NULL,
  `version` varchar(225) NOT NULL,
  `website` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `copyright` varchar(255) NOT NULL,
  `contact_us_desc` varchar(255) NOT NULL,
  `adminPercent` int(11) NOT NULL,
  `videoUrl` varchar(255) NOT NULL,
  `topPlayerLimit` int(11) NOT NULL,
  `referralBonus` double NOT NULL,
  `signupBonus` double NOT NULL,
  `withdrawAmtMinus` double NOT NULL,
  `cashBonus` double NOT NULL,
  `baseUrl` varchar(255) NOT NULL,
  `maintainance` enum('Yes','No') NOT NULL DEFAULT 'No',
  `maintainanceMsg` varchar(255) NOT NULL,
  `joinRoomName` varchar(255) NOT NULL,
  `systemPassword` varchar(255) NOT NULL,
  `cdh` varchar(255) NOT NULL,
  `remoteip` varchar(255) NOT NULL,
  `spinWheelTimer` varchar(255) NOT NULL,
  `referalField1` double NOT NULL,
  `referalField2` double NOT NULL,
  `referalField3` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `version` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `mst_settings` (`id`, `site_title`, `companyName`, `address`, `email1`, `email2`, `phone`, `apk`, `version`, `website`, `logo`, `copyright`, `contact_us_desc`, `adminPercent`, `videoUrl`, `topPlayerLimit`, `referralBonus`, `signupBonus`, `withdrawAmtMinus`, `cashBonus`, `baseUrl`, `maintainance`, `maintainanceMsg`, `joinRoomName`, `systemPassword`, `cdh`, `remoteip`, `spinWheelTimer`, `referalField1`, `referalField2`, `referalField3`, `created`, `modified`) VALUES
(4,	'Ludo',	'Company',	'India',	'info@company.com',	'company@gmail.com',	9999999999,	'test.apk',	'0.9',	'https://www.company.com',	'',	'Copyright © Ludo 2020',	'contact us 24x7. we are here to help ',	3,	'',	30,	0,	25,	0,	0,	'http://company.com/admin/',	'No',	'test',	'JOINGAME!@#',	'SKILL!@#$%',	'CB!@#$!@',	'remoteip',	'120',	50,	50,	50,	'2019-09-26 15:32:25',	'2021-05-17 12:31:13');

DROP TABLE IF EXISTS `mst_sms_body`;
CREATE TABLE `mst_sms_body` (
  `smsId` int(11) NOT NULL AUTO_INCREMENT,
  `smsType` varchar(255) NOT NULL,
  `smsBody` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`smsId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `mst_sms_body` (`smsId`, `smsType`, `smsBody`, `created`, `modified`) VALUES
(1,	'Otp-verification',	'{otp} is your OTP (One Time Password) to verify your user account on Ludo',	'0000-00-00 00:00:00',	'2019-10-31 07:27:23'),
(2,	'Forgot_password',	'Hello {user_name}, Your new password is {password}',	'2019-11-01 15:22:39',	'2019-11-01 15:22:39'),
(3,	'kyc_type_message',	'Dear {user_name},Your {kyc_type} {message}',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(4,	'redeem_rejection_sms',	'Hello {user_name}, Your withdraw request is rejected reason is {message} ',	'2019-12-17 11:14:11',	'2019-12-17 11:14:11'),
(5,	'refund-reedem-amount',	'Hello {user_name},Your withdraw amount {amt} is refunded due to {reason}',	'2020-01-15 12:52:17',	'2020-01-15 12:52:17'),
(6,	'contactus_reply',	'Dear {user_name},Your Reply {message}',	'2019-12-13 18:42:32',	'2019-12-13 18:42:32'),
(7,	'supports_reply',	'Dear {user_name},Your Reply {message}',	'2019-12-13 18:42:32',	'2019-12-13 18:42:32'),
(8,	'Email_Verification',	'Dear {user_name}, Your email is verified successfully',	'2020-02-24 12:46:10',	'2020-02-24 12:46:10'),
(9,	'Add_money',	'Dear {user_name}, ludo has added Rs. {amount} bonus in your wallet.',	'2020-02-25 10:55:01',	'2020-02-25 10:55:01'),
(11,	'withdraw_amt_msg_bank',	'Dear {user_name}, Your withdrawal request of Rs {amt} successfully credited to your bank by ludoGames pvt ltd  (Ludoskill).',	'2020-01-15 12:52:17',	'2020-01-15 12:52:17'),
(12,	'withdraw_amt_msg_paytm',	'Dear {user_name}, Your paytm wallet credited Rs {amt} by ludo Games pvt lt',	'2020-01-15 12:52:17',	'2020-01-15 12:52:17');

DROP TABLE IF EXISTS `mst_tournaments`;
CREATE TABLE `mst_tournaments` (
  `tournamentId` int(11) NOT NULL AUTO_INCREMENT,
  `tournamentTitle` varchar(255) NOT NULL,
  `tournamentDescription` varchar(255) NOT NULL,
  `startDate` varchar(50) NOT NULL,
  `startTime` time NOT NULL,
  `entryFee` double NOT NULL,
  `winningPrice` int(11) NOT NULL,
  `playerLimitInRoom` int(11) NOT NULL,
  `noOfRoundInTournament` int(11) NOT NULL,
  `playerLimitInTournament` int(11) NOT NULL,
  `commision` int(11) NOT NULL,
  `currentRound` int(11) NOT NULL DEFAULT 1,
  `lastRound` int(11) NOT NULL,
  `lastRoundPlayerLeft` int(11) NOT NULL,
  `registerPlayerCount` int(11) NOT NULL,
  `startRoundTime` int(11) NOT NULL,
  `tokenMoveTime` int(11) NOT NULL,
  `rollDiceTime` int(11) NOT NULL,
  `status` enum('Active','Inactive','Next','Complete') NOT NULL DEFAULT 'Inactive',
  `gameMode` enum('Quick','Classic') NOT NULL DEFAULT 'Quick',
  `roundTimer` enum('Start','End') NOT NULL DEFAULT 'Start',
  `isUpdateWinPrice` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`tournamentId`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

INSERT INTO `mst_tournaments` (`tournamentId`, `tournamentTitle`, `tournamentDescription`, `startDate`, `startTime`, `entryFee`, `winningPrice`, `playerLimitInRoom`, `noOfRoundInTournament`, `playerLimitInTournament`, `commision`, `currentRound`, `lastRound`, `lastRoundPlayerLeft`, `registerPlayerCount`, `startRoundTime`, `tokenMoveTime`, `rollDiceTime`, `status`, `gameMode`, `roundTimer`, `isUpdateWinPrice`, `created`, `modified`) VALUES
(43,	'Test10',	'Teta',	'2020-06-16',	'19:41:00',	10,	36,	2,	5,	6,	10,	2,	0,	0,	4,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-06-16 18:35:30',	'2020-10-14 19:33:05'),
(50,	'Test 2',	'Testing tournament.',	'2020-11-10',	'15:47:00',	10,	26,	2,	3,	8,	15,	2,	0,	0,	3,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-10-22 16:25:33',	'2020-11-10 15:40:46'),
(59,	'testing2',	'Test2',	'2020-10-27',	'18:52:02',	5,	17,	2,	2,	8,	15,	2,	0,	0,	4,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-10-27 17:34:29',	'2020-10-27 17:52:02'),
(61,	'Tour27',	'Test',	'2020-10-27',	'22:36:32',	10,	36,	2,	4,	6,	10,	2,	0,	0,	4,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-10-27 21:03:44',	'2020-10-27 21:36:32'),
(63,	'tournamnet1',	'Hello',	'2020-10-29',	'12:26:17',	5,	17,	2,	2,	6,	15,	2,	0,	0,	4,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-10-29 11:04:58',	'2020-10-29 11:26:17'),
(65,	'tournament 2',	'Test',	'2020-11-02',	'15:33:36',	10,	51,	2,	3,	6,	15,	3,	0,	0,	6,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-11-02 12:39:26',	'2020-11-02 14:33:36'),
(69,	'Tournament 5',	'Testing',	'2020-11-02',	'19:25:24',	5,	21,	2,	2,	5,	15,	2,	0,	0,	5,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-11-02 18:11:47',	'2020-11-02 18:25:24'),
(70,	'Tournament 6',	'Test',	'2020-11-03',	'14:10:05',	5,	13,	2,	2,	3,	15,	2,	0,	0,	3,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-11-03 13:02:38',	'2020-11-03 13:10:05'),
(71,	'Tournament 7',	'Test',	'2020-11-03',	'14:39:04',	10,	34,	2,	2,	4,	15,	2,	0,	0,	4,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-11-03 13:31:42',	'2020-11-03 13:39:04'),
(73,	'Tournament 8',	'Test',	'2020-11-03',	'16:10:53',	5,	17,	2,	2,	4,	15,	2,	0,	0,	4,	60,	30,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-11-03 14:41:26',	'2020-11-03 15:10:53'),
(75,	'Tournament 10',	'Testing',	'2020-11-04',	'14:50:01',	5,	17,	2,	3,	5,	15,	2,	0,	0,	4,	60,	60,	30,	'Next',	'Classic',	'Start',	'Yes',	'2020-11-04 13:05:30',	'2020-11-04 13:50:01'),
(80,	'Test price',	'Test',	'2020-11-10',	'17:08:49',	10,	27,	2,	3,	4,	10,	2,	0,	0,	3,	60,	30,	30,	'Complete',	'Classic',	'End',	'Yes',	'2020-11-10 15:41:59',	'2020-11-10 17:13:52'),
(81,	'Tournament 11',	'Testing',	'2020-11-26',	'19:38:11',	5,	68,	2,	4,	20,	15,	4,	0,	0,	16,	30,	60,	30,	'Next',	'Quick',	'Start',	'Yes',	'2020-11-26 15:21:38',	'2020-11-26 18:38:11'),
(82,	'Tournament 12',	'Testing',	'2020-11-26',	'16:55:00',	5,	68,	2,	4,	16,	15,	1,	0,	0,	16,	30,	60,	30,	'Active',	'Classic',	'End',	'Yes',	'2020-11-26 16:34:26',	'2021-03-31 12:05:01'),
(83,	'Tournament 13',	'Testing',	'2020-11-27',	'14:04:09',	5,	68,	2,	4,	16,	15,	3,	0,	0,	16,	30,	60,	30,	'Complete',	'Classic',	'End',	'Yes',	'2020-11-27 10:50:10',	'2020-11-27 14:09:10');

DROP TABLE IF EXISTS `mst_tournaments_old`;
CREATE TABLE `mst_tournaments_old` (
  `tournamentId` int(11) NOT NULL AUTO_INCREMENT,
  `tournamentName` varchar(200) NOT NULL,
  `tournamentDate` date NOT NULL,
  `tournamentTime` time NOT NULL,
  `noOfPlayers` int(11) NOT NULL,
  `totalPlayers` int(11) NOT NULL,
  `entryFee` double NOT NULL,
  `winnerPriceRank1` double NOT NULL,
  `winnerPriceRank2` double NOT NULL,
  `winnerPriceRank3` double NOT NULL,
  `nextRoundMinute` int(11) NOT NULL,
  `adminPercent` int(11) NOT NULL,
  `tournamentStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`tournamentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mst_tournament_logs`;
CREATE TABLE `mst_tournament_logs` (
  `tournamenLogtId` int(11) NOT NULL AUTO_INCREMENT,
  `tournamentId` int(11) NOT NULL,
  `tournamentTitle` varchar(150) NOT NULL,
  `startDate` varchar(155) NOT NULL,
  `startTime` varchar(155) NOT NULL,
  `playerLimitInRoom` int(11) NOT NULL,
  `playerLimitInTournament` int(11) NOT NULL,
  `registerPlayerCount` int(11) NOT NULL,
  `currentRound` int(11) NOT NULL DEFAULT 1,
  `status` enum('Registration','Join','StartMatch','End','NextRound') NOT NULL DEFAULT 'Join',
  `gameMode` varchar(155) NOT NULL,
  `playerInGameCount` int(11) NOT NULL,
  `winPlayerCount` int(11) NOT NULL,
  `totalRoomInCurrentRound` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`tournamenLogtId`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

INSERT INTO `mst_tournament_logs` (`tournamenLogtId`, `tournamentId`, `tournamentTitle`, `startDate`, `startTime`, `playerLimitInRoom`, `playerLimitInTournament`, `registerPlayerCount`, `currentRound`, `status`, `gameMode`, `playerInGameCount`, `winPlayerCount`, `totalRoomInCurrentRound`, `created`) VALUES
(1,	5,	'Checking',	'2020-06-05',	'16:45:00',	2,	0,	0,	1,	'End',	'Classic',	1,	0,	0,	'2020-06-05 16:50:03'),
(2,	13,	'Three',	'2020-06-09',	'20:05:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-06-09 20:10:18'),
(3,	14,	'Four',	'2020-06-09',	'21:19:03',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-09 20:19:03'),
(4,	14,	'Four',	'2020-06-09',	'20:20:00',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-06-09 20:25:02'),
(5,	15,	'Five',	'2020-06-10',	'10:20:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-06-10 10:32:40'),
(6,	17,	'Seven',	'2020-06-10',	'12:04:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-06-10 12:09:04'),
(7,	22,	'Eight',	'2020-06-10',	'18:26:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-06-10 18:31:03'),
(8,	24,	'Tested',	'2020-06-10',	'19:19:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-06-10 19:24:03'),
(9,	25,	'Testing',	'2020-06-10',	'19:29:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-06-10 19:34:02'),
(10,	27,	'SUNDAY bonanza',	'2020-06-14',	'17:15:24',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-14 16:15:24'),
(11,	29,	'test',	'2020-06-15',	'14:46:37',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-15 13:46:37'),
(12,	33,	'Test12',	'2020-06-16',	'14:17:01',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-16 13:17:01'),
(13,	33,	'Test12',	'2020-06-16',	'13:17:00',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-06-16 13:22:02'),
(14,	34,	'Test1',	'2020-06-16',	'16:14:04',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-16 15:14:04'),
(15,	35,	'Test2',	'2020-06-16',	'16:36:01',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-16 15:36:01'),
(16,	36,	'Test3',	'2020-06-16',	'16:49:00',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-16 15:49:00'),
(17,	36,	'Test3',	'2020-06-16',	'15:51:00',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-06-16 15:56:01'),
(18,	43,	'Test10',	'2020-06-16',	'19:41:05',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-16 18:41:05'),
(19,	45,	'Test21',	'2020-06-17',	'13:56:06',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-17 12:56:06'),
(20,	45,	'Test21',	'2020-06-17',	'12:56:00',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-06-17 13:01:04'),
(21,	46,	'testskill',	'2020-06-17',	'17:37:05',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-06-17 16:37:05'),
(22,	46,	'testskill',	'2020-06-17',	'16:37:00',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-06-17 16:42:04'),
(23,	47,	'test111',	'2020-07-03',	'19:20:05',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-07-03 18:20:05'),
(24,	47,	'test111',	'2020-07-03',	'18:21:00',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-07-03 18:26:06'),
(25,	50,	'Test 2',	'2020-10-22',	'19:20:02',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-10-22 18:20:02'),
(26,	52,	'Tour12',	'2020-10-24',	'13:26:00',	2,	0,	0,	1,	'End',	'Quick',	2,	0,	0,	'2020-10-24 13:31:01'),
(27,	53,	'Tour13',	'2020-10-24',	'13:36:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-10-24 13:41:02'),
(28,	54,	'Tour1',	'2020-10-26',	'16:07:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-10-26 16:12:01'),
(29,	55,	'Tour1',	'2020-10-26',	'16:23:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-10-26 16:28:02'),
(30,	56,	'Tour26',	'2020-10-26',	'17:50:01',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-10-26 16:50:01'),
(31,	56,	'Tour26',	'2020-10-26',	'16:53:00',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-10-26 16:58:01'),
(32,	57,	'Tour27',	'2020-10-27',	'12:26:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-10-27 12:31:01'),
(33,	59,	'testing2',	'2020-10-27',	'18:52:02',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-10-27 17:52:02'),
(34,	60,	'Tour278',	'2020-10-27',	'20:45:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-10-27 20:50:22'),
(35,	61,	'Tour27',	'2020-10-27',	'22:36:32',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-10-27 21:36:32'),
(36,	62,	'Tour28',	'2020-10-28',	'16:31:27',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-10-28 15:31:27'),
(37,	62,	'Tour28',	'2020-10-28',	'16:31:27',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-10-28 16:36:29'),
(38,	63,	'tournamnet1',	'2020-10-29',	'12:26:17',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-10-29 11:26:17'),
(39,	64,	'Tour2',	'2020-11-02',	'12:06:00',	2,	0,	0,	1,	'End',	'Classic',	2,	0,	0,	'2020-11-02 12:17:38'),
(40,	65,	'tournament 2',	'2020-11-02',	'14:28:32',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-02 13:28:32'),
(41,	67,	'Tour4',	'2020-11-02',	'13:49:00',	2,	0,	0,	1,	'End',	'Classic',	1,	0,	0,	'2020-11-02 13:54:09'),
(42,	68,	'Tournament 4',	'2020-11-02',	'14:12:00',	3,	0,	0,	1,	'End',	'Classic',	3,	0,	0,	'2020-11-02 14:19:18'),
(43,	65,	'tournament 2',	'2020-11-02',	'15:33:36',	2,	0,	0,	3,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-02 14:33:36'),
(44,	69,	'Tournament 5',	'2020-11-02',	'19:25:24',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-02 18:25:24'),
(45,	70,	'Tournament 6',	'2020-11-03',	'14:10:05',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-03 13:10:05'),
(46,	71,	'Tournament 7',	'2020-11-03',	'14:39:04',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-03 13:39:04'),
(47,	73,	'Tournament 8',	'2020-11-03',	'16:10:53',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-03 15:10:53'),
(48,	74,	'Tournament 9',	'2020-11-03',	'20:21:15',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-03 19:21:15'),
(49,	74,	'Tournament 9',	'2020-11-03',	'20:21:15',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-11-03 20:26:21'),
(50,	75,	'Tournament 10',	'2020-11-04',	'14:50:01',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-04 13:50:01'),
(51,	76,	'Tournament 11',	'2020-11-04',	'15:30:15',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-04 14:30:15'),
(52,	76,	'Tournament 11',	'2020-11-04',	'15:30:15',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-11-04 15:35:16'),
(53,	77,	'Tournament 12',	'2020-11-04',	'18:22:24',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-04 17:22:24'),
(54,	77,	'Tournament 12',	'2020-11-04',	'19:27:25',	2,	0,	0,	3,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-04 18:27:25'),
(55,	77,	'Tournament 12',	'2020-11-04',	'20:32:27',	2,	0,	0,	4,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-04 19:32:27'),
(56,	77,	'Tournament 12',	'2020-11-04',	'20:32:27',	2,	0,	0,	4,	'End',	'Classic',	2,	0,	0,	'2020-11-04 20:37:28'),
(57,	78,	'Tournament 13',	'2020-11-06',	'13:13:53',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-06 12:13:53'),
(58,	78,	'Tournament 13',	'2020-11-06',	'13:13:53',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-11-06 13:18:54'),
(59,	79,	'Tournament 14',	'2020-11-06',	'16:49:51',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-06 15:49:51'),
(60,	79,	'Tournament 14',	'2020-11-06',	'16:49:51',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-11-06 16:54:52'),
(61,	80,	'Test price',	'2020-11-10',	'17:08:49',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-10 16:08:49'),
(62,	80,	'Test price',	'2020-11-10',	'17:08:49',	2,	0,	0,	2,	'End',	'Classic',	2,	0,	0,	'2020-11-10 17:13:52'),
(63,	81,	'Tournament 11',	'2020-11-26',	'17:28:07',	2,	0,	0,	2,	'NextRound',	'Quick',	0,	0,	0,	'2020-11-26 16:28:07'),
(64,	81,	'Tournament 11',	'2020-11-26',	'18:33:09',	2,	0,	0,	3,	'NextRound',	'Quick',	0,	0,	0,	'2020-11-26 17:33:09'),
(65,	81,	'Tournament 11',	'2020-11-26',	'19:38:11',	2,	0,	0,	4,	'NextRound',	'Quick',	0,	0,	0,	'2020-11-26 18:38:11'),
(66,	83,	'Tournament 13',	'2020-11-27',	'12:59:07',	2,	0,	0,	2,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-27 11:59:07'),
(67,	83,	'Tournament 13',	'2020-11-27',	'14:04:09',	2,	0,	0,	3,	'NextRound',	'Classic',	0,	0,	0,	'2020-11-27 13:04:09'),
(68,	83,	'Tournament 13',	'2020-11-27',	'14:04:09',	2,	0,	0,	3,	'End',	'Classic',	1,	0,	0,	'2020-11-27 14:09:10');

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(100) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `paymentMode` varchar(255) NOT NULL,
  `isPayment` enum('Yes','No') NOT NULL DEFAULT 'No',
  `transaction_id` varchar(255) NOT NULL,
  `json_data` text NOT NULL,
  `coupanCode` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL COMMENT 'discount amount',
  `discountAmount` double NOT NULL COMMENT 'amount minus discount is discountAmount',
  `isCoupan` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `isamountgiven` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `txnMode` varchar(255) NOT NULL,
  `type` enum('Deposit','Withdraw','Gratification') NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `status` enum('Approved','Pending','Rejected','Success','Failed','Process','BankExport') NOT NULL DEFAULT 'Pending',
  `paytmStatus` varchar(255) NOT NULL,
  `json_data` text NOT NULL,
  `paymentMode` varchar(255) NOT NULL DEFAULT 'PPI' COMMENT 'PPI=>Wallet',
  `bankName` varchar(255) NOT NULL DEFAULT 'WALLET',
  `statusMessage` varchar(255) NOT NULL,
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `checkSum` varchar(255) NOT NULL,
  `rejectedReason` varchar(255) NOT NULL,
  `isReadNotification` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isAdminReedem` enum('Yes','No') NOT NULL DEFAULT 'No',
  `statusCode` varchar(255) DEFAULT NULL,
  `coupanCode` varchar(150) DEFAULT NULL,
  `isCoupan` enum('Yes','No') NOT NULL DEFAULT 'No',
  `discount` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `mobileNo` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_detail_id` (`user_detail_id`),
  KEY `status` (`status`),
  KEY `created` (`created`),
  KEY `paymentType` (`paymentType`),
  KEY `type` (`type`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

INSERT INTO `payments` (`id`, `user_detail_id`, `orderId`, `paymentType`, `txnMode`, `type`, `amount`, `balance`, `mainWallet`, `winWallet`, `status`, `paytmStatus`, `json_data`, `paymentMode`, `bankName`, `statusMessage`, `transactionId`, `checkSum`, `rejectedReason`, `isReadNotification`, `isAdminReedem`, `statusCode`, `coupanCode`, `isCoupan`, `discount`, `created`, `modified`, `mobileNo`) VALUES
(1,	5,	'Ord8305',	'mainWallet',	'Bonus',	'Deposit',	10000,	10000,	10000,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd83055',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-22 14:13:50',	'2021-03-22 14:13:50',	0),
(2,	9,	'Ord8938',	'mainWallet',	'Bonus',	'Deposit',	100,	100,	100,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd89389',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-23 12:33:07',	'2021-03-23 12:33:07',	0),
(3,	14,	'Ord9185',	'mainWallet',	'Bonus',	'Deposit',	5000,	5000,	5000,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd918514',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-26 12:03:35',	'2021-03-26 12:03:35',	0),
(4,	13,	'Ord3510',	'mainWallet',	'Bonus',	'Deposit',	5000,	5000,	5000,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd351013',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-26 12:03:53',	'2021-03-26 12:03:53',	0),
(5,	11,	'Ord5928',	'mainWallet',	'Bonus',	'Deposit',	100,	100,	100,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd592811',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-27 13:02:17',	'2021-03-27 13:02:17',	0),
(6,	15,	'Ord9091',	'mainWallet',	'Bonus',	'Deposit',	100,	100,	100,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd909115',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-27 13:07:20',	'2021-03-27 13:07:20',	0),
(7,	17,	'Ord8752',	'mainWallet',	'Bonus',	'Deposit',	100,	110,	110,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd875217',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-27 13:20:45',	'2021-03-27 13:20:45',	0),
(8,	28,	'Ord8038',	'mainWallet',	'Bonus',	'Deposit',	10000,	10000,	10000,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd803828',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-30 14:51:21',	'2021-03-30 14:51:21',	0),
(9,	29,	'Ord6239',	'mainWallet',	'Bonus',	'Deposit',	1000,	1000,	1000,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd623929',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-30 14:57:36',	'2021-03-30 14:57:36',	0),
(10,	29,	'Ord3572',	'winWallet',	'Bonus',	'Deposit',	1000,	2000,	1000,	1000,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd357229',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-30 14:59:28',	'2021-03-30 14:59:28',	0),
(11,	22,	'Ord7863',	'mainWallet',	'Bonus',	'Deposit',	100,	100,	100,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd786322',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-31 09:26:40',	'2021-03-31 09:26:40',	0),
(12,	21,	'Ord9913',	'mainWallet',	'Bonus',	'Deposit',	100,	100,	100,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd991321',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-31 09:28:39',	'2021-03-31 09:28:39',	0),
(13,	20,	'Ord5001',	'mainWallet',	'Bonus',	'Deposit',	100,	100,	100,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd500120',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-31 09:29:41',	'2021-03-31 09:29:41',	0),
(14,	17,	'Ord2323',	'mainWallet',	'Bonus',	'Deposit',	500,	505,	505,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd232317',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-31 12:15:32',	'2021-03-31 12:15:32',	0),
(15,	21,	'Ord6602',	'mainWallet',	'Bonus',	'Deposit',	500,	505,	505,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd660221',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-31 13:32:37',	'2021-03-31 13:32:37',	0),
(16,	21,	'Ord2584',	'mainWallet',	'Bonus',	'Deposit',	500,	1005,	1005,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd258421',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-03-31 13:35:05',	'2021-03-31 13:35:05',	0),
(17,	41,	'Ord5736',	'mainWallet',	'Bonus',	'Deposit',	2000,	2000,	2000,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd573641',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-01 16:28:30',	'2021-05-01 16:28:30',	0),
(18,	44,	'Ord1738',	'mainWallet',	'Bonus',	'Deposit',	1000,	1025,	1025,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd173844',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-11 17:16:14',	'2021-05-11 17:16:14',	0),
(19,	43,	'Ord5855',	'mainWallet',	'Bonus',	'Deposit',	1000,	1025,	1025,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd585543',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-11 17:16:30',	'2021-05-11 17:16:30',	0),
(20,	45,	'Ord9976',	'mainWallet',	'Bonus',	'Deposit',	500,	500,	500,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd997645',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-12 14:08:13',	'2021-05-12 14:08:13',	0),
(21,	18,	'Ord3125',	'mainWallet',	'Bonus',	'Deposit',	500,	525,	525,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd312518',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-13 13:11:18',	'2021-05-13 13:11:18',	0),
(22,	42,	'Ord840',	'mainWallet',	'Bonus',	'Deposit',	10000,	10025,	10025,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd84042',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-14 14:46:49',	'2021-05-14 14:46:49',	0),
(23,	43,	'Ord5579',	'mainWallet',	'Bonus',	'Deposit',	100,	992.25,	750,	242.25,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd557943',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-17 12:14:38',	'2021-05-17 12:14:38',	0),
(24,	44,	'Ord6833',	'mainWallet',	'Bonus',	'Deposit',	60,	865,	700,	165,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd683344',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-17 12:21:51',	'2021-05-17 12:21:51',	0),
(25,	49,	'Ord9253',	'mainWallet',	'Bonus',	'Deposit',	1,	1,	1,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd925349',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-19 12:12:12',	'2021-05-19 12:12:12',	0),
(26,	49,	'Ord8716',	'mainWallet',	'Bonus',	'Deposit',	1,	2,	2,	0,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd871649',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-19 13:50:42',	'2021-05-19 13:50:42',	0),
(27,	43,	'Ord5863',	'mainWallet',	'Bonus',	'Deposit',	100,	1000.8,	924,	76.80000000000001,	'Success',	'',	'',	'PPI',	'WALLET',	'',	'ADMOrd586343',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-19 17:21:51',	'2021-05-19 17:21:51',	0);

DROP TABLE IF EXISTS `payment_logs`;
CREATE TABLE `payment_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userPhone` bigint(20) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `txnMode` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `checkSum` varchar(255) NOT NULL,
  `paytmType` varchar(255) NOT NULL,
  `type` enum('Deposit','Withdraw') NOT NULL,
  `paytmStatus` varchar(255) NOT NULL,
  `statusCode` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `status` enum('Approved','Pending','Rejected','Process','Failed','Success','BankExport') NOT NULL DEFAULT 'Pending',
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `json_data` text NOT NULL,
  `rejectedReason` varchar(255) NOT NULL,
  `paymentMode` varchar(255) NOT NULL DEFAULT 'PPI' COMMENT 'PPI=>Wallet',
  `paymentBy` enum('Bank','Paytm') NOT NULL COMMENT 'Paytm',
  `bankName` varchar(255) NOT NULL DEFAULT 'WALLET',
  `isPayment` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `mobileNo` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_detail_id` (`user_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

INSERT INTO `payment_logs` (`id`, `payment_id`, `user_detail_id`, `userName`, `userEmail`, `userPhone`, `orderId`, `paymentType`, `txnMode`, `amount`, `balance`, `mainWallet`, `winWallet`, `checkSum`, `paytmType`, `type`, `paytmStatus`, `statusCode`, `statusMessage`, `status`, `transactionId`, `json_data`, `rejectedReason`, `paymentMode`, `paymentBy`, `bankName`, `isPayment`, `created`, `mobileNo`) VALUES
(1,	0,	4,	'Shubham',	'aarya@123.com',	9604443180,	'20213191653743169',	'',	'',	100,	0,	0,	0,	'',	'',	'Deposit',	'',	'',	'',	'Pending',	'',	'',	'',	'PPI',	'Paytm',	'WALLET',	'No',	'2021-03-19 16:53:08',	0),
(2,	0,	4,	'Shubham',	'aarya@123.com',	9604443180,	'202131916534443327',	'',	'',	250,	0,	0,	0,	'',	'',	'Deposit',	'',	'',	'',	'Pending',	'',	'',	'',	'PPI',	'Paytm',	'WALLET',	'No',	'2021-03-19 16:53:44',	0),
(3,	0,	3,	'newsam',	'sam@12345.com',	8830286846,	'20213191883538190',	'',	'',	100,	0,	0,	0,	'',	'',	'Deposit',	'',	'',	'',	'Pending',	'',	'',	'',	'PPI',	'Paytm',	'WALLET',	'No',	'2021-03-19 18:04:13',	0),
(4,	0,	4,	'Shubham',	'aarya@123.com',	9604443180,	'20213191821194675',	'',	'',	100,	0,	0,	0,	'',	'',	'Deposit',	'',	'',	'',	'Pending',	'',	'',	'',	'PPI',	'Paytm',	'WALLET',	'No',	'2021-03-19 18:21:24',	0),
(5,	0,	4,	'Shubham',	'aarya@123.com',	9604443180,	'20213191822047665',	'',	'',	250,	0,	0,	0,	'',	'',	'Deposit',	'',	'',	'',	'Pending',	'',	'',	'',	'PPI',	'Paytm',	'WALLET',	'No',	'2021-03-19 18:22:05',	0),
(6,	0,	4,	'Shubham',	'aarya@123.com',	9604443180,	'202131918394949413',	'',	'',	250,	0,	0,	0,	'',	'',	'Deposit',	'',	'',	'',	'Pending',	'',	'',	'',	'PPI',	'Paytm',	'WALLET',	'No',	'2021-03-19 18:39:54',	0),
(7,	0,	5,	'',	'',	0,	'Ord8305',	'mainWallet',	'Bonus',	10000,	10000,	10000,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-22 14:13:50',	0),
(8,	0,	9,	'',	'',	0,	'Ord8938',	'mainWallet',	'Bonus',	100,	100,	100,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-23 12:33:07',	0),
(9,	0,	14,	'',	'',	0,	'Ord9185',	'mainWallet',	'Bonus',	5000,	5000,	5000,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-26 12:03:35',	0),
(10,	0,	13,	'',	'',	0,	'Ord3510',	'mainWallet',	'Bonus',	5000,	5000,	5000,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-26 12:03:53',	0),
(11,	0,	11,	'',	'',	0,	'Ord5928',	'mainWallet',	'Bonus',	100,	100,	100,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-27 13:02:17',	0),
(12,	0,	15,	'',	'',	0,	'Ord9091',	'mainWallet',	'Bonus',	100,	100,	100,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-27 13:07:20',	0),
(13,	0,	17,	'',	'',	0,	'Ord8752',	'mainWallet',	'Bonus',	100,	110,	110,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-27 13:20:45',	0),
(14,	0,	28,	'',	'',	0,	'Ord8038',	'mainWallet',	'Bonus',	10000,	10000,	10000,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-30 14:51:21',	0),
(15,	0,	29,	'',	'',	0,	'Ord6239',	'mainWallet',	'Bonus',	1000,	1000,	1000,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-30 14:57:36',	0),
(16,	0,	29,	'',	'',	0,	'Ord3572',	'winWallet',	'Bonus',	1000,	2000,	1000,	1000,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-30 14:59:28',	0),
(17,	0,	22,	'',	'',	0,	'Ord7863',	'mainWallet',	'Bonus',	100,	100,	100,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-31 09:26:40',	0),
(18,	0,	21,	'',	'',	0,	'Ord9913',	'mainWallet',	'Bonus',	100,	100,	100,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-31 09:28:39',	0),
(19,	0,	20,	'',	'',	0,	'Ord5001',	'mainWallet',	'Bonus',	100,	100,	100,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-31 09:29:41',	0),
(20,	0,	17,	'',	'',	0,	'Ord2323',	'mainWallet',	'Bonus',	500,	505,	505,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-31 12:15:32',	0),
(21,	0,	21,	'',	'',	0,	'Ord6602',	'mainWallet',	'Bonus',	500,	505,	505,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-31 13:32:37',	0),
(22,	0,	21,	'',	'',	0,	'Ord2584',	'mainWallet',	'Bonus',	500,	1005,	1005,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-03-31 13:35:05',	0),
(23,	0,	41,	'',	'',	0,	'Ord5736',	'mainWallet',	'Bonus',	2000,	2000,	2000,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-01 16:28:30',	0),
(24,	0,	44,	'',	'',	0,	'Ord1738',	'mainWallet',	'Bonus',	1000,	1025,	1025,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-11 17:16:14',	0),
(25,	0,	43,	'',	'',	0,	'Ord5855',	'mainWallet',	'Bonus',	1000,	1025,	1025,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-11 17:16:30',	0),
(26,	0,	45,	'',	'',	0,	'Ord9976',	'mainWallet',	'Bonus',	500,	500,	500,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-12 14:08:13',	0),
(27,	0,	18,	'',	'',	0,	'Ord3125',	'mainWallet',	'Bonus',	500,	525,	525,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-13 13:11:18',	0),
(28,	0,	42,	'',	'',	0,	'Ord840',	'mainWallet',	'Bonus',	10000,	10025,	10025,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-14 14:46:49',	0),
(29,	0,	43,	'',	'',	0,	'Ord5579',	'mainWallet',	'Bonus',	100,	992.25,	750,	242.25,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-17 12:14:38',	0),
(30,	0,	44,	'',	'',	0,	'Ord6833',	'mainWallet',	'Bonus',	60,	865,	700,	165,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-17 12:21:51',	0),
(31,	0,	49,	'',	'',	0,	'Ord9253',	'mainWallet',	'Bonus',	1,	1,	1,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-19 12:12:12',	0),
(32,	0,	49,	'',	'',	0,	'Ord8716',	'mainWallet',	'Bonus',	1,	2,	2,	0,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-19 13:50:42',	0),
(33,	0,	43,	'',	'',	0,	'Ord5863',	'mainWallet',	'Bonus',	100,	1000.8,	924,	76.80000000000001,	'',	'',	'Deposit',	'',	'',	'',	'Success',	'',	'',	'',	'PPI',	'Bank',	'WALLET',	'No',	'2021-05-19 17:21:52',	0);

DROP TABLE IF EXISTS `payment_process`;
CREATE TABLE `payment_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `setInOrders` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paytm_refunds`;
CREATE TABLE `paytm_refunds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `checkSum` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `statusCode` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paytm_refund_logs`;
CREATE TABLE `paytm_refund_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paytm_refund_id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `type` enum('byBank','byQuery') NOT NULL,
  `amount` double NOT NULL,
  `checkSum` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `statusCode` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `referal_user_logs`;
CREATE TABLE `referal_user_logs` (
  `referLogId` int(11) NOT NULL AUTO_INCREMENT,
  `fromUserId` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `referalAmount` double NOT NULL,
  `toUserName` varchar(50) NOT NULL,
  `tableId` int(11) NOT NULL,
  `referalAmountBy` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`referLogId`),
  KEY `fromUserId` (`fromUserId`),
  KEY `referalAmountBy` (`referalAmountBy`),
  KEY `toUserId` (`toUserId`),
  KEY `tableId` (`tableId`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

INSERT INTO `referal_user_logs` (`referLogId`, `fromUserId`, `toUserId`, `referalAmount`, `toUserName`, `tableId`, `referalAmountBy`, `created`) VALUES
(1,	1,	0,	25,	'Sam',	0,	'Signup',	'2021-03-19 10:50:34'),
(2,	2,	0,	25,	'sam',	0,	'Signup',	'2021-03-19 10:51:03'),
(3,	3,	0,	25,	'newsam',	0,	'Signup',	'2021-03-19 10:54:08'),
(4,	4,	0,	25,	'Shubham',	0,	'Signup',	'2021-03-19 11:10:53'),
(5,	5,	0,	25,	'Vinod',	0,	'Signup',	'2021-03-20 04:43:14'),
(6,	0,	5,	10000,	'Vinod',	0,	'Admin',	'2021-03-22 14:13:50'),
(7,	9,	0,	25,	'Saya',	0,	'Signup',	'2021-03-23 05:58:46'),
(8,	10,	0,	25,	'Amol ',	0,	'Signup',	'2021-03-23 06:03:19'),
(9,	11,	0,	25,	'Amol',	0,	'Signup',	'2021-03-23 06:05:40'),
(10,	0,	9,	100,	'Saya',	0,	'Admin',	'2021-03-23 12:33:07'),
(11,	12,	0,	25,	'Prashant',	0,	'Signup',	'2021-03-26 05:30:03'),
(12,	13,	0,	25,	'Yashashri',	0,	'Signup',	'2021-03-26 05:41:59'),
(13,	14,	0,	25,	'Dipesh',	0,	'Signup',	'2021-03-26 05:44:34'),
(14,	0,	14,	5000,	'Ulka',	0,	'Admin',	'2021-03-26 12:03:35'),
(15,	0,	13,	5000,	'Yashashri',	0,	'Admin',	'2021-03-26 12:03:53'),
(16,	15,	0,	25,	'Dipesh',	0,	'Signup',	'2021-03-26 06:43:41'),
(17,	16,	0,	25,	'Anil',	0,	'Signup',	'2021-03-26 06:44:59'),
(18,	17,	0,	25,	'Tejas',	0,	'Signup',	'2021-03-26 06:47:01'),
(19,	18,	0,	25,	'Jayesh',	0,	'Signup',	'2021-03-26 06:49:08'),
(20,	19,	0,	25,	'kirti ramchandani',	0,	'Signup',	'2021-03-26 06:55:23'),
(21,	20,	0,	25,	'Surav',	0,	'Signup',	'2021-03-26 07:06:48'),
(22,	21,	0,	25,	'Gaurav',	0,	'Signup',	'2021-03-26 07:08:42'),
(23,	22,	0,	25,	'Yuvi',	0,	'Signup',	'2021-03-26 07:10:06'),
(24,	23,	0,	25,	'govind',	0,	'Signup',	'2021-03-26 07:11:10'),
(25,	24,	0,	25,	'bhavesh ramchandani',	0,	'Signup',	'2021-03-26 07:20:57'),
(26,	25,	0,	25,	'bhavesh ramchandani',	0,	'Signup',	'2021-03-26 07:20:57'),
(27,	26,	0,	25,	'diya ramchandani',	0,	'Signup',	'2021-03-26 07:24:41'),
(28,	27,	0,	25,	'satya ramchandani',	0,	'Signup',	'2021-03-26 07:25:52'),
(29,	0,	11,	100,	'Amol',	0,	'Admin',	'2021-03-27 13:02:17'),
(30,	0,	15,	100,	'Dipesh',	0,	'Admin',	'2021-03-27 13:07:20'),
(31,	0,	17,	100,	'Tejas',	0,	'Admin',	'2021-03-27 13:20:45'),
(32,	28,	0,	25,	'Mitesh',	0,	'Signup',	'2021-03-30 07:33:49'),
(33,	0,	28,	10000,	'Mitesh',	0,	'Admin',	'2021-03-30 14:51:21'),
(34,	29,	0,	25,	'mits',	0,	'Signup',	'2021-03-30 09:26:36'),
(35,	0,	29,	1000,	'mits',	0,	'Admin',	'2021-03-30 14:57:36'),
(36,	0,	29,	1000,	'mits',	0,	'Admin',	'2021-03-30 14:59:28'),
(37,	0,	22,	100,	'Yuvi',	0,	'Admin',	'2021-03-31 09:26:40'),
(38,	0,	21,	100,	'Gaurav',	0,	'Admin',	'2021-03-31 09:28:39'),
(39,	0,	20,	100,	'Surav',	0,	'Admin',	'2021-03-31 09:29:41'),
(40,	30,	0,	25,	'Y',	0,	'Signup',	'2021-03-31 05:26:01'),
(41,	31,	0,	25,	'Y',	0,	'Signup',	'2021-03-31 05:28:40'),
(42,	32,	0,	25,	'Testing',	0,	'Signup',	'2021-03-31 05:53:25'),
(43,	33,	0,	25,	'Test',	0,	'Signup',	'2021-03-31 05:55:15'),
(44,	0,	17,	500,	'Vinod',	0,	'Admin',	'2021-03-31 12:15:32'),
(45,	0,	21,	500,	'mitesh',	0,	'Admin',	'2021-03-31 13:32:37'),
(46,	0,	21,	500,	'mitesh',	0,	'Admin',	'2021-03-31 13:35:05'),
(47,	34,	0,	25,	'bill',	0,	'Signup',	'2021-04-06 07:32:34'),
(48,	35,	0,	25,	'bill',	0,	'Signup',	'2021-04-06 07:35:36'),
(49,	36,	0,	25,	'bill1',	0,	'Signup',	'2021-04-06 07:39:34'),
(50,	37,	0,	25,	'bill11',	0,	'Signup',	'2021-04-06 07:40:14'),
(51,	38,	0,	25,	'abc',	0,	'Signup',	'2021-04-06 07:40:52'),
(52,	39,	0,	25,	'abc1',	0,	'Signup',	'2021-04-06 07:41:08'),
(53,	40,	0,	25,	'abc11',	0,	'Signup',	'2021-04-06 07:41:32'),
(54,	41,	0,	25,	'testing11',	0,	'Signup',	'2021-04-06 07:53:39'),
(55,	0,	41,	2000,	'testing11',	0,	'Admin',	'2021-05-01 16:28:30'),
(56,	42,	0,	25,	'vishwa',	0,	'Signup',	'2021-05-10 14:11:50'),
(57,	43,	0,	25,	'test123',	0,	'Signup',	'2021-05-11 10:28:31'),
(58,	44,	0,	25,	'abcd',	0,	'Signup',	'2021-05-11 10:29:25'),
(59,	0,	44,	1000,	'abcd',	0,	'Admin',	'2021-05-11 17:16:14'),
(60,	0,	43,	1000,	'test123',	0,	'Admin',	'2021-05-11 17:16:30'),
(61,	45,	0,	25,	'Sagar',	0,	'Signup',	'2021-05-12 08:27:27'),
(62,	0,	45,	500,	'Sagar',	0,	'Admin',	'2021-05-12 14:08:13'),
(63,	46,	0,	25,	'JustOOH',	0,	'Signup',	'2021-05-12 09:08:23'),
(64,	47,	0,	25,	'12MayTesting',	0,	'Signup',	'2021-05-12 09:33:19'),
(65,	0,	18,	500,	'Jayesh',	0,	'Admin',	'2021-05-13 13:11:18'),
(66,	0,	42,	10000,	'vishwa',	0,	'Admin',	'2021-05-14 14:46:49'),
(67,	0,	43,	100,	'test123',	0,	'Admin',	'2021-05-17 12:14:38'),
(68,	0,	44,	60,	'abcd',	0,	'Admin',	'2021-05-17 12:21:51'),
(69,	48,	0,	25,	'may20212021',	0,	'Signup',	'2021-05-18 06:23:54'),
(70,	49,	0,	25,	'hey',	0,	'Signup',	'2021-05-18 11:23:46'),
(71,	0,	49,	1,	'hey',	0,	'Admin',	'2021-05-19 12:12:12'),
(72,	0,	49,	1,	'hey',	0,	'Admin',	'2021-05-19 13:50:42'),
(73,	0,	43,	100,	'test123',	0,	'Admin',	'2021-05-19 17:21:51'),
(74,	50,	0,	25,	'pp123',	0,	'Signup',	'2021-05-19 12:35:32');

DROP TABLE IF EXISTS `referral_users`;
CREATE TABLE `referral_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromReferralUserId` int(11) NOT NULL,
  `toReferralUserId` int(11) NOT NULL,
  `referralBonus` double NOT NULL,
  `isRegister` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `reply_logs`;
CREATE TABLE `reply_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Support','Contact') NOT NULL,
  `from_id` int(11) NOT NULL,
  `reply` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `reportId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `reportTitle` varchar(150) NOT NULL,
  `reportDescription` text NOT NULL,
  `reportScreenShot` varchar(255) NOT NULL,
  `reply` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`reportId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `spin_rolls`;
CREATE TABLE `spin_rolls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(225) NOT NULL,
  `value` float NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

INSERT INTO `spin_rolls` (`id`, `title`, `value`, `status`, `created`, `modified`) VALUES
(1,	'Offer1',	1,	'Active',	'2020-05-01 17:59:13',	'2020-05-12 15:56:00'),
(4,	'offer2',	2,	'Active',	'2020-05-12 12:17:05',	'2020-05-12 15:55:32'),
(5,	'Offer3',	3,	'Active',	'2020-05-12 15:56:24',	'2020-05-12 15:56:24'),
(6,	'Offer4',	4,	'Active',	'2020-05-12 15:56:43',	'2020-05-12 15:56:43'),
(7,	'Offer5',	5,	'Active',	'2020-05-12 15:57:02',	'2020-05-12 15:57:02'),
(8,	'Offer6',	6,	'Active',	'2020-05-12 15:57:16',	'2020-05-12 15:57:16'),
(9,	'Offer7',	7,	'Active',	'2020-05-12 15:57:30',	'2020-05-12 15:57:30'),
(10,	'Offer8',	8,	'Active',	'2020-05-12 15:57:45',	'2020-05-12 15:57:45'),
(11,	'Offer9',	9,	'Inactive',	'2020-05-12 15:58:02',	'2020-05-12 15:58:02');

DROP TABLE IF EXISTS `support_logs`;
CREATE TABLE `support_logs` (
  `supportLogId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `type` enum('User','Admin') NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `isRead` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created` datetime NOT NULL,
  PRIMARY KEY (`supportLogId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tournaments`;
CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `betAmt` double NOT NULL,
  `winningAmt` double NOT NULL,
  `noOfPlayers` bigint(20) NOT NULL,
  `round` int(11) NOT NULL,
  `startTime` time NOT NULL,
  `commision` double NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tournament_registrations`;
CREATE TABLE `tournament_registrations` (
  `tournamentRegtrationId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL,
  `userName` varchar(150) NOT NULL,
  `entryFee` int(11) NOT NULL,
  `isEnter` enum('Yes','No') NOT NULL DEFAULT 'No',
  `roundStatus` enum('Win','Loss','Out','Pending','Left','TournamentWiner') NOT NULL DEFAULT 'Pending',
  `round` int(11) NOT NULL,
  `winningPrice` int(11) NOT NULL DEFAULT 0,
  `isDelete` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isWin` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isJoin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 means not joined 1 means joined',
  `formMainWallet` double NOT NULL,
  `formWinWallet` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`tournamentRegtrationId`),
  KEY `userId` (`userId`),
  KEY `tournamentId` (`tournamentId`),
  KEY `userName` (`userName`),
  KEY `tournamentRegtrationId` (`tournamentRegtrationId`),
  KEY `entryFee` (`entryFee`),
  KEY `isEnter` (`isEnter`),
  KEY `isWin` (`roundStatus`),
  KEY `round` (`round`),
  KEY `isDelete` (`isDelete`),
  KEY `created` (`created`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=utf8mb4;

INSERT INTO `tournament_registrations` (`tournamentRegtrationId`, `userId`, `tournamentId`, `userName`, `entryFee`, `isEnter`, `roundStatus`, `round`, `winningPrice`, `isDelete`, `isWin`, `isJoin`, `formMainWallet`, `formWinWallet`, `created`, `modified`) VALUES
(96,	1,	43,	'Vishwa',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-06-16 18:35:52',	'2020-06-16 18:35:52'),
(97,	46,	43,	'vishwa1',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-06-16 18:35:53',	'2020-06-16 18:35:53'),
(98,	27,	43,	'rajan',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-06-16 18:35:55',	'2020-06-16 18:35:55'),
(99,	48,	43,	'Ashish',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-06-16 18:36:28',	'2020-06-16 18:36:28'),
(100,	25,	44,	'testplayer',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-06-17 12:14:17',	'2020-06-17 12:14:17'),
(101,	46,	44,	'vishwa1',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-06-17 12:14:50',	'2020-06-17 12:14:50'),
(102,	1,	44,	'Vishwa',	10,	'No',	'Pending',	1,	0,	'No',	'No',	0,	10,	0,	'2020-06-17 12:14:54',	'2020-06-17 12:14:54'),
(103,	1,	45,	'Vishwa',	100,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	100,	0,	'2020-06-17 12:30:13',	'2020-06-17 12:30:13'),
(104,	46,	45,	'vishwa1',	100,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	100,	0,	'2020-06-17 12:31:03',	'2020-06-17 12:31:03'),
(105,	25,	45,	'testplayer',	100,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	100,	0,	'2020-06-17 12:31:26',	'2020-06-17 12:31:26'),
(106,	27,	45,	'rajan',	100,	'Yes',	'Win',	3,	360,	'No',	'Yes',	0,	100,	0,	'2020-06-17 12:49:32',	'2020-06-17 12:49:32'),
(107,	53,	46,	'test',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-06-17 16:28:29',	'2020-06-17 16:28:29'),
(108,	27,	46,	'rajan',	10,	'Yes',	'Win',	3,	36,	'No',	'Yes',	0,	10,	0,	'2020-06-17 16:29:14',	'2020-06-17 16:29:14'),
(109,	25,	46,	'testplayer',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-06-17 16:31:14',	'2020-06-17 16:31:14'),
(110,	1,	46,	'Vishwa',	10,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	10,	0,	'2020-06-17 16:31:46',	'2020-06-17 16:31:46'),
(111,	53,	47,	'test',	10,	'Yes',	'Loss',	2,	0,	'No',	'Yes',	0,	5,	5,	'2020-07-03 18:14:34',	'2020-07-03 18:14:34'),
(112,	25,	47,	'testplayer',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-07-03 18:14:38',	'2020-07-03 18:14:38'),
(113,	27,	47,	'rajan',	10,	'Yes',	'Loss',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-07-03 18:15:03',	'2020-07-03 18:15:03'),
(115,	73109,	49,	'smstest',	10,	'No',	'Pending',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-22 12:47:38',	'2020-10-22 12:47:38'),
(117,	73300,	50,	'testing',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-22 16:46:58',	'2020-10-22 16:46:58'),
(119,	79186,	50,	'aishu',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-10-22 16:54:40',	'2020-10-22 16:54:40'),
(123,	73089,	50,	'mahi',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-10-22 17:22:33',	'2020-10-22 17:22:33'),
(124,	73300,	51,	'testing',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-23 11:57:34',	'2020-10-23 11:57:34'),
(125,	76162,	51,	'aishu',	10,	'No',	'Pending',	1,	0,	'No',	'No',	0,	10,	0,	'2020-10-23 12:07:48',	'2020-10-23 12:07:48'),
(126,	79186,	51,	'aishu',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-10-23 12:07:58',	'2020-10-23 12:07:58'),
(127,	73089,	51,	'mahi',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-10-23 12:13:21',	'2020-10-23 12:13:21'),
(128,	81635,	51,	'Anjali G',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-10-23 12:15:01',	'2020-10-23 12:15:01'),
(129,	73109,	52,	'smstest',	10,	'Yes',	'Win',	2,	18,	'No',	'Yes',	0,	0,	10,	'2020-10-24 13:14:29',	'2020-10-24 13:14:29'),
(130,	2204,	52,	'Rajan',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-10-24 13:25:06',	'2020-10-24 13:25:06'),
(131,	2204,	53,	'Rajan',	10,	'Yes',	'Win',	2,	18,	'No',	'Yes',	0,	10,	0,	'2020-10-24 13:35:19',	'2020-10-24 13:35:19'),
(132,	73109,	53,	'smstest',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-24 13:35:33',	'2020-10-24 13:35:33'),
(133,	76581,	54,	'testplayer',	10,	'Yes',	'Win',	2,	18,	'No',	'Yes',	0,	10,	0,	'2020-10-26 16:06:35',	'2020-10-26 16:06:35'),
(134,	73109,	54,	'smstest',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-26 16:06:39',	'2020-10-26 16:06:39'),
(135,	73109,	55,	'smstest',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-26 16:20:16',	'2020-10-26 16:20:16'),
(136,	76581,	55,	'testplayer',	10,	'Yes',	'Win',	2,	18,	'No',	'Yes',	0,	10,	0,	'2020-10-26 16:22:15',	'2020-10-26 16:22:15'),
(137,	2204,	56,	'Rajan',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-10-26 16:43:43',	'2020-10-26 16:43:43'),
(138,	76581,	56,	'testplayer',	10,	'Yes',	'Loss',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-10-26 16:43:54',	'2020-10-26 16:43:54'),
(139,	73109,	56,	'smstest',	10,	'Yes',	'Loss',	2,	0,	'No',	'Yes',	0,	0,	10,	'2020-10-26 16:44:00',	'2020-10-26 16:44:00'),
(140,	73109,	57,	'smstest',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-27 12:23:54',	'2020-10-27 12:23:54'),
(141,	76581,	57,	'testplayer',	10,	'Yes',	'Win',	2,	18,	'No',	'Yes',	0,	10,	0,	'2020-10-27 12:25:19',	'2020-10-27 12:25:19'),
(142,	76162,	58,	'aishu',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-10-27 16:56:39',	'2020-10-27 16:56:39'),
(143,	76092,	58,	'SK sanjay',	10,	'No',	'Pending',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-27 17:04:05',	'2020-10-27 17:04:05'),
(144,	73300,	58,	'testing',	10,	'No',	'Out',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-27 17:07:02',	'2020-10-27 17:07:02'),
(145,	79186,	58,	'aishu',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-10-27 17:14:39',	'2020-10-27 17:14:39'),
(146,	76162,	59,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-10-27 17:37:20',	'2020-10-27 17:37:20'),
(147,	79186,	59,	'aishu',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-10-27 17:37:28',	'2020-10-27 17:37:28'),
(148,	76092,	59,	'SK sanjay',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-10-27 17:37:49',	'2020-10-27 17:37:49'),
(149,	73300,	59,	'testing',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-10-27 17:38:55',	'2020-10-27 17:38:55'),
(150,	76581,	60,	'testplayer',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-10-27 20:43:47',	'2020-10-27 20:43:47'),
(151,	73109,	60,	'smstest',	10,	'Yes',	'Win',	2,	18,	'No',	'Yes',	0,	0,	10,	'2020-10-27 20:44:12',	'2020-10-27 20:44:12'),
(152,	73109,	61,	'smstest',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-27 21:09:47',	'2020-10-27 21:09:47'),
(153,	76162,	61,	'aishu1',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-10-27 21:13:46',	'2020-10-27 21:13:46'),
(154,	79186,	61,	'aishu',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-10-27 21:16:33',	'2020-10-27 21:16:33'),
(155,	73300,	61,	'testing',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	10,	'2020-10-27 21:18:43',	'2020-10-27 21:18:43'),
(156,	73109,	62,	'smstest',	10,	'Yes',	'Win',	3,	36,	'No',	'Yes',	0,	0,	10,	'2020-10-28 14:48:30',	'2020-10-28 14:48:30'),
(157,	76162,	62,	'aishu1',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-10-28 15:12:00',	'2020-10-28 15:12:00'),
(158,	76092,	62,	'SK sanjay',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-10-28 15:13:32',	'2020-10-28 15:13:32'),
(159,	79186,	62,	'aishu',	10,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	10,	0,	'2020-10-28 15:14:34',	'2020-10-28 15:14:34'),
(160,	76162,	63,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-10-29 11:09:02',	'2020-10-29 11:09:02'),
(161,	79186,	63,	'aishu',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-10-29 11:09:04',	'2020-10-29 11:09:04'),
(162,	76092,	63,	'SK sanjay',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-10-29 11:09:17',	'2020-10-29 11:09:17'),
(163,	73300,	63,	'testing',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-10-29 11:17:32',	'2020-10-29 11:17:32'),
(164,	76581,	64,	'testplayer',	10,	'Yes',	'Win',	2,	18,	'No',	'Yes',	0,	10,	0,	'2020-11-02 12:05:35',	'2020-11-02 12:05:35'),
(165,	73109,	64,	'smstest',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-11-02 12:05:41',	'2020-11-02 12:05:41'),
(166,	73300,	65,	'testing',	10,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	0,	10,	'2020-11-02 12:40:29',	'2020-11-02 12:40:29'),
(167,	79186,	65,	'aishu',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-11-02 12:41:05',	'2020-11-02 12:41:05'),
(168,	76162,	65,	'aishu1',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	10,	0,	'2020-11-02 12:41:12',	'2020-11-02 12:41:12'),
(169,	76092,	65,	'SK sanjay',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-11-02 12:41:24',	'2020-11-02 12:41:24'),
(170,	73109,	65,	'smstest',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	10,	'2020-11-02 12:47:40',	'2020-11-02 12:47:40'),
(171,	73104,	65,	'Amol G',	10,	'Yes',	'Win',	3,	0,	'No',	'Yes',	0,	0,	10,	'2020-11-02 13:06:24',	'2020-11-02 13:06:24'),
(172,	76162,	66,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-02 13:21:47',	'2020-11-02 13:21:47'),
(173,	79186,	66,	'aishu',	5,	'No',	'Pending',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-02 13:22:15',	'2020-11-02 13:22:15'),
(174,	73109,	66,	'smstest',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-02 13:26:59',	'2020-11-02 13:26:59'),
(175,	73109,	67,	'smstest',	10,	'Yes',	'Win',	2,	9,	'No',	'Yes',	0,	0,	10,	'2020-11-02 13:48:00',	'2020-11-02 13:48:00'),
(176,	73300,	68,	'testing',	5,	'Yes',	'Win',	2,	13,	'No',	'Yes',	0,	0,	5,	'2020-11-02 14:10:39',	'2020-11-02 14:10:39'),
(177,	76162,	68,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-02 14:12:32',	'2020-11-02 14:12:32'),
(178,	73109,	68,	'smstest',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-02 14:12:37',	'2020-11-02 14:12:37'),
(179,	76162,	69,	'aishu1',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-02 18:13:28',	'2020-11-02 18:13:28'),
(180,	79186,	69,	'aishu',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-02 18:14:01',	'2020-11-02 18:14:01'),
(181,	76092,	69,	'SK sanjay',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-02 18:14:11',	'2020-11-02 18:14:11'),
(182,	73300,	69,	'testing',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-02 18:14:13',	'2020-11-02 18:14:13'),
(183,	73104,	69,	'Amol G',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-02 18:15:35',	'2020-11-02 18:15:35'),
(184,	79186,	70,	'aishu',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-03 13:03:44',	'2020-11-03 13:03:44'),
(185,	73109,	70,	'smstest',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-03 13:04:40',	'2020-11-03 13:04:40'),
(186,	76092,	70,	'SK sanjay',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-03 13:05:19',	'2020-11-03 13:05:19'),
(187,	73300,	71,	'testing',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-11-03 13:32:29',	'2020-11-03 13:32:29'),
(188,	79186,	71,	'aishu',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-11-03 13:33:30',	'2020-11-03 13:33:30'),
(189,	73109,	71,	'smstest',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	10,	'2020-11-03 13:33:46',	'2020-11-03 13:33:46'),
(190,	76581,	71,	'testplayer',	10,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-11-03 13:33:54',	'2020-11-03 13:33:54'),
(191,	79186,	72,	'aishu',	5,	'No',	'Pending',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-03 13:55:46',	'2020-11-03 13:55:46'),
(192,	73300,	72,	'testing',	5,	'No',	'Out',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-03 13:57:24',	'2020-11-03 13:57:24'),
(193,	73109,	72,	'smstest',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-03 14:31:19',	'2020-11-03 14:31:19'),
(194,	76581,	72,	'testplayer',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-03 14:31:29',	'2020-11-03 14:31:29'),
(195,	79186,	73,	'aishu',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-03 14:43:39',	'2020-11-03 14:43:39'),
(196,	73300,	73,	'testing',	5,	'No',	'Out',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-03 14:51:10',	'2020-11-03 14:51:10'),
(197,	73109,	73,	'smstest',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-03 14:53:57',	'2020-11-03 14:53:57'),
(198,	76581,	73,	'testplayer',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-03 14:54:54',	'2020-11-03 14:54:54'),
(199,	76162,	74,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-03 18:52:53',	'2020-11-03 18:52:53'),
(200,	79186,	74,	'aishu',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-03 18:53:01',	'2020-11-03 18:53:01'),
(201,	73300,	74,	'testing',	5,	'Yes',	'Loss',	2,	17,	'No',	'No',	0,	0,	5,	'2020-11-03 18:57:48',	'2020-11-03 18:57:48'),
(202,	76092,	74,	'SK sanjay',	5,	'Yes',	'Win',	3,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-03 19:03:04',	'2020-11-03 19:03:04'),
(203,	76162,	75,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 13:06:04',	'2020-11-04 13:06:04'),
(204,	73300,	75,	'testing',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-04 13:06:20',	'2020-11-04 13:06:20'),
(205,	76092,	75,	'SK sanjay',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-04 13:06:47',	'2020-11-04 13:06:47'),
(206,	79186,	75,	'aishu',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-04 13:06:54',	'2020-11-04 13:06:54'),
(207,	79186,	76,	'aishu',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-04 14:04:41',	'2020-11-04 14:04:41'),
(208,	73300,	76,	'testing',	5,	'Yes',	'Win',	3,	17,	'No',	'Yes',	0,	0,	5,	'2020-11-04 14:05:39',	'2020-11-04 14:05:39'),
(209,	76162,	76,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 14:06:10',	'2020-11-04 14:06:10'),
(210,	76092,	76,	'SK sanjay',	5,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 14:07:26',	'2020-11-04 14:07:26'),
(212,	100752,	77,	'shubham jagtap',	5,	'Yes',	'Loss',	3,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:46:35',	'2020-11-04 16:46:35'),
(213,	157,	77,	'Ramkrushna Kadam',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:46:40',	'2020-11-04 16:46:40'),
(214,	100750,	77,	'piyush bhandari',	5,	'Yes',	'Win',	5,	52,	'No',	'Yes',	0,	5,	0,	'2020-11-04 16:46:57',	'2020-11-04 16:46:57'),
(215,	100801,	77,	'Swapnali Dabhade',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:46:58',	'2020-11-04 16:46:58'),
(216,	100763,	77,	'shraddha Jadhav',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:46:58',	'2020-11-04 16:46:58'),
(218,	100751,	77,	'Harshal',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:47:16',	'2020-11-04 16:47:16'),
(220,	79186,	77,	'aishu',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-04 16:47:45',	'2020-11-04 16:47:45'),
(221,	100800,	77,	'Sohail Mujawar',	5,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:48:10',	'2020-11-04 16:48:10'),
(223,	76092,	77,	'SK sanjay',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:49:06',	'2020-11-04 16:49:06'),
(224,	76162,	77,	'aishu1',	5,	'Yes',	'Loss',	4,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:50:19',	'2020-11-04 16:50:19'),
(225,	73300,	77,	'testing',	5,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	0,	5,	'2020-11-04 16:50:48',	'2020-11-04 16:50:48'),
(226,	70961,	77,	'ji',	5,	'Yes',	'Loss',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-04 16:52:14',	'2020-11-04 16:52:14'),
(227,	100758,	77,	'Mayuri',	5,	'Yes',	'Loss',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-04 16:52:40',	'2020-11-04 16:52:40'),
(228,	100788,	77,	'Mamta',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:57:23',	'2020-11-04 16:57:23'),
(230,	81635,	77,	'Anjali G',	5,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	5,	0,	'2020-11-04 16:59:37',	'2020-11-04 16:59:37'),
(231,	79186,	78,	'aishu',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-06 11:55:22',	'2020-11-06 11:55:22'),
(232,	73300,	78,	'testing',	5,	'Yes',	'Loss',	2,	17,	'No',	'No',	0,	0,	5,	'2020-11-06 11:55:54',	'2020-11-06 11:55:54'),
(234,	76092,	78,	'SK sanjay',	5,	'Yes',	'Win',	3,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-06 11:56:57',	'2020-11-06 11:56:57'),
(235,	76162,	78,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-06 11:57:33',	'2020-11-06 11:57:33'),
(236,	76092,	79,	'SK sanjay',	5,	'Yes',	'Win',	3,	17,	'No',	'Yes',	0,	5,	0,	'2020-11-06 14:43:03',	'2020-11-06 14:43:03'),
(237,	73300,	79,	'testing',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-06 14:43:36',	'2020-11-06 14:43:36'),
(238,	79186,	79,	'aishu',	5,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	0,	5,	'2020-11-06 15:33:14',	'2020-11-06 15:33:14'),
(239,	76162,	79,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-06 15:33:59',	'2020-11-06 15:33:59'),
(240,	76581,	80,	'testplayer',	10,	'Yes',	'Loss',	2,	0,	'No',	'Yes',	0,	10,	0,	'2020-11-10 15:55:59',	'2020-11-10 15:55:59'),
(241,	73109,	80,	'smstest',	10,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	10,	'2020-11-10 15:56:06',	'2020-11-10 15:56:06'),
(242,	22325,	80,	'vishwa',	10,	'Yes',	'Loss',	2,	0,	'No',	'Yes',	0,	0,	10,	'2020-11-10 15:56:08',	'2020-11-10 15:56:08'),
(243,	76162,	81,	'aishu1',	5,	'Yes',	'Win',	3,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 15:22:55',	'2020-11-26 15:22:55'),
(245,	76092,	81,	'SK sanjay',	5,	'Yes',	'Win',	4,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-26 15:26:34',	'2020-11-26 15:26:34'),
(246,	100750,	81,	'piyush bhandari',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 15:26:49',	'2020-11-26 15:26:49'),
(247,	123709,	81,	'pragati',	5,	'Yes',	'Loss',	3,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 15:27:18',	'2020-11-26 15:27:18'),
(248,	100788,	81,	'Mamta',	5,	'No',	'Out',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 15:28:53',	'2020-11-26 15:28:53'),
(249,	123725,	81,	'shraddha ',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 15:28:58',	'2020-11-26 15:28:58'),
(250,	73300,	81,	'testing',	5,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	0,	5,	'2020-11-26 15:30:24',	'2020-11-26 15:30:24'),
(252,	123728,	81,	'jay',	5,	'No',	'Out',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 15:30:58',	'2020-11-26 15:30:58'),
(253,	100751,	81,	'Harshal',	5,	'Yes',	'Win',	3,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 15:31:29',	'2020-11-26 15:31:29'),
(254,	123727,	81,	'rubina',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 15:34:01',	'2020-11-26 15:34:01'),
(256,	123729,	81,	'raj gaikwad',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 15:35:07',	'2020-11-26 15:35:07'),
(257,	100758,	81,	'Mayuri',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 15:35:25',	'2020-11-26 15:35:25'),
(259,	70961,	81,	'ji',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-26 15:37:07',	'2020-11-26 15:37:07'),
(260,	123732,	81,	'varun',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 15:41:49',	'2020-11-26 15:41:49'),
(261,	100797,	81,	'Shabista',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 15:42:51',	'2020-11-26 15:42:51'),
(263,	73104,	81,	'Amol G',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-26 15:57:09',	'2020-11-26 15:57:09'),
(264,	76162,	82,	'aishu1',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 16:36:03',	'2020-11-26 16:36:03'),
(265,	100758,	82,	'Mayuri',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 16:36:36',	'2020-11-26 16:36:36'),
(266,	100751,	82,	'Harshal',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 16:36:53',	'2020-11-26 16:36:53'),
(267,	123731,	82,	'Rushi sathe ',	5,	'No',	'Pending',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 16:37:26',	'2020-11-26 16:37:26'),
(268,	123732,	82,	'varun',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 16:37:44',	'2020-11-26 16:37:44'),
(269,	123725,	82,	'shraddha ',	5,	'No',	'Pending',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 16:38:22',	'2020-11-26 16:38:22'),
(270,	123709,	82,	'pragati',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 16:38:29',	'2020-11-26 16:38:29'),
(271,	73104,	82,	'Amol G',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-26 16:38:54',	'2020-11-26 16:38:54'),
(272,	73300,	82,	'testing',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-26 16:38:56',	'2020-11-26 16:38:56'),
(273,	76092,	82,	'SK sanjay',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	0,	5,	'2020-11-26 16:39:26',	'2020-11-26 16:39:26'),
(275,	100800,	82,	'Sohail Mujawar',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 16:41:42',	'2020-11-26 16:41:42'),
(277,	100750,	82,	'piyush bhandari',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 16:42:15',	'2020-11-26 16:42:15'),
(278,	100788,	82,	'Mamta',	5,	'No',	'Out',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 16:42:39',	'2020-11-26 16:42:39'),
(280,	123694,	82,	'ankita',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-26 16:43:03',	'2020-11-26 16:43:03'),
(281,	123728,	82,	'jay',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-26 16:50:06',	'2020-11-26 16:50:06'),
(282,	70961,	82,	'ji',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-26 16:54:20',	'2020-11-26 16:54:20'),
(283,	76162,	83,	'aishu1',	5,	'Yes',	'Win',	2,	68,	'No',	'Yes',	0,	5,	0,	'2020-11-27 10:50:55',	'2020-11-27 10:50:55'),
(286,	100750,	83,	'piyush bhandari',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-27 10:51:22',	'2020-11-27 10:51:22'),
(287,	123694,	83,	'ankita',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 10:51:26',	'2020-11-27 10:51:26'),
(288,	100752,	83,	'shubham jagtap',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 10:52:08',	'2020-11-27 10:52:08'),
(289,	100758,	83,	'Mayuri',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-27 10:52:17',	'2020-11-27 10:52:17'),
(290,	70961,	83,	'ji',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	0,	5,	'2020-11-27 10:54:37',	'2020-11-27 10:54:37'),
(291,	100800,	83,	'Sohail Mujawar',	5,	'Yes',	'Loss',	2,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 11:01:28',	'2020-11-27 11:01:28'),
(292,	100751,	83,	'Harshal',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 11:01:41',	'2020-11-27 11:01:41'),
(293,	157,	83,	'R.K',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 11:02:04',	'2020-11-27 11:02:04'),
(300,	123902,	83,	'snehal',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-27 11:30:50',	'2020-11-27 11:30:50'),
(301,	73300,	83,	'testing',	5,	'Yes',	'Win',	2,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-27 11:32:17',	'2020-11-27 11:32:17'),
(302,	81635,	83,	'Anjali G',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 11:34:44',	'2020-11-27 11:34:44'),
(303,	81638,	83,	'Osomose',	5,	'Yes',	'Win',	4,	0,	'No',	'Yes',	0,	5,	0,	'2020-11-27 11:40:02',	'2020-11-27 11:40:02'),
(307,	123727,	83,	'rubina',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 11:41:19',	'2020-11-27 11:41:19'),
(308,	100763,	83,	'shraddha Jadhav',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 11:41:28',	'2020-11-27 11:41:28'),
(309,	123728,	83,	'jay',	5,	'Yes',	'Loss',	1,	0,	'No',	'No',	0,	5,	0,	'2020-11-27 11:41:37',	'2020-11-27 11:41:37');

DROP TABLE IF EXISTS `tournament_win_loss_logs`;
CREATE TABLE `tournament_win_loss_logs` (
  `tournamentWinLossLogId` int(11) NOT NULL AUTO_INCREMENT,
  `tournamentId` int(11) NOT NULL,
  `tournamentTitle` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `startDate` varchar(100) NOT NULL,
  `startTime` varchar(100) NOT NULL,
  `userName` varchar(150) NOT NULL,
  `entryFee` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `roundStatus` enum('Win','Loss') NOT NULL,
  `playerLimitInRoom` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`tournamentWinLossLogId`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=latin1;

INSERT INTO `tournament_win_loss_logs` (`tournamentWinLossLogId`, `tournamentId`, `tournamentTitle`, `userId`, `startDate`, `startTime`, `userName`, `entryFee`, `round`, `roundStatus`, `playerLimitInRoom`, `created`) VALUES
(42,	43,	'Test10',	46,	'2020-06-16',	'18:36:00',	'vishwa1',	10,	1,	'Loss',	2,	'2020-06-16 18:38:40'),
(43,	43,	'Test10',	1,	'2020-06-16',	'18:36:00',	'Vishwa',	10,	1,	'Win',	2,	'2020-06-16 18:38:40'),
(44,	43,	'Test10',	27,	'2020-06-16',	'18:36:00',	'rajan',	10,	1,	'Loss',	2,	'2020-06-16 18:39:21'),
(45,	43,	'Test10',	48,	'2020-06-16',	'18:36:00',	'Ashish',	10,	1,	'Win',	2,	'2020-06-16 18:39:21'),
(46,	44,	'Test123',	46,	'2020-06-17',	'12:15:00',	'vishwa1',	10,	1,	'Loss',	2,	'2020-06-17 12:16:14'),
(47,	44,	'Test123',	25,	'2020-06-17',	'12:15:00',	'testplayer',	10,	1,	'Win',	2,	'2020-06-17 12:16:14'),
(48,	45,	'Test21',	46,	'2020-06-17',	'12:51:00',	'vishwa1',	100,	1,	'Loss',	2,	'2020-06-17 12:52:42'),
(49,	45,	'Test21',	25,	'2020-06-17',	'12:51:00',	'testplayer',	100,	1,	'Win',	2,	'2020-06-17 12:52:43'),
(50,	45,	'Test21',	1,	'2020-06-17',	'12:51:00',	'Vishwa',	100,	1,	'Loss',	2,	'2020-06-17 12:53:04'),
(51,	45,	'Test21',	27,	'2020-06-17',	'12:51:00',	'rajan',	100,	1,	'Win',	2,	'2020-06-17 12:53:05'),
(52,	45,	'Test21',	25,	'2020-06-17',	'12:56:00',	'testplayer',	100,	2,	'Loss',	2,	'2020-06-17 12:58:19'),
(53,	45,	'Test21',	27,	'2020-06-17',	'12:56:00',	'rajan',	100,	2,	'Win',	2,	'2020-06-17 12:58:20'),
(54,	46,	'testskill',	25,	'2020-06-17',	'16:32:00',	'testplayer',	10,	1,	'Loss',	2,	'2020-06-17 16:33:28'),
(55,	46,	'testskill',	27,	'2020-06-17',	'16:32:00',	'rajan',	10,	1,	'Win',	2,	'2020-06-17 16:33:29'),
(56,	46,	'testskill',	53,	'2020-06-17',	'16:32:00',	'test',	10,	1,	'Loss',	2,	'2020-06-17 16:35:53'),
(57,	46,	'testskill',	1,	'2020-06-17',	'16:32:00',	'Vishwa',	10,	1,	'Win',	2,	'2020-06-17 16:35:53'),
(58,	46,	'testskill',	27,	'2020-06-17',	'16:37:00',	'rajan',	10,	2,	'Win',	2,	'2020-06-17 16:43:11'),
(59,	46,	'testskill',	1,	'2020-06-17',	'16:37:00',	'Vishwa',	10,	2,	'Loss',	2,	'2020-06-17 16:43:11'),
(60,	47,	'test111',	25,	'2020-07-03',	'18:15:00',	'testplayer',	10,	1,	'Loss',	2,	'2020-07-03 18:16:28'),
(61,	47,	'test111',	53,	'2020-07-03',	'18:15:00',	'test',	10,	1,	'Win',	2,	'2020-07-03 18:16:28'),
(62,	47,	'test111',	27,	'2020-07-03',	'18:15:00',	'rajan',	10,	1,	'Win',	2,	'2020-07-03 18:19:55'),
(63,	47,	'test111',	53,	'2020-07-03',	'18:21:00',	'test',	10,	2,	'Loss',	2,	'2020-07-03 18:23:07'),
(64,	47,	'test111',	27,	'2020-07-03',	'18:21:00',	'rajan',	10,	2,	'Loss',	2,	'2020-07-03 18:23:42'),
(65,	50,	'Test 2',	73300,	'2020-10-22',	'18:15:00',	'testing',	10,	1,	'Loss',	2,	'2020-10-22 18:18:25'),
(66,	50,	'Test 2',	73089,	'2020-10-22',	'18:15:00',	'mahi',	10,	1,	'Win',	2,	'2020-10-22 18:18:26'),
(67,	50,	'Test 2',	79186,	'2020-10-22',	'18:15:00',	'aishu',	10,	1,	'Win',	2,	'2020-10-22 18:19:52'),
(68,	51,	'weekend contest',	81635,	'2020-10-23',	'12:30:00',	'Anjali G',	10,	1,	'Loss',	2,	'2020-10-23 12:32:12'),
(69,	51,	'weekend contest',	73089,	'2020-10-23',	'12:30:00',	'mahi',	10,	1,	'Win',	2,	'2020-10-23 12:32:13'),
(70,	51,	'weekend contest',	73300,	'2020-10-23',	'12:30:00',	'testing',	10,	1,	'Loss',	2,	'2020-10-23 12:32:19'),
(71,	51,	'weekend contest',	79186,	'2020-10-23',	'12:30:00',	'aishu',	10,	1,	'Win',	2,	'2020-10-23 12:32:20'),
(72,	52,	'Tour12',	2204,	'2020-10-24',	'13:26:00',	'Rajan',	10,	1,	'Loss',	2,	'2020-10-24 13:28:13'),
(73,	52,	'Tour12',	73109,	'2020-10-24',	'13:26:00',	'smstest',	10,	1,	'Win',	2,	'2020-10-24 13:28:14'),
(74,	53,	'Tour13',	73109,	'2020-10-24',	'13:36:00',	'smstest',	10,	1,	'Loss',	2,	'2020-10-24 13:38:15'),
(75,	53,	'Tour13',	2204,	'2020-10-24',	'13:36:00',	'Rajan',	10,	1,	'Win',	2,	'2020-10-24 13:38:16'),
(76,	54,	'Tour1',	73109,	'2020-10-26',	'16:07:00',	'smstest',	10,	1,	'Loss',	2,	'2020-10-26 16:09:27'),
(77,	54,	'Tour1',	76581,	'2020-10-26',	'16:07:00',	'testplayer',	10,	1,	'Win',	2,	'2020-10-26 16:09:28'),
(78,	55,	'Tour1',	73109,	'2020-10-26',	'16:23:00',	'smstest',	10,	1,	'Loss',	2,	'2020-10-26 16:27:11'),
(79,	55,	'Tour1',	76581,	'2020-10-26',	'16:23:00',	'testplayer',	10,	1,	'Win',	2,	'2020-10-26 16:27:12'),
(80,	56,	'Tour26',	2204,	'2020-10-26',	'16:45:00',	'Rajan',	10,	1,	'Loss',	2,	'2020-10-26 16:47:38'),
(81,	56,	'Tour26',	73109,	'2020-10-26',	'16:45:00',	'smstest',	10,	1,	'Win',	2,	'2020-10-26 16:47:38'),
(82,	56,	'Tour26',	76581,	'2020-10-26',	'16:45:00',	'testplayer',	10,	1,	'Win',	2,	'2020-10-26 16:49:53'),
(83,	56,	'Tour26',	73109,	'2020-10-26',	'16:53:00',	'smstest',	10,	2,	'Loss',	2,	'2020-10-26 16:54:04'),
(84,	56,	'Tour26',	76581,	'2020-10-26',	'16:53:00',	'testplayer',	10,	2,	'Loss',	2,	'2020-10-26 16:54:11'),
(85,	57,	'Tour27',	73109,	'2020-10-27',	'12:26:00',	'smstest',	10,	1,	'Loss',	2,	'2020-10-27 12:30:58'),
(86,	57,	'Tour27',	76581,	'2020-10-27',	'12:26:00',	'testplayer',	10,	1,	'Win',	2,	'2020-10-27 12:30:58'),
(87,	58,	'Testing',	79186,	'2020-10-27',	'17:20:00',	'aishu',	10,	1,	'Loss',	2,	'2020-10-27 17:22:28'),
(88,	58,	'Testing',	76162,	'2020-10-27',	'17:20:00',	'aishu',	10,	1,	'Win',	2,	'2020-10-27 17:22:29'),
(89,	59,	'testing2',	76092,	'2020-10-27',	'17:42:00',	'SK sanjay',	5,	1,	'Loss',	2,	'2020-10-27 17:43:50'),
(90,	59,	'testing2',	73300,	'2020-10-27',	'17:42:00',	'testing',	5,	1,	'Win',	2,	'2020-10-27 17:43:51'),
(91,	59,	'testing2',	79186,	'2020-10-27',	'17:42:00',	'aishu',	5,	1,	'Win',	2,	'2020-10-27 17:51:59'),
(92,	59,	'testing2',	76162,	'2020-10-27',	'17:42:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-10-27 17:51:59'),
(93,	60,	'Tour278',	76581,	'2020-10-27',	'20:45:00',	'testplayer',	10,	1,	'Loss',	2,	'2020-10-27 20:50:19'),
(94,	60,	'Tour278',	73109,	'2020-10-27',	'20:45:00',	'smstest',	10,	1,	'Win',	2,	'2020-10-27 20:50:20'),
(95,	61,	'Tour27',	73109,	'2020-10-27',	'21:22:00',	'smstest',	10,	1,	'Loss',	2,	'2020-10-27 21:30:22'),
(96,	61,	'Tour27',	76162,	'2020-10-27',	'21:22:00',	'aishu1',	10,	1,	'Win',	2,	'2020-10-27 21:30:23'),
(97,	61,	'Tour27',	79186,	'2020-10-27',	'21:22:00',	'aishu',	10,	1,	'Loss',	2,	'2020-10-27 21:36:29'),
(98,	61,	'Tour27',	73300,	'2020-10-27',	'21:22:00',	'testing',	10,	1,	'Win',	2,	'2020-10-27 21:36:30'),
(99,	62,	'Tour28',	76092,	'2020-10-28',	'15:16:00',	'SK sanjay',	10,	1,	'Loss',	2,	'2020-10-28 15:17:58'),
(100,	62,	'Tour28',	79186,	'2020-10-28',	'15:16:00',	'aishu',	10,	1,	'Win',	2,	'2020-10-28 15:17:59'),
(101,	62,	'Tour28',	76162,	'2020-10-28',	'15:16:00',	'aishu1',	10,	1,	'Loss',	2,	'2020-10-28 15:31:24'),
(102,	62,	'Tour28',	73109,	'2020-10-28',	'15:16:00',	'smstest',	10,	1,	'Win',	2,	'2020-10-28 15:31:25'),
(103,	62,	'Tour28',	79186,	'2020-10-28',	'16:31:27',	'aishu',	10,	2,	'Loss',	2,	'2020-10-28 16:35:54'),
(104,	62,	'Tour28',	73109,	'2020-10-28',	'16:31:27',	'smstest',	10,	2,	'Win',	2,	'2020-10-28 16:35:55'),
(105,	63,	'tournamnet1',	76092,	'2020-10-29',	'11:18:00',	'SK sanjay',	5,	1,	'Loss',	2,	'2020-10-29 11:21:35'),
(106,	63,	'tournamnet1',	73300,	'2020-10-29',	'11:18:00',	'testing',	5,	1,	'Win',	2,	'2020-10-29 11:21:36'),
(107,	63,	'tournamnet1',	76162,	'2020-10-29',	'11:18:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-10-29 11:26:15'),
(108,	63,	'tournamnet1',	79186,	'2020-10-29',	'11:18:00',	'aishu',	5,	1,	'Win',	2,	'2020-10-29 11:26:16'),
(109,	64,	'Tour2',	76581,	'2020-11-02',	'12:06:00',	'testplayer',	10,	1,	'Win',	2,	'2020-11-02 12:17:34'),
(110,	64,	'Tour2',	73109,	'2020-11-02',	'12:06:00',	'smstest',	10,	1,	'Loss',	2,	'2020-11-02 12:17:34'),
(111,	65,	'tournament 2',	79186,	'2020-11-02',	'13:08:00',	'aishu',	10,	1,	'Loss',	2,	'2020-11-02 13:14:28'),
(112,	65,	'tournament 2',	73300,	'2020-11-02',	'13:08:00',	'testing',	10,	1,	'Win',	2,	'2020-11-02 13:14:29'),
(113,	65,	'tournament 2',	76162,	'2020-11-02',	'13:08:00',	'aishu1',	10,	1,	'Loss',	2,	'2020-11-02 13:15:54'),
(114,	65,	'tournament 2',	73109,	'2020-11-02',	'13:08:00',	'smstest',	10,	1,	'Win',	2,	'2020-11-02 13:15:55'),
(115,	65,	'tournament 2',	73104,	'2020-11-02',	'13:08:00',	'Amol G',	10,	1,	'Win',	2,	'2020-11-02 13:28:30'),
(116,	65,	'tournament 2',	76092,	'2020-11-02',	'13:08:00',	'SK sanjay',	10,	1,	'Loss',	2,	'2020-11-02 13:28:30'),
(117,	66,	'Tournament 3',	73109,	'2020-11-02',	'13:27:00',	'smstest',	5,	1,	'Win',	2,	'2020-11-02 13:39:47'),
(118,	66,	'Tournament 3',	76162,	'2020-11-02',	'13:27:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-11-02 13:39:47'),
(119,	67,	'Tour4',	73109,	'2020-11-02',	'13:49:00',	'smstest',	10,	1,	'Win',	2,	'2020-11-02 13:53:58'),
(120,	68,	'Tournament 4',	73109,	'2020-11-02',	'14:12:00',	'smstest',	5,	1,	'Loss',	3,	'2020-11-02 14:15:41'),
(121,	68,	'Tournament 4',	76162,	'2020-11-02',	'14:12:00',	'aishu1',	5,	1,	'Loss',	3,	'2020-11-02 14:19:13'),
(122,	68,	'Tournament 4',	73300,	'2020-11-02',	'14:12:00',	'testing',	5,	1,	'Win',	3,	'2020-11-02 14:19:15'),
(123,	65,	'tournament 2',	73300,	'2020-11-02',	'14:28:32',	'testing',	10,	2,	'Loss',	2,	'2020-11-02 14:39:35'),
(124,	65,	'tournament 2',	73104,	'2020-11-02',	'14:28:32',	'Amol G',	10,	2,	'Win',	2,	'2020-11-02 14:39:36'),
(125,	69,	'Tournament 5',	76162,	'2020-11-02',	'18:16:00',	'aishu1',	5,	1,	'Win',	2,	'2020-11-02 18:20:55'),
(126,	69,	'Tournament 5',	73104,	'2020-11-02',	'18:16:00',	'Amol G',	5,	1,	'Loss',	2,	'2020-11-02 18:22:26'),
(127,	69,	'Tournament 5',	73300,	'2020-11-02',	'18:16:00',	'testing',	5,	1,	'Win',	2,	'2020-11-02 18:22:27'),
(128,	69,	'Tournament 5',	79186,	'2020-11-02',	'18:16:00',	'aishu',	5,	1,	'Loss',	2,	'2020-11-02 18:25:21'),
(129,	69,	'Tournament 5',	76092,	'2020-11-02',	'18:16:00',	'SK sanjay',	5,	1,	'Win',	2,	'2020-11-02 18:25:23'),
(130,	70,	'Tournament 6',	76092,	'2020-11-03',	'13:05:00',	'SK sanjay',	5,	1,	'Loss',	2,	'2020-11-03 13:07:40'),
(131,	70,	'Tournament 6',	79186,	'2020-11-03',	'13:05:00',	'aishu',	5,	1,	'Win',	2,	'2020-11-03 13:07:41'),
(132,	70,	'Tournament 6',	73109,	'2020-11-03',	'13:05:00',	'smstest',	5,	1,	'Win',	2,	'2020-11-03 13:09:56'),
(133,	71,	'Tournament 7',	73300,	'2020-11-03',	'13:34:00',	'testing',	10,	1,	'Loss',	2,	'2020-11-03 13:36:36'),
(134,	71,	'Tournament 7',	76581,	'2020-11-03',	'13:34:00',	'testplayer',	10,	1,	'Win',	2,	'2020-11-03 13:36:37'),
(135,	71,	'Tournament 7',	79186,	'2020-11-03',	'13:34:00',	'aishu',	10,	1,	'Loss',	2,	'2020-11-03 13:37:40'),
(136,	71,	'Tournament 7',	73109,	'2020-11-03',	'13:34:00',	'smstest',	10,	1,	'Win',	2,	'2020-11-03 13:37:41'),
(137,	72,	'Tournament 8',	76581,	'2020-11-03',	'14:34:00',	'testplayer',	5,	1,	'Loss',	2,	'2020-11-03 14:38:28'),
(138,	72,	'Tournament 8',	73109,	'2020-11-03',	'14:34:00',	'smstest',	5,	1,	'Win',	2,	'2020-11-03 14:38:29'),
(139,	73,	'Tournament 8',	76581,	'2020-11-03',	'14:58:00',	'testplayer',	5,	1,	'Win',	2,	'2020-11-03 15:02:55'),
(140,	73,	'Tournament 8',	79186,	'2020-11-03',	'14:58:00',	'aishu',	5,	1,	'Loss',	2,	'2020-11-03 15:10:51'),
(141,	73,	'Tournament 8',	73109,	'2020-11-03',	'14:58:00',	'smstest',	5,	1,	'Win',	2,	'2020-11-03 15:10:51'),
(142,	74,	'Tournament 9',	76092,	'2020-11-03',	'19:04:00',	'SK sanjay',	5,	1,	'Win',	2,	'2020-11-03 19:19:02'),
(143,	74,	'Tournament 9',	76162,	'2020-11-03',	'19:04:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-11-03 19:19:02'),
(144,	74,	'Tournament 9',	73300,	'2020-11-03',	'19:04:00',	'testing',	5,	1,	'Win',	2,	'2020-11-03 19:21:12'),
(145,	74,	'Tournament 9',	79186,	'2020-11-03',	'19:04:00',	'aishu',	5,	1,	'Loss',	2,	'2020-11-03 19:21:12'),
(146,	74,	'Tournament 9',	73300,	'2020-11-03',	'20:21:15',	'testing',	5,	2,	'Loss',	2,	'2020-11-03 20:33:11'),
(147,	74,	'Tournament 9',	76092,	'2020-11-03',	'20:21:15',	'SK sanjay',	5,	2,	'Win',	2,	'2020-11-03 20:33:11'),
(148,	75,	'Tournament 10',	79186,	'2020-11-04',	'13:45:00',	'aishu',	5,	1,	'Loss',	2,	'2020-11-04 13:48:17'),
(149,	75,	'Tournament 10',	73300,	'2020-11-04',	'13:45:00',	'testing',	5,	1,	'Win',	2,	'2020-11-04 13:48:18'),
(150,	75,	'Tournament 10',	76162,	'2020-11-04',	'13:45:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-11-04 13:48:42'),
(151,	75,	'Tournament 10',	76092,	'2020-11-04',	'13:45:00',	'SK sanjay',	5,	1,	'Win',	2,	'2020-11-04 13:48:43'),
(152,	76,	'Tournament 11',	76162,	'2020-11-04',	'14:13:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-11-04 14:17:18'),
(153,	76,	'Tournament 11',	76092,	'2020-11-04',	'14:13:00',	'SK sanjay',	5,	1,	'Win',	2,	'2020-11-04 14:17:19'),
(154,	76,	'Tournament 11',	73300,	'2020-11-04',	'14:13:00',	'testing',	5,	1,	'Win',	2,	'2020-11-04 14:30:14'),
(155,	76,	'Tournament 11',	79186,	'2020-11-04',	'14:13:00',	'aishu',	5,	1,	'Loss',	2,	'2020-11-04 14:30:14'),
(156,	76,	'Tournament 11',	73300,	'2020-11-04',	'15:30:15',	'testing',	5,	2,	'Win',	2,	'2020-11-04 15:53:14'),
(157,	76,	'Tournament 11',	76092,	'2020-11-04',	'15:30:15',	'SK sanjay',	5,	2,	'Loss',	2,	'2020-11-04 15:53:14'),
(158,	77,	'Tournament 12',	81635,	'2020-11-04',	'16:56:00',	'Anjali G',	5,	1,	'Win',	2,	'2020-11-04 17:00:52'),
(159,	77,	'Tournament 12',	100788,	'2020-11-04',	'16:56:00',	'Mamta',	5,	1,	'Loss',	2,	'2020-11-04 17:01:11'),
(160,	77,	'Tournament 12',	100752,	'2020-11-04',	'16:56:00',	'shubham jagtap',	5,	1,	'Win',	2,	'2020-11-04 17:01:12'),
(161,	77,	'Tournament 12',	70961,	'2020-11-04',	'16:56:00',	'ji',	5,	1,	'Win',	2,	'2020-11-04 17:07:40'),
(162,	77,	'Tournament 12',	157,	'2020-11-04',	'16:56:00',	'Ramkrushna Kadam',	5,	1,	'Loss',	2,	'2020-11-04 17:07:40'),
(163,	77,	'Tournament 12',	76092,	'2020-11-04',	'16:56:00',	'SK sanjay',	5,	1,	'Loss',	2,	'2020-11-04 17:10:31'),
(164,	77,	'Tournament 12',	100758,	'2020-11-04',	'16:56:00',	'Mayuri',	5,	1,	'Win',	2,	'2020-11-04 17:10:31'),
(165,	77,	'Tournament 12',	100751,	'2020-11-04',	'16:56:00',	'Harshal',	5,	1,	'Loss',	2,	'2020-11-04 17:12:11'),
(166,	77,	'Tournament 12',	100800,	'2020-11-04',	'16:56:00',	'Sohail Mujawar',	5,	1,	'Win',	2,	'2020-11-04 17:12:11'),
(167,	77,	'Tournament 12',	73300,	'2020-11-04',	'16:56:00',	'testing',	5,	1,	'Win',	2,	'2020-11-04 17:12:29'),
(168,	77,	'Tournament 12',	79186,	'2020-11-04',	'16:56:00',	'aishu',	5,	1,	'Loss',	2,	'2020-11-04 17:12:29'),
(169,	77,	'Tournament 12',	100763,	'2020-11-04',	'16:56:00',	'shraddha Jadhav',	5,	1,	'Loss',	2,	'2020-11-04 17:13:31'),
(170,	77,	'Tournament 12',	100750,	'2020-11-04',	'16:56:00',	'piyush bhandari',	5,	1,	'Win',	2,	'2020-11-04 17:13:31'),
(171,	77,	'Tournament 12',	100801,	'2020-11-04',	'16:56:00',	'Swapnali Dabhade',	5,	1,	'Loss',	2,	'2020-11-04 17:22:23'),
(172,	77,	'Tournament 12',	76162,	'2020-11-04',	'16:56:00',	'aishu1',	5,	1,	'Win',	2,	'2020-11-04 17:22:23'),
(173,	77,	'Tournament 12',	70961,	'2020-11-04',	'18:22:24',	'ji',	5,	2,	'Loss',	2,	'2020-11-04 18:24:21'),
(174,	77,	'Tournament 12',	100758,	'2020-11-04',	'18:22:24',	'Mayuri',	5,	2,	'Loss',	2,	'2020-11-04 18:25:00'),
(175,	77,	'Tournament 12',	100800,	'2020-11-04',	'18:22:24',	'Sohail Mujawar',	5,	2,	'Loss',	2,	'2020-11-04 18:35:12'),
(176,	77,	'Tournament 12',	100752,	'2020-11-04',	'18:22:24',	'shubham jagtap',	5,	2,	'Win',	2,	'2020-11-04 18:35:12'),
(177,	77,	'Tournament 12',	100750,	'2020-11-04',	'18:22:24',	'piyush bhandari',	5,	2,	'Win',	2,	'2020-11-04 18:35:32'),
(178,	77,	'Tournament 12',	81635,	'2020-11-04',	'18:22:24',	'Anjali G',	5,	2,	'Loss',	2,	'2020-11-04 18:35:32'),
(179,	77,	'Tournament 12',	76162,	'2020-11-04',	'18:22:24',	'aishu1',	5,	2,	'Win',	2,	'2020-11-04 18:39:34'),
(180,	77,	'Tournament 12',	73300,	'2020-11-04',	'18:22:24',	'testing',	5,	2,	'Loss',	2,	'2020-11-04 18:39:34'),
(181,	77,	'Tournament 12',	100752,	'2020-11-04',	'19:27:25',	'shubham jagtap',	5,	3,	'Loss',	2,	'2020-11-04 19:30:48'),
(182,	77,	'Tournament 12',	76162,	'2020-11-04',	'19:27:25',	'aishu1',	5,	3,	'Win',	2,	'2020-11-04 19:30:49'),
(183,	77,	'Tournament 12',	100750,	'2020-11-04',	'19:27:25',	'piyush bhandari',	5,	3,	'Win',	2,	'2020-11-04 19:32:18'),
(184,	77,	'Tournament 12',	76162,	'2020-11-04',	'20:32:27',	'aishu1',	5,	4,	'Loss',	2,	'2020-11-04 20:49:12'),
(185,	77,	'Tournament 12',	100750,	'2020-11-04',	'20:32:27',	'piyush bhandari',	5,	4,	'Win',	2,	'2020-11-04 20:49:12'),
(186,	78,	'Tournament 13',	79186,	'2020-11-06',	'12:02:00',	'aishu',	5,	1,	'Loss',	2,	'2020-11-06 12:04:30'),
(187,	78,	'Tournament 13',	76092,	'2020-11-06',	'12:02:00',	'SK sanjay',	5,	1,	'Win',	2,	'2020-11-06 12:04:31'),
(188,	78,	'Tournament 13',	76162,	'2020-11-06',	'12:02:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-11-06 12:13:51'),
(189,	78,	'Tournament 13',	73300,	'2020-11-06',	'12:02:00',	'testing',	5,	1,	'Win',	2,	'2020-11-06 12:13:51'),
(190,	78,	'Tournament 13',	73300,	'2020-11-06',	'13:13:53',	'testing',	5,	2,	'Loss',	2,	'2020-11-06 13:28:14'),
(191,	78,	'Tournament 13',	76092,	'2020-11-06',	'13:13:53',	'SK sanjay',	5,	2,	'Win',	2,	'2020-11-06 13:28:14'),
(192,	79,	'Tournament 14',	76092,	'2020-11-06',	'15:34:00',	'SK sanjay',	5,	1,	'Win',	2,	'2020-11-06 15:47:24'),
(193,	79,	'Tournament 14',	76162,	'2020-11-06',	'15:34:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-11-06 15:47:24'),
(194,	79,	'Tournament 14',	79186,	'2020-11-06',	'15:34:00',	'aishu',	5,	1,	'Win',	2,	'2020-11-06 15:49:51'),
(195,	79,	'Tournament 14',	73300,	'2020-11-06',	'15:34:00',	'testing',	5,	1,	'Loss',	2,	'2020-11-06 15:49:51'),
(196,	79,	'Tournament 14',	76092,	'2020-11-06',	'16:49:51',	'SK sanjay',	5,	2,	'Win',	2,	'2020-11-06 17:07:21'),
(197,	79,	'Tournament 14',	79186,	'2020-11-06',	'16:49:51',	'aishu',	5,	2,	'Loss',	2,	'2020-11-06 17:07:21'),
(198,	80,	'Test price',	22325,	'2020-11-10',	'15:57:00',	'vishwa',	10,	1,	'Win',	2,	'2020-11-10 16:01:51'),
(199,	80,	'Test price',	73109,	'2020-11-10',	'15:57:00',	'smstest',	10,	1,	'Loss',	2,	'2020-11-10 16:08:47'),
(200,	80,	'Test price',	76581,	'2020-11-10',	'15:57:00',	'testplayer',	10,	1,	'Win',	2,	'2020-11-10 16:08:48'),
(201,	80,	'Test price',	22325,	'2020-11-10',	'17:08:49',	'vishwa',	10,	2,	'Loss',	2,	'2020-11-10 17:09:35'),
(202,	80,	'Test price',	76581,	'2020-11-10',	'17:08:49',	'testplayer',	10,	2,	'Loss',	2,	'2020-11-10 17:10:00'),
(203,	81,	'Tournament 11',	123732,	'2020-11-26',	'16:18:00',	'varun',	5,	1,	'Loss',	2,	'2020-11-26 16:21:11'),
(204,	81,	'Tournament 11',	123729,	'2020-11-26',	'16:18:00',	'raj gaikwad',	5,	1,	'Win',	2,	'2020-11-26 16:21:11'),
(205,	81,	'Tournament 11',	73104,	'2020-11-26',	'16:18:00',	'Amol G',	5,	1,	'Loss',	2,	'2020-11-26 16:21:52'),
(206,	81,	'Tournament 11',	76162,	'2020-11-26',	'16:18:00',	'aishu1',	5,	1,	'Win',	2,	'2020-11-26 16:21:53'),
(207,	81,	'Tournament 11',	100797,	'2020-11-26',	'16:18:00',	'Shabista',	5,	1,	'Loss',	2,	'2020-11-26 16:22:55'),
(208,	81,	'Tournament 11',	100751,	'2020-11-26',	'16:18:00',	'Harshal',	5,	1,	'Win',	2,	'2020-11-26 16:22:56'),
(209,	81,	'Tournament 11',	123727,	'2020-11-26',	'16:18:00',	'rubina',	5,	1,	'Win',	2,	'2020-11-26 16:24:01'),
(210,	81,	'Tournament 11',	100758,	'2020-11-26',	'16:18:00',	'Mayuri',	5,	1,	'Loss',	2,	'2020-11-26 16:24:01'),
(211,	81,	'Tournament 11',	73300,	'2020-11-26',	'16:18:00',	'testing',	5,	1,	'Win',	2,	'2020-11-26 16:24:31'),
(212,	81,	'Tournament 11',	70961,	'2020-11-26',	'16:18:00',	'ji',	5,	1,	'Loss',	2,	'2020-11-26 16:24:31'),
(213,	81,	'Tournament 11',	100750,	'2020-11-26',	'16:18:00',	'piyush bhandari',	5,	1,	'Loss',	2,	'2020-11-26 16:26:35'),
(214,	81,	'Tournament 11',	123709,	'2020-11-26',	'16:18:00',	'pragati',	5,	1,	'Win',	2,	'2020-11-26 16:26:35'),
(215,	81,	'Tournament 11',	76092,	'2020-11-26',	'16:18:00',	'SK sanjay',	5,	1,	'Win',	2,	'2020-11-26 16:28:03'),
(216,	81,	'Tournament 11',	123725,	'2020-11-26',	'16:18:00',	'shraddha ',	5,	1,	'Loss',	2,	'2020-11-26 16:28:03'),
(217,	82,	'Tournament 12',	123728,	'2020-11-26',	'16:55:00',	'jay',	5,	1,	'Loss',	2,	'2020-11-26 16:58:16'),
(218,	82,	'Tournament 12',	100751,	'2020-11-26',	'16:55:00',	'Harshal',	5,	1,	'Win',	2,	'2020-11-26 16:58:17'),
(219,	82,	'Tournament 12',	76092,	'2020-11-26',	'16:55:00',	'SK sanjay',	5,	1,	'Loss',	2,	'2020-11-26 16:58:52'),
(220,	82,	'Tournament 12',	100800,	'2020-11-26',	'16:55:00',	'Sohail Mujawar',	5,	1,	'Win',	2,	'2020-11-26 16:58:53'),
(221,	82,	'Tournament 12',	70961,	'2020-11-26',	'16:55:00',	'ji',	5,	1,	'Win',	2,	'2020-11-26 17:06:13'),
(222,	82,	'Tournament 12',	123732,	'2020-11-26',	'16:55:00',	'varun',	5,	1,	'Win',	2,	'2020-11-26 17:07:07'),
(223,	82,	'Tournament 12',	100758,	'2020-11-26',	'16:55:00',	'Mayuri',	5,	1,	'Loss',	2,	'2020-11-26 17:07:07'),
(224,	82,	'Tournament 12',	123709,	'2020-11-26',	'16:55:00',	'pragati',	5,	1,	'Loss',	2,	'2020-11-26 17:09:39'),
(225,	82,	'Tournament 12',	100750,	'2020-11-26',	'16:55:00',	'piyush bhandari',	5,	1,	'Win',	2,	'2020-11-26 17:09:39'),
(226,	82,	'Tournament 12',	76162,	'2020-11-26',	'16:55:00',	'aishu1',	5,	1,	'Loss',	2,	'2020-11-26 17:09:48'),
(227,	82,	'Tournament 12',	73104,	'2020-11-26',	'16:55:00',	'Amol G',	5,	1,	'Win',	2,	'2020-11-26 17:09:48'),
(228,	82,	'Tournament 12',	73300,	'2020-11-26',	'16:55:00',	'testing',	5,	1,	'Loss',	2,	'2020-11-26 17:10:57'),
(229,	82,	'Tournament 12',	123694,	'2020-11-26',	'16:55:00',	'ankita',	5,	1,	'Win',	2,	'2020-11-26 17:10:57'),
(230,	81,	'Tournament 11',	123709,	'2020-11-26',	'17:28:07',	'pragati',	5,	2,	'Win',	2,	'2020-11-26 17:32:49'),
(231,	81,	'Tournament 11',	76092,	'2020-11-26',	'17:28:07',	'SK sanjay',	5,	2,	'Win',	2,	'2020-11-26 17:32:56'),
(232,	81,	'Tournament 11',	76162,	'2020-11-26',	'17:28:07',	'aishu1',	5,	2,	'Win',	2,	'2020-11-26 17:33:02'),
(233,	81,	'Tournament 11',	73300,	'2020-11-26',	'17:28:07',	'testing',	5,	2,	'Loss',	2,	'2020-11-26 17:35:39'),
(234,	81,	'Tournament 11',	100751,	'2020-11-26',	'17:28:07',	'Harshal',	5,	2,	'Win',	2,	'2020-11-26 17:35:39'),
(235,	81,	'Tournament 11',	123709,	'2020-11-26',	'18:33:09',	'pragati',	5,	3,	'Loss',	2,	'2020-11-26 18:36:17'),
(236,	81,	'Tournament 11',	76092,	'2020-11-26',	'18:33:09',	'SK sanjay',	5,	3,	'Win',	2,	'2020-11-26 18:38:00'),
(237,	83,	'Tournament 13',	81635,	'2020-11-27',	'11:43:00',	'Anjali G',	5,	1,	'Loss',	2,	'2020-11-27 11:45:22'),
(238,	83,	'Tournament 13',	73300,	'2020-11-27',	'11:43:00',	'testing',	5,	1,	'Win',	2,	'2020-11-27 11:45:23'),
(239,	83,	'Tournament 13',	100751,	'2020-11-27',	'11:43:00',	'Harshal',	5,	1,	'Loss',	2,	'2020-11-27 11:47:27'),
(240,	83,	'Tournament 13',	100750,	'2020-11-27',	'11:43:00',	'piyush bhandari',	5,	1,	'Win',	2,	'2020-11-27 11:47:28'),
(241,	83,	'Tournament 13',	123728,	'2020-11-27',	'11:43:00',	'jay',	5,	1,	'Loss',	2,	'2020-11-27 11:49:51'),
(242,	83,	'Tournament 13',	70961,	'2020-11-27',	'11:43:00',	'ji',	5,	1,	'Win',	2,	'2020-11-27 11:49:52'),
(243,	83,	'Tournament 13',	76162,	'2020-11-27',	'11:43:00',	'aishu1',	5,	1,	'Win',	2,	'2020-11-27 11:57:26'),
(244,	83,	'Tournament 13',	100752,	'2020-11-27',	'11:43:00',	'shubham jagtap',	5,	1,	'Loss',	2,	'2020-11-27 11:57:26'),
(245,	83,	'Tournament 13',	157,	'2020-11-27',	'11:43:00',	'R.K',	5,	1,	'Loss',	2,	'2020-11-27 11:57:51'),
(246,	83,	'Tournament 13',	100758,	'2020-11-27',	'11:43:00',	'Mayuri',	5,	1,	'Win',	2,	'2020-11-27 11:57:51'),
(247,	83,	'Tournament 13',	81638,	'2020-11-27',	'11:43:00',	'Osomose',	5,	1,	'Win',	2,	'2020-11-27 11:58:01'),
(248,	83,	'Tournament 13',	123727,	'2020-11-27',	'11:43:00',	'rubina',	5,	1,	'Loss',	2,	'2020-11-27 11:58:01'),
(249,	83,	'Tournament 13',	123694,	'2020-11-27',	'11:43:00',	'ankita',	5,	1,	'Loss',	2,	'2020-11-27 11:58:09'),
(250,	83,	'Tournament 13',	123902,	'2020-11-27',	'11:43:00',	'snehal',	5,	1,	'Win',	2,	'2020-11-27 11:58:09'),
(251,	83,	'Tournament 13',	100763,	'2020-11-27',	'11:43:00',	'shraddha Jadhav',	5,	1,	'Loss',	2,	'2020-11-27 11:59:05'),
(252,	83,	'Tournament 13',	100800,	'2020-11-27',	'11:43:00',	'Sohail Mujawar',	5,	1,	'Win',	2,	'2020-11-27 11:59:05'),
(253,	83,	'Tournament 13',	100800,	'2020-11-27',	'12:59:07',	'Sohail Mujawar',	5,	2,	'Loss',	2,	'2020-11-27 13:11:12'),
(254,	83,	'Tournament 13',	81638,	'2020-11-27',	'12:59:07',	'Osomose',	5,	2,	'Win',	2,	'2020-11-27 13:11:12'),
(255,	83,	'Tournament 13',	81638,	'2020-11-27',	'14:04:09',	'Osomose',	5,	3,	'Win',	2,	'2020-11-27 14:09:00');

DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `txnMode` varchar(255) NOT NULL,
  `type` enum('Deposit','Withdraw','Gratification') NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `status` enum('Approved','Pending','Rejected','Success','Failed','Process','BankExport') NOT NULL DEFAULT 'Pending',
  `paytmStatus` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `checkSum` varchar(255) NOT NULL,
  `rejectedReason` varchar(255) NOT NULL,
  `isReadNotification` enum('Yes','No') NOT NULL DEFAULT 'No',
  `isAdminReedem` enum('Yes','No') NOT NULL DEFAULT 'No',
  `statusCode` varchar(255) DEFAULT NULL,
  `coupanCode` varchar(150) DEFAULT NULL,
  `isCoupan` enum('Yes','No') NOT NULL DEFAULT 'No',
  `discount` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `mobileNo` bigint(20) NOT NULL,
  `pm` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_detail_id` (`user_detail_id`),
  KEY `status` (`status`),
  KEY `created` (`created`),
  KEY `paymentType` (`paymentType`),
  KEY `type` (`type`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `user_account` (`id`, `user_detail_id`, `orderId`, `paymentType`, `txnMode`, `type`, `amount`, `balance`, `mainWallet`, `winWallet`, `status`, `paytmStatus`, `statusMessage`, `transactionId`, `checkSum`, `rejectedReason`, `isReadNotification`, `isAdminReedem`, `statusCode`, `coupanCode`, `isCoupan`, `discount`, `created`, `modified`, `mobileNo`, `pm`) VALUES
(1,	44,	'Ord8964',	'mainWallet',	'',	'Withdraw',	10,	805,	640,	165,	'Approved',	'',	'',	'',	'',	'',	'Yes',	'No',	NULL,	NULL,	'No',	0,	'2021-05-17 12:20:20',	'2021-05-17 12:29:42',	0,	''),
(2,	44,	'Ord2528',	'winWallet',	'',	'Withdraw',	1,	864,	700,	164,	'Approved',	'',	'',	'',	'',	'',	'Yes',	'No',	NULL,	NULL,	'No',	0,	'2021-05-17 12:22:05',	'2021-05-17 12:29:42',	0,	''),
(3,	49,	'Ord7553',	'mainWallet',	'',	'Withdraw',	1,	1,	1,	0,	'Approved',	'',	'',	'',	'',	'',	'No',	'No',	NULL,	NULL,	'No',	0,	'2021-05-19 13:51:02',	'2021-05-19 13:51:02',	0,	'');

DROP TABLE IF EXISTS `user_account_logs`;
CREATE TABLE `user_account_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account_id` int(11) NOT NULL,
  `user_detail_id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `txnMode` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `checkSum` varchar(255) NOT NULL,
  `paytmType` varchar(255) NOT NULL,
  `type` enum('Deposit','Withdraw') NOT NULL,
  `paytmStatus` varchar(255) NOT NULL,
  `statusCode` varchar(255) NOT NULL,
  `statusMessage` varchar(255) NOT NULL,
  `status` enum('Approved','Pending','Rejected','Process','Failed','Success','BankExport') NOT NULL DEFAULT 'Pending',
  `coupanCode` varchar(150) NOT NULL,
  `isCoupan` enum('Yes','No') NOT NULL DEFAULT 'No',
  `discount` double NOT NULL,
  `transactionId` varchar(255) NOT NULL COMMENT 'value will get from payment gateway response',
  `rejectedReason` varchar(255) NOT NULL,
  `tmp` longtext NOT NULL,
  `created` datetime NOT NULL,
  `mobileNo` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_detail_id` (`user_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `user_account_logs` (`id`, `user_account_id`, `user_detail_id`, `orderId`, `paymentType`, `txnMode`, `amount`, `balance`, `mainWallet`, `winWallet`, `checkSum`, `paytmType`, `type`, `paytmStatus`, `statusCode`, `statusMessage`, `status`, `coupanCode`, `isCoupan`, `discount`, `transactionId`, `rejectedReason`, `tmp`, `created`, `mobileNo`) VALUES
(1,	1,	44,	'Ord8964',	'mainWallet',	'',	10,	805,	640,	165,	'',	'',	'Withdraw',	'',	'',	'',	'Approved',	'',	'No',	0,	'',	'',	'',	'2021-05-17 12:20:20',	0),
(2,	2,	44,	'Ord2528',	'winWallet',	'',	1,	864,	700,	164,	'',	'',	'Withdraw',	'',	'',	'',	'Approved',	'',	'No',	0,	'',	'',	'',	'2021-05-17 12:22:05',	0),
(3,	3,	49,	'Ord7553',	'mainWallet',	'',	1,	1,	1,	0,	'',	'',	'Withdraw',	'',	'',	'',	'Approved',	'',	'No',	0,	'',	'',	'',	'2021-05-19 13:51:02',	0);

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE `user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agendId` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `playerId` varchar(255) NOT NULL,
  `playerType` enum('Real','Bot') NOT NULL DEFAULT 'Real',
  `registrationType` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `socialId` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_img` varchar(255) NOT NULL,
  `adharUserName` varchar(255) NOT NULL,
  `adharFron_img` varchar(255) NOT NULL,
  `adharBack_img` varchar(255) NOT NULL,
  `panUserName` varchar(255) NOT NULL,
  `pan_img` varchar(255) NOT NULL,
  `adharCard_no` varchar(255) NOT NULL,
  `panCard_no` varchar(255) NOT NULL,
  `kyc_status` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `kycDate` date NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `otp` int(11) NOT NULL,
  `otp_verify` enum('Yes','No') NOT NULL DEFAULT 'No',
  `blockuser` enum('Yes','No') NOT NULL DEFAULT 'No',
  `signup_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `referal_code` varchar(255) NOT NULL,
  `referred_by` varchar(255) NOT NULL,
  `referredByUserId` int(11) NOT NULL,
  `referredAmt` double NOT NULL,
  `userLevel` int(11) NOT NULL,
  `bankRejectionReason` varchar(255) NOT NULL,
  `aadharRejectionReason` varchar(255) NOT NULL,
  `panRejectionReason` varchar(255) NOT NULL,
  `is_emailVerified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_mobileVerified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_aadharVerified` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `is_panVerified` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `is_bankVerified` enum('Pending','Verified','Rejected') NOT NULL DEFAULT 'Pending',
  `playerProgress` varchar(255) NOT NULL,
  `coins` double NOT NULL,
  `totalScore` double NOT NULL,
  `totalWin` int(11) NOT NULL,
  `balance` double NOT NULL,
  `totalLoss` int(11) NOT NULL,
  `mainWallet` double NOT NULL,
  `winWallet` double NOT NULL,
  `totalMatches` double NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `deviceName` varchar(255) NOT NULL,
  `deviceModel` varchar(255) NOT NULL,
  `deviceOs` varchar(255) NOT NULL,
  `deviceRam` varchar(255) NOT NULL,
  `lastSpinDate` datetime NOT NULL,
  `deviceProcessor` varchar(255) NOT NULL,
  `firstReferalUpdate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `secondReferalUpdate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `thirdReferalUpdate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `totalCoinSpent` double NOT NULL,
  `version` double NOT NULL,
  `bonusUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `ip` varchar(100) NOT NULL,
  `kyc_request_date` datetime DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `bank_account_holder_name` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_city` varchar(255) DEFAULT NULL,
  `bank_branch` varchar(255) DEFAULT NULL,
  `bank_ifsc` varchar(255) DEFAULT NULL,
  `block` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `id` (`id`),
  KEY `user_name` (`user_name`),
  KEY `email_id` (`email_id`),
  KEY `mobile` (`mobile`),
  KEY `referal_code` (`referal_code`),
  KEY `is_mobileVerified` (`is_mobileVerified`),
  KEY `kyc_status` (`kyc_status`),
  KEY `is_aadharVerified` (`is_aadharVerified`),
  KEY `is_panVerified` (`is_panVerified`),
  KEY `device_id` (`device_id`),
  KEY `is_emailVerified` (`is_emailVerified`),
  KEY `otp` (`otp`),
  KEY `otp_verify` (`otp_verify`),
  KEY `playerType` (`playerType`),
  KEY `playerId` (`playerId`),
  KEY `registrationType` (`registrationType`),
  KEY `signup_date` (`signup_date`),
  KEY `password` (`password`),
  KEY `adharCard_no` (`adharCard_no`),
  KEY `panCard_no` (`panCard_no`),
  KEY `version` (`version`),
  KEY `bonusUpdate` (`bonusUpdate`),
  KEY `mainWallet` (`mainWallet`),
  KEY `kyc_request_date` (`kyc_request_date`),
  KEY `block` (`block`),
  KEY `socialId` (`socialId`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

INSERT INTO `user_details` (`id`, `agendId`, `user_id`, `playerId`, `playerType`, `registrationType`, `name`, `socialId`, `user_name`, `email_id`, `country_name`, `mobile`, `password`, `profile_img`, `adharUserName`, `adharFron_img`, `adharBack_img`, `panUserName`, `pan_img`, `adharCard_no`, `panCard_no`, `kyc_status`, `kycDate`, `status`, `otp`, `otp_verify`, `blockuser`, `signup_date`, `last_login`, `referal_code`, `referred_by`, `referredByUserId`, `referredAmt`, `userLevel`, `bankRejectionReason`, `aadharRejectionReason`, `panRejectionReason`, `is_emailVerified`, `is_mobileVerified`, `is_aadharVerified`, `is_panVerified`, `is_bankVerified`, `playerProgress`, `coins`, `totalScore`, `totalWin`, `balance`, `totalLoss`, `mainWallet`, `winWallet`, `totalMatches`, `device_id`, `deviceName`, `deviceModel`, `deviceOs`, `deviceRam`, `lastSpinDate`, `deviceProcessor`, `firstReferalUpdate`, `secondReferalUpdate`, `thirdReferalUpdate`, `totalCoinSpent`, `version`, `bonusUpdate`, `ip`, `kyc_request_date`, `bank_account_no`, `bank_account_holder_name`, `bank_name`, `bank_city`, `bank_branch`, `bank_ifsc`, `block`) VALUES
(3,	0,	3,	'Test_Player_Id_2021319184442-17773',	'Real',	'custom',	'newsam',	'',	'newsam',	'sam@12345.com',	'India',	8830286846,	'$2a$10$OLPNNzdrdfW3wYkqzXVhruukyHV9k735dnfX5eoLRBZKG5NuBSPuO',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	585450,	'Yes',	'No',	'2021-03-19 10:54:08',	'2021-05-20 06:08:21',	'RN3',	'',	0,	0,	0,	'',	'',	'',	'No',	'Yes',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"8\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,6,0,4,0,0,0,0,0,0,0,0,0]}',	0,	0,	28,	828.8,	54,	585,	243.8,	82,	'e7fd588909febca7d6df1ff66c2e589abe704c57',	'DESKTOP-4GMM7LD',	'All Series (ASUS)',	'Windows 10  (10.0.0) 64bit',	'16321',	'0000-00-00 00:00:00',	'Intel(R) Core(TM) i5-4460 CPU @ 3.20GHz',	'No',	'No',	'No',	1197,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(4,	0,	4,	'Test_Player_Id_20213231506-14337',	'Real',	'custom',	'Shubham',	'',	'Abhi',	'aarya@123.com',	'India',	9604443180,	'$2a$10$Gg1J0/MHa/V7yK6w2ycvT.vIk0QlpkomgGqZTJ2Odv5lBQHlCdrLC',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	338213,	'Yes',	'No',	'2021-03-19 11:10:53',	'2021-05-13 11:47:04',	'RS4',	'',	0,	0,	0,	'',	'',	'',	'No',	'Yes',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	3,	1000,	6,	1000,	0,	9,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	235,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(5,	0,	5,	'Test_Player_Id_2021320101314-17545',	'Real',	'custom',	'Vinod',	'',	'Vinod',	'newyugemp8@gmail.com',	'India',	9689382614,	'$2a$10$CX10syxZq0IUd8iwazO8zenx5RBHwqFUrNchHpv57NNa4u0ZJ66au',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	546596,	'Yes',	'No',	'2021-03-20 04:43:14',	'0000-00-00 00:00:00',	'RV5',	'',	0,	25,	0,	'',	'',	'',	'Yes',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(6,	0,	6,	'',	'Bot',	'',	'',	'',	'Bot1',	'',	'India',	0,	'',	'162133665008.png',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	0,	'Yes',	'No',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	17,	1044.05,	24,	681,	363.05,	41,	'',	'',	'',	'',	'',	'0000-00-00 00:00:00',	'',	'No',	'No',	'No',	424,	0,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(7,	0,	7,	'',	'Bot',	'',	'',	'',	'Bot2',	'',	'India',	0,	'',	'162133663908.png',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	0,	'Yes',	'No',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	54,	1203.3499999999997,	21,	568,	635.3499999999997,	75,	'',	'',	'',	'',	'',	'0000-00-00 00:00:00',	'',	'No',	'No',	'No',	637,	0,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(8,	0,	8,	'',	'Bot',	'',	'',	'',	'Bot3',	'',	'India',	0,	'',	'162133662008.png',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	0,	'Yes',	'No',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	33,	984.35,	27,	616,	368.35,	60,	'',	'',	'',	'',	'',	'0000-00-00 00:00:00',	'',	'No',	'No',	'No',	564,	0,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(9,	0,	9,	'Test_Player_Id_2021323112846-18908',	'Real',	'custom',	'Saya',	'',	'Saya',	'saya@gmail.com',	'India',	8421806456,	'$2a$10$sT3dYAFsW74k9qF1YTo7uuY.hkOcACRb7phaZFu45gbe3zphFpfmC',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	281930,	'Yes',	'No',	'2021-03-23 05:58:46',	'0000-00-00 00:00:00',	'RS9',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(10,	0,	10,	'Test_Player_Id_2021323113318-15215',	'Real',	'custom',	'Amol ',	'',	'Amol ',	'newyugemp19@gmail.com',	'India',	8668922345,	'$2a$10$kakw/NFWJWVko8bHDPx4l.vk7qBJpD.vz7RFF/Yt.grh1gbgsf6QW',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	226201,	'Yes',	'No',	'2021-03-23 06:03:19',	'0000-00-00 00:00:00',	'RA10',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'ed251cccd139db87324d4feebe74d3ea',	'Amol',	'motorola moto e(6s)',	'Android OS 9 / API-28 (PTBS29.401-58-3/58-3)',	'3777',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(11,	0,	11,	'Test_Player_Id_2021323113539102741',	'Real',	'custom',	'Amol',	'',	'Amol',	'amolarunpatil771@gmail.com',	'India',	8806688487,	'$2a$10$3BN9y7f112QptvH8Vd.vNemRH4aBBVDOGbM/VO1ByyX98V3ZoXVmG',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	850560,	'Yes',	'No',	'2021-03-23 06:05:40',	'0000-00-00 00:00:00',	'RA11',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'ed251cccd139db87324d4feebe74d3ea',	'Amol',	'motorola moto e(6s)',	'Android OS 9 / API-28 (PTBS29.401-58-3/58-3)',	'3777',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(12,	0,	12,	'Test_Player_Id_20213261102-15882',	'Real',	'custom',	'Prashant',	'',	'Prashant',	'prashant@gmail.com',	'India',	9420287190,	'$2a$10$xYoDgX79SBtMEvQI2nCL.OB5cELTfshI3VigNFLor/rOyLbfEG2ma',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	599609,	'Yes',	'No',	'2021-03-26 05:30:03',	'2021-03-26 05:33:45',	'RP12',	'',	0,	25,	0,	'',	'',	'',	'Yes',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	0,	1000,	0,	1000,	0,	0,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(13,	0,	13,	'Test_Player_Id_2021326111158-15901',	'Real',	'custom',	'Yashashri',	'',	'Y',	'yash1@gmail.com',	'India',	9988776655,	'$2a$10$8xT568Fc3h612vOagCelG.5ghGzwZPmry4nRF7jHPKBamdDwBYUv6',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	512846,	'Yes',	'No',	'2021-03-26 05:41:59',	'2021-05-18 12:13:19',	'RY13',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"10\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0]}',	0,	0,	0,	916,	13,	916,	0,	13,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	184,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(14,	0,	14,	'Test_Player_Id_2021326111433-18185',	'Real',	'custom',	'Dipesh',	'',	'Ulka',	'newyugemp9@gmail.com',	'India',	8877665544,	'$2a$10$SHuKNde8BhGVjz6iq0cD3.RNT6fORZf4G1LY9J39t3UehJ7UQ96ca',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	137830,	'Yes',	'No',	'2021-03-26 05:44:34',	'2021-03-27 07:35:37',	'RD14',	'',	0,	0,	0,	'',	'',	'',	'Yes',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	0,	1000,	0,	1000,	0,	0,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(15,	0,	15,	'Test_Player_Id_2021326121342-18884',	'Real',	'custom',	'Dipesh',	'',	'Amol dada',	'newyugemp17@gmail.com',	'India',	9966887744,	'$2a$10$4Sgh4La5fEF0QcWxYrU2Pu.3OyufyOwyF.ZjBy8BkXXevhg.hfB7C',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	813636,	'Yes',	'No',	'2021-03-26 06:43:41',	'2021-05-18 07:40:10',	'RD15',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,3,0,3,0,0,0,0,0,0,0,0,0]}',	0,	0,	10,	962,	19,	941,	21,	29,	'b128be2c98f72e88d0089549bfaeb9e5',	'redmi C2',	'Realme RMX1941',	'Android OS 9 / API-28 (PPR1.180610.011/1582877892)',	'1813',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	244,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(16,	0,	16,	'Test_Player_Id_202132612151152674',	'Real',	'custom',	'Anil',	'',	'Anil',	'anil@gmail.com',	'India',	7744335511,	'$2a$10$LVQBcIjDvbRRHUc1MDy26uY0g4ipBVMFEU0EXUF0ANZdWj6cqhx2G',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	156783,	'Yes',	'No',	'2021-03-26 06:44:59',	'0000-00-00 00:00:00',	'RA16',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'b128be2c98f72e88d0089549bfaeb9e5',	'redmi C2',	'Realme RMX1941',	'Android OS 9 / API-28 (PPR1.180610.011/1582877892)',	'1813',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(17,	0,	17,	'Test_Player_Id_202132612171-1357',	'Real',	'custom',	'Tejas',	'',	'Vinod',	'tejas@gmail.com',	'India',	9977551122,	'$2a$10$gAmWgch5ObqMKc4Q4OF0dOTIOWxYcG1aT/rnhYpzhwBHpbMFoKwMu',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	858624,	'Yes',	'No',	'2021-03-26 06:47:01',	'2021-05-18 06:40:35',	'RT17',	'',	0,	0,	0,	'',	'',	'',	'Yes',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,2,0,2,0,0,0,0,0,0,0,0,0]}',	0,	0,	6,	1026,	14,	990,	36,	20,	'b128be2c98f72e88d0089549bfaeb9e5',	'redmi C2',	'Realme RMX1941',	'Android OS 9 / API-28 (PPR1.180610.011/1582877892)',	'1813',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	205,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(18,	0,	18,	'Test_Player_Id_202132612199175731',	'Real',	'custom',	'Jayesh',	'',	'Jayesh',	'jayesh@gmail.com',	'India',	3355226611,	'$2a$10$ACIm.qSxxRaJ64yovNXNruPER57FlIVSfGCK/FtPJRPZyTxC8/0h6',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	662872,	'Yes',	'No',	'2021-03-26 06:49:08',	'2021-05-13 07:50:33',	'RJ18',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'b128be2c98f72e88d0089549bfaeb9e5',	'redmi C2',	'Realme RMX1941',	'Android OS 9 / API-28 (PPR1.180610.011/1582877892)',	'1813',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(19,	0,	19,	'Test_Player_Id_2021326122521-15818',	'Real',	'custom',	'kirti ramchandani',	'',	'kirti ramchandani',	'ramchandanikittu@gmail.com',	'India',	8149074312,	'$2a$10$ctNBAcF5k8wLbPlZySH6uOiwhOHjzMEmZYzhIZfwsySRsQkhTdzkO',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	638385,	'Yes',	'No',	'2021-03-26 06:55:23',	'2021-03-26 11:23:38',	'RK19',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'91245fd66fde1090a82a654e70f3290a',	'Redmi',	'Xiaomi Redmi Note 7 Pro',	'Android OS 9 / API-28 (PKQ1.181203.001/V11.0.10.0.PFHINXM)',	'3664',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(20,	0,	20,	'Test_Player_Id_2021326123647-13297',	'Real',	'custom',	'Surav',	'',	'Surav',	'saurav@gmail.com',	'India',	7799884422,	'$2a$10$7HVRACP5g2kAVfLAcnyZyen8h0NElENnDEg3nYjLUdsKDjcDv/HzK',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	335670,	'Yes',	'No',	'2021-03-26 07:06:48',	'2021-05-13 08:29:56',	'RS20',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(21,	0,	21,	'Test_Player_Id_2021326123841-1384',	'Real',	'custom',	'Gaurav',	'',	'mitesh',	'gaurav@gmail.com',	'India',	9977886644,	'$2a$10$.9TkVudURMj.6nniy.POk.liAgzpa9BFuNwU74K3PkRUoSfdVgcKy',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	933774,	'Yes',	'No',	'2021-03-26 07:08:42',	'2021-05-17 11:29:35',	'RG21',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"9\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0]}',	0,	0,	2,	1000,	8,	1000,	0,	10,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	200,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(22,	0,	22,	'Test_Player_Id_202132612406-17730',	'Real',	'custom',	'Yuvi',	'',	'Dipesh',	'yuvi@gmail.com',	'India',	9965884331,	'$2a$10$7LtgrPn5ejcgReWTEii9YeWitA3YVBsJ3BvgNPR/wnsgYH/jZY0KK',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	537932,	'Yes',	'No',	'2021-03-26 07:10:06',	'2021-05-18 13:05:49',	'RY22',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[1,0,0,0,4,0,2,0,0,0,0,0,0,0,0,0]}',	0,	0,	12,	980,	14,	980,	0,	26,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	245,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(23,	0,	23,	'Test_Player_Id_202132612419-17862',	'Real',	'custom',	'govind',	'',	'govind',	'govind@gmail.com',	'India',	6688994455,	'$2a$10$7tLK5d2jKxXZ.9cFL2uS.OKKQzJC8dhTS2D0ntiMZtaVJjSohoSDC',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	460690,	'Yes',	'No',	'2021-03-26 07:11:10',	'2021-05-12 10:00:43',	'RG23',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-54/1fcf8)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(24,	0,	24,	'Test_Player_Id_2021326124959-17364',	'Real',	'custom',	'bhavesh ramchandani',	'',	'bhavesh ramchandani',	'bsr151104@gmail.com',	'India',	8149745533,	'$2a$10$X0QjAvVt422VvVc6yckTKOcXgs7BkYdkTTMekO/5qLeOeiFeK3kAe',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	106022,	'Yes',	'No',	'2021-03-26 07:20:57',	'0000-00-00 00:00:00',	'RB24',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'236a692b2e8a914d4da040fb5edb2368',	'TECNO CAMON iClick',	'TECNO MOBILE LIMITED TECNO IN6',	'Android OS 8.1.0 / API-27 (O11019/A-201023V208)',	'3824',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(25,	0,	25,	'Test_Player_Id_2021326124959-17364',	'Real',	'custom',	'bhavesh ramchandani',	'',	'bhavesh ramchandani',	'bsr151104@gmail.com',	'India',	8149745533,	'$2a$10$nXhpyBf2JUtUfbwaPohSiOSPzcIGXUu62UdjufJGAey6VNXYvAYoi',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	642273,	'Yes',	'No',	'2021-03-26 07:20:57',	'0000-00-00 00:00:00',	'RB25',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'236a692b2e8a914d4da040fb5edb2368',	'TECNO CAMON iClick',	'TECNO MOBILE LIMITED TECNO IN6',	'Android OS 8.1.0 / API-27 (O11019/A-201023V208)',	'3824',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(26,	0,	26,	'Test_Player_Id_2021326125440-17663',	'Real',	'custom',	'diya ramchandani',	'',	'diya ramchandani',	'kavitaramchandani78@gmail.com',	'India',	8087153550,	'$2a$10$iwxXB/q3wv4Euj2EbP0lmuf2KBiJ90rpre32XKK/0US2EZ/nI2tjO',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	873283,	'Yes',	'No',	'2021-03-26 07:24:41',	'0000-00-00 00:00:00',	'RD26',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'a5bbafb2ae39f35ca04d1fddffbb3627',	'Redmi Note 6 Pro',	'Xiaomi Redmi Note 6 Pro',	'Android OS 9 / API-28 (PKQ1.180904.001/V12.0.1.0.PEKMIXM)',	'3742',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(27,	0,	27,	'Test_Player_Id_2021326125435-14403',	'Real',	'custom',	'satya ramchandani',	'',	'satya ramchandani',	'shamlalmr1june@gmail.com',	'India',	7020736324,	'$2a$10$.gQecmakmwxCsAe6gryk4uC9JQ51tUJPvCXCKpp.9GDwjB5qixDN6',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	63721,	'Yes',	'No',	'2021-03-26 07:25:52',	'0000-00-00 00:00:00',	'RS27',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'bf5c709f8cfbc132db59fe72b9bfd704',	'Redmi',	'Xiaomi Redmi 5',	'Android OS 8.1.0 / API-27 (OPM1.171019.026/V11.0.2.0.ODAMIXM)',	'2829',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(28,	0,	28,	'Test_Player_Id_202133013348-11939',	'Real',	'custom',	'Mitesh',	'',	'Mitesh',	'chintu@gmail.com',	'India',	9373008157,	'$2a$10$IFKZsylWjtNK7C2A9Rs0weo.uPc/3Yo7JDRYjLxyKSRGefhDqaPtu',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	969158,	'Yes',	'No',	'2021-03-30 07:33:49',	'0000-00-00 00:00:00',	'RM28',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'df80d059dce3d86de517d11110717744a1dca9f4',	'LAPTOP-C7DM5TEQ',	'GL553VE (ASUSTeK COMPUTER INC.)',	'Windows 10  (10.0.0) 64bit',	'8076',	'0000-00-00 00:00:00',	'Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(29,	0,	29,	'Test_Player_Id_2021330145636-1342',	'Real',	'custom',	'mits',	'',	'mits',	'chintuconnect@gmail.com',	'India',	9999988888,	'$2a$10$2yae1sKvXVoAJzjtiIZ2AeQhS5N2psvUc5lZJJ9Z6a/qy933bfNza',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	287550,	'Yes',	'No',	'2021-03-30 09:26:36',	'2021-03-30 09:30:21',	'RM29',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	0,	1000,	0,	1000,	0,	0,	'cb2c4dd077ec4173f53c36fa9eb9422f',	'OnePlus 8',	'OnePlus IN2011',	'Android OS 11 / API-30 (RP1A.201005.001/2102011800)',	'5530',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(30,	0,	30,	'Test_Player_Id_202133110560-11539',	'Real',	'custom',	'Y',	'',	'Y',	'newyugemp1@gmail.com',	'India',	8698433026,	'$2a$10$YdUZdYYLEnELeqv8jwMvLeB2H6/plpwOE./Bky1Ue2N70upwQCbFe',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	969009,	'Yes',	'No',	'2021-03-31 05:26:01',	'0000-00-00 00:00:00',	'RY30',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'652e283e5a1deca342c5e5baefcf3e9d',	'Moto C Plus',	'motorola Moto C Plus',	'Android OS 7.0 / API-24 (NRD90M.03.045/045)',	'1917',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(31,	0,	31,	'Test_Player_Id_2021331105839302356',	'Real',	'custom',	'Y',	'',	'Y',	'newyugemp21@gmail.com',	'India',	8698433029,	'$2a$10$a9knFgeQxn3ZL0CSE1WLfOCkt3YroFVqfFYZGAZYK8UAtY1sj6XJq',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	631948,	'Yes',	'No',	'2021-03-31 05:28:40',	'0000-00-00 00:00:00',	'RY31',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'652e283e5a1deca342c5e5baefcf3e9d',	'Moto C Plus',	'motorola Moto C Plus',	'Android OS 7.0 / API-24 (NRD90M.03.045/045)',	'1917',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(32,	0,	32,	'Test_Player_Id_2021331112323-12824',	'Real',	'custom',	'Testing',	'',	'Testing',	'newyugemp@gmail.com',	'India',	9856230879,	'$2a$10$IEBH7AmzKYnPAB/NT/wszeeUWR3f/Ol1e/u/nJTxYd4bb9zJxiSjG',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	46671,	'Yes',	'No',	'2021-03-31 05:53:25',	'0000-00-00 00:00:00',	'RT32',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'652e283e5a1deca342c5e5baefcf3e9d',	'Moto C Plus',	'motorola Moto C Plus',	'Android OS 7.0 / API-24 (NRD90M.03.045/045)',	'1917',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(33,	0,	33,	'Test_Player_Id_2021331112514-11393',	'Real',	'custom',	'Test',	'',	'Test',	'test@gmail.com',	'India',	6589325648,	'$2a$10$bs2k4KRq0kas8kbbAW4xjeJFxakKHZaRcMXQnYw3Zi3/jdH3Ey1D.',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	673533,	'Yes',	'No',	'2021-03-31 05:55:15',	'2021-03-31 06:02:21',	'RT33',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	0,	1000,	0,	1000,	0,	0,	'652e283e5a1deca342c5e5baefcf3e9d',	'Moto C Plus',	'motorola Moto C Plus',	'Android OS 7.0 / API-24 (NRD90M.03.045/045)',	'1917',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(35,	12,	35,	'',	'Real',	'custom',	'bill',	'',	'bill',	'bill@gmail.com',	'india',	9999999999,	'1100',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	683441,	'Yes',	'No',	'2021-04-06 07:35:36',	'0000-00-00 00:00:00',	'RB35',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'111212121313',	'samsung',	'123',	'adnroid',	'125',	'0000-00-00 00:00:00',	'core',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(36,	12,	36,	'',	'Real',	'custom',	'bill1',	'',	'bill1',	'bill1@gmail.com',	'india',	9999999998,	'1100',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	790486,	'Yes',	'No',	'2021-04-06 07:39:34',	'0000-00-00 00:00:00',	'RB36',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'111212121313',	'samsung',	'123',	'adnroid',	'125',	'0000-00-00 00:00:00',	'core',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(37,	12,	37,	'',	'Real',	'custom',	'bill11',	'',	'bill11',	'bill11@gmail.com',	'india',	9999999997,	'1100',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	336654,	'Yes',	'No',	'2021-04-06 07:40:14',	'0000-00-00 00:00:00',	'RB37',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'111212121313',	'samsung',	'123',	'adnroid',	'125',	'0000-00-00 00:00:00',	'core',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(38,	13,	38,	'',	'Real',	'custom',	'abc',	'',	'abc',	'abc@gmail.com',	'india',	9999999996,	'1100',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	270208,	'Yes',	'No',	'2021-04-06 07:40:52',	'0000-00-00 00:00:00',	'RA38',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'111212121313',	'samsung',	'123',	'adnroid',	'125',	'0000-00-00 00:00:00',	'core',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(39,	13,	39,	'',	'Real',	'custom',	'abc1',	'',	'abc1',	'abc1@gmail.com',	'india',	9999999995,	'1100',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	87160,	'Yes',	'No',	'2021-04-06 07:41:08',	'0000-00-00 00:00:00',	'RA39',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'111212121313',	'samsung',	'123',	'adnroid',	'125',	'0000-00-00 00:00:00',	'core',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(40,	13,	40,	'',	'Real',	'custom',	'abc11',	'',	'abc11',	'abc11@gmail.com',	'india',	9999999994,	'1100',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	331281,	'Yes',	'No',	'2021-04-06 07:41:32',	'0000-00-00 00:00:00',	'RA40',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'111212121313',	'samsung',	'123',	'adnroid',	'125',	'0000-00-00 00:00:00',	'core',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(41,	12,	41,	'',	'Real',	'custom',	'testing11',	'',	'testing11',	'testing11@gmail.com',	'india',	1111111111,	'1100',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	614607,	'Yes',	'No',	'2021-04-06 07:53:39',	'0000-00-00 00:00:00',	'RT41',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1000,	0,	1000,	0,	0,	'11121212131',	'samsung',	'123',	'adnroid',	'125',	'0000-00-00 00:00:00',	'core',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(42,	0,	42,	'Test_Player_Id_2021510194148-16713',	'Real',	'custom',	'vishwa',	'',	'vishwa',	'vishwa@gmail.com',	'India',	8329772751,	'$2a$10$uZro2poTq/8MnQdQBW0KLOj3VPoahwBmZyLLDNhx4mGVfWV/zTGXK',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	907610,	'Yes',	'No',	'2021-05-10 14:11:50',	'2021-05-20 06:04:19',	'RV42',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	6,	952.7,	25,	947,	5.7,	31,	'f1e2cc7eb0f2f733292d4136d7323bd1',	'Lenovo Tab M10 FHD Plus',	'LENOVO Lenovo TB-X606V',	'Android OS 10 / API-29 (QP1A.190711.020/TB-X606V_USR_S300112_2101100015_V9.56_BMP_ROW)',	'3723',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	253,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(43,	0,	43,	'Test_Player_Id_2021511155829-1978',	'Real',	'custom',	'test123',	'',	'test123',	'test123@gmail.com',	'India',	9898989898,	'$2a$10$uZro2poTq/8MnQdQBW0KLOj3VPoahwBmZyLLDNhx4mGVfWV/zTGXK',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	705434,	'Yes',	'No',	'2021-05-11 10:28:31',	'2021-05-20 06:08:21',	'RT43',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,1,0,7,0,14,0,0,0,0,0,0,0,0,0]}',	0,	0,	26,	925.7,	48,	845,	80.70000000000002,	74,	'f1e2cc7eb0f2f733292d4136d7323bd1',	'Lenovo Tab M10 FHD Plus',	'LENOVO Lenovo TB-X606V',	'Android OS 10 / API-29 (QP1A.190711.020/TB-X606V_USR_S300112_2101100015_V9.56_BMP_ROW)',	'3723',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	670,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(44,	0,	44,	'Test_Player_Id_2021511155923433752',	'Real',	'custom',	'abcd',	'',	'abcd',	'abcd@gmail.com',	'India',	9797979797,	'$2a$10$q1MWXpmIhD8Uzy74bqWjOulI273CRIPxiGz/pu.F6Ll6qHmyX6Z5C',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	207500,	'Yes',	'No',	'2021-05-11 10:29:25',	'2021-05-20 05:59:48',	'RA44',	'',	0,	0,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,11,0,13,0,0,0,0,0,0,0,0,0]}',	0,	0,	46,	894.3,	67,	840,	54.29999999999999,	113,	'f1e2cc7eb0f2f733292d4136d7323bd1',	'Lenovo Tab M10 FHD Plus',	'LENOVO Lenovo TB-X606V',	'Android OS 10 / API-29 (QP1A.190711.020/TB-X606V_USR_S300112_2101100015_V9.56_BMP_ROW)',	'3723',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	927,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(45,	0,	45,	'Test_Player_Id_2021512135725-12154',	'Real',	'custom',	'Sagar',	'',	'Sagar',	'newyugemp25@gmail.com',	'India',	8855119977,	'$2a$10$gIxI7ODEMm8CdihON8Tqp.IeywXdzn7qcXpAth5NHN/rwE/jJLiVK',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	753700,	'No',	'No',	'2021-05-12 08:27:27',	'2021-05-12 08:32:57',	'RS45',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"11\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	0,	1000,	0,	1000,	0,	0,	'98d65d59fd42329aeed5540732ffbc62',	'moto e(7) plus',	'motorola moto e(7) plus',	'Android OS 10 / API-29 (QPZ30.30-Q3-38-69/0e470)',	'3726',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(46,	0,	46,	'Test_Player_Id_2021512143822-11025',	'Real',	'custom',	'JustOOH',	'',	'JustOOH',	'yash21just@gmail.com',	'India',	9856231470,	'$2a$10$2X5oRA6c.RUUiky7lwpmbO42DSXANjF5gMgejxrywmynHQAGn2Nyy',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	97398,	'No',	'No',	'2021-05-12 09:08:23',	'2021-05-12 09:17:48',	'RJ46',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"user\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	0,	1000,	0,	1000,	0,	0,	'2bcf1e872612157888f83d4b7b3484bc',	'Moto C Plus',	'motorola Moto C Plus',	'Android OS 7.0 / API-24 (NRD90M.03.045/045)',	'1917',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(47,	0,	47,	'Test_Player_Id_202151215317-1827',	'Real',	'custom',	'12MayTesting',	'',	'12MayTesting',	'12maytest@gmail.com',	'India',	8698433020,	'$2a$10$H9CXFegG/vdA7OE4i41PnuDvFRyThcrb/D3efDe.KmpnrbV650ni6',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	549166,	'No',	'No',	'2021-05-12 09:33:19',	'2021-05-12 09:41:39',	'R147',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'{\"purchasedMagicDice\":0,\"selectedDiceVarientsIndex\":0,\"purchasedDiceVarients\":[false,false,false,false,false,false],\"profilePicture\":\"10\",\"socialId\":\"\",\"winMatches\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}',	0,	0,	0,	1000,	0,	1000,	0,	0,	'2bcf1e872612157888f83d4b7b3484bc',	'Moto C Plus',	'motorola Moto C Plus',	'Android OS 7.0 / API-24 (NRD90M.03.045/045)',	'1917',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(48,	0,	48,	'Test_Player_Id_2021518115353-12542',	'Real',	'custom',	'may20212021',	'',	'may20212021',	'new20@gmail.com',	'India',	9856230809,	'$2a$10$9S7iS6zMM/EuZfi3.gSADu2xz7rjJWoCi47EOARbckETMaESLLawa',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	205878,	'No',	'No',	'2021-05-18 06:23:54',	'0000-00-00 00:00:00',	'RM48',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	0,	0,	0,	0,	0,	'2bcf1e872612157888f83d4b7b3484bc',	'Moto C Plus',	'motorola Moto C Plus',	'Android OS 7.0 / API-24 (NRD90M.03.045/045)',	'1917',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(49,	0,	49,	'Test_Player_Id_2021518165346-17893',	'Real',	'custom',	'hey',	'',	'hey',	'hey18@gmail.com',	'India',	6523894512,	'$2a$10$BnEfI2CNeNzpjUdF9MhBLOMCc2VvVbj8tkNH/mJmr40yROHH.SG7a',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	560365,	'No',	'No',	'2021-05-18 11:23:46',	'0000-00-00 00:00:00',	'RH49',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	1,	0,	1,	0,	0,	'2bcf1e872612157888f83d4b7b3484bc',	'Moto C Plus',	'motorola Moto C Plus',	'Android OS 7.0 / API-24 (NRD90M.03.045/045)',	'1917',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
(50,	0,	50,	'Test_Player_Id_202151918532-14022',	'Real',	'custom',	'pp123',	'',	'pp123',	'peru@gmail.com',	'India',	7066657397,	'$2a$10$C1Bos.lzx/R1iScP8vC.XepQg7IUw7hugwrUkei0j4eVcBPlX3Qp2',	'',	'',	'',	'',	'',	'',	'',	'',	'Pending',	'0000-00-00',	'Active',	105974,	'No',	'No',	'2021-05-19 12:35:32',	'0000-00-00 00:00:00',	'RP50',	'',	0,	25,	0,	'',	'',	'',	'No',	'No',	'Pending',	'Pending',	'Pending',	'',	0,	0,	0,	0,	0,	0,	0,	0,	'f1e463b77cbb19577b66efbda19e44d8',	'Nokia 5.4',	'HMD Global Nokia 5.4',	'Android OS 10 / API-29 (QKQ1.200719.002/00WW_1_15B)',	'3674',	'0000-00-00 00:00:00',	'ARMv7 VFPv3 NEON',	'No',	'No',	'No',	0,	0.9,	0,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0);

DROP TABLE IF EXISTS `values`;
CREATE TABLE `values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `web_banners`;
CREATE TABLE `web_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `banner_type` enum('Image','Video') NOT NULL,
  `order_no` int(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2021-05-20 06:11:54
