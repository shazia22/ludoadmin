var express = require("express");
var path = require('path');
const request = require('request');
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var fs = require("fs");
var app = express();
var http = require('http');
var morgan = require('morgan');
var signUp = require('./model/signUp.js');
var config = require('./config.js');
var port     = process.env.PORT || config.port;
process.env.NODE_TLS_REJECT_UNAUTHORIZED=0;
var tournaments = require('./model/tournaments.js');
var dice = require('./model/dice.js');
var other = require('./model/other.js');

var common_model = require('./model/common_model.js');
const server =require('http').createServer(app);
var options = {transport: ['websocket']};

var io = require('socket.io')(options).listen(server, {
    handlePreflightRequest: (req, res) => {
        const headers = {
           "Content-type": "application/json",
           "Access-Control-Allow-Origin":"*",
           "Access-Control-Allow-Methods":"GET, PUT, POST, DELETE, OPTIONS",
           "Access-Control-Max-Age":"1000",
           "Access-Control-Allow-Headers":"Content-Type, Authorization, X-Requested-With",
           "Access-Control-Allow-Credentials":"true"
        };
        res.writeHead(200, headers);
        res.end();
    }
});
server.listen(port,function(){
     console.log(config.portMsg)
});
//skill!@#
//app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');

var tokenColor = ['Red','Blue','Yellow','Green'];
var tokenGlobleValue = ['0','13','26','39'];

function GetGlobalPositionFunction(currentPosition,globalValue){
    if (currentPosition > 51){
        return -1;
    }
    var tempPos = globalValue + currentPosition;
    if (tempPos > 52)
    {
        tempPos -= 52;
    }
    return tempPos;
}
// var matches = [];
var matches = {};
var connectCounter =1000;
// updateTimers();
updateroom();
function updateroom() {
	var sql ="update ludo_join_rooms set activePlayer=0 where gameStatus='Pending'";
    common_model.sqlQuery(sql,function(res){
    });
}
var totolBotMatches=0;
var currentBotMatches=0;
var totalPrivatePlayers = 340;
var totalOnlineGamePlayers = 480;
io.sockets.on('connection',function(socket){
	connectCounter++;
	// online Player Count
    socket.on('onlinePlayerCount',function(){ 
       var isPrivateData={
			online:totalOnlineGamePlayers,
			private:totalPrivatePlayers
		};
		io.emit('onlinePlayerCount',isPrivateData);
	});
    // for join room
    socket.on('joinRoom',function(data){ 
       joinRoomTable(socket,data);
	});
	// join Private Room
	socket.on('joinPrivateRoom',function(data){		
		
       joinPrivateRoom(socket,data);
	});
	//dice Roll
	socket.on("diceRoll",function(data){
		// console.log(data);
		var isTest = data.isTest;			
		var number = Number(data.diceNumber);
		var passData={
			userId:data.userId,
			tableId:data.tableId,
			playerType:'Real',
		};
		diceRollFunction(passData,isTest,number);
	});
	// move Token
	socket.on("moveToken",function(data){
		data['userId'] =data.userId;
		data['tableId'] =data.tableId;
		moveToken(data);
	});
	// disconnect
	socket.on("disconnect",function(reason){
		connectCounter --;
        io.emit('connectCounter',connectCounter);
		disconnectFunction(socket);
	});
	// user reconnect
	socket.on("userReconnect",function(data){
        userReconnect(socket,data);
    });
     // for left table
    socket.on("leftFromTable",function(data){
        leftFromTable(socket,data);
    });
    // used Booster
    socket.on("usedBooster",function(data){
    	var data={
    		tableId:data.tableId,
			userId:data.userId,
			type:data.type,
    	};
        usedBooster(data,socket);
    });
    // add Choice Booster
    socket.on("addChoiceBooster",function(data){
    	var data={
    		tableId:data.tableId,
			userId:data.userId,
			number:data.number,
    	};
        addChoiceBooster(data);
    });
    // create Private Room
    socket.on("createPrivateRoom",function(data){    	
    	createPrivateRoom(socket,data);
    });
    socket.on("getPrivateRoomDetails",function(data){
    	getPrivateRoomDetails(socket,data);
    });
    // total Active Token
    socket.on('sendMessage',function(data){
       sendMessage(data);
	});
	// token Purchase
	socket.on('tokenPurchase',function(data){
       tokenPurchase(data);
	});
});
//totalActiveToken data {tableId,userId,noOfToken} emit to tokenPurchase listen to tokenPurchase
function tokenPurchase(data) {
	var match = findMatchByTableId(data.tableId);	
	if(match){
		var player  = findPlayerById(match,data.userId);		
		if(player){
			player.totalActiveToken += Number(data.noOfToken);
			player.fourToken[0].isActiveToken='Active';
			player.fourToken[1].isActiveToken='Active';
			io.in(match.tableId).emit('tokenPurchase',"Success");
		}
	}
}
function addChoiceBooster(data) {
	var match = findMatchByTableId(data.tableId);	
	if(match){
		var player  = findPlayerById(match,data.userId);		
		if(player){
			player.choiseBooster = Number(data.number);
			io.in(match.tableId).emit('playerObject',match.players);
		}
	}
}
function usedBooster(data,socket) {
	var match = findMatchByTableId(data.tableId);	
	if(match){
		var player  = findPlayerById(match,data.userId);		
		if(player){
			if(data.type=='choiseBooster'){
				if(player.choiseBooster !=0){
					player.nextturndice = player.choiseBooster;
    				match.matchNextDiceNumber = player.nextturndice;
    				player.choiseBooster = 0;
				}
				player.choiseBooster = 0;
			}else{
				if(player.randomBooster!=0){
					player.nextturndice = player.randomBooster;
	    			match.matchNextDiceNumber = player.nextturndice;
					player.randomBooster = 0;				
				}
			}
		}
	}
}
// add bot function 
function addBotsFunction(match){ 
    if(match){
        var remainingPlayer =3;
        if(remainingPlayer > 0){        
            var sql ="select id,user_id,user_name,coins from user_details where playerType='Bot' order by rand() limit 3";
            common_model.sqlQuery(sql,function(res){
                if(res.success==1){ 
                	match.botUsersData =res.data;  
                    joinBotDataFunction(match,0);
                }                       
            });
        }
    }
}
//join Bot Data Function 
function joinBotDataFunction(match,position){
    if(match.botUsersData[position]){
        var userData= {
                userId:match.botUsersData[position].id,
                roomId:match.roomId,
                players:match.matchPlayers,
                value:match.matchValue,
                playerType:'Bot',
                gameMode:match.gameMode,
                isFree:'No',
                tableId:match.tableId
        }
        joinBotsRoom(userData);
    }
}

function winLossCoinsDistribution(match){
	var sql="";
	for (var i = 0; i < match.players.length; i++) {
		var isBotWin = "No";
		if(match.players[i].isDeductMoney=="No"  ){
			match.players[i].isDeductMoney="Yes";
			if(match.players[i].isWin==true){	
				if(match.gameMode=='Fancy'  && match.isWinFirstThreeToken == false){	
					match.players[i].isWinFirstThreeToken =true;
					firstThreeTokenInHouse(match);
					match.isWinFirstThreeToken = true;
				}
				if(match.gameMode=='Fancy'  && match.isWinFirstToken == false){	
					match.players[i].isWinFirstToken =true;
					firstTokenInHouse(match);
					match.isWinFirstToken = true;
				}		
				var isAdd= "Add";
				var isWin= "Win";
				var adminAmount = (parseFloat(match.winningBet)*parseFloat(match.adminCommision)/100);
				var winningPrice =(parseFloat(match.winningBet) - parseFloat(adminAmount));
				
				var lastAmount = parseFloat(winningPrice) -parseFloat(match.matchValue);
			}else{
				var adAmt = (Number(parseFloat(match.matchValue)) * Number(parseFloat(match.adminCommision)/100));
				if(match.players[i].playerType == 'Bot'){
					var lastAmount = parseFloat(match.matchValue) - parseFloat(adAmt);
				}else{
					var lastAmount = parseFloat(match.matchValue);
				}
				
				var isAdd= "Sub";
				var isWin= "Loss";
			}
			if(match.players[i].playerType=='Bot'){
				var adminCoins = 0;
			}else{
				var adminCoins = (Number(parseFloat(match.matchValue)) * Number(match.adminCommision)/100);
			}
			
			var coinsDeductData={
		          userId:match.players[i].userId,
		          coins:lastAmount,
		          tableId:match.players[i].tableId,
		          gameType:match.roomTitle+' '+match.gameMode,
		          betValue:match.matchValue,
		          rummyPoints:0,
		          isWin:isWin,
		          adminCommition:match.adminCommision,
		          type:isAdd,
		          adminCoins:adminCoins,
		      };
		      var mainfirstToken = 0;
              var mainfirstThreeToken = 0;
              var winLossType = "Four Token";
		      sql += "CALL userCoinsUpdate('"+coinsDeductData.userId+"','"+coinsDeductData.coins+"','"+coinsDeductData.tableId+"','"+coinsDeductData.gameType+"','"+coinsDeductData.betValue+"','"+coinsDeductData.rummyPoints+"','"+coinsDeductData.isWin+"','"+coinsDeductData.adminCommition+"','"+coinsDeductData.type+"','"+coinsDeductData.adminCoins+"','"+mainfirstToken+"','"+mainfirstThreeToken+"','"+winLossType+"','"+match.matchValue+"');"; 
		     console.log(sql,"Four Token");
		}
	}
	common_model.sqlQuery(sql,function(res){ 
	}); 
}
function joinBotsRoom(data){
	common_model.joinBotsRoomTable(data,function(res){
		if(res.success==1){	
			var tableId = res.joinRoomId;
			var userId = res.userId;
			var userName = res.userName;
			var tokenColor = res.tokenColor;
			var playerType = 'Bot';
			var playerObj ={
				image:res.profile,
				isDeductMoney:"No",
				isDeductMoneyFirstToken:"No",
				isDeductMoneyFirstThreeToken:"No",
				tableId:tableId,
				roomId:res.roomId,
				winPosition:0,
				diceSixInTurn:0,
				userName:userName,
				totalLifes:3,
				coins:res.coins,
				status:'Active',
				isBlocked:false,
				isWin:false,
				isWinFirstToken:false,
				isWinFirstThreeToken:false,
				isturn:false,
				isStart:false,
				diceNumber:0,
				userId:userId,
				playerIndex:0,
				positionArray:[],
				fourToken:[{tokenIndex:0,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:1,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:2,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:3,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'}],
				tokenColor:tokenColor,
				winnerPosition:0,
				socketId:res.userId,
				type:res.playerType,
				inactiveTokenCount:4,
				totalActiveToken:4,
				winCount:0,
				playerType:playerType,
				tokenTopPosition:57,
				tossValue:0,
				randomBooster:0,
				choiseBooster:0,
				isExtratime:false,
				extratime:20,
				nextturndice:Math.floor(Math.random() * 6) + 1,
			};	
			
			var match = findMatchByTableId(tableId);
			if(match){
				match.isAddBot = true;
				match.noOfBots += 1;
				match.players.push(playerObj);	
				if(match.players.length==match.matchPlayers){
					if(match.matchPlayers==2){
						match.isTossTimmer=1;
					}				 
					updateStatus(match);					
				}
				io.in(tableId).emit("playerObject",match.players);
			}
		}       
	});
}
// join private room
function joinPrivateRoom(socket,data){
	common_model.joinPrivateRoom(data,function(res){		
		if(res.success==1){	
			var tableId = res.joinRoomId;
			var userId = res.userId;
			var userName = res.userName;
			var tokenColor = res.tokenColor;
			var playerType = res.playerType;
			var sessMes={
				message:res.message,
				tableId:tableId
			}
			var rDice = [1,2,3,4,5,6];
    		var matchNextDiceNumber = rDice[Math.floor(Math.random()*rDice.length)];

			io.to(socket.id).emit("sessionMessage",sessMes);	
			var playerObj ={
				image:res.profile,
				isDeductMoney:"No",
				isDeductMoneyFirstToken:"No",
				isDeductMoneyFirstThreeToken:"No",
				tableId:tableId,
				roomId:res.roomId,
				winPosition:0,
				diceSixInTurn:0,
				userName:userName,
				totalLifes:3,
				coins:res.coins,
				status:'Active',
				isBlocked:false,
				isWin:false,
				isWinFirstToken:false,
				isWinFirstThreeToken:false,
				isturn:false,
				isStart:false,
				diceNumber:0,
				userId:userId,
				playerIndex:0,
				positionArray:[],
				fourToken:[{tokenIndex:0,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:1,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:2,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:3,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'}],
				tokenColor:tokenColor,
				winnerPosition:0,
				socketId:socket.id,
				type:data.type,
				inactiveTokenCount:4,
				totalActiveToken:4,
				winCount:0,
				playerType:playerType,
				tokenTopPosition:57,
				tossValue:0,
				randomBooster:0,
				choiseBooster:0,
				isExtratime:false,
				extratime:20,
				nextturndice:matchNextDiceNumber,
			};	
			socket.tableId = tableId;
			socket.userId= userId;
			socket.join(tableId);	
			totalPrivatePlayers +=1;
			var isPrivateData={
        		online:totalOnlineGamePlayers,
        		private:totalPrivatePlayers
        	};
        	
        	io.emit('onlinePlayerCount',isPrivateData);	
			var match = findMatchByTableId(tableId);
			if(match){
				match.players.push(playerObj);	
				if(match.players.length==match.matchPlayers){	
					if(match.matchPlayers==2){
						match.isTossTimmer=1;
					}			
					updateStatus(match);					
				}
				io.in(match.tableId).emit("playerObject",match.players);
			}else{
				var match = {
					tableId:tableId,
					roomId:res.roomId,
					roomTitle:res.roomTitle,
					isDeductReservemoney:"No",
					isBotConnect:res.isBotConnect,
					isFree:res.isFree,
					botWin:'No',
					isTokenMove:false,
					tokenMoveTime:2,
					consttokenMoveTime:2,
					isGameStart:false,
					winnerCount :0,
					startRoundWaiting:60,
					gameOverTime:5,
					isGameOver:false,
					throwDieTime:20,
					constthrowDieTime:20,
					botthrowDieTime:20,
					matchPlayers:Number(res.players),
					leftPlayers:Number(res.players),
					players:[],
					isReturn:false,
					isKill:false,
					isAddBot:false,
					noOfBots:0,
					isBotFirstSix:false,
					currentRoundBot:Number(res.currentRoundBot),
					totalRoundBot:Number(res.totalRoundBot),
					isBotWinner:false,
					matchDiceNumber:0,
					matchNextDiceNumber:matchNextDiceNumber,
					isReturnMove :false,
					matchValue:Number(data.value),
					winningBet:0,
					winningkillToken:0,
					winningfirstToken:0,
					winningfirstThreeToken:0,
					gameMode:res.gameMode,
					isPrivate:res.isPrivate,
					botUsersData:[],
					whosTurn:false,
					currentTurnUserId:false,
					adminCommision:Number(res.adminCommision),
					isToss:false,
					isTossTimmer:1,
					startDicePosition:[],
					killToken:res.killToken,
					firstToken:res.firstToken,
					firstThreeToken:res.firstThreeToken,
					continueSixes:0,
					isWinFirstToken:false,
					isWinFirstThreeToken:false,
				};
				match.players.push(playerObj);		
				matches[tableId] = match; // Key => Value
                setTimeout(()=>startWaitingTimer(tableId),0);			
				io.in(socket.tableId).emit("playerObject",match.players);
			}
			
		}else{
			var sessMes={
				message:res.message,tableId:0
			}
			io.to(socket.id).emit("sessionMessage",sessMes);
		}   
	});
}


function sendMessage(data){
	io.in(data.tableId).emit("sendMessage",data);
}
function getPrivateRoomDetails(socket,data){
	var data ={
		table:'ludo_join_rooms',
		fields:'joinRoomId,roomId,noOfPlayers,betValue,gameMode',
		condition:'isPrivate="Yes" and joinRoomId="'+data.tableId+'"',
	};
	common_model.GetData(data,function(res){	
		
		var  passData ={};
		if(res.success==1){
	 		var passData ={
				roomId:res.data[0].roomId,
				tableId:res.data[0].joinRoomId,
				players:res.data[0].noOfPlayers,
				gameMode:res.data[0].gameMode,
				betValue:res.data[0].betValue,
		 	};
	 	}
	 	passData['success']=res.success;
		passData['message']=res.message;
		io.to(socket.id).emit("getPrivateRoomDetails",passData);			
	});
}
// create Private Room
function createPrivateRoom(socket,data) {
	 common_model.createPrivateRoom(data,function(res){
	 	var  passData ={};
	 	if(res.success==1){
	 		var passData ={
				roomId:res.roomId,
				tableId:res.joinRoomId,
				gameMode:res.gameMode,
				betValue:res.betValue,
				players:data.players,
				isFree:data.isFree,
		 	}
	 	}
		passData['success']=res.success;
		passData['message']=res.message;	 	
	 	io.to(socket.id).emit("getPrivateRoomDetails",passData);
	 });
}

// when disconnect 
function disconnectFunction(socket){
    var match = findMatchByTableId(socket.tableId);
    if(match){
        var player = findPlayerById(match,socket.userId);
        if(player){
            var pIndex  = match.players.indexOf(player);
            var playerlength =match.players.length; 
            if(!match.isGameStart){
                if (pIndex > -1){
                    match.players.splice(pIndex, 1);
                }
                if(playerlength==1){
                	delete matches[match.tableId];
                }
            }
            io.in(match.tableId).emit("playerObject",match.players);
            socket.leave(socket.tableId);  
        }          
    }
    
}
// when user left the table
function leftFromTable(socket,data){
    var match = findMatchByTableId(data.tableId);
    if(match){
        var player = findPlayerById(match,data.userId);
        if(player){
        	for (var i = 0; i < player.fourToken.length; i++) {
        		player.fourToken[i].status='Inactive';
				player.fourToken[i].position=0;
				player.fourToken[i].globlePosition=0;
				player.fourToken[i].zone='safe';
				player.inactiveTokenCount += 1;
        	}
            var pIndex  = match.players.indexOf(player);
            var playerlength =match.players.length; 
            var updateData = {
                tableId:data.tableId,
                userId:data.userId,
                playerlength:playerlength -1,
                isGameStart:match.isGameStart
            }
            common_model.dbUpdates(updateData);
            if(match.isGameStart){            	
            	if(match.isPrivate=="No"){
					totalOnlineGamePlayers -=1;
            	}else{
					totalPrivatePlayers -=1;
            	}
            	var isPrivateData={
            		online:totalOnlineGamePlayers,
            		private:totalPrivatePlayers
            	};
            	io.emit('onlinePlayerCount',isPrivateData);
                 	if(player.isDeductMoney == "No"){
	            		player.isDeductMoney = "Yes";
                 		var mainValue = Number(parseFloat(match.matchValue)) ;
                 		var mainfirstToken = 0;
                 		var mainfirstThreeToken = 0;
                 		var totalAmountLost = Number(parseFloat(match.matchValue));

                 		var adminCoins = (Number(parseFloat(totalAmountLost)) * Number(match.adminCommision)/100);
						var coinsDeductData = {
					          userId:player.userId,
					          coins:totalAmountLost,
					          tableId:player.tableId,
					          gameType:match.roomTitle+' '+match.gameMode,
					          betValue:match.matchValue,
					          rummyPoints:0,
					          isWin:"Loss",
					          adminCommition:match.adminCommision,
					          type:"Sub",
					          adminCoins:adminCoins,
					          mainfirstToken:mainfirstToken,
					          mainfirstThreeToken:mainfirstThreeToken,
				      	};
				      	var winLossType = "Four Token left";
				      	var sql = "CALL userCoinsUpdate('"+coinsDeductData.userId+"','"+coinsDeductData.coins+"','"+coinsDeductData.tableId+"','"+coinsDeductData.gameType+"','"+coinsDeductData.betValue+"','"+coinsDeductData.rummyPoints+"','"+coinsDeductData.isWin+"','"+coinsDeductData.adminCommition+"','"+coinsDeductData.type+"','"+coinsDeductData.adminCoins+"','"+mainfirstToken+"','"+mainfirstThreeToken+"','"+winLossType+"','"+match.matchValue+"');"; 

                 		if(player.isDeductMoneyFirstToken == "No" && match.gameMode=='Fancy'){
                 			var winLossType ="First Token left";
                 			var totalAmountLost = Number(parseFloat(match.firstToken));
                 			var mainfirstToken = Number(parseFloat(match.firstToken));
                 			var adminCoins = (Number(parseFloat(match.firstToken)) * Number(match.adminCommision)/100);
                 			player.isDeductMoneyFirstToken = "Yes";
                 			sql += "CALL userCoinsUpdate('"+coinsDeductData.userId+"','"+match.firstToken+"','"+coinsDeductData.tableId+"','"+coinsDeductData.gameType+"','"+mainfirstToken+"','"+coinsDeductData.rummyPoints+"','"+coinsDeductData.isWin+"','"+coinsDeductData.adminCommition+"','"+coinsDeductData.type+"','"+adminCoins+"','"+mainfirstToken+"','"+mainfirstThreeToken+"','"+winLossType+"','"+match.matchValue+"');"; 
                 		}
                 		if(player.isDeductMoneyFirstThreeToken=="No" && match.gameMode=='Fancy'){
                 			var winLossType ="First Three Token left";
                 			var totalAmountLost = Number(parseFloat(match.firstThreeToken));
                 			var mainfirstThreeToken = Number(parseFloat(match.firstThreeToken));
                 			player.isDeductMoneyFirstThreeToken = "Yes";
                 			var adminCoins = (Number(parseFloat(match.firstThreeToken)) * Number(match.adminCommision)/100);
                 			sql += "CALL userCoinsUpdate('"+coinsDeductData.userId+"','"+match.firstThreeToken+"','"+coinsDeductData.tableId+"','"+coinsDeductData.gameType+"','"+mainfirstThreeToken+"','"+coinsDeductData.rummyPoints+"','"+coinsDeductData.isWin+"','"+coinsDeductData.adminCommition+"','"+coinsDeductData.type+"','"+adminCoins+"','"+mainfirstToken+"','"+mainfirstThreeToken+"','"+winLossType+"','"+match.matchValue+"');"; 
                 		}
                 		// console.log(sql,"leftWS");
				      common_model.sqlQuery(sql,function(res){ 

					  }); 
				      
                }
                if(player.isturn == true){
                	player.isBlocked=true;
                	if(player.status!='Win' && player.status!='left'){
                		match.leftPlayers -=1;
                	}
                	player.status="left";
                	if(match.leftPlayers!=1){
                		nextTurnFunction(match,pIndex);
                	}
                }else{
                	if(player.status!='Win' && player.status!='left'){
                		match.leftPlayers -=1;
                	}
                	player.isBlocked=true;
                	player.status="left";
                }
            }else{
            	var updateData = {
	                tableId:data.tableId,
	                userId:data.userId,
	                playerlength:playerlength -1,
	                isGameStart:match.isGameStart
	            }
	            common_model.dbUpdates(updateData);
                if (pIndex > -1){
                    match.players.splice(pIndex, 1);
                }
                if(playerlength==1){
                	delete matches[data.tableId];
                    // matches.splice(matches.indexOf(match), 1);
                }
            }
            io.in(match.tableId).emit("playerObject",match.players);
            if(socket!=''){
            	socket.leave(data.tableId);  
            }
        }          
    }
    
}
//getLastActivePlayer
function getLastActivePlayer(match){	
	for (var i = 0; i < match.players.length; i++) {
		if(match.players[i].status=='Active'){
			 return match.players[i];
		}
	}
	return false;

}
// reconnect user
function userReconnect(socket,data){
	var match = findMatchByTableId(data.tableId);    
    if(match){
        var player = findPlayerById(match,data.userId);
        if(player){
            if(match.isGameOver){
            	var data = {
                    success:2,
                    message:"Match is ended."
                };
            }else{
            	socket.tableId = player.tableId;
                socket.userId= player.userId;
               
                player.socketId = socket.id;
                socket.join(data.tableId);
                io.in(match.tableId).emit("playerObject",match.players);
                io.to(socket.id).emit("jocker",match.jocker);
                io.to(socket.id).emit("remainingCardsAndTurn",{turnAndCard:match.turnAndCard});
                io.to(socket.id).emit("updateCards",player.cards);
        		io.in(match.tableId).emit("isBotWinner",match.botWin);
                var data = {
                    success:1,
                    message:"Reconnect Success",
                };
            }
        }
    }else{
        var data = {
            success:0,
            message:"Match is ended."
        };
    }
    io.to(socket.id).emit("userReconnectMsg",data);

}

function killingDisdribution(match,winUserId,lossUserId){
	var sql = "";
	for (var i = 0; i < match.players.length; i++) {
		var isSqlUpdate = "No";
		if(match.players[i].userId==lossUserId){
			var isAdd = "Sub";
			var isWin = "Loss";
			var lastAmount = parseFloat(match.killToken);
			match.players[i].coins  -=lastAmount;
			isSqlUpdate = "Yes";
		}else if(match.players[i].userId==winUserId){
			isSqlUpdate = "Yes";
			var isAdd = "Add";
			var isWin = "Win";
			var adminAmount = (parseFloat(match.killToken)*parseFloat(match.adminCommision)/100);
			var lastAmount = (parseFloat(match.killToken) - parseFloat(adminAmount));
			match.players[i].coins  +=lastAmount;
		}
		var adminCoins = (Number(parseFloat(match.killToken)) * Number(match.adminCommision)/100);
		var coinsDeductData={
		  userId:match.players[i].userId,
		  coins:lastAmount,
		  tableId:match.players[i].tableId,
		  gameType:match.roomTitle+' '+match.gameMode,
		  betValue:match.killToken,
		  rummyPoints:0,
		  isWin:isWin,
		  adminCommition:match.adminCommision,
		  type:isAdd,
		  adminCoins:adminCoins,
		};
		var mainfirstToken = 0;
        var mainfirstThreeToken = 0;
        var winLossType = "Kill Token";
		if(isSqlUpdate=="Yes"){
			sql += "CALL userCoinsUpdate('"+coinsDeductData.userId+"','"+coinsDeductData.coins+"','"+coinsDeductData.tableId+"','"+coinsDeductData.gameType+"','"+coinsDeductData.betValue+"','"+coinsDeductData.rummyPoints+"','"+coinsDeductData.isWin+"','"+coinsDeductData.adminCommition+"','"+coinsDeductData.type+"','"+coinsDeductData.adminCoins+"','"+mainfirstToken+"','"+mainfirstThreeToken+"','"+winLossType+"','"+match.matchValue+"');"; 
		}
			// console.log(sql,"isSqlUpdate");
		if(match.players[i].coins < lastAmount){
			io.to(match.players[i].socketId).emit("balanceMessage",{message:"Insufficiant Balance"});
			setTimeout(function(){
				var data={
		    		tableId:match.players[i].tableId,
					userId:match.players[i].userId,
		    	};
				leftFromTable('',data);
			},3000);
			
		}else{
			io.to(match.players[i].socketId).emit("balanceMessage",{message:"Sufficiant Balance"});
		}
		     
	}
	common_model.sqlQuery(sql,function(res){ 
	}); 
	
}
function firstThreeTokenInHouse(match){
	var winningBet = match.winningfirstThreeToken;
	var matchValue = match.firstThreeToken;
	var winLossType ="First Three Token";
	
	var sql="";
	for (var i = 0; i < match.players.length; i++) {
		var isBotWin = "No";
		if(match.players[i].isDeductMoneyFirstThreeToken=="No" ){
			match.players[i].isDeductMoneyFirstThreeToken="Yes";
			if(match.players[i].isWinFirstThreeToken==true){				
				var isAdd= "Add";
				var isWin= "Win";
				var adminAmount = (parseFloat(winningBet)*parseFloat(match.adminCommision)/100);
				var winningPrice =(parseFloat(winningBet) - parseFloat(adminAmount));
				
				var lastAmount = parseFloat(winningPrice) -parseFloat(matchValue);
				if(match.players[i].playerType=='Bot'){
					isBotWin ="Yes";
					var adAmt = (Number(parseFloat(matchValue)) * Number(parseFloat(match.adminCommision)/100));
					lastAmount =  (Number(parseFloat(lastAmount)) +  Number(parseFloat(adAmt)));
				}
			}else{
				var adAmt = (Number(parseFloat(matchValue)) * Number(parseFloat(match.adminCommision)/100));
				if(match.players[i].playerType == 'Bot'){
					var lastAmount = parseFloat(matchValue) - parseFloat(adAmt);
				}else{
					var lastAmount = parseFloat(matchValue);
				}
				
				var isAdd= "Sub";
				var isWin= "Loss";
			}
			if(match.players[i].playerType=='Bot'){
				var adminCoins = 0;
			}else{
				var adminCoins = (Number(parseFloat(matchValue)) * Number(match.adminCommision)/100);
			}
			
			var coinsDeductData={
		          userId:match.players[i].userId,
		          coins:lastAmount,
		          tableId:match.players[i].tableId,
		          gameType:match.roomTitle+' '+match.gameMode,
		          betValue:matchValue,
		          rummyPoints:0,
		          isWin:isWin,
		          adminCommition:match.adminCommision,
		          type:isAdd,
		          adminCoins:adminCoins,
		      };
		      var mainfirstToken = 0;
              var mainfirstThreeToken = 0;
		      sql += "CALL userCoinsUpdate('"+coinsDeductData.userId+"','"+coinsDeductData.coins+"','"+coinsDeductData.tableId+"','"+coinsDeductData.gameType+"','"+coinsDeductData.betValue+"','"+coinsDeductData.rummyPoints+"','"+coinsDeductData.isWin+"','"+coinsDeductData.adminCommition+"','"+coinsDeductData.type+"','"+coinsDeductData.adminCoins+"','"+mainfirstToken+"','"+mainfirstThreeToken+"','"+winLossType+"','"+match.matchValue+"');"; 
		      console.log(sql,"firstThreeTokenInHouse");
		     
		}
	}
	common_model.sqlQuery(sql,function(res){ 
	}); 
}
function firstTokenInHouse(match){
	var winningBet = match.winningfirstToken;
	var matchValue = match.firstToken;
	var winLossType ="First Token";
	var sql="";
	for (var i = 0; i < match.players.length; i++) {
		var isBotWin = "No";
		if(match.players[i].isDeductMoneyFirstToken=="No" ){
			match.players[i].isDeductMoneyFirstToken="Yes";
			if(match.players[i].isWinFirstToken==true){				
				var isAdd= "Add";
				var isWin= "Win";
				var adminAmount = (parseFloat(winningBet)*parseFloat(match.adminCommision)/100);
				var winningPrice =(parseFloat(winningBet) - parseFloat(adminAmount));
				
				var lastAmount = parseFloat(winningPrice) -parseFloat(matchValue);
				if(match.players[i].playerType=='Bot'){
					isBotWin ="Yes";
					var adAmt = (Number(parseFloat(matchValue)) * Number(parseFloat(match.adminCommision)/100));
					lastAmount =  (Number(parseFloat(lastAmount)) +  Number(parseFloat(adAmt)));
				}
			}else{
				var adAmt = (Number(parseFloat(matchValue)) * Number(parseFloat(match.adminCommision)/100));
				if(match.players[i].playerType == 'Bot'){
					var lastAmount = parseFloat(matchValue) - parseFloat(adAmt);
				}else{
					var lastAmount = parseFloat(matchValue);
				}
				var isAdd= "Sub";
				var isWin= "Loss";
			}
			if(match.players[i].playerType=='Bot'){
				var adminCoins = 0;
			}else{
				var adminCoins = (Number(parseFloat(matchValue)) * Number(match.adminCommision)/100);
			}
			var coinsDeductData={
		          userId:match.players[i].userId,
		          coins:lastAmount,
		          tableId:match.players[i].tableId,
		          gameType:match.roomTitle+' '+match.gameMode,
		          betValue:matchValue,
		          rummyPoints:0,
		          isWin:isWin,
		          adminCommition:match.adminCommision,
		          type:isAdd,
		          adminCoins:adminCoins,
		      };
		      var mainfirstToken = 0;
              var mainfirstThreeToken = 0;
		      sql += "CALL userCoinsUpdate('"+coinsDeductData.userId+"','"+coinsDeductData.coins+"','"+coinsDeductData.tableId+"','"+coinsDeductData.gameType+"','"+coinsDeductData.betValue+"','"+coinsDeductData.rummyPoints+"','"+coinsDeductData.isWin+"','"+coinsDeductData.adminCommition+"','"+coinsDeductData.type+"','"+coinsDeductData.adminCoins+"','"+mainfirstToken+"','"+mainfirstThreeToken+"','"+winLossType+"','"+match.matchValue+"');"; 
		     console.log(sql,"firstTokenInHouse");
		}
	}
	common_model.sqlQuery(sql,function(res){ 
	}); 
}
//moveToken
function moveToken(data){
	var match = findMatchByTableId(data.tableId);	
	if(match){
		var player  = findPlayerById(match,data.userId);		
		if(player){
			var diceNum =match.matchDiceNumber;
			// 
			if(data.status=='Inactive' && diceNum!=6){

			}else if(player.isturn==true && diceNum!=0){
				match.isReturnMove = true;
				var sefPosition = [1,9,14,22,27,35,40,48,52,53,54,55,56,57];
				var isMove ="Yes";
				if(player.fourToken[data.tokenIndex].status=='Active'){
					var movPosition = Number(diceNum)+Number(player.fourToken[data.tokenIndex].position);
					if(movPosition > 57){
						isMove="No";
					}
				}
				if(isMove=='Yes'){
					if(player.fourToken[data.tokenIndex].status == 'Inactive' && diceNum == 6){
						player.inactiveTokenCount -=1;
						player.fourToken[data.tokenIndex].position =1;
						player.fourToken[data.tokenIndex].status = 'Active';
						player.fourToken[data.tokenIndex].isActiveToken = 'Active';
					}else if(player.fourToken[data.tokenIndex].status=='Active'){
						player.fourToken[data.tokenIndex].position = Number(diceNum)+Number(player.fourToken[data.tokenIndex].position);
						if(player.fourToken[data.tokenIndex].position==57){
							player.fourToken[data.tokenIndex].status ='Win';
							player.winCount +=1;
							match.isReturn = true;
						}
						if(match.gameMode=='Fancy' && player.winCount==1 && match.isWinFirstToken == false){
							player.isWinFirstToken =true;
							firstTokenInHouse(match);
							match.isWinFirstToken = true;
							io.in(data.tableId).emit("playerObject",match.players);
						}
						if(match.gameMode=='Fancy' && player.winCount==3 && match.isWinFirstThreeToken == false){
							player.isWinFirstThreeToken =true;
							firstThreeTokenInHouse(match);
							match.isWinFirstThreeToken = true;
							io.in(data.tableId).emit("playerObject",match.players);
						}
						if(player.winCount==4){
							match.winnerCount += 1;
							match.isReturn = false;
							player.isWin = true;
							player.isBlocked=true;
	                    	player.status="Win";
	                    	match.leftPlayers -=1;
							player.winPosition = match.winnerCount;
							if(match.winnerCount==1){
								winLossCoinsDistribution(match);
							}
							if(match.winnerCount == 3){
								match.isGameOver =true;
							}
							io.in(data.tableId).emit("playerObject",match.players);
						}
					}
					var gPosition = GetGlobalPositionFunction(player.fourToken[data.tokenIndex].position,player.tokenGlobleValue);
					
					player.fourToken[data.tokenIndex].globlePosition = gPosition;
					var isKillToken = false;
					if(gPosition != -1){					
						var isKillToken = findKillingFunction(match,player,gPosition);
						if(isKillToken==true){
							match.isReturn = true;
							isKillToken = true;
						}
					}
					var isSafeIndex = sefPosition.indexOf(player.fourToken[data.tokenIndex].position);
					if (isSafeIndex > -1){
						player.fourToken[data.tokenIndex].zone = 'safe';
					}else{
						player.fourToken[data.tokenIndex].zone = 'kill';
					}
					var tokenResulltData={
						tokenIndex:data.tokenIndex,
						isKillToken:isKillToken,
						userId:player.userId,
						diceNum:diceNum
					};
					io.in(match.tableId).emit('moveTokenResult',tokenResulltData);
					//io.in(match.tableId).emit("playerObject",match.players);
					if(match.matchDiceNumber==6){
						match.continueSixes +=1;
					}else{
						match.continueSixes=0;
					}
					match.matchDiceNumber= 0;
					// player.diceSixInTurn
					var rDice  = [1,6,6,2,3,4,5,6];
    				var rDiceNum = rDice[Math.floor(Math.random()*rDice.length)];
					if(match.isReturn==true && player.diceSixInTurn==2){
						rDice  = [1,2,3,4,5];
						rDiceNum = rDice[Math.floor(Math.random()*rDice.length)];
						player.nextturndice =rDiceNum;
					}
					if(match.continueSixes == 2){
						rDice  = [1,2,3,4,5];
						rDiceNum = rDice[Math.floor(Math.random()*rDice.length)];
					}
					match.matchNextDiceNumber = rDiceNum;
					match.isTokenMove= true;					
				}
			}
		}
	}
}
// winner function coins distribution
function winnerFuncion(match,player) {
	var data ={
		table:'user_details',
		fields:'coins,user_id,balance',
		condition:'user_id='+player.userId,
	};
	var adminAmount = (parseFloat(match.winningBet)*parseFloat(match.adminCommision)/100);
	var winningPrice =(parseFloat(match.winningBet) - parseFloat(adminAmount));
	common_model.GetData(data,function(res){	
		if(res.success==1){
			var balance = res.data[0].balance;
			var user_id = res.data[0].user_id;
			var lastCoin = Number(parseFloat(winningPrice)) +Number(parseFloat(balance));	
			player.coins = lastCoin;
			io.in(match.tableId).emit('winningBet',0);
			io.to(player.socketId).emit('coinsUpdate',lastCoin);
			// update user coins data to ludo_join_rooms
			var passData ={
				table:'user_details',
				setdata:'balance="'+lastCoin+'"',
				condition:'user_id='+user_id,
			};
			common_model.SaveData(passData,function(res){

			});
			// update data to ludo_join_rooms
			var passData2 ={
				table:'ludo_join_rooms',
				setdata:'gameStatus="Complete"',
				condition:'joinRoomId='+match.tableId,
			};
			common_model.SaveData(passData2,function(res1){
				// update data to ludo_join_room_users 
				var passData3 ={
					table:'ludo_join_room_users',
					setdata:'isWin="Yes"',
					condition:'joinRoomId="'+match.tableId+'" and userId="'+user_id+'"',
				};
				common_model.SaveData(passData3,function(res2){
					// insert winner data to ludo_winners
					var ludoWinData ={
						table:'ludo_winners',
						setdata:'joinRoomId="'+match.tableId+'",userId="'+user_id+'",adminPercent="'+match.adminCommision+'",totalWinningPrice="'+match.winningBet+'",adminAmount="'+adminAmount+'",winningPrice="'+winningPrice+'",gameMode="'+match.gameMode+'",isPrivate="'+match.isPrivate+'",created=now()',
						condition:'',
					};
					common_model.SaveData(ludoWinData,function(res3){
						
					});
				});
			});
			
		}
	});	
}

function findKillingFunction(match,player,gPosition){
	var isreturn = false;
	for (let i = 0; i < match.players.length; i++) {
		if(match.players[i].userId != player.userId){
			for (let j = 0; j < match.players[i].fourToken.length; j++) {
				if(gPosition==match.players[i].fourToken[j].globlePosition){		
					if(match.players[i].fourToken[j].zone=='kill'){
						match.players[i].fourToken[j].status='Inactive';
						match.players[i].fourToken[j].isActiveToken='Inactive';
						match.players[i].fourToken[j].position=0;
						match.players[i].fourToken[j].globlePosition=0;
						match.players[i].fourToken[j].zone='safe';
						match.players[i].inactiveTokenCount += 1;						
						match.players[i].totalActiveToken -= 1;				
						isreturn = true;
						if(match.gameMode=='Fancy'){
							killingDisdribution(match,player.userId,match.players[i].userId);
						}
						// killingDisdribution(match,winUserId,lossUserId)
					}						
				}
			}			
		}				
	}
	return isreturn;
}


// diceRollFunction
function diceRollFunction(data,isTest,num){
	var match = findMatchByTableId(data.tableId);
	if(match){
		var player  = findPlayerById(match,data.userId);
		if(player){	
			
			if(player.isturn==true && match.matchDiceNumber==0){

				var diceNumber = Math.floor(Math.random() * 6) + 1;		
				if(isTest=='Yes'){
					diceNumber=Number(num);
				}	
				
				if(diceNumber == 6){				
					player.diceSixInTurn +=1;
					match.isReturn = true;
				}else{
					player.diceSixInTurn =0;
					match.isReturn = false;
				}			
				var dicData={
					diceNumber:diceNumber,
					isReturn:match.isReturn,
					userId:player.userId
				};
				player.diceNumber= diceNumber;
				match.matchDiceNumber = diceNumber;
				
				io.to(match.tableId).emit("diceResult",dicData);
				if(player.inactiveTokenCount == 4 && diceNumber != 6){
					match.isTokenMove= true;
				}else if(player.inactiveTokenCount < 4){					
					var lastno = 0;
					//var inActiveCount = 0;
					for (let i = 0; i < player.fourToken.length; i++) {
						if(player.fourToken[i].status=='Active'){
							var no = 57 - player.fourToken[i].position;
							if(lastno < no){
								lastno = no;
							}
						}						
					}
					if(diceNumber==6 && player.inactiveTokenCount > 0){
					}else if(lastno < diceNumber){
						match.isTokenMove= true;
					}
				}
				
			}
		}
	}
}

// diceRollFunction
function diceRollFunction_main(data,isTest,num){
	var match = findMatchByTableId(data.tableId);
	if(match){
		var player  = findPlayerById(match,data.userId);
		if(player){	
			if(player.playerType=='Bot'){
				match.matchDiceNumber = num;
			}
			if(player.isturn==true && match.matchDiceNumber==0){

				var diceNumber = match.matchNextDiceNumber;		
				
				if(diceNumber == 6){				
					player.diceSixInTurn +=1;
					match.isReturn = true;
				}else{
					player.diceSixInTurn =0;
					match.isReturn = false;
				}			
				var dicData={
					diceNumber:diceNumber,
					isReturn:match.isReturn,
					userId:player.userId
				};
				player.diceNumber= diceNumber;
				match.matchDiceNumber = diceNumber;
				io.to(match.tableId).emit("diceResult",dicData);
				if(player.inactiveTokenCount == 4 && diceNumber != 6){
					match.isTokenMove= true;
				}else if(player.inactiveTokenCount < 4){					
					var lastno = 0;
					for (let i = 0; i < player.fourToken.length; i++) {
						if(player.fourToken[i].status=='Active'){
							var no = 57 - player.fourToken[i].position;
							if(lastno < no){
								lastno = no;
							}
						}						
					}
					if(diceNumber==6 && player.inactiveTokenCount > 0){
					}else if(lastno < diceNumber){
						match.isTokenMove= true;
					}
				}
				
			}
		}
	}
}

/*
// diceRollFunction
function diceRollFunction(data,isTest,num){
	var match = findMatchByTableId(data.tableId);
	if(match){
		var player  = findPlayerById(match,data.userId);
		if(player){	
			
			if(player.isturn==true && match.matchDiceNumber==0){


				var diceNumber=1;	
				if(player.nextturndice==num){
					diceNumber=Number(num);
				}
				else{

					diceNumber=1;
				}
				
				var diceNumber1 = GetDiceNumber(player.fourToken,player.continuesSixAppearCount,player);	
				player.nextturndice=diceNumber1;

				//console.log(player.nextturndice);

				if(diceNumber == 6){				
					player.diceSixInTurn +=1;
					match.isReturn = true;
				}else{
					player.diceSixInTurn =0;
					match.isReturn = false;
				}			
				var dicData={
					diceNumber:diceNumber,
					isReturn:match.isReturn,
					userId:player.userId
				};
				player.diceNumber= diceNumber;
				match.matchDiceNumber = diceNumber;
				
				io.to(match.tableId).emit("diceResult",dicData);
				if(player.inactiveTokenCount == 4 && diceNumber != 6){
					match.isTokenMove= true;
				}else if(player.inactiveTokenCount < 4){					
					var lastno = 0;
					//var inActiveCount = 0;
					for (let i = 0; i < player.fourToken.length; i++) {
						if(player.fourToken[i].status=='Active'){
							var no = 57 - player.fourToken[i].position;
							if(lastno < no){
								lastno = no;
							}
						}						
					}
					if(diceNumber==6 && player.inactiveTokenCount > 0){
					}else if(lastno < diceNumber){
						match.isTokenMove= true;
					}
				}
				
			}
		}
	}
}*/

 function  GetDiceNumber(tokens,continuesSixAppearCount,player)
    {
        var randomDiceNumber = 0;


        if (continuesSixAppearCount >= 1)
        {

            randomDiceNumber = Math.floor(Math.random() * 5) + 1;	
        }
        else
        {
           
                if (Math.floor(Math.random() * 20) <= 5)
                {
                    randomDiceNumber = 6;
                }
                else
                {
                    randomDiceNumber = Math.floor(Math.random() * 5) + 1;
                }
            }
        

        if (randomDiceNumber == 6)
        {
            ++continuesSixAppearCount;
        }
        else
        {
            if (continuesSixAppearCount > 0)
            {
                --continuesSixAppearCount;
            }
        }
        player.continuesSixAppearCount=continuesSixAppearCount;
        return randomDiceNumber;
    }

var lastdicenumber;
// find to token position 
function  findTopTokenPosition(player){
	var topposition = 57;
	for (let i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active'){
			var position = 57 - player.fourToken[j].position;
			if(topposition < position){

			}
		}		
	}
}
function changePlayerIndex(match) {    
    var points= match.startDicePosition.sort(function(a, b){return b-a});  
    var allPlayer=[];
    for (var i = 0; i < points.length; i++) {
        for (var j = 0; j < match.players.length; j++) {
          if(match.players[j].tossValue==points[i]){
            allPlayer.push(match.players[j]);
    		io.in(match.tableId).emit("playerObject",match.players);
          }
        }      
    }  
    match.players=allPlayer;
    io.in(match.tableId).emit("playerObject",match.players);
}
//takePlayerIndex tokenColor
function takePlayerIndex(match){
	for (let i = 0; i < match.players.length; i++) {	
		if(i==0){
			match.players[i].isturn = true;
			match.whosTurn	=	match.players[i].playerType;
			match.currentTurnUserId	=	match.players[i].userId;
		}else{
			match.players[i].isturn = false;
		}	
		match.players[i].isStart = true;
		match.players[i].playerIndex = i;
		if(match.players.length==2 && i==1){			
			match.players[i].playerIndex = 2;
		}
		match.players[i].tokenColor = tokenColor[match.players[i].playerIndex];
		match.players[i].tokenGlobleValue = Number(tokenGlobleValue[match.players[i].playerIndex]);
		// io.in(match.tableId).emit("playerObject",match.players);
	}
	io.in(match.tableId).emit("playerObject",match.players);

}
function startWaitingTimer(matchIndex){
	 let i = matchIndex;
	if(matches[i]){
		if(!matches[i].isGameStart){
			matches[i].startRoundWaiting -= 1;
			io.in(matches[i].tableId).emit("startRoundWaiting",matches[i].startRoundWaiting);	
			
			if(matches[i].isPrivate=='No'){
				if(matches[i].isBotConnect=="Yes"){
					if(matches[i].startRoundWaiting==35){
						addBotsFunction(matches[i]);
					}	
					if(matches[i].startRoundWaiting==25){
						joinBotDataFunction(matches[i],1);
					}	
					if(matches[i].startRoundWaiting==15){
						joinBotDataFunction(matches[i],2);
					}
				}	
				// if (matches[i].startRoundWaiting == 3) {
				// 	if(matches[i].players.length >1){
				// 		if(matches[i].players.length == 2){
				// 			matches[i].isTossTimmer = 30;
				// 		}
				// 		if(matches[i].players.length == 3){
				// 			matches[i].isTossTimmer = 34;
				// 		}
				// 		updateStatus(matches[i]);
				// 	}
				// }
				// else 
				if (matches[i].startRoundWaiting == 0) {
					if(matches[i].isGameStart==false){
						for (var j = 0; j < matches[i].players.length; j++) {
							var updateData = {
				                tableId:matches[i].tableId,
				                userId:matches[i].players[j].userId,
				                playerlength:0,
				                isGameStart:matches[i].isGameStart
				            }
				            common_model.dbUpdates(updateData);
						}	
						delete matches[matches[i].tableId];  				 
						// matches.splice(matches.indexOf(matches[i]), 1);							
					}
				}else{
					setTimeout(()=>startWaitingTimer(matchIndex), 1000);   
				}
			}			
		}
	}else{
		console.log("Stop");
	}
}

// Call Match Timers for all individual matches asynchronously.
function startMatchTimer(matchIndex){
    let i = matchIndex; 
    // console.log(matchIndex," matchIndex")
    if(matches[i]){   
    	if(matches[i].isGameStart){
    	//	console.log(matches[i].isGameStart," matches[i].isGameStart",matches[i].whosTurn," startMatchTimer ",matches[i].botthrowDieTime);
	    	if(matches[i].isToss==false){
        		io.in(matches[i].tableId).emit("isTossTimmer",matches[i].isTossTimmer);	
        		matches[i].isTossTimmer -= 1;
        		if (matches[i].isTossTimmer == 0) {
        			changePlayerIndex(matches[i]);
					takePlayerIndex(matches[i]);
					matches[i].isToss = true;
					io.in(matches[i].tableId).emit("playerObject",matches[i].players);
        		}  
        	}else if(matches[i].isGameOver==false && matches[i].leftPlayers==1){
    			var winPlayer = getLastActivePlayer(matches[i]);
        		winPlayer.isWin= true;
        		matches[i].isGameOver =true;
				winLossCoinsDistribution(matches[i]);
	     //    		if(matches[i].gameMode != 'KillToken'){
	     //    		}
				io.in(matches[i].tableId).emit("playerObject",matches[i].players);
        	}else if(matches[i].isGameOver==true){
				matches[i].gameOverTime -= 1;
				io.in(matches[i].tableId).emit("gameOverTime",matches[i].gameOverTime); 
				if (matches[i].gameOverTime == 0) {    
                    delete matches[matches[i].tableId];            
                }
			}else if(matches[i].isTokenMove==true){
				matches[i].tokenMoveTime -= 1;
				io.in(matches[i].tableId).emit("tokenMoveTime",matches[i].tokenMoveTime); 
				if (matches[i].tokenMoveTime == 0) {
					matches[i].tokenMoveTime =matches[i].consttokenMoveTime;
					timesup(matches[i],'Yes');
				}
			}else{
				matches[i].throwDieTime -= 1; 
				if(matches[i].throwDieTime==matches[i].botthrowDieTime && matches[i].whosTurn=='Bot'){   
	                botDiceRollFunction(matches[i]);	                                 
                }
				io.in(matches[i].tableId).emit("rollDiceTimer",matches[i].throwDieTime);
				if (matches[i].throwDieTime == 0) {
					timesup(matches[i],'No');
				}
				else if(matches[i].throwDieTime <  0){
					matches[i].throwDieTime =matches[i].constthrowDieTime;
				}
			}
	    	setTimeout(()=>startMatchTimer(matchIndex), 1000);   
    	} 
    }else{
    	 console.log("Stopped startMatchTimer : "+matchIndex);
    }
}
// update timmer function
// function updateTimers(){
// 	for (var i = 0; i < matches.length; i++) {        	
//         if(!matches[i].isGameStart){
//         	if(!matches[i]){
//         		console.log("match off");
// 				return false;
// 			}
// 			if(matches[i]==undefined){
//         		console.log("match blash");
// 				return false;
// 			}
// 			if(matches[i].isPrivate==undefined){
// 				return false;
// 			}
// 			matches[i].startRoundWaiting -= 1;
// 			io.in(matches[i].tableId).emit("startRoundWaiting",matches[i].startRoundWaiting);	
// 			if(matches[i].isPrivate=='No'){
				
// 				// if(matches[i].isBotConnect=="Yes"){
// 				// 	if(matches[i].startRoundWaiting==35){
// 				// 		addBotsFunction(matches[i]);
// 				// 	}	
// 				// 	if(matches[i].startRoundWaiting==25){
// 				// 		joinBotDataFunction(matches[i],1);
// 				// 	}	
// 				// 	if(matches[i].startRoundWaiting==15){
// 				// 		joinBotDataFunction(matches[i],2);
// 				// 	}
// 				// }		
// 				// if (matches[i].startRoundWaiting == 3) {
// 				// 	if(matches[i].players.length >1){
// 				// 		if(matches[i].players.length == 2){
// 				// 			matches[i].isTossTimmer = 1;
// 				// 		}
// 				// 		if(matches[i].players.length == 3){
// 				// 			matches[i].isTossTimmer = 1;
// 				// 		}
// 				// 		updateStatus(matches[i]);
// 				// 	}
// 				// }
// 				if (matches[i].startRoundWaiting == 0) {
// 					if(matches[i].isGameStart==false){
// 						for (var j = 0; j < matches[i].players.length; j++) {
// 							var updateData = {
// 				                tableId:matches[i].tableId,
// 				                userId:matches[i].players[j].userId,
// 				                playerlength:0,
// 				                isGameStart:matches[i].isGameStart
// 				            }
// 				            common_model.dbUpdates(updateData);
// 						}	
// 						delete matches[matches[i].tableId];				 
// 						// matches.splice(matches.indexOf(matches[i]), 1);							
// 					}
// 				}
// 			}
			
//         }else{
//         	if(matches[i].isToss==false){
//         		io.in(matches[i].tableId).emit("isTossTimmer",matches[i].isTossTimmer);	
//         		matches[i].isTossTimmer -= 1;
//         		if (matches[i].isTossTimmer == 0) {
//         			changePlayerIndex(matches[i]);
// 					takePlayerIndex(matches[i]);

// 					matches[i].isToss = true;
// 					io.in(matches[i].tableId).emit("playerObject",matches[i].players);
//         		}  
//         	}else if(matches[i].isGameOver==false && matches[i].leftPlayers==1){
//     			var winPlayer = getLastActivePlayer(matches[i]);
//         		winPlayer.isWin= true;
//         		matches[i].isGameOver =true;
//         		if(matches[i].gameMode == 'Fancy'){
// 					winLossCoinsDistribution(matches[i]);
//         		}else{
//         			winLossCoinsDistribution(matches[i]);
//         		}
// 				io.in(matches[i].tableId).emit("playerObject",matches[i].players);
//         	}else if(matches[i].isGameOver==true){
// 				matches[i].gameOverTime -= 1;
// 				io.in(matches[i].tableId).emit("gameOverTime",matches[i].gameOverTime); 
// 				if (matches[i].gameOverTime == 0) {
// 					delete matches[matches[i].tableId];		
//                     // matches.splice(matches.indexOf(matches[i]), 1);                    
//                 }
// 			}else if(matches[i].isTokenMove==true){
// 				matches[i].tokenMoveTime -= 1;
// 				io.in(matches[i].tableId).emit("tokenMoveTime",matches[i].tokenMoveTime); 
// 				if (matches[i].tokenMoveTime == 0) {
// 					matches[i].tokenMoveTime =matches[i].consttokenMoveTime;
// 					timesup(matches[i],'Yes');
// 				}
// 			}else{
// 				matches[i].throwDieTime -= 1; 
// 				if(matches[i].throwDieTime==matches[i].botthrowDieTime && matches[i].whosTurn=='Bot'){   
// 	                botDiceRollFunction(matches[i]);	                                 
//                 }

// 				io.in(matches[i].tableId).emit("rollDiceTimer",matches[i].throwDieTime);
// 				if (matches[i].throwDieTime == 0) {
// 					timesup(matches[i],'No');
// 				}
// 				else if(matches[i].throwDieTime <  0){
// 					matches[i].throwDieTime =matches[i].constthrowDieTime;
// 				}
// 			}
// 		}
// 	}
// 	setTimeout(updateTimers, 1000);
// }
//if(matches[i].extratime!=0 && matches[i].isExtratime==true){
//     	matches[i].extratime -= 1;
//}
//if(matches[i].throwDieTime == 0 && matches[i].extratime != 0 && matches[i].isExtratime==false){
// 	    matches[i].isExtratime=true;
// 		matches[i].throwDieTime =matches[i].extratime;
//}
//                 isExtratime
// extratime
function getDiceNo(match,player){
	player.diceSixInTurn =0;
	if(match.botWin=="Yes"){
		var diceNum  =  [4,5];
	}else{
		var diceNum  =  [3,4,5];
	}		
	var randomNum = diceNum[Math.floor(Math.random()*diceNum.length)];
	return randomNum;
}
function findWinningNumber(player){
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active'){
			if(player.fourToken[i].position >=51 && player.fourToken[i].position <57){
				var num = 57-Number(player.fourToken[i].position);
				return num;
			}
		}
	}
	return false;
}

function getUnsafeToken(match,player){
	var data={
		tokenPosition:0,
		number:5,
		token:false
	};
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active' && player.fourToken[i].zone=='kill'){
			var globlePosition = Number(player.fourToken[i].globlePosition);
			var globlePosition2 = Number(player.fourToken[i].globlePosition) - Number(6);
			for (var j = 0; j < match.players.length; j++) {
				if(player.userId != match.players[j].userId){
					for (var k = 0; k < match.players[j].fourToken.length; k++) {
						var oppositeGloblePosition = Number(match.players[j].fourToken[k].globlePosition);
						if(globlePosition > oppositeGloblePosition && globlePosition2 < oppositeGloblePosition && match.players[j].fourToken[k].status=='Active'){
							if(data.tokenPosition < globlePosition){
								var gNum= globlePosition - oppositeGloblePosition;
								data.tokenPosition=globlePosition;
								data.token=player.fourToken[i];
							}
						}						
					}
				}					
			}
		}		
	}
	return data;
}
function moveTopPostionToken(player){
	//if(0 < 40){
	var movedata={
		tokenPosition:0,
		token:false
	};
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active'){
			if(movedata.tokenPosition < player.fourToken[i].position){
				movedata.tokenPosition =player.fourToken[i].position;
				movedata.token =player.fourToken[i];
			}
		}
	}
	return movedata;
}
// bot dice roll function 
// bot dice roll function 
function botDiceRollFunction(match){
	var timeArray = [2000,3000,4000];
    var randomTime=timeArray[Math.floor(Math.random()*timeArray.length)];

	var passData={
		userId:match.currentTurnUserId,
		tableId:match.tableId
	};
	
	var player  = findIsTurnIndex(match);
	if(player){	
		if(match.botWin == "Yes"){
			var diceNum  =  [1,2,3,4,5,6,6];
		}else{
			var diceNum  =  [1,2,3,4,5,6,6];
		}
		var randomNum = diceNum[Math.floor(Math.random()*diceNum.length)];
		
		var winnerNumber=findWinningNumber(player);
		var killingNumber=getKillerPositionNumber(match,player);
		var isBehindToken=getUnsafeToken(match,player);
		var moveTopPosition=moveTopPostionToken(player);
		// if(player.diceSixInTurn==1){
		// 	randomNum =6;
		// }
		//if(player.diceSixInTurn==0){
		//	randomNum =6;
		//}
		if(match.isBotFirstSix==false){
			randomNum=6;
			match.isBotFirstSix=true;
		}else if(winnerNumber){
			randomNum = winnerNumber;
		}else if(killingNumber){
			randomNum = killingNumber;
		}else if(isBehindToken.token){
			randomNum = isBehindToken.number;
		}

		if(player.diceSixInTurn==2){
			var getranNum = getDiceNo(match,player);
		}else{
			var getranNum = randomNum;
		}

		var randomNum =getranNum;
		
		diceRollFunction(passData,"Yes",randomNum); 
			var isKillerToken = isKillerTokenFunction(match,player,randomNum);
			var isSafferToken = isSafferTokenFunction(player,randomNum);
			var isWinnerToken = isWinnerTokenFunction(player,randomNum);
		setTimeout(function() {
			//if(isKillerToken)
			if(player.inactiveTokenCount==4 && randomNum == 6){
				var data ={
					userId:player.userId,
					tableId:player.tableId,
					tokenIndex:player.fourToken[0].tokenIndex,
					status:player.fourToken[0].status,
				};
				moveToken(data);	
			}else if(isWinnerToken){
				var data ={
					userId:player.userId,
					tableId:player.tableId,
					tokenIndex:isWinnerToken.tokenIndex,
					status:isWinnerToken.status,
				};
				moveToken(data);
			}else if(isKillerToken){
				var data ={
					userId:player.userId,
					tableId:player.tableId,
					tokenIndex:isKillerToken.tokenIndex,
					status:isKillerToken.status,
				};
				moveToken(data);
			}else if(isBehindToken.token){
				var data ={
					userId:player.userId,
					tableId:player.tableId,
					tokenIndex:isBehindToken.token.tokenIndex,
					status:isBehindToken.token.status,
				};
				moveToken(data);
			}else if(match.gameMode=='Quick' && player.inactiveTokenCount==2 && moveTopPosition.token!=false){
				var data ={
					userId:player.userId,
					tableId:player.tableId,
					tokenIndex:moveTopPosition.token.tokenIndex,
					status:moveTopPosition.token.status,
				};
				
				moveToken(data);
			}else if(isSafferToken){
				var data ={
					userId:player.userId,
					tableId:player.tableId,
					tokenIndex:isSafferToken.tokenIndex,
					status:isSafferToken.status,
				};
				moveToken(data);
			}else if(player.inactiveTokenCount==3){
				var activeTokenDetail = findActiveToken(player,randomNum);
				var activeCurrentPosition = activeTokenDetail.position + randomNum;
				if(randomNum != 6){
					if(activeCurrentPosition <= 57){
						var data ={
							userId:player.userId,
							tableId:player.tableId,
							tokenIndex:activeTokenDetail.tokenIndex,
							status:activeTokenDetail.status,
						};
						moveToken(data);
					}				
				}else if(randomNum==6){
					var inactiveTokenDetail = findInactiveToken(player);
					var inactiveCurrentPosition = inactiveTokenDetail.position + randomNum;
					var data ={
						userId:player.userId,
						tableId:player.tableId,
						tokenIndex:inactiveTokenDetail.tokenIndex,
						status:inactiveTokenDetail.status,
					};
					moveToken(data);
				}
			}else if(player.inactiveTokenCount!=0 && randomNum==6){
				var allActiveTokenData = findActiveTokenWithGposition(player,randomNum);
				if(allActiveTokenData.isKillReturn==true){
					var rannum  = Math.floor(Math.random() * (allActiveTokenData.tokenKillArray.length - 0)) + 0;
					var data ={
						userId:player.userId,
						tableId:player.tableId,
						tokenIndex:allActiveTokenData.tokenKillArray[rannum].tokenIndex,
						status:allActiveTokenData.tokenKillArray[rannum].status,
					};
					moveToken(data);					
				}else{
					var inactiveTokenDetail = findInactiveToken(player);
					var inactiveCurrentPosition = inactiveTokenDetail.position + randomNum;
					var data ={
						userId:player.userId,
						tableId:player.tableId,
						tokenIndex:inactiveTokenDetail.tokenIndex,
						status:inactiveTokenDetail.status,
					};
					moveToken(data);
				}
				
			}else{
				var allActiveTokenData = findActiveTokenWithGposition(player,randomNum);
				if(allActiveTokenData.isKillReturn==true){
					var rannum  = Math.floor(Math.random() * (allActiveTokenData.tokenKillArray.length - 0)) + 0;
					var data ={
						userId:player.userId,
						tableId:player.tableId,
						tokenIndex:allActiveTokenData.tokenKillArray[rannum].tokenIndex,
						status:allActiveTokenData.tokenKillArray[rannum].status,
					};
					moveToken(data);					
				}else{
					if(allActiveTokenData.isSafeReturn==true){
						var rannum  = Math.floor(Math.random() * (allActiveTokenData.tokenSafeArray.length - 0)) + 0;
						var data ={
							userId:player.userId,
							tableId:player.tableId,
							tokenIndex:allActiveTokenData.tokenSafeArray[rannum].tokenIndex,
							status:allActiveTokenData.tokenSafeArray[rannum].status,
						};
						moveToken(data);					
					}else{
						match.isTokenMove= true;
					}
				}
			}
		},randomTime);		
	}	
}
// function botDiceRollFunction_main(match){
// 	var timeArray = [2000,3000,4000];
//     var randomTime=timeArray[Math.floor(Math.random()*timeArray.length)];

	
	
// 	var player  = findIsTurnIndex(match);
// 	if(player){	
// 		if(match.botWin == "Yes"){
// 			var diceNum  =  [4,5,6];
// 		}else{
// 			var diceNum  =  [1,2,3,4,5,6];
// 		}
// 		var randomNum = diceNum[Math.floor(Math.random()*diceNum.length)];
		
// 		var winnerNumber=findWinningNumber(player);
// 		var killingNumber=getKillerPositionNumber(match,player);
// 		var isBehindToken=getUnsafeToken(match,player);
// 		var moveTopPosition=moveTopPostionToken(player);
// 		// if(player.diceSixInTurn==1){
// 		// 	randomNum =6;
// 		// }
// 		if(player.inactiveTokenCount==4){
// 			randomNum =6;
// 		}
// 		// if(match.isBotFirstSix==false){
// 		// 	randomNum=6;
// 		// 	match.isBotFirstSix=true;
// 		// }else 
// 		if(winnerNumber){
// 			randomNum = winnerNumber;
// 		}else if(killingNumber){
// 			randomNum = killingNumber;
// 		}else if(isBehindToken.token){
// 			randomNum = isBehindToken.number;
// 		}

// 		if(player.diceSixInTurn==2){
// 			var getranNum = getDiceNo(match,player);
// 		}else{
// 			var getranNum = randomNum;
// 		}

// 		var randomNum =getranNum;
// 		var passData={
// 			userId:player.userId,
// 			tableId:match.tableId
// 		};
		
// 		console.log("randomNum ",randomNum,"  userId ",player.userId," nextturndice ",player.diceNumber)
// 		diceRollFunction(passData,"Yes",randomNum);  
// 			var isKillerToken = isKillerTokenFunction(match,player,randomNum);
// 			var isSafferToken = isSafferTokenFunction(player,randomNum);
// 			var isWinnerToken = isWinnerTokenFunction(player,randomNum);
// 		setTimeout(function() {
// 			//if(isKillerToken)
// 			if(player.inactiveTokenCount==4 && randomNum == 6){
// 				console.log(" one ")
// 				var data ={
// 					userId:player.userId,
// 					tableId:player.tableId,
// 					tokenIndex:player.fourToken[0].tokenIndex,
// 					status:player.fourToken[0].status,
// 				};
// 				moveToken(data);	
// 			}else if(isWinnerToken){
// 					console.log(" two ")
// 				var data ={
// 					userId:player.userId,
// 					tableId:player.tableId,
// 					tokenIndex:isWinnerToken.tokenIndex,
// 					status:isWinnerToken.status,
// 				};
// 				moveToken(data);
// 			}else if(isKillerToken){
// 				console.log(" three ")
// 				var data ={
// 					userId:player.userId,
// 					tableId:player.tableId,
// 					tokenIndex:isKillerToken.tokenIndex,
// 					status:isKillerToken.status,
// 				};
// 				moveToken(data);
// 			}else if(isBehindToken.token){
// 				console.log(" four ")
// 				var data ={
// 					userId:player.userId,
// 					tableId:player.tableId,
// 					tokenIndex:isBehindToken.token.tokenIndex,
// 					status:isBehindToken.token.status,
// 				};
// 				moveToken(data);
// 			}else if(match.gameMode=='Quick' && player.inactiveTokenCount==2 && moveTopPosition.token!=false){
// 				console.log(" five ")
// 				var data ={
// 					userId:player.userId,
// 					tableId:player.tableId,
// 					tokenIndex:moveTopPosition.token.tokenIndex,
// 					status:moveTopPosition.token.status,
// 				};
				
// 				moveToken(data);
// 			}else if(isSafferToken){
// 				console.log(" six ")
// 				var data ={
// 					userId:player.userId,
// 					tableId:player.tableId,
// 					tokenIndex:isSafferToken.tokenIndex,
// 					status:isSafferToken.status,
// 				};
// 				moveToken(data);
// 			}else if(player.inactiveTokenCount==3){
// 					console.log(" seven ")
// 				var activeTokenDetail = findActiveToken(player,randomNum);
// 				var activeCurrentPosition = activeTokenDetail.position + randomNum;
// 				if(randomNum != 6){
// 					if(activeCurrentPosition <= 57){
// 						console.log(" eight ")
// 						var data ={
// 							userId:player.userId,
// 							tableId:player.tableId,
// 							tokenIndex:activeTokenDetail.tokenIndex,
// 							status:activeTokenDetail.status,
// 						};
// 						moveToken(data);
// 					}				
// 				}else if(randomNum==6){
// 					console.log(" nile ")
// 					var inactiveTokenDetail = findInactiveToken(player);
// 					var inactiveCurrentPosition = inactiveTokenDetail.position + randomNum;
// 					var data ={
// 						userId:player.userId,
// 						tableId:player.tableId,
// 						tokenIndex:inactiveTokenDetail.tokenIndex,
// 						status:inactiveTokenDetail.status,
// 					};
// 					moveToken(data);
// 				}
// 			}else if(player.inactiveTokenCount!=0 && randomNum==6){
// 					console.log(" 11 ")
// 				var allActiveTokenData = findActiveTokenWithGposition(player,randomNum);
// 				if(allActiveTokenData.isKillReturn==true){
// 					console.log(" 12222 ")
// 					var rannum  = Math.floor(Math.random() * (allActiveTokenData.tokenKillArray.length - 0)) + 0;
// 					var data ={
// 						userId:player.userId,
// 						tableId:player.tableId,
// 						tokenIndex:allActiveTokenData.tokenKillArray[rannum].tokenIndex,
// 						status:allActiveTokenData.tokenKillArray[rannum].status,
// 					};
// 					moveToken(data);					
// 				}else{
// 					console.log("else else")
// 					var inactiveTokenDetail = findInactiveToken(player);
// 					var inactiveCurrentPosition = inactiveTokenDetail.position + randomNum;
// 					var data ={
// 						userId:player.userId,
// 						tableId:player.tableId,
// 						tokenIndex:inactiveTokenDetail.tokenIndex,
// 						status:inactiveTokenDetail.status,
// 					};
// 					moveToken(data);
// 				}
				
// 			}else{
// 				console.log(" 1333 ")
// 				var allActiveTokenData = findActiveTokenWithGposition(player,randomNum);
// 				if(allActiveTokenData.isKillReturn==true){
// 						console.log(" isKillReturn else ")
// 					var rannum  = Math.floor(Math.random() * (allActiveTokenData.tokenKillArray.length - 0)) + 0;
// 					var data ={
// 						userId:player.userId,
// 						tableId:player.tableId,
// 						tokenIndex:allActiveTokenData.tokenKillArray[rannum].tokenIndex,
// 						status:allActiveTokenData.tokenKillArray[rannum].status,
// 					};
// 					moveToken(data);					
// 				}else{
// 					if(allActiveTokenData.isSafeReturn==true){
// 						console.log(" isSafeReturn true ")
// 						var rannum  = Math.floor(Math.random() * (allActiveTokenData.tokenSafeArray.length - 0)) + 0;
// 						var data ={
// 							userId:player.userId,
// 							tableId:player.tableId,
// 							tokenIndex:allActiveTokenData.tokenSafeArray[rannum].tokenIndex,
// 							status:allActiveTokenData.tokenSafeArray[rannum].status,
// 						};
// 						moveToken(data);					
// 					}else{
// 						match.isTokenMove= true;
// 					console.log(" console.log() ")
// 					}
// 				}
// 			}
// 		},randomTime);		
// 	}	
// }

function getKillerPositionNumber(match,player){
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active'){
			var globlePosition = Number(player.fourToken[i].globlePosition);
			var globlePosition2 = Number(player.fourToken[i].globlePosition) + Number(6);
			for (var j = 0; j < match.players.length; j++) {
				if(player.userId != match.players[j].userId){
					for (var k = 0; k < match.players[j].fourToken.length; k++) {
						var oppositeGloblePosition = Number(match.players[j].fourToken[k].globlePosition);
						if(oppositeGloblePosition > globlePosition && oppositeGloblePosition <= globlePosition2 && match.players[j].fourToken[k].zone=='kill' && match.players[j].fourToken[k].status=='Active'){
							var gNum = oppositeGloblePosition - globlePosition;
							return gNum;
						}						
					}
				}					
			}
		}		
	}
	return false;
}
function isKillerTokenFunction(match,player,randomNum){
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active'){
			//var globlePosition = player.fourToken[i].globlePosition + randomNum;
			var globlePosition = Number(player.fourToken[i].globlePosition) + Number(randomNum);
			for (var j = 0; j < match.players.length; j++) {
				if(player.userId != match.players[j].userId){
					for (var k = 0; k < match.players[j].fourToken.length; k++) {
						if(match.players[j].fourToken[k].globlePosition==globlePosition && match.players[j].fourToken[k].zone=='kill' && match.players[j].fourToken[k].status=='Active'){
							
							return player.fourToken[i];
						}
					}
				}					
			}
		}
		
	}
	return false;
}
function isSafferTokenFunction(player,randomNum){
	var sefPosition = [1,9,14,22,27,35,40,48,52,53,54,55,56,57];				
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active'){
			var currentPosition = Number(player.fourToken[i].position) + Number(randomNum);
			var isSafeIndex = sefPosition.indexOf(currentPosition);
			if (isSafeIndex > -1){
				return player.fourToken[i];
			}	
		}
			
	}
	return false;
}
function isWinnerTokenFunction(player,randomNum){
	for (var i = 0; i < player.fourToken.length; i++) {
	    var currentPosition = Number(player.fourToken[i].position) + Number(randomNum);
		if(player.fourToken[i].status=='Active' && currentPosition==57){
			return player.fourToken[i];
		}
	}
	return false;
}
// find active token
function findisKillToken(player){
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active'){
			return player.fourToken[i];
		}
	}
	return false;
}
// find active token
function findActiveToken(player,randomNum){
	for (var i = 0; i < player.fourToken.length; i++) {
		var activeCurrentPosition = player.fourToken[i].position + randomNum;
		if(player.fourToken[i].status=='Active' && activeCurrentPosition==57){
			return player.fourToken[i];
		}
	}
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Active'){
			return player.fourToken[i];
		}
	}
	return false;
}
//var rannum  = Math.floor(Math.random() * (match.players.length - 0)) + 0;
// find active token
function findActiveTokenWithGposition(player,randomNum){
	var data = {
		tokenSafeArray:[],
		tokenKillArray:[],
		isSafeReturn:false,
		isKillReturn:false
	};
	for (var i = 0; i < player.fourToken.length; i++) {
		var activeCurrentPosition = player.fourToken[i].position + randomNum;
		if(player.fourToken[i].status=='Active' &&  activeCurrentPosition <= 57 && player.fourToken[i].zone =="safe"){
			data.isSafeReturn = true;
			data.tokenSafeArray.push(player.fourToken[i]);
		}
	}
	for (var i = 0; i < player.fourToken.length; i++) {
		var activeCurrentPosition = player.fourToken[i].position + randomNum;
		if(player.fourToken[i].status=='Active' &&  activeCurrentPosition <= 57 && player.fourToken[i].zone =="kill"){
			data.isKillReturn = true;
			data.tokenKillArray.push(player.fourToken[i]);
		}
	}
	return data;
}
// find active token
function findInactiveToken(player){
	for (var i = 0; i < player.fourToken.length; i++) {
		if(player.fourToken[i].status=='Inactive'){
			return player.fourToken[i];
		}
	}
	return false;
}
// timesup
function timesup(match,isPlay){
	 match.isTokenMove= false;
	 var player  = findIsTurnIndex(match);
	 if(player){
	 	var rDice  = [1,2,6,3,4,5,6,6];
    	var matchNextDiceNumber = rDice[Math.floor(Math.random()*rDice.length)];
    	match.matchNextDiceNumber = matchNextDiceNumber;
		var pIndex = match.players.indexOf(player);
		if (pIndex > -1){
			if(isPlay=='No'){
				player.totalLifes -=1;
			}
			console.log(player.totalLifes," totalLifes ",player.userName," userName leftPlayers ",match.leftPlayers)
			if(player.totalLifes <= 0){
				var data={
		    		tableId:player.tableId,
					userId:player.userId,
		    	};
				leftFromTable('',data);
			}else{
				nextTurnFunction(match,pIndex);
			}
		}
	 }
}
// findIsTurnIndex
function findIsTurnIndex(match){
    var players = match.players;
    for (let i = 0; i < players.length; i++) {
        if(players[i].isturn == true){
           return players[i];
        }
    }
    return false;
}

// next turn function
function nextTurnFunction(match,pIndex){ 
	var timeArray  = [2,3,2,3,2,3,2,3,3,2,3];
    var randomTime = timeArray[Math.floor(Math.random()*timeArray.length)];
    match.botthrowDieTime =match.constthrowDieTime -randomTime;


	match.matchDiceNumber = 0;
	if(match.isReturn==true && match.isReturnMove ==true){
		var currentTrn = pIndex;
	}else{
		var currentTrn = nextTurnIndex(match,pIndex);
	}
	// nextturndice
	// matchNextDiceNumber

    for (let i = 0; i < match.players.length; i++) {
		match.players[i].diceNumber = 0;
        if(i == currentTrn){
        	match.whosTurn = match.players[i].playerType;
        	match.currentTurnUserId = match.players[i].userId;
            match.players[i].isturn=true;
            match.players[i].nextturndice = match.matchNextDiceNumber;
        }else{
            match.players[i].isturn=false;
        }
    }
 
	io.in(match.tableId).emit("playerObject",match.players);
	match.isReturn=false;
	match.isReturnMove=false;
    match.throwDieTime = match.constthrowDieTime;
}
function nextTurnIndex(match,pIndex) {
    var next = pIndex+1;
    //first
    if(match.players.length <= next){
       next = 0;
    }
    if(match.players[next].isBlocked == true){
        next += 1;
        if(match.players.length <= next){
            next =0;
        }
          //second
        if(match.players[next].isBlocked == true){
            next += 1;
            if(match.players.length <= next){
                next =0;
            }
            //third
            if(match.players[next].isBlocked == true){
                next += 1;
                if(match.players.length <= next){
                    next =0;
                }
            }
        }
        return next;
    }else{
        return next;
    }
}

// function nextTurnIndex(match,pIndex){
// 	if(match.leftPlayers!=1){
// 		if(match.players.length-1 == pIndex){
//         	next = 0;
// 	    }else{
// 	        next = pIndex+1;
// 	    } 


// 	    if(match.players[next].status != 'Active' || match.players[next].isBlocked==true){
// 	        pIndex=next;
//             if(match.players.length-1 == pIndex){
//         	   next = 0;
// 	        }else{
// 	            next = pIndex+1;
// 	        }
// 	        if(match.players[next].status != 'Active' || match.players[next].isBlocked==true){
// 			    pIndex=next;
// 			    if(match.players.length-1 == pIndex){
// 				   next = 0;
// 			    }else{
// 			       next = pIndex+1;
// 			    } 
// 			    if(match.players[next].status != 'Active' || match.players[next].isBlocked==true){
// 			        pIndex=next;

// 			        if(match.players.length-1 == pIndex){
// 			    	   next = 0;
// 			        }else{
// 			           next = pIndex+1;
// 			        } 
// 			    }
// 			} 
// 	    }
	   
// 	    return next;
// 	}
    
// }
// find player list using socket id
function findPlayerById(match,userId) {
    for (var i = 0; i < match.players.length; i++) {
		if (match.players[i].userId  ==  userId) {
			return match.players[i];
		}       
    }
    return false;
}

function joinRoomTable(socket,data){
	common_model.joinRoomTable(data,function(res){
		if(res.success == 1){	

			var tableId = res.joinRoomId;
			var userId = res.userId;
			var userName = res.userName;
			var tokenColor = res.tokenColor;
			var playerType = res.playerType;
			var sessMes={
				message:res.message,tableId:tableId
			}
			var rDice  = [1,2,3,4,5,6];
    		var matchNextDiceNumber = rDice[Math.floor(Math.random()*rDice.length)];
			io.to(socket.id).emit("sessionMessage",sessMes);
			var playerObj ={
				image:res.profile,
				isDeductMoney:"No",
				isDeductMoneyFirstToken:"No",
				isDeductMoneyFirstThreeToken:"No",
				tableId:tableId,
				roomId:res.roomId,
				winPosition:0,
				diceSixInTurn:0,
				userName:userName,
				totalLifes:3,
				coins:res.coins,
				status:'Active',
				isBlocked:false,
				isWin:false,
				isWinFirstToken:false,
				isWinFirstThreeToken:false,
				isturn:false,
				isStart:false,
				diceNumber:0,
				userId:userId,
				playerIndex:0,
				positionArray:[],
				fourToken:[{tokenIndex:0,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:1,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:2,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'},{tokenIndex:3,position:0,globlePosition:0,status:'Inactive',zone:'safe',isActiveToken:'Active'}],
				tokenColor:tokenColor,
				winnerPosition:0,
				socketId:socket.id,
				type:data.type,
				inactiveTokenCount:4,
				totalActiveToken:4,
				isKillTokenMoneyDeduct:0, // 0 is not and 1 is kill token but not deduct money
				winCount:0,
				playerType:playerType,
				tokenTopPosition:57,
				tossValue:0,
				randomBooster:0,
				choiseBooster:0,
				isExtratime:false,
				extratime:20,
				nextturndice:matchNextDiceNumber,
			};	
			socket.tableId = tableId;
			socket.userId= userId;
			socket.join(tableId);		


			var match = findMatchByTableId(tableId);
			totalOnlineGamePlayers+=1;
			var isPrivateData = {
        		online:totalOnlineGamePlayers,
        		private:totalPrivatePlayers
        	};
        	io.emit('onlinePlayerCount',isPrivateData);
			if(match){
				var player  = findPlayerById(match,data.userId);		
				if(player){
					  var pIndex = match.players.indexOf(player);
	                  match.players[pIndex]=playerObj;
	                   var leftPlayers =match.players.length ;
		                if(leftPlayers >= 1){
		                }else{
		                	leftPlayers=1;
		                }
		                var sql2="update ludo_join_rooms set activePlayer='"+leftPlayers+"' where joinRoomId='"+match.tableId+"';";
		                common_model.sqlQuery(sql2,function(res){
		                 
		                });
				}else{
					match.players.push(playerObj);	
				}
				if(match.players.length==match.matchPlayers){	
					if(match.matchPlayers==2){
						match.isTossTimmer=1;
					}	
					updateStatus(match);					
				}
				io.in(tableId).emit("playerObject",match.players);
			}else{
				var match = {
					tableId:tableId,
					roomId:res.roomId,
					roomTitle:res.roomTitle,
					isDeductReservemoney:"No",
					isBotConnect:res.isBotConnect,
					isFree:res.isFree,
					botWin:'No',
					isTokenMove:false,
					tokenMoveTime:2,
					consttokenMoveTime:2,
					isGameStart:false,
					winnerCount :0,
					startRoundWaiting:60,
					gameOverTime:5,
					isGameOver:false,
					throwDieTime:20,
					constthrowDieTime:20,
					botthrowDieTime:20,
					matchPlayers:Number(res.players),
					leftPlayers:Number(res.players),
					players:[],
					isReturn:false,
					isKill:false,
					isAddBot:false,
					noOfBots:0,
					isBotFirstSix:false,
					currentRoundBot:Number(res.currentRoundBot),
					totalRoundBot:Number(res.totalRoundBot),
					isBotWinner:false,
					matchDiceNumber:0,
					matchNextDiceNumber:matchNextDiceNumber,
					isReturnMove :false,
					matchValue:Number(data.value),
					winningBet:0,
					winningkillToken:0,
					winningfirstToken:0,
					winningfirstThreeToken:0,
					gameMode:res.gameMode,
					isPrivate:res.isPrivate,
					botUsersData:[],
					whosTurn:false,
					currentTurnUserId:false,
					adminCommision:Number(res.adminCommision),
					isToss:false,
					isTossTimmer:1,
					startDicePosition:[],
					killToken:res.killToken,
					firstToken:res.firstToken,
					firstThreeToken:res.firstThreeToken,
					continueSixes:0,
					isWinFirstToken:false,
					isWinFirstThreeToken:false,
				};
				match.players.push(playerObj);			
				matches[tableId] = match; // Key => Value
                setTimeout(()=>startWaitingTimer(tableId),0);	
				io.in(tableId).emit("playerObject",match.players);
			}
			
		}else{
			var sessMes={
				message:res.message,tableId:0
			}
			io.to(socket.id).emit("sessionMessage",sessMes);
		}        
	});
	
}
// var totolBotMatches=0;
// var currentBotMatches=0;
function updateStatus(match){
    if(match){
        var data ={
            table:'ludo_join_rooms',
            setdata:"gameStatus='Active'",
            condition:"joinRoomId="+match.tableId
        } 
        common_model.SaveData(data,function(res){
            
        	var timeArray  =  [2,3,2,3,2,3,2,3,4,3,2,3,4];
		    var randomTime = timeArray[Math.floor(Math.random()*timeArray.length)];
		    match.botthrowDieTime =match.constthrowDieTime -randomTime;

        	match.winningBet =0;
        	// match.killToken =0;
        	match.winningfirstToken =0;
        	match.winningfirstThreeToken =0;
        	for (let i = 0; i < match.players.length; i++) {
				match.winningBet +=  Number(match.matchValue);	
				// match.winningkillToken +=  Number(match.killToken);	
				match.winningfirstToken +=  Number(match.firstToken);	
				match.winningfirstThreeToken +=  Number(match.firstThreeToken);	
			}
			io.in(match.tableId).emit('winningBet',match.winningBet);	
			match.leftPlayers =match.players.length;
			match.matchPlayers =match.players.length;
        	if(match.isAddBot==true){
        		var data ={
        			roomId:match.roomId,
        		};
        		var data2={
					isSub:"Yes",
					type:"Sub"
				};
        		common_model.updateRoom(data,function(res){
        			if(res.success==1){
        				if(res.currentRoundBot <=5){
        					match.botWin="Yes";
        					io.in(match.tableId).emit("isBotWinner","Yes");
        				}else{
        					match.botWin ="No";
        					io.in(match.tableId).emit("isBotWinner","No");
        				}        				
        			}else{
        				match.botWin="Yes";
        				io.in(match.tableId).emit("isBotWinner","Yes");
        			}
        		    var rNum = Math.floor(Math.random() * 5) + 1;	
        		    var randNum = [1,2,3,4,5,6];
        		    io.to(match.tableId).emit("randomBooster",rNum);
        			for (var i = 0; i < match.players.length; i++) {
        				var randTossNo = randNum[Math.floor(Math.random()*randNum.length)];
        				randNum.splice(randNum.indexOf(randTossNo), 1);
						match.startDicePosition.push(randTossNo);
        				match.players[i].tossValue = randTossNo;
        				match.players[i].randomBooster = rNum;
        			}
        			io.in(match.tableId).emit("playerObject",match.players);
        			match.startDicePosition.sort(function(a, b){ return b-a; });
        			match.isGameStart = true;
        			setTimeout(()=> startMatchTimer(match.tableId),0);
				});
        	}else{
        		var rNum = Math.floor(Math.random() * 5) + 1;	
    		    var randNum = [1,2,3,4,5,6];
    		    io.to(match.tableId).emit("randomBooster",rNum);
    			for (var i = 0; i < match.players.length; i++) {
    				var randTossNo = randNum[Math.floor(Math.random()*randNum.length)];
    				randNum.splice(randNum.indexOf(randTossNo), 1);
					match.startDicePosition.push(randTossNo);
    				match.players[i].tossValue = randTossNo;
    				match.players[i].randomBooster = rNum;
    			}
    			io.in(match.tableId).emit("playerObject",match.players);
    			match.startDicePosition.sort(function(a, b){return b-a});
    			match.isGameStart = true;
    			setTimeout(()=>startMatchTimer(match.tableId),0);
        	}
			
        });

    }
}

// start Match Function
function startMatchFunction(match){
	
	for (let i = 0; i < match.players.length; i++) {
		coinUpdateFunction(match,match.players[i],i);		
	}
}

function coinUpdateFunction(match,player,i){
	var data ={
		table:'user_details',
		fields:'coins,user_id,balance',
		condition:'user_id='+player.userId,
	};
	common_model.GetData(data,function(res){			
		if(res.success==1){
			var balance = res.data[0].balance;
			var user_id = res.data[0].user_id;
			var lastCoin = balance - match.matchValue;	
			var passData ={
				table:'user_details',
				setdata:'balance="'+lastCoin+'"',
				condition:'user_id='+user_id,
			};
			common_model.SaveData(passData,function(res){
				player.coins = lastCoin;
				
				io.to(player.socketId).emit('coinsUpdate',lastCoin);				
			});
		}
	});
	
}
// find match object using table id
function findMatchByTableId(tableId){
	if (matches[tableId]) {
        return matches[tableId];
    } else {
        return false;
    } 
}

app.get('/',function(req,res){
    res.end('WELCOME TO NODE');
});

app.post("/signUp",function(req,res){
	var reqData = req.body;
    signUp.registration(reqData,function(response){
         res.send(response);
    });
});

app.post("/forgotPassword",function(req,res){
        var reqData = req.body;
        signUp.forgotPassword(reqData,function(response){
            res.send(response);
        });
});

app.post("/OtpVerify",function(req,res){
    var reqData = req.body;
    signUp.OtpVerifyFunction(reqData,function(response){
        res.send(response);
    });
});

app.post("/resendOtp",function(req,res){
    var reqData = req.body;
    signUp.ResendOtpFunction(reqData,function(response){
        res.send(response);
    });
});

app.post("/changePassword",function(req,res){
    var reqData =req.body;
    signUp.changePassword(reqData,function(response){
        res.send(response);
    });
});

app.post("/updateProfile",function(req,res){
    var reqData = req.body;
    signUp.profileUpdateFunction(reqData,function(response){
        res.send(response);
    });
});

app.post("/login",function(req,res){
    var reqData =req.body;
    signUp.loginAction(reqData,function(response){
        res.send(response);
    });
});
/*---------------------------- Get tournaments ------------------------------*/
app.post("/getTournaments",function(req,res){
    var reqData = req.body;
    tournaments.getTournaments(reqData,function(response){
        res.send(response);
    });
});
/*---------------------------- Get Bonus ------------------------------*/
app.post("/getBonus",function(req,res){
     var reqData = req.body;
     tournaments.getBonus(reqData,function(response){
        res.send(response);
     });
});
/*---------------------------- Get Rooms ------------------------------*/
app.post("/getRoomDetails",function(req,res){
     var reqData = req.body;
     tournaments.getRoomDetails(reqData,function(response){
        res.send(response);
     });
});
/*---------------------------- Update Device Id ------------------------------*/
app.post("/updateDevice",function(req,res){
	var reqData = req.body;
     signUp.updateDeviceId(reqData,function(response){
        res.send(response);
     });
});
/*---------------------------- Update player progress ------------------------------*/
app.post("/updateplayerProgress",function(req,res){
  var reqData = req.body;
     tournaments.updateplayerProgress(reqData,function(response){
        res.send(response);
     });
});
/*---------------------------- Get player progress------------------------------*/
app.post("/getplayerProgress",function(req,res){
     var reqData = req.body;
     tournaments.getplayerProgress(reqData,function(response){
        res.send(response);
     });
});
/*---------------------------- Get transaction list------------------------------*/
app.post("/getTransactionList",function(req,res){
     var reqData = req.body;
     tournaments.getTransactionListByCustId(reqData,function(response){
        res.send(response);
     });
});
/*---------------------------- Get transaction list------------------------------*/
app.post("/purchaseDice",function(req,res){
        var reqData =req.body;
        dice.purchaseDice(reqData,function(response){
            res.send(response);
        });
});

//------------------ save support------------------------------------
app.post("/support",function(req,res){
    var reqData =req.body;
    tournaments.addSupport(reqData,function(response){
        res.send(response);
    });
});

app.post("/getSupport",function(req,res){
    var reqData =req.body;
    tournaments.getSupport(reqData,function(response){
        res.send(response);
    });
});
/*------------------- For get game version -------------------------*/
app.post("/getGameVersion",function(req,res){
    var reqData =req.body;
    dice.getGameVersion(reqData,function(response){
        res.send(response);
    });
});


/*------------------- get top 30 players record -------------------------*/
app.post("/topPlayersRecord",function(req,res){
    var reqData =req.body;
    other.topPlayersRecord(reqData,function(response){
        res.send(response);
    });
});

/*------------------- get items record -------------------------*/
app.post("/getItems",function(req,res){
    var reqData =req.body;
    dice.getItems(reqData,function(response){
        res.send(response);
    });
});


/*------------------- get admin Percent record -------------------------*/
app.post("/getadminPercent",function(req,res){
    var reqData =req.body;
    other.getadminPercent(reqData,function(response){
        res.send(response);
    });
});



/*------------------- get Maintainance -------------------------*/
app.post("/getMaintenance",function(req,res){
    var reqData =req.body;
    other.getMaintenance(reqData,function(response){
        res.send(response);
    });
});

/*------------------- get addSpinWheel -------------------------*/
app.post("/addSpinWheel",function(req,res){
    var reqData =req.body;
    other.addSpinWheel(reqData,function(response){
        res.send(response);
    });
});

/*------------------- get getCoupons -------------------------*/
app.post("/getCoupons",function(req,res){
    var reqData =req.body;
    other.getCoupons(reqData,function(response){
        res.send(response);
    });
});

